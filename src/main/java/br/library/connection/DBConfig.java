package br.library.connection;

/**
 *
 * @author michelle
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

public class DBConfig {

  private static Properties props = new Properties();
  private static String dbConf = "../conf/DBConfig.properties";

  public static Properties getProp() throws IOException, URISyntaxException {

    String path = Thread.currentThread().getContextClassLoader().getResource(
            "/").toURI().resolve(DBConfig.dbConf).getPath();
    DBConfig.props.load(new FileInputStream(path));

    return DBConfig.props;

  }
}