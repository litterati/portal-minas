package br.library.connection;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * BuildConnection
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class BuildConnection {

    public static Connection getConnection() throws ClassNotFoundException, SQLException, IOException, URISyntaxException {

        Connection connection;
        Properties prop = DBConfig.getProp();

        String url = prop.getProperty("database.url");
        String user = prop.getProperty("database.user");
        String password = prop.getProperty("database.password");

        Class.forName(prop.getProperty("database.driver"));
        connection = DriverManager.getConnection(url, user, password);

        return connection;
    }
}
