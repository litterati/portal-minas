/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.tagger;

/**
 *
 * @author marcel
 */
public class MapEs extends PosMap {    
   
    public MapEs(){
        hashMap.put("Fa",".");
        hashMap.put("Fc",".");
        hashMap.put("Fd",".");
        hashMap.put("Fe",".");
        hashMap.put("Fg",".");
        hashMap.put("Fh",".");
        hashMap.put("Fi",".");
        hashMap.put("Fp",".");
        hashMap.put("Fs",".");
        hashMap.put("Fx",".");
        hashMap.put("Fz",".");
        hashMap.put("X","X");
        hashMap.put("Y","X");
        hashMap.put("Zm","NUM");
        hashMap.put("Zp","NUM");
        hashMap.put("ao","ADJ");
        hashMap.put("aq","ADJ");
        hashMap.put("cc","CONJ");
        hashMap.put("cs","CONJ");
        hashMap.put("da","DET");
        hashMap.put("dd","DET");
        hashMap.put("de","DET");
        hashMap.put("di","DET");
        hashMap.put("dn","DET");
        hashMap.put("dp","DET");
        hashMap.put("dt","DET");
        hashMap.put("i","X");
        hashMap.put("nc","NOUN");
        hashMap.put("np","NOUN");
        hashMap.put("p0","PRON");
        hashMap.put("pd","PRON");
        hashMap.put("pe","PRON");
        hashMap.put("pi","PRON");
        hashMap.put("pn","PRON");
        hashMap.put("pp","PRON");
        hashMap.put("pr","PRON");
        hashMap.put("pt","PRON");
        hashMap.put("px","PRON");
        hashMap.put("rg","ADV");
        hashMap.put("rn","ADV");
        hashMap.put("sn","ADP");
        hashMap.put("sp","ADP");
        hashMap.put("va","VERB");
        hashMap.put("vm","VERB");
        hashMap.put("vs","VERB");
        hashMap.put("w","NOUN");
        hashMap.put("z","NUM");
    }
}
