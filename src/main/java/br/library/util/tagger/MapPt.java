/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.tagger;

/**
 *
 * @author marcel
 */
public class MapPt extends PosMap{
    
    public MapPt(){
        hashMap.put("?",".");
        hashMap.put("adj","ADJ");
        hashMap.put("adv","ADV");
        hashMap.put("art","DET");
        hashMap.put("conj-c","CONJ");
        hashMap.put("conj-s","CONJ");
        hashMap.put("ec","X");
        hashMap.put("in","X");
        hashMap.put("n","NOUN");
        hashMap.put("num","NUM");
        hashMap.put("pp","NOUN");
        hashMap.put("pron-det","PRON");
        hashMap.put("pron-indp","PRON");
        hashMap.put("pron-pers","PRON");
        hashMap.put("prop","NOUN");
        hashMap.put("prp","ADP");
        hashMap.put("punc",".");
        hashMap.put("v-fin","VERB");
        hashMap.put("v-ger","VERB");
        hashMap.put("v-inf","VERB");
        hashMap.put("v-pcp","VERB");
        hashMap.put("vp","VERB");
    }
    
}
