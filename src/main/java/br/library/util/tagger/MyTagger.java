/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.tagger;

import br.project.entity.Text;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

/**
 * Lógica da classe.
 * 1 - Recebe um id_corpus para ser etiquetado (PoS);
 * 2 - Verifica textos do córpus que ainda não foram etiquetados (PoS);
 * 3 - A partir do id_text etiqueta o texto.
 * 
 * @author Marcel Akira Serikawa
 * @email marcel.serikawa@gmail.com
 */
public class MyTagger {
    
    private List<Integer> notTaggedTexts;
    private String[] tags = null;
    private String[] arrayWords = null;
    private String[] arrayIds = null;
    private final TaggerDAO taggerDAO = new TaggerDAO();
    POSModel model = null;
    PosMap posMap = null;
    Text text = null;
    String language = "";
    
    public void taggerCorpus(int idCorpus){
        verifyNotTaggedTexts(idCorpus);
        
        for(int idText : notTaggedTexts){
            tagText(idText);
        }
        
    }
    
    /**
     * Verifica quais os textos ainda não foram etiquetados (PoS).
     * Adiciona o resultado em um array de inteiros (notTaggedTexts).
     * @param idCorpus
     */
    public void verifyNotTaggedTexts(int idCorpus){
        notTaggedTexts = taggerDAO.selectNotTaggedText(idCorpus);
    }
   
    public void loadLanguageModel(int idText){
        String textLang = taggerDAO.verifyLanguage(idText);
        if(!textLang.equals(language)){
            switch (textLang){
            case "PORTUGUESE" :
                loadModel("pt-pos-maxent.bin");
                this.posMap = new MapPt();
                break;
            case "ENGLISH" :
                loadModel("en-pos-maxent.bin");
                this.posMap = new MapEn();
                break;
            case "SPANISH" :
                loadModel("es-perceptron-pos.bin");
                this.posMap = new MapEs();
                break;
//            case "ITALIAN" :
//                loadModel("it-pos-maxent.bin");
//                break;
            }  
        }
        language = textLang;
    }
    
    public void loadModel(String fileName) {
        try (InputStream modelIn = new FileInputStream("/opt/portal-minas/openNLP-models/"+fileName)) {
          this.model = new POSModel(modelIn);
        }
        catch (Exception e) {
            throw new RuntimeException("Erro ao carregar modelo do Tagger. " + fileName + "." + e.getMessage());
        }
    }

    public void tagText(int idText) {
        loadText(idText);
        loadLanguageModel(idText);
        setWordsIds(getWordsWithId());
        tagText(this.arrayWords);
        insertTags(idText);
    }
    
    public void tagText(String[] sentText) {
        POSTaggerME tagger = new POSTaggerME(model);
        tags = tagger.tag(sentText);
        universalPos();
    }
    
    /**
     * Carrega o texto do objeto texto a partir de um id fornecido.
     * @param idText
     * @throws TaggerException 
     */
    private void loadText(int idText) {
        text = new Text();
        text.setId(idText);
        taggerDAO.getWordsWithId(text);
    }
    
    /**
     * Pega do BD as palavras combinadas com seu ID.
     * @return 
     */
    private String[] getWordsWithId(){
        String textWithId = text.getTextString().toString();
        String[] wordsWithId = textWithId.split(" ");
        return wordsWithId;
    }
    
    /**
     * Atribui o valor do atributo ids
     * @param wordsWithIds 
     */
    private void setWordsIds(String[] wordsWithIds){
        String words, ids;
        words=null;
        ids=null;
        for(String word : wordsWithIds){
            String[] splited = word.split("%_");
            if(words == null){
                words = splited[0];
                ids = splited[splited.length -1];
            }else{
                words = words + " " + splited[0];
                ids = ids + " " + splited[splited.length -1];                
            }
        }
        if(words!=null && ids!=null){
            this.arrayWords = words.split(" ");
            this.arrayIds = ids.split(" ");            
        }
    }
    
    /**
     * Insere as tags no BD.
     * @throws TaggerException 
     */
    private void insertTags(int idText) {
        try {
            taggerDAO.updatePOS(tags, arrayIds);
            taggerDAO.unknownPos(idText);
        } catch (Exception ex) {
            Logger.getLogger(MyTagger.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro em MyTagger.insertTags()." + ex.getMessage());
        }
    }
    
    private void universalPos(){
        String[] uniTags = new String[tags.length];
        int count = 0;
        for(String tag : tags){
            if(posMap.hashMap.containsKey(tag)){
                uniTags[count] = posMap.hashMap.get(tag);
            }
            count++;
        }
        tags = uniTags;
    }
    
}
