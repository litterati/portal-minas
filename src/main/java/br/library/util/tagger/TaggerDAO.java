/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.library.util.tagger;

import br.library.connection.BuildConnection;
import br.project.entity.Text;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Marcel, Thiago Vieira
 * @since 2014
 */
public class TaggerDAO {

    private Connection c;
    private PreparedStatement ps;
    private ResultSet rs;
    public static final String TABLE_NAME = "words";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String ID_TEXT = "id_text";
    public static final String ID_SENTENCE = "id_sentence";
    public static final String POSITION = "position";
    public static final String WORD = "word";
    public static final String LEMMA = "lemma";
    public static final String POS = "pos";
    public static final String NORM = "norm";
    public static final String TRANSKRIPTION = "transkription";
    public static final String DEP_HEAD = "dep_head";
    public static final String DEP_FUNCTION = "dep_function";
    public static final String CONTRACTION = "contraction";
 
    private void startConections() {
        try {
            this.c = BuildConnection.getConnection();
            this.ps = null;
            this.rs = null;
        } catch (ClassNotFoundException | SQLException | IOException | URISyntaxException ex) {
            throw new RuntimeException("Erro ao conectar." + ex.getMessage());
        }
    }

    private void closeConections() {
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.ps != null) {
                this.ps.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro em SuperDAO.closeConections()." + ex.getMessage());
        }
    }
    /**
     * Seleciona o id dos textos que ainda não foram etiquetados (PoS).
     *
     * @param idCorpus
     * @return lista de Id de textos não etiquetados com (PoS) ordenados por linguagem
     */
    public ArrayList<Integer> selectNotTaggedText(int idCorpus){
        ArrayList<Integer> notTaggedTexts = new ArrayList<>(); 
        try {
            this.startConections();
            this.ps = this.c.prepareStatement(
                    "SELECT * FROM texts where id IN (SELECT id_text as id FROM words WHERE pos IS NULL AND id_corpus = ?) ORDER BY language"
            );
            this.ps.setInt(1, idCorpus);
            this.rs = this.ps.executeQuery();
            while(this.rs.next()){
                notTaggedTexts.add(this.rs.getInt("id"));
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao executar TaggerDAO.");
        }
        closeConections();
        return notTaggedTexts;
    }
    
    public ArrayList<Integer> selectTaggedText(int idCorpus){
        ArrayList<Integer> notTaggedTexts = new ArrayList<>(); 
        try {
            this.startConections();
            this.ps = this.c.prepareStatement(
                    "SELECT * FROM texts where id IN (SELECT id_text as id FROM words WHERE pos IS NOT NULL AND id_corpus = ?) ORDER BY language"
            );
            this.ps.setInt(1, idCorpus);
            this.rs = this.ps.executeQuery();
            while(this.rs.next()){
                notTaggedTexts.add(this.rs.getInt("id"));
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao executar TaggerDAO." + ex.getMessage());
        }
        closeConections();
        return notTaggedTexts;
    }

    /**
     * Verifica a linguagem de um determinado texto.
     *
     * @param idText
     * @return
     */
    public String verifyLanguage(int idText) {
        String language = null;
        startConections();
        try {
            ps = c.prepareStatement("SELECT language FROM texts WHERE id = ?");
            ps.setInt(1, idText);
            this.rs = ps.executeQuery();
            if (rs.next()) {
                language = rs.getString("language");
            }
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(TaggerDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro em TaggerDAO.verifyLanguage(). " + ex.getMessage());
        }
        closeConections();
        return language;
    }

    /**
     * Remonta o text com o id de token separado por '%'.
     *
     * @param text
     */
    public void getWordsWithId(Text text) {
        startConections();
        try{
            ps = c.prepareStatement(
                        "SELECT w." + WORD + ", w." + ID + " "
                        + "FROM " + TABLE_NAME + " w "
                        + "WHERE w." + ID_TEXT + " = ? "
                        + "ORDER BY w." + POSITION + ";");
            ps.setInt(1, text.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                if(!rs.getString("word").equalsIgnoreCase("_NEWLINE_")){
                    text.addTextString(rs.getString("word"));
                    text.addTextString("%_");
                    Integer i = rs.getInt("id");
                    text.addTextString(i.toString());
                    text.addTextString(" ");   
                }
//                System.out.println(rs.getString("word"));
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao executar TaggerDAO.getWordsWithId()." + ex.getMessage());
        }
        closeConections();
    }

    public void updatePOS(String[] pos, String[] id) {
        startConections();
        
        try{
            ps = c.prepareStatement(
                    "UPDATE " + TABLE_NAME + " "
                    + "SET " + POS + " = ? "
                    + "WHERE " + ID + " = ?");
            c.setAutoCommit(false);
            if (pos.length == id.length) {
                for (int i = 0; i < pos.length; i++) {
                    if (!id[i].isEmpty()) {
                       
                        ps.setString(1, pos[i]);
                        ps.setInt(2, Integer.parseInt(id[i]));
                        ps.addBatch();
                    }
                }
                //Testando estas 2 linhas abaixo. Verificando se código fica mais rápido
                ps.executeBatch();
                c.commit();
                ps.clearBatch();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao executar WordDAO.updatePOS()" + ex.getMessage());
        }
        closeConections();
    }
    
    public void unknownPos(int idCorpus){
        for (int idText : selectTaggedText(idCorpus)){
            startConections();
            try{
                    ps = c.prepareStatement(
                            "UPDATE " + TABLE_NAME + " "
                            + "SET " + POS + " = ? "
                            + "WHERE " + ID_TEXT + " = ? AND pos IS NULL");
                    ps.setString(1, "UNKNOWN");
                    ps.setInt(2, idText);
                    ps.executeBatch();
                    c.commit();
                    ps.clearBatch();
            } catch (SQLException ex) {
                throw new RuntimeException("Erro ao executar WordDAO.updatePOS()" + ex.getMessage());
            }
            closeConections();
        }
    }
}
