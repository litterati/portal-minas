/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.tagger;

/**
 *
 * @author marcel
 */
public class MapEn extends PosMap{
   
    public MapEn(){
        hashMap.put("!",".");
        hashMap.put("#",".");
        hashMap.put("$",".");
        hashMap.put("''",".");
        hashMap.put("(",".");
        hashMap.put(")",".");
        hashMap.put(",",".");
        hashMap.put("-LRB-",".");
        hashMap.put("-RRB-",".");
        hashMap.put(".",".");
        hashMap.put(":",".");
        hashMap.put("?",".");
        hashMap.put("CC","CONJ");
        hashMap.put("CD","NUM");
        hashMap.put("CD|RB","X");
        hashMap.put("DT","DET");
        hashMap.put("EX","DET");
        hashMap.put("FW","X");
        hashMap.put("IN","ADP");
        hashMap.put("IN|RP","ADP");
        hashMap.put("JJ","ADJ");
        hashMap.put("JJR","ADJ");
        hashMap.put("JJRJR","ADJ");
        hashMap.put("JJS","ADJ");
        hashMap.put("JJ|RB","ADJ");
        hashMap.put("JJ|VBG","ADJ");
        hashMap.put("LS","X");
        hashMap.put("MD","VERB");
        hashMap.put("NN","NOUN");
        hashMap.put("NNP","NOUN");
        hashMap.put("NNPS","NOUN");
        hashMap.put("NNS","NOUN");
        hashMap.put("NN|NNS","NOUN");
        hashMap.put("NN|SYM","NOUN");
        hashMap.put("NN|VBG","NOUN");
        hashMap.put("NP","NOUN");
        hashMap.put("PDT","DET");
        hashMap.put("POS","PRT");
        hashMap.put("PRP","PRON");
        hashMap.put("PRP$","PRON");
        hashMap.put("PRP|VBP","PRON");
        hashMap.put("PRT","PRT");
        hashMap.put("RB","ADV");
        hashMap.put("RBR","ADV");
        hashMap.put("RBS","ADV");
        hashMap.put("RB|RP","ADV");
        hashMap.put("RB|VBG","ADV");
        hashMap.put("RN","X");
        hashMap.put("RP","PRT");
        hashMap.put("SYM","X");
        hashMap.put("TO","PRT");
        hashMap.put("UH","X");
        hashMap.put("VB","VERB");
        hashMap.put("VBD","VERB");
        hashMap.put("VBD|VBN","VERB");
        hashMap.put("VBG","VERB");
        hashMap.put("VBG|NN","VERB");
        hashMap.put("VBN","VERB");
        hashMap.put("VBP","VERB");
        hashMap.put("VBP|TO","VERB");
        hashMap.put("VBZ","VERB");
        hashMap.put("VP","VERB");
        hashMap.put("WDT","DET");
        hashMap.put("WH","X");
        hashMap.put("WP","PRON");
        hashMap.put("WP$","PRON");
        hashMap.put("WRB","ADV");
        hashMap.put("``",".");
    }
}
