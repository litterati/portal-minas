package br.library.util;

import java.io.FileInputStream;
import java.io.InputStream;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

/**
 * Class used the Opennlp library to tokenize and get sentences from a text.
 * @author michelle
 */
public class Opennlp {

  private static SentenceModel sentenceModel;
  private static TokenizerModel tokenizerModel;
  
  /**
  * Method to load the sentence detector model
  * @return <code>SentenceModel</code>
  */
  private static SentenceModel getSentenceModel() throws Exception{
    
    if(sentenceModel == null){
      
      InputStream modelIn = null;
      
      try{
        String path = Thread.currentThread().getContextClassLoader().getResource(
              "/").toURI().resolve("../../bin/pt-sent.bin").getPath();

        // Loading sentence detection model
        modelIn = new FileInputStream(path); 
        sentenceModel = new SentenceModel(modelIn);

        return sentenceModel;
      }
      catch(Exception e){
        throw e;
      } finally{
        modelIn.close();
      }

    }
    else{
      return sentenceModel;
    }
  }
  
  /**
  * Method to load the token detector model
  * @return <code>TokenizerModel</code>
  */
  private static TokenizerModel getTokenizerModel() throws Exception{
     
    if(tokenizerModel == null){
      
      InputStream modelIn = null;
      
      try{
        String path = Thread.currentThread().getContextClassLoader().getResource(
              "/").toURI().resolve("../../bin/pt-token.bin").getPath();

        // Loading token detection model
        modelIn = new FileInputStream(path); 
        tokenizerModel = new TokenizerModel(modelIn);

        return tokenizerModel;
      }
      catch(Exception e){
        throw e;
      } finally{
        modelIn.close();
      }

    }
    else{
      return tokenizerModel;
    }
    
  }

  /**
  * Method to get the file sentences
  * @return <code>String[]</code> whith the sentences of the text
  */
  public static String[] returnSentences(String text) throws Exception {
    
    try{
      
      SentenceDetector sentence_detector = new SentenceDetectorME(Opennlp.getSentenceModel());
      return sentence_detector.sentDetect(text);
    
    }catch(Exception e){
      throw new RuntimeException("Problemas na geração de sentenças do texto.");
    }
  }
  
  /**
  * Method to get the file tokens
  * @return <code>String[]</code> whith the tokens of the text
  */
  public static String[] returnTokens(String text) throws Exception {
    
    try{
      
      Tokenizer tokenizer = new TokenizerME(Opennlp.getTokenizerModel());
      return tokenizer.tokenize(text);
      
    }catch(Exception e){
      throw new RuntimeException("Problemas para tokenizar texto.");
    }
  }

}
