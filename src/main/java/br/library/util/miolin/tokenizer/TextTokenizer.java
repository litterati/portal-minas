/*
 * Copyright 2012 Arnaldo Candido Junior
 *
 * This file is part of Miolin
 *
 * Miolin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Miolin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Miolin.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.library.util.miolin.tokenizer;
import java.io.BufferedReader;

/**
 * Tokenizes a raw test input.
 * 
 * <p>This module uses a quite simple approach for tokenization, and therefore a
 * little limited. Tokens are continuous string separated by space with few 
 * execeptions. Alphabet characteres followed by punctuation are divided in two
 * tokens, among others.
 */
public class TextTokenizer {

  /** Used prevent reading before first invokation. */
  private boolean started = false;

  /** Used for detecting whitespace. */
  protected static final char[] blanks = new char[] {' ', '\t', '\n', '\r'};
  
  /** Stores the eof char for convenience. */
  private static final char eof = '\uffff';

  /** Used for detecting special character. */
  protected static final char[] specialCharacter = new char[] {'\'', '‘', '’', 
    '\"', '“', '”', '(', ')', '[', '{', '.', '!', '?', ';', ':'};
  
  /** Reads chars from stdin. */
  private CharUterator charReader;

  /** contains an extra token generated due ahead analysis. */
  private String extraToken;
  
  /** Stores the last token read. */
  private String token;

  public String get() {
    return token;
  }
  
  /**
   * Looks up if a char is inside a set.
   *
   * @param value the value to be searched
   * @param set the set
   * @return true when the char is inside the set
   */
  public static boolean in(char value, char... set) {
    for (char item: set) {
      if (value == item) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether a char is blank.
   *
   * @param c a char
   * @return true whether a char is blank
   */
  private boolean isBlank(char c) {
    return TextTokenizer.in(c, blanks);
  }
  
  /**
   * Checks whether a char is 
   *
   * @param c a char is special character.
   * @return true whether a char is special character
   */
  private boolean isSpecialCharacter(char c) {
    return TextTokenizer.in(c, specialCharacter);
  }
  
  /**
   * Checks whether a char is alphanumeric.
   *
   * @param c a char
   * @return true whether a char is alphanumeric
   */
  private boolean isAlphaNum(char c) {
    return Character.isAlphabetic(c) ||
      Character.isDigit(c);
  }

  /**
   * Advances to the next token.
   * 
   * <p> This function is expeced to work as follow (a=letter or number, 
   * s=special):
   * <br> - aaa: aaa
   * <br> - sss: sss
   * <br> - aass: aa + ss
   * <br> - asa: asa
   * <br> - asas: asa + s
   * 
   * @return true when there is new tokens
   */
  public boolean next() {
    StringBuilder strb = new StringBuilder();
    StringBuilder strb2 = new StringBuilder();
    boolean alphanum = false;
    boolean special = false;

    // storing raw token
    if (! started) {
      started = true;
      charReader.next();
    }
    
    if (extraToken != null) {
      token = extraToken;
      extraToken = null;
      return true;
    }
    skipWhitespace();

    while (charReader.get() != eof && ! isBlank(charReader.get())) {
      
      // detecting token type (alphanumeric, special or boths)
      if (isAlphaNum(charReader.get())) {
        if (! alphanum) {
          alphanum = true;
        }        
      } else {
        if (! special) {
          special = true;
        }
      }
       
      if(isSpecialCharacter(charReader.get())){  
        if(strb.length() < 1){
          strb.append(charReader.get());
        }else{
          strb2.append(charReader.get());
        }
        charReader.next(); 
        break;
      }
      
      // processing combination of alphanum and special
      if (alphanum && ! special) {
        strb.append(charReader.get());
      } else if (! alphanum && special) {
        strb.append(charReader.get());
      } else if (special && isAlphaNum(charReader.get())) {
        if (strb2.length() > 0) {
          strb.append(strb2);
          strb2.setLength(0);
        }
        strb.append(charReader.get());
      } else {
        strb2.append(charReader.get());
      }
      
      // advancing to the next char
      charReader.next();
    }

    if (strb2.length() != 0) {
      extraToken = strb2.toString();
    }
    if (strb.length() == 0) {
      token = null;
      return false;
    }

    // building a new text token
    token = strb.toString();
    return true;
  }

  /**
   * Skips irrelevant chars during the tokenizing process.
   */
  private void skipWhitespace() {
    while (charReader.get() != eof && isBlank(charReader.get())) {
      charReader.next();
    } 
  }

  /** 
   * TextTokenizer constructor.
   * 
   * @param stdin the token source
   */
  public TextTokenizer(BufferedReader stdin) {
    charReader = new CharUterator(stdin);
  }

}