/*
 * Copyright 2012 Arnaldo Candido Junior
 *
 * This file is part of Miolin
 *
 * Miolin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Miolin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Miolin.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.library.util.miolin.tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Util Iterator for chars: alternative iterator operations based on get and 
 * next.
 * 
 * <p>Note: uterator not used because java does not allow primitive type 
 * generics.
 * 
 * <p>Iteration is over when next returns false.
 */
public class CharUterator {
  
  /** Reads chars from stdin. */
  BufferedReader stdin;
  
  /** Stores the eof char for convenience. */
  private final char eof = '\uffff';

  /** The read char. */
  private char currentChar = eof;
  
  /**
   * Creates a new char uterator reading data from stdin.
   * 
   * @param stdin the char source input
   */
  public CharUterator(BufferedReader stdin) {
    this.stdin = stdin;
  }

  /** 
    * Returns the last char read.
    * 
    * @return the last char read
    */
  public char get() {
    return currentChar;
  }

  /**
    * Consumes a char from the input (useful to building multi-char tokens).
    */
  public void next() {
    try {
      currentChar = (char) stdin.read();
    } catch (IOException ex) {
      Logger.getLogger(CharUterator.class.getName()).log(Level.SEVERE, 
        null, ex);
      System.exit(1);
    }
  }
    
  
}
