package br.library.util.align;

/**
 * ReferenceWord
 *
 * @author Thiago Vieira
 * @since 23/09/2014
 */
public class ReferenceWord {

    private String id;
    private String idSentence;
    private String position;
    private String superficialForm;

    public ReferenceWord(String id, String idSentence, String position, String superficialForm) {
        this.id = id;
        this.idSentence = idSentence;
        this.position = position;
        this.superficialForm = superficialForm;
    }

    public int getIntegerId() {
        return Integer.parseInt(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSuperficialForm() {
        return superficialForm;
    }

    public void setSuperficialForm(String superficialForm) {
        this.superficialForm = superficialForm;
    }

    public int getIntegerIdSentence() {
        return Integer.parseInt(idSentence);
    }
    
    public String getIdSentence() {
        return idSentence;
    }

    public void setIdSentence(String idSentence) {
        this.idSentence = idSentence;
    }

}
