package br.library.util.align;

/**
 * ReferenceAlign
 *
 * @author Thiago Vieira
 * @since 23/09/2014
 */
public class ReferenceAlign {

    private String id;
    private String[] crr;
    private String atype;
    private String superficialForm;
    private ReferenceWord word;

    public ReferenceAlign(String id, String crr, String atype, String superficialForm) {
        this.id = id;
        if (crr.contains(",")) {
            this.crr = crr.split(",");
        } else {
            this.crr = new String[]{crr};
        }
        this.atype = atype;
        this.superficialForm = superficialForm;
    }

    public int getIntegerId() {
        return Integer.parseInt(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getCrr() {
        return crr;
    }

    public void setCrr(String[] crr) {
        this.crr = crr;
    }

    public String getAtype() {
        return atype;
    }

    public void setAtype(String atype) {
        this.atype = atype;
    }

    public String getSuperficialForm() {
        return superficialForm;
    }

    public void setSuperficialForm(String superficialForm) {
        this.superficialForm = superficialForm;
    }

    public ReferenceWord getWord() {
        return word;
    }

    public void setWord(ReferenceWord word) {
        this.word = word;
    }

}
