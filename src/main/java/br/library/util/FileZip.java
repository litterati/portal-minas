package br.library.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Class used to read and write in a .zip file
 *
 * @author Michelle
 */
public class FileZip {

  // Buffer size for extraction
  static final int BUFFER = 2048;
  
  // Zip file localization 
  private String pathZip;
  
  // Path where the files will be extracted or the path where there are files to be zipped
  private String destinationDirectory;

  // List with the files to be zipped
  private List<String> filesToZip = new ArrayList<>();
  
  // Extensions that can be extracted
  private String[] allowedExtensions = {".doc", ".docx", ".pdf", ".txt", ".jpg", ".png"};

  private List<String> filesNotExtracted = new ArrayList<>();
  

  /**
   * Method to put the files that will be zipped inside the variable filesToZip
   */
  private void fillFilesToZip() {
    File f = new File(this.getDestinationDirectory());
    File[] files = f.listFiles();
    for (File file : files) {
      filesToZip.add(file.getName());
    }
  }
  
  /**
   * Method to verify if a file have a allowed extension
   * @param entry The file name
   * @return <code>boolean</code>
   */
  private boolean isExtensionAllowed(String entry){
    
    boolean isAllowed = false;
    
    for(int i = 0; i < this.allowedExtensions.length; i++){
      if( entry.toLowerCase().endsWith(this.allowedExtensions[i].toLowerCase())){
        return true;
      }
    }
            
    return isAllowed;
  }

  /**
   * Method to unzip a file
   */
  public void unzip() throws IOException {
    
    String currentEntry =  "";
    ZipEntry entry;
    File sourceZipFile = new File(this.getPathZip());
    File unzipDestinationDirectory = new File(this.getDestinationDirectory());
    
    if (!unzipDestinationDirectory.exists()) {
      unzipDestinationDirectory.mkdir();
    }
    
    try (ZipFile zipFile = new ZipFile(sourceZipFile, ZipFile.OPEN_READ)) {
    
      Enumeration zipFileEntries = zipFile.entries();

      // Process each entry
      while (zipFileEntries.hasMoreElements()) {
        // Grab a zip file entry
        entry = (ZipEntry) zipFileEntries.nextElement();

        if (this.isExtensionAllowed(entry.toString())) {

          currentEntry = entry.getName();

          File destFile = new File(unzipDestinationDirectory, currentEntry);

          // Grab file's parent directory structure
          File destinationParent = destFile.getParentFile();

          // Create the parent directory structure if needed
          destinationParent.mkdirs();

          // Extract file if not a directory
          if (!entry.isDirectory()) {
            BufferedInputStream is = new BufferedInputStream(zipFile.getInputStream(entry));
            int currentByte;

            // Establish buffer for writing file
            byte data[] = new byte[BUFFER];

            // Write the current file to disk
            FileOutputStream fos = new FileOutputStream(destFile);
            BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);

            // Read and write until last byte is encountered
            while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
              dest.write(data, 0, currentByte);
            }

            dest.flush();
            dest.close();
            is.close();
          }
        } else { 
          /* It's not allowed files with extensions different from .doc, .docx, 
          .pdf, .txt or .jpg in the zip */
          
          filesNotExtracted.add(entry.toString());

          //Removing file that was extracted to the disk
          File file = new File(this.getDestinationDirectory() + entry.toString());
          file.delete();
        }
      }
    }catch(Exception e){
      throw e;
    }

  }

  /**
   * Method to zip a file
   */
  public void zip() throws FileNotFoundException, IOException {

    byte[] buffer = new byte[18024];
    int len;

    ZipOutputStream out = new ZipOutputStream(new FileOutputStream(this.getPathZip()));

    // Set the compression ratio
    out.setLevel(Deflater.DEFAULT_COMPRESSION);

    // Files to be zipped
    this.fillFilesToZip();

    // Iterate through the array of files, adding each to the zip file
    for (int i = 0; i < filesToZip.size(); i++) {

      // Associate a file input stream for the current file
      FileInputStream in = new FileInputStream(filesToZip.get(i));

      // Add ZIP entry to output stream.
      out.putNextEntry(new ZipEntry(filesToZip.get(i)));

      // Transfer bytes from the current file to the ZIP file
      //out.write(buffer, 0, in.read(buffer));

      while ((len = in.read(buffer)) > 0) {
        out.write(buffer, 0, len);
      }

      // Close the current entry
      out.closeEntry();

      // Close the current file input stream
      in.close();

    }
    // Close the ZipOutPutStream
    out.close();
  }

  /**
   * @return the diretory where the .zip file was extracted
   */
  public String getDestinationDirectory() {
    return destinationDirectory;
  }

  /**
   * @param destinationDirectory set the diretory where the .zip file will be extract
   */
  public void setDestinationDirectory(String destinationDirectory) {
    this.destinationDirectory = destinationDirectory;
  }

  /**
   * @return the path to the .zip file
   */
  public String getPathZip() {
    return pathZip;
  }

  /**
   * @param pathZip set the path to the .zip file
   */
  public void setPathZip(String pathZip) {
    this.pathZip = pathZip;
  }

   /**
   * @return files that wasn't extracted because have extensions not allowed
   */
  public List<String> getFilesNotExtracted() {
    return filesNotExtracted;
  }
  
}