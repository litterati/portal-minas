/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.decontract;

import java.util.HashMap;

/**
 * A super Class responsible for all HashMap class
 * @author marcel
 */
public abstract class ContractionLanguage {
    
    protected HashMap<String,String> contractions = new HashMap<>();

    public boolean hasWord(String word) {
        return contractions.containsKey(word);
    }
    
    public String[] expandWord(String word){
        String[] returnWords = new String[3];
        String[] expanded = contractions.get(word).split("_");
        returnWords[0] = expanded[0];
        returnWords[1] = expanded[1];
        returnWords[2] = word;
        return returnWords;
    }
}
