/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.decontract;

/**
 * Defining the contractions and expansions hashmap in Italian language.
 * @author marcel
 */
public class ContractionIt extends ContractionLanguage{
   
    public ContractionIt(){
        this.contractions.put("word", "expansion");
    }
    
}
