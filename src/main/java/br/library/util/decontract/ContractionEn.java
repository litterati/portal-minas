/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.decontract;

/**
 * Defining the contractions and expansions hashmap in English language.
 * @author marcel
 */
public class ContractionEn extends ContractionLanguage{
   
    public ContractionEn(){
        this.contractions.put("aren't", "are_not");
        this.contractions.put("can't", "can_not");
        this.contractions.put("couldn't", "could_not");
        this.contractions.put("didn't", "did_not");
        this.contractions.put("doesn't", "does_not");
        this.contractions.put("don't", "do_not");
        this.contractions.put("hadn't", "had_not");
        this.contractions.put("hasn't", "has_not");
        this.contractions.put("haven't", "have_not");
        this.contractions.put("he'd", "he_had");
        this.contractions.put("he'll", "he_will");
        this.contractions.put("he's", "he_is");
        this.contractions.put("I'd", "I_had");
        this.contractions.put("I'll", "I_will");
        this.contractions.put("I'm", "I_am");
        this.contractions.put("I've", "I_have");
        this.contractions.put("isn't", "is_not");
        this.contractions.put("it's", "it_is");
        this.contractions.put("let's", "let_us");
        this.contractions.put("mightn't", "might_not");
        this.contractions.put("mustn't_", "must_not");
        this.contractions.put("shan't", "shall_not");
        this.contractions.put("she'd", "she_had");
        this.contractions.put("she'll", "she_will");
        this.contractions.put("she's", "she_is");
        this.contractions.put("shouldn't", "should_not");
        this.contractions.put("that's", "that_is");
        this.contractions.put("there's", "there_is");
        this.contractions.put("they'd", "they_had");
        this.contractions.put("they'll", "they_will");
        this.contractions.put("they're", "they_are");
        this.contractions.put("they've", "they_have");
        this.contractions.put("we'd", "we_had");
        this.contractions.put("we're", "we_are");
        this.contractions.put("we've", "we_have");
        this.contractions.put("weren't", "were_not");
        this.contractions.put("what'll", "what_will");
        this.contractions.put("what're", "what_are");
        this.contractions.put("what's", "what_is");
        this.contractions.put("what've", "what_have");
        this.contractions.put("where's", "where_is");
        this.contractions.put("who'd", "who_had");
        this.contractions.put("who'll", "who_will");
        this.contractions.put("who're", "who_are");
        this.contractions.put("who's", "who_is");
        this.contractions.put("who've", "who_have");
        this.contractions.put("won't", "will_not");
        this.contractions.put("wouldn't", "would_not");
        this.contractions.put("you'd", "you_had");
        this.contractions.put("you'll", "you_will");
        this.contractions.put("you're", "you_are");
        this.contractions.put("you've", "you_have");     
    }
    
}
