/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.decontract;

/**
 * Defining the contractions and expansions hashmap in Spanish language.
 * @author marcel
 */
public class ContractionEs extends ContractionLanguage{
   
    public ContractionEs(){
        this.contractions.put("al", "a_el");
        this.contractions.put("del", "de_el");
    }
    
}
