package br.library.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author michelle
 */
public class Cryptography {

  public static String encryptography(String word) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    
    StringBuilder hexString = new StringBuilder();
    MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
    byte messageDigest[] = algorithm.digest(word.getBytes("UTF-8"));

    for (byte b : messageDigest) {
      hexString.append(String.format("%02X", 0xFF & b));
    }

    return hexString.toString();
  }
  /*
   * private static byte[] iv = { 0x0a, 0x01, 0x02, 0x03, 0x04, 0x0b, 0x0c, 0x0d
   * };
   *
   * public static byte[] encrypt(byte[] inpBytes, SecretKey key, String xform)
   * throws Exception {
   *
   * Cipher cipher = Cipher.getInstance(xform); IvParameterSpec ips = new
   * IvParameterSpec(iv); cipher.init(Cipher.ENCRYPT_MODE, key, ips);
   *
   * return cipher.doFinal(inpBytes); }
   *
   * public static byte[] decrypt(byte[] inpBytes, SecretKey key, String xform)
   * throws Exception {
   *
   * Cipher cipher = Cipher.getInstance(xform); IvParameterSpec ips = new
   * IvParameterSpec(iv); cipher.init(Cipher.DECRYPT_MODE, key, ips);
   *
   * return cipher.doFinal(inpBytes);
    }
   */
}
