package br.library.util.access;

/**
 * AccessAnalysis.java
 *
 * @author Thiago Vieira
 * @since 27/01/2015
 */
public class AccessAnalysis<T> {

    private String dateAccess;
    private int count;
    private T analyzed;

    public AccessAnalysis(String dateAccess, int count, T analyzed) {
        this.dateAccess = dateAccess;
        this.count = count;
        this.analyzed = analyzed;
    }

    public String getDateAccess() {
        return dateAccess;
    }

    public void setDateAccess(String dateAccess) {
        this.dateAccess = dateAccess;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public T getAnalyzed() {
        return analyzed;
    }

    public void setAnalyzed(T analyzed) {
        this.analyzed = analyzed;
    }

}
