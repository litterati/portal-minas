/*
 * Configura todos os campos que uma classe especifica necessita.
 */
package br.library.util.template;

import br.library.util.Config;
import br.project.dao.AccessDAO;
import br.project.dao.CorpusDAO;
import br.project.entity.Access;
import br.project.entity.Corpus;
import br.project.entity.User;
import br.project.exception.DAOException;
import br.project.exception.InvalidCorpusException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.http.client.utils.URIBuilder;

/**
 * TemplateAdmin
 *
 * @author Thiago Vieira
 * @since 12/08/2014
 */
public final class TemplateAdmin {

    private final HttpServletRequest request;
    private final HttpServletResponse response;

    public TemplateAdmin(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public Corpus isCorpusValid() throws DAOException, InvalidCorpusException {
        return this.isCorpusValid("id_corpus");
    }

    public Corpus isCorpusValid(String id_corpus_parameter_name) throws DAOException, InvalidCorpusException {
        Corpus corpus = CorpusDAO.selectById(Integer.parseInt(request.getParameter(id_corpus_parameter_name)));
        if (corpus == null) {
            //Corpus inválido
            throw new InvalidCorpusException();
        }
        return corpus;
    }

    public boolean hasUserPermission() {
        HttpSession session = request.getSession();
        if (session.getAttribute("userLoged") != null) {
            User user = (User) session.getAttribute("userLoged");
            if (user.getType() == User.ADMINISTRATOR) {
                return true;
            } else {
                //check the permissions
            }
        }
        return false;
    }

    public void getAlerts() {
        try {
            String msg = request.getParameter("alert_success");
            if (msg != null) {
                request.setAttribute("alert_success", URLDecoder.decode(msg, "UTF-8"));
            }
            msg = request.getParameter("alert_info");
            if (msg != null) {
                request.setAttribute("alert_info", URLDecoder.decode(msg, "UTF-8"));
            }
            msg = request.getParameter("alert_warning");
            if (msg != null) {
                request.setAttribute("alert_warning", URLDecoder.decode(msg, "UTF-8"));
            }
            msg = request.getParameter("alert_danger");
            if (msg != null) {
                request.setAttribute("alert_danger", URLDecoder.decode(msg, "UTF-8"));
            }
        } //Aqui a exceção tá generica porque URLDecoder.decode tem retornado uma erro IllegalArgumentException
        catch (UnsupportedEncodingException ex) {
            request.setAttribute("alert_danger", "ErrorAlert");
        }
    }

    public TemplateAdmin pageViewer(
            String page_menu,
            String page_breadcrumb,
            Corpus corpus,
            String page_menu_option,
            String page_menu_tools,
            String page_menu_tools_sub,
            String page_title,
            String page
    ) throws DAOException {
        return pageViewer(page_menu, page_breadcrumb, String.valueOf(corpus.getId()), corpus.getName(), page_menu_option, "corpus-" + corpus.getId(), page_menu_tools, page_menu_tools_sub, page_title, corpus.getName(), page);
    }

    public TemplateAdmin pageViewer(
            String page_menu,
            String page_breadcrumb,
            String page_menu_id_corpus,
            String page_menu_name_corpus,
            String page_menu_option,
            String page_menu_option_sub,
            String page_menu_tools,
            String page_menu_tools_sub,
            String page_title,
            String page_subtitle,
            String page
    ) throws DAOException {

        //Page viewer
        if (page_menu != null) {
            request.setAttribute("page_menu", page_menu);
        }
        if (page_breadcrumb != null) {
            request.setAttribute("page_breadcrumb", page_breadcrumb);
        }
        if (page_menu_id_corpus != null) {
            request.setAttribute("page_menu_id_corpus", page_menu_id_corpus);
        }
        if (page_menu_name_corpus != null) {
            request.setAttribute("page_menu_name_corpus", page_menu_name_corpus);
        }
        if (page_menu_option != null) {
            request.setAttribute("page_menu_option", page_menu_option);
        }
        if (page_menu_option_sub != null) {
            request.setAttribute("page_menu_option_sub", page_menu_option_sub);
        }
        if (page_menu_tools != null) {
            request.setAttribute("page_menu_tools", page_menu_tools);
        }
        if (page_menu_tools_sub != null) {
            request.setAttribute("page_menu_tools_sub", page_menu_tools_sub);
        }
        if (page_title != null) {
            request.setAttribute("page_title", page_title);
        }
        if (page_subtitle != null) {
            request.setAttribute("page_subtitle", page_subtitle);
        }
        request.setAttribute("page_menu_corpus", CorpusDAO.selectAll(Corpus.WORK));
        if (page != null) {
            request.setAttribute("page", page);
        }

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userLoged");
        request.setAttribute("user_permission_level", user.getType());

        return this;
    }

    public void forward() throws IOException, URISyntaxException, ServletException {
        if (!response.isCommitted()) {
            request.getRequestDispatcher(
                    Config.getPathProperties().getProperty("path.template.admin")
            ).forward(request, response);
        }
    }

    public void redirect(HttpServlet servlet, String path, String alert_success, String alert_warning, String alert_danger) throws IOException {
        redirect(servlet, path, null, alert_success, alert_warning, alert_danger);
    }

    public void redirect(HttpServlet servlet, String path, String id_corpus, String alert_success, String alert_warning, String alert_danger, String[]   ... param) throws IOException {
        URIBuilder builder = new URIBuilder()
                //.setScheme("http")
                //.setHost(servlet.getServletContext().getContextPath())
                //.setPort(80)
                .setPath(servlet.getServletContext().getContextPath() + path);
        if (id_corpus != null) {
            builder.setParameter("id_corpus", id_corpus);
        }
        if (alert_success != null) {
            builder.setParameter("alert_success", alert_success);
        }
        if (alert_warning != null) {
            builder.setParameter("alert_warning", alert_warning);
        }
        if (alert_danger != null) {
            builder.setParameter("alert_danger", alert_danger);
        }
        if (param != null) {
            for (String[] row : param) {
                builder.setParameter(row[0], row[1]);
            }
        }
        try {
            response.sendRedirect(builder.build().toString());
        } catch (URISyntaxException ex) {
            Logger.getLogger(TemplateAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean recordUserAccess(Corpus corpus, Tool tool) {
        HttpSession session = request.getSession();
        if (session.getAttribute("userLoged") != null) {
            User user = (User) session.getAttribute("userLoged");
            Access obj = new Access();
            obj.setIdUser(user.getId());
            obj.setIdCorpus(corpus.getId());
            obj.setTool(tool.getValue());
            try {
                AccessDAO.insert(obj);
            } catch (DAOException ex) {
                //
            }
        }
        return false;
    }

}
