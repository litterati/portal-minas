/*

 Enum são tipos de campos que consistem em um conjunto fixo de constantes 
 (static final), sendo como uma lista de valores pré-definidos.
 http://www.devmedia.com.br/tipos-enum-no-java/25729#ixzz3OivG0vlG

 */
package br.library.util.template;

/**
 * Tool.java
 *
 * @author Thiago Vieira
 * @since 13/01/2015
 */
public enum Tool {

    ALIGN("align", null),
    ANNOTATION("annotation", null),
    CONCORDANCER("concordancer", null),
    METADATA("metadata", null),
    FREQUENCY("frequency", null),
    INFO("info", null),
    KEYWORDS("keywords", null),
    NGRAMA("ngrama", null),
    SUBCORPUS("subcorpus", null),
    TAGGER("tagger", null),
    TEXT("text", null);

    private final String value;
    private final String description;

    private Tool(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

}
