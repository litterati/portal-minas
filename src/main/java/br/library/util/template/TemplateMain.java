/*
 * Configura todos os campos que uma classe especifica necessita.
 */
package br.library.util.template;

import br.library.util.Config;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.client.utils.URIBuilder;

/**
 * TemplateMain
 *
 * @author Thiago Vieira
 * @since 12/08/2014
 */
public final class TemplateMain {

    private final HttpServletRequest request;
    private final HttpServletResponse response;

    public TemplateMain(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public void getAlerts() {
        try {
            String msg = request.getParameter("alert_success");
            if (msg != null) {
                request.setAttribute("alert_success", URLDecoder.decode(msg, "UTF-8"));
            }
            msg = request.getParameter("alert_info");
            if (msg != null) {
                request.setAttribute("alert_info", URLDecoder.decode(msg, "UTF-8"));
            }
            msg = request.getParameter("alert_warning");
            if (msg != null) {
                request.setAttribute("alert_warning", URLDecoder.decode(msg, "UTF-8"));
            }
            msg = request.getParameter("alert_danger");
            if (msg != null) {
                request.setAttribute("alert_danger", URLDecoder.decode(msg, "UTF-8"));
            }
        } //Aqui a exceção tá generica porque URLDecoder.decode tem retornado uma erro IllegalArgumentException
        catch (UnsupportedEncodingException ex) {
            request.setAttribute("alert_danger", "ErrorAlert");
        }
    }

    public TemplateMain pageViewer(
            String page_title,
            String page_menu,
            String page
    ) {

        //Page viewer
        if (page_menu != null) {
            request.setAttribute("page_menu", page_menu);
        }
        if (page_title != null) {
            request.setAttribute("page_title", page_title);
        }
        if (page != null) {
            request.setAttribute("page", page);
        }

        return this;
    }

    public void forward() throws IOException, URISyntaxException, ServletException {
        if (!response.isCommitted()) {
            request.getRequestDispatcher(
                    Config.getPathProperties().getProperty("path.template.default")
            ).forward(request, response);
        }
    }

    public void redirect(HttpServlet servlet, String path, String alert_success, String alert_warning, String alert_danger) throws IOException {
        redirect(servlet, path, null, alert_success, alert_warning, alert_danger);
    }

    public void redirect(HttpServlet servlet, String path, String id_corpus, String alert_success, String alert_warning, String alert_danger) throws IOException {
        URIBuilder builder = new URIBuilder()
                //.setScheme("http")
                //.setHost(servlet.getServletContext().getContextPath())
                //.setPort(80)
                .setPath(servlet.getServletContext().getContextPath() + path);
        if (id_corpus != null) {
            builder.setParameter("id_corpus", id_corpus);
        }
        if (alert_success != null) {
            builder.setParameter("alert_success", alert_success);
        }
        if (alert_warning != null) {
            builder.setParameter("alert_warning", alert_success);
        }
        if (alert_danger != null) {
            builder.setParameter("alert_danger", alert_success);
        }
        try {
            response.sendRedirect(builder.build().toString());
        } catch (URISyntaxException ex) {
            Logger.getLogger(TemplateMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
