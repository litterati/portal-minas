/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.library.util.ngrama;

import br.library.util.Config;
import br.project.dao.TextDAO;
import br.project.dao.WordDAO;
import br.project.entity.Ngram;
import br.project.entity.NgramResults;
import br.project.entity.Text;
import br.project.exception.DAOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marcel Akira Serikawa
 * @email marcel.serikawa@gmail.com
 */
public class NgramaGenerator {
    private final String idUser;
    private int idCorpus;
    private int gramSize;
    private int frequency;
    private String statistic;
    private final Double score;
    
    String corpusText;
    String outputFile;
    //Executar local 
    //String tmpPath = "/tmp/";
    //String perlScriptPath = "/usr/local/bin/";
    //Para executar no servidor descomentar
    String tmpPath = "";
    String perlScriptPath = "";
    
    public NgramaGenerator(String idUser, Double score){
        this.idUser = idUser;
        this.score = score;
    }
    
    public NgramaGenerator(String idUser, int idCorpus, int gramSize, int frequency, String statistic, Double score){
        this.idUser = idUser;
        this.idCorpus = idCorpus;
        this.gramSize = gramSize;
        this.frequency = frequency;
        this.statistic = statistic;
        this.score = score;
    
        //Para executar no servidor descomentar
        try {
          this.perlScriptPath = Config.getPathProperties().getProperty("path.textnsp");
          this.tmpPath = Config.getPathProperties().getProperty("path.tmp");
        }
        catch (IOException | URISyntaxException ex) {
            throw new RuntimeException("Erro ao carregar modelo do Tagger. " + ex.getMessage());
        }
    }
    
    public void begin(){
        //removeAllFiles();
        //Checking if files exists
        if(!existFiles()){
            corpusToFile();
        }
        removeTemp();
        createCountFile();
        applyStatistic();
    }
    
    public  List<NgramResults>  filter(){
        //ler o arquivo de stat e gerar nova lista com resultados
        List<NgramResults> resultadosFiltrados = new ArrayList<>();
            
        return resultadosFiltrados;
    }
    
    private boolean existFiles(){
        File pt = new File(tmpPath.concat(idUser + "_pt.input"));
        File en = new File(tmpPath.concat(idUser + "_en.input"));
        File es = new File(tmpPath.concat(idUser + "_es.input"));
        
        return pt.exists() && en.exists() && es.exists();
    }
    
    private void corpusToFile(){
        FileOutputStream fos_pt;
        FileOutputStream fos_en;
        FileOutputStream fos_es;
        try {
            fos_pt = new FileOutputStream(new File(tmpPath.concat(idUser + "_pt.input")), true); // to write in file
            fos_en = new FileOutputStream(new File(tmpPath.concat(idUser + "_en.input")), true); // to write in file
            fos_es = new FileOutputStream(new File(tmpPath.concat(idUser + "_es.input")), true); // to write in file
            List<Text> listTexts;
            listTexts = TextDAO.selectAllByIdCorpus(idCorpus, null);
            for(Text text : listTexts){
                StringBuilder lws = WordDAO.getOnlyWordsFromText(text.getId());
                switch (text.getLanguage()) {
                    case "PORTUGUESE":
                        fos_pt.write(lws.toString().getBytes());
                        break;
                    case "ENGLISH":
                        fos_en.write(lws.toString().getBytes());                
                        break;
                    case "SPANISH":
                        fos_es.write(lws.toString().getBytes());
                        break;
                }
            }
            fos_pt.close();
            fos_en.close();
            fos_es.close();
            
        } catch (DAOException | IOException ex) {
            Logger.getLogger(NgramaGenerator.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro em Ngrama.corpusToFile()." + ex.getMessage());
        }
    }
    
    private void createCountFile() {        
        String path = tmpPath + idUser;
        String gramProp = "--ngram 2 --token " + perlScriptPath + "tk3.pl --stop " + perlScriptPath + "stopor_";
        String freqProp = "";
        if(gramSize == 3){
            gramProp = "--ngram 3 --token " + perlScriptPath + "tk3.pl --stop" + perlScriptPath + " stopand_";
        }
        
        if(frequency != 0){
            freqProp = "--frequency " + frequency + " ";
        }
        
        try {
            //Executando count.pl todos os arquivos
            //Portuguese
            Process process1 = Runtime.getRuntime().exec(
                    "perl " + 
                    perlScriptPath + "count.pl "+ gramProp+"pt.pl " + freqProp + path + "_pt.count " + path + "_pt.input");
            //English
            Process process2 = Runtime.getRuntime().exec(
                    "perl " + 
                    perlScriptPath + "count.pl "+ gramProp+"en.pl " + freqProp + path + "_en.count " + path + "_en.input");
            //Spanish
            Process process3 = Runtime.getRuntime().exec(
                    "perl " + 
                    perlScriptPath + "count.pl "+ "es.pl " + freqProp + path + "_es.count " + path + "_es.input");

            process1.waitFor();              
            process2.waitFor();
            process3.waitFor();

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(NgramaGenerator.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Ngram.createCountFile(). " + ex.getMessage());
        }
     
    }
    
    private void applyStatistic(){
        String path = tmpPath + idUser;
        String scoreProp = "";
        if(score != 0){
            scoreProp = "--score " + score + " ";
        }
        try {
            //Executando count.pl todos os arquivos
            //Portuguese
            //String command = "perl " + perlScriptPath + "statistic.pl "+ statistic + " " + scoreProp + path + "_pt.stat " + path + "_pt.count";
            Process process1 = Runtime.getRuntime().exec(
                "perl " + 
                perlScriptPath + "statistic.pl "+ statistic + " " + scoreProp + path + "_pt.stat " + path + "_pt.count");
            //English
            Process process2 = Runtime.getRuntime().exec(
                "perl " + 
                perlScriptPath + "statistic.pl "+ statistic + " " + scoreProp + path + "_en.stat " + path + "_en.count");
            //Spanish
            Process process3 = Runtime.getRuntime().exec(
                "perl " + 
                perlScriptPath + "statistic.pl "+ statistic + " " + scoreProp + path + "_es.stat " + path + "_es.count");
        
            process1.waitFor();
            process2.waitFor();
            process3.waitFor();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(NgramaGenerator.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Ngram.applyStatistic(). " + ex.getMessage());
        }        
    }
    
    public NgramResults ptResults(int page, int window){
        String path = tmpPath + idUser + "_pt.stat";
//        String path = tmpPath + "teste_pt.stat";
        NgramResults ptNgram = new NgramResults();
        ptNgram.setLanguage("Portuguese");
        ptNgram.setPage(page);
        ptNgram.setWindow(window);
        int from = (page-1) * window + 1;
        ptNgram.setResults(fileToList(path, from, window));
        ptNgram.setTotal(countLines(path));
        return ptNgram;
    }
    
    public NgramResults esResults(int page, int window){
        String path = tmpPath + idUser + "_es.stat";
//        String path = tmpPath + "teste_es.stat";
        NgramResults esNgram = new NgramResults();
        esNgram.setLanguage("Spanish");
        esNgram.setPage(page);
        esNgram.setWindow(window);
        int from = (page-1) * window + 1;
        esNgram.setResults(fileToList(path, from, window));      
        esNgram.setTotal(countLines(path));
        return esNgram;
    }
    
    public NgramResults enResults(int page, int window){
        String path = tmpPath + idUser + "_en.stat";
//        String path = tmpPath + "teste_en.stat";
        NgramResults enNgram = new NgramResults();
        enNgram.setLanguage("English");
        enNgram.setPage(page);
        enNgram.setWindow(window);
        int from = (page-1) * window + 1;
        enNgram.setResults(fileToList(path, from, window));
        enNgram.setTotal(countLines(path));
        return enNgram;
    }
    
    public List<NgramResults> allResults(int window){
        List<NgramResults> allResults = new ArrayList<>();
        allResults.add(ptResults(1, window));
        allResults.add(esResults(1, window));
        allResults.add(enResults(1, window));
        return allResults;
    }
    
    /**
     * 
     * @param filePath Caminho do arquivo
     * @param from Linha inicial
     * @param window Janela do resultado
     * @return 
     */
    public ArrayList<Ngram> fileToList(String filePath, int from, int window){
        ArrayList<Ngram> results = new ArrayList<>();
        try (FileReader fileReader = new FileReader(filePath); BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String linha = bufferedReader.readLine();
            int count = 0;
            while(linha != null){
                if(from <= count && count < from+window){
                    results.add(createNgram(linha));                
                }
                linha = bufferedReader.readLine();
                count++;
                if(count > from+window)
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(NgramaGenerator.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro em Ngrama.fileToList(). " + ex.getMessage());
        }
        return results;
    }
    
    public Ngram createNgram(String linha){
        
        Ngram ngram = new Ngram();
        String[] words = linha.split("<>");
        ngram.setNgram(words[0] + " " + words[1]);
        String[] numbers = words[2].split(" ");
        ngram.setStat(numbers[1]);
        
        return ngram;
    }
    
    public int countLines(String filePath){
        int count = 0;
        try (FileReader fileReader = new FileReader(filePath); BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            while(bufferedReader.readLine() != null){
                count++;
            }
        } catch (IOException ex) {
            Logger.getLogger(NgramaGenerator.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro em Ngrama.fileToList(). " + ex.getMessage());
        }
        return count;
    }
    
    //Remove files when user logout...
    public void removeAllFiles(){
        String filePath = tmpPath.concat(idUser);
        //Removendo arquivos de .input (corpus2file)
        removeFile(filePath + "_pt.input");
        removeFile(filePath + "_en.input");
        removeFile(filePath + "_es.input");
        
        //Removendo arquivos .count
        removeFile(filePath + "_pt.count");
        removeFile(filePath + "_en.count");
        removeFile(filePath + "_es.count");
        
        //Removendo arquivos .stat
        removeFile(filePath + "_pt.stat");
        removeFile(filePath + "_en.stat");
        removeFile(filePath + "_es.stat");
         
    }
    
    private void removeTemp(){
        String filePath = tmpPath.concat(idUser);
        //Removendo arquivos .count
        removeFile(filePath + "_pt.count");
        removeFile(filePath + "_en.count");
        removeFile(filePath + "_es.count");
        
        //Removendo arquivos .stat
        removeFile(filePath + "_pt.stat");
        removeFile(filePath + "_en.stat");
        removeFile(filePath + "_es.stat");
    }
           
    private static void removeFile(String filePath) {
        File file = new File(filePath);
        if(file.exists()){
            file.delete();
        }
    }

}
