package br.library.util.sentence;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michelle
 */
public class SentenceDetector {

    public static enum Languages {

        PORTUGUESE, ENGLISH, FRENCH, SPANISH, ITALIAN;
    }

    /**
     * Method to break a text into sentences
     *
     * @param text A string with the text
     * @param language The language (Can be: portuguese, english, spanish,
     * french or italian)
     * @return <code>List<String></code> with a list of sentences
     */
    public static List<String> breakTextIntoSentences(String text, String language) throws RuntimeException {
        
        //Fill the var whith the language abbreviations
        String[] ABBREVIATIONS = SentenceDetector.getAbbreviattions(language);

        List<String> lstSentences = new ArrayList<>();
        String sentence = "";
        boolean existAbbreviation = false;

        BreakIterator bi = BreakIterator.getSentenceInstance();
        bi.setText(text);

        int start = bi.first();
        int end = bi.next();
        int tempStart = start;

        while (end != BreakIterator.DONE) {
            sentence = text.substring(start, end);
            existAbbreviation = hasAbbreviation(sentence, ABBREVIATIONS);

            if (!existAbbreviation) {
                sentence = text.substring(tempStart, end);
                tempStart = end;

                lstSentences.add(sentence);
            }
            

            start = end;
            end = bi.next();
        }

        if (existAbbreviation) {
            lstSentences.add(sentence);
        }

        return lstSentences;
    }

    /**
     * Method to get abbreviation based on the language
     *
     * @param language The language (Can be: portuguese, english, spanish,
     * french or italian)
     * @return <code>String[]</code> with the abbreviation list
     */
    public static String[] getAbbreviattions(String language) throws RuntimeException {

        String[] ABBREVIATIONS = {""};

        switch (language) {
            case "PORTUGUESE": {
                ABBREVIATIONS = PortugueseAbbreviations.getABBREVIATIONS();
                break;
            }
            case "ENGLISH": {
                ABBREVIATIONS = EnglishAbbreviations.getABBREVIATIONS();
                break;
            }
            case "FRENCH": {
                ABBREVIATIONS = SpanishAbbreviations.getABBREVIATIONS();
                break;
            }
            case "SPANISH": {
                ABBREVIATIONS = FrenchAbbreviations.getABBREVIATIONS();
                break;
            }
            case "ITALIAN": {
                ABBREVIATIONS = ItalianAbbreviations.getABBREVIATIONS();
                break;
            }
            default: {  //Language not allowed
                throw new RuntimeException("É necessário especificar a linguagem do texto.");
            }
        }

        return ABBREVIATIONS;
    }

    /**
     * Method to verify if there is a abbreviation in a sentence
     *
     * @param sentence The sentence
     * @param ABBREVIATIONS The abbreviation list
     * @return <code>boolean</code> with true when the abbreviation exist in the
     * sentence, otherwise return false
     */
    private static boolean hasAbbreviation(String sentence, String[] ABBREVIATIONS) {
        if (sentence == null || sentence.isEmpty()) {
            return false;
        }
        for (String w : ABBREVIATIONS) {
            if (sentence.toLowerCase().trim().endsWith(w)) {
                return true;
            }
        }
        return false;
    }

}
