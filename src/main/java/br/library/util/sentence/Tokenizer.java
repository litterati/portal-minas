package br.library.util.sentence;

import br.library.util.Config;
import br.project.exception.TokenizerException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

/**
 *
 * @author Thiago Vieira
 * @since 22 de maio de 2014
 */
public class Tokenizer {

    public static enum Languages {
        PORTUGUESE, ENGLISH, SPANISH, ITALIAN;
    }

    /**
     * Method to break a text into tokens and tags
     *
     * @param text A string with the text
     * @param language A string with the language of the text (en, es, it, pt)
     * @return <code>List< List< String > ></code> with a list of sentences
     * which is a list of tokens
     * @throws br.project.exception.TokenizerException
     */
    public static List<List<String>> tokenize(String text, String language) throws TokenizerException {

        List<List<String>> allSentences = new ArrayList<>();

        InputStream modelSentIn = null;
        InputStream modelTokenIn = null;

        String modelSentFileName = null;
        String modelTokenFileName = null;

        try {
            switch (language) {
                case "ENGLISH":
                    modelSentFileName = "en-sent.bin";
                    modelTokenFileName = "en-token.bin";
                    break;
                case "SPANISH":
                    /*modelSentFileName = "es-sent.bin";
                    modelTokenFileName = "es-token.bin";*/
                    modelSentFileName = "pt-sent.bin";
                    modelTokenFileName = "pt-token.bin";
                    break;
                case "ITALIAN":
                    /*modelSentFileName = "it-sent.bin";
                    modelTokenFileName = "it-token.bin";*/
                    modelSentFileName = "pt-sent.bin";
                    modelTokenFileName = "pt-token.bin";
                    break;
                case "PORTUGUESE":
                    modelSentFileName = "pt-sent.bin";
                    modelTokenFileName = "pt-token.bin";
                    break;
                default:
                    throw TokenizerException.languageNotAcceptable(language);
            }
            
            Properties prop = Config.getPathProperties();

            modelSentIn = new FileInputStream(prop.getProperty("openNLP.models") + modelSentFileName);
            modelTokenIn = new FileInputStream(prop.getProperty("openNLP.models") + modelTokenFileName);

            SentenceModel modelSent = new SentenceModel(modelSentIn);
            TokenizerModel modelToken = new TokenizerModel(modelTokenIn);

            //Trata as tags
            Map<String, String> tags = new HashMap();
            Pattern pattern = Pattern.compile("(<[^>]+>)");
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                //Hash
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(matcher.group(1).getBytes());
                byte[] bytes = md.digest();
                //Convert it to hexadecimal format
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.length; i++) {
                    sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }
                //End Hash

                tags.put(sb.toString(), matcher.group(1));
                text = text.replace(matcher.group(1), " " + sb.toString() + " ");
            }
            text = text.replaceAll("\n", " _NEWLINE_ ");//nova linha
            text = text.replaceAll("“", " \" ");
            text = text.replaceAll("”", " \" ");
            text = text.replaceAll("''", " \" ");
            text = text.replaceAll("``", " \" ");
            text = text.replaceAll("`", " ' ");
            text = text.replaceAll("‘", " ' ");
            text = text.replaceAll("’", " ' ");
            text = text.replaceAll("«", " << ");
            text = text.replaceAll("»", " >> ");
            text = text.replaceAll("/([\\\\\\/\\\"\\!\\(\\)\\{\\}\\[\\]\\?\\:\\;\\@\\%\\&\\=\\+\\~\\>\\<])/", "/ $1 /");
            text = text.replaceAll("/([¡¿º°ª])/", "/ $1 /");

            SentenceDetectorME sentenceDetector = new SentenceDetectorME(modelSent);
            String sentences[] = sentenceDetector.sentDetect(text);

            TokenizerME tokenizer = new TokenizerME(modelToken);
            List<String> allTokens;
            String tokens[];
            for (String sentence : sentences) {
                allTokens = new ArrayList<>();
                tokens = tokenizer.tokenize(sentence);

                //allTokens.addAll(Arrays.asList(tokens));
                for (String token : tokens) {
                    //substitui o hash pela tag
                    if (tags.containsKey(token)) {
                        allTokens.add(tags.get(token));
                    } else {
                        allTokens.add(token);
                    }
                }
                allSentences.add(allTokens);
            }

        } catch (FileNotFoundException ex) {
            throw new TokenizerException(ex);
        } catch (IOException ex) {
            throw new TokenizerException(ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new TokenizerException(ex);
        } catch (URISyntaxException ex) {
            throw new TokenizerException(ex);
        } finally {
            try {
                modelSentIn.close();
                modelTokenIn.close();
            } catch (IOException ex) {
                throw new TokenizerException(ex);
                //Logger.getLogger(Tokenizer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return allSentences;
    }

}
