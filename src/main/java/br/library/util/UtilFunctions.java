package br.library.util;

import java.text.Normalizer;

/**
 *
 * @author michelle
 */
public class UtilFunctions {
  
  /**
   * Method to know about the file extension.
   * @param file_path
   * @return
   * <code>String</code> with the file extension
   */
  public static String fileExtension(String file_path) {

    int i = file_path.lastIndexOf('.');
    return file_path.substring(i);
  }
  
  /**
   * Method to remove annotations from a file. The annotation is anything between < and >.
   * @param text The text string you would like to remove the annotations
   * @return
   * <code>String</code> without the annotations
   */
  public static String removeAnnotation(String text) {
//return text.replaceAll("<cabeçalho>.*?</cabeçalho>", "").replaceAll("(<([^>]+)>)", "");
    return text.replaceAll("(<([^>]+)>)", "");
  }
  
  public static String removeAccents(String str) {
    str = Normalizer.normalize(str, Normalizer.Form.NFD);
    str = str.replaceAll("[^\\p{ASCII}]", "");
    return str;
  }
  
  public static String[] splitParagraph(String str){
    return str.split("\r\n|\n\r|\r\r|\n\n|\n|\r");
  }
  
}
