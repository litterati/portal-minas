package br.library.util;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.*;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.mozilla.universalchardet.UniversalDetector;

/**
 * Class used to read a file and return a text string. It's possible to read a
 * .text, .doc, .docx and .pdf file.
 *
 * @author Michelle, Thigo Vieira
 * @since 2013
 */
public class FileReaderManipulation {

    /**
     * Method to read from a TEXT file
     *
     * @param filePath path of the file
     * @return <code>String</code>
     */
    private static String returnTxtString(String filePath) throws IOException {
        String line;
        StringBuilder text = new StringBuilder();

        BufferedReader br =  new BufferedReader(new InputStreamReader(new FileInputStream(filePath), getFileEncoding(filePath)));
        
        try {
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append(System.lineSeparator());
            }
        } finally {
            br.close();
        }

        return text.toString();
    }

    /**
     * Method to return the enconding of file
     *
     * @param filePath path of the file
     * @return <code>String</code>
     */
    private static String getFileEncoding(String filePath) throws IOException {
        FileInputStream fis = new FileInputStream(filePath);

        byte[] buf = new byte[4096];
        UniversalDetector detector = new UniversalDetector(null);
        int nread;
        while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
            detector.handleData(buf, 0, nread);
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        System.out.println(encoding);
        detector.reset();
        return encoding;
    }

    /**
     * Method to read from a PDF file
     *
     * @param filePath path of the file
     * @return <code>String</code>
     */
    private static String returnTexPDFFile(String filePath) throws IOException {
        int numberOfPages;
        StringBuilder text = new StringBuilder();

        RandomAccessFileOrArray raFileOrArray = new RandomAccessFileOrArray(filePath);
        PdfReader reader = new PdfReader(raFileOrArray, null);
        numberOfPages = reader.getNumberOfPages();

        for (int page = 1; page <= numberOfPages; page++) {
            text.append(PdfTextExtractor.getTextFromPage(reader, page));
        }

        return text.toString();
    }

    /**
     * Method to read from a DOC file
     *
     * @param filePath path of the file
     * @return <code>String</code>
     */
    private static String returnTexDocFile(String filePath) throws IOException {

        StringBuilder text = new StringBuilder();

        HWPFDocument doc = new HWPFDocument(new POIFSFileSystem(new FileInputStream(filePath)));

        WordExtractor we = new WordExtractor(doc);
        String[] paragraphs = we.getParagraphText(); // Total number of paragraphs

        for (int i = 0; i < paragraphs.length; i++) {
            text.append(paragraphs[i]);
        }
        return text.toString();
    }

    /**
     * Method to read from a DOCX file
     *
     * @param filePath path of the file
     * @return <code>filePath</code>
     */
    private static String returnTexDocxFile(String filePath) throws IOException {

        File docFile = new File(filePath);
        FileInputStream fis = new FileInputStream(docFile.getAbsolutePath());

        XWPFDocument doc = new XWPFDocument(fis);
        XWPFWordExtractor docExtractor = new XWPFWordExtractor(doc);

        return docExtractor.getText();
    }

    /**
     * Method to read from a file with extension .txt, .pdf, .doc, .docx and
     * return a string with the file content
     *
     * @param filePath path of the file
     * @return String with the file content <code>String</c>
     */
    public static String returnTextString(String filePath) throws IOException {

        String text = "";
        String extension = UtilFunctions.fileExtension(filePath);

        if (extension.compareTo(".txt") == 0) {
            text = FileReaderManipulation.returnTxtString(filePath);
        }
        if (extension.compareTo(".pdf") == 0) {
            text = FileReaderManipulation.returnTexPDFFile(filePath);
        }
        if (extension.compareTo(".doc") == 0) {
            text = FileReaderManipulation.returnTexDocFile(filePath);
        }
        if (extension.compareTo(".docx") == 0) {
            text = FileReaderManipulation.returnTexDocxFile(filePath);
        }

        return text;
    }
}
