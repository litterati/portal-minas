/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.library.util;

/**
 *
 * @author matheus_silva
 */
public interface Constant {

    public interface TAGS {
        String TEXTHEADER = "(<[/]?cabe(c|ç)alho>)";
        //String TEXTHEADER = "(<[/]?(cabecalho.*|cabeçalho.*)>$)";
        //String[] METATEXT = {"front", "note", "nota sobre o autor", "nota do editor", "orelha", "dedicatória", "índice", "back", "contracapa", "glossário","cabecalho","cabeçalho","contra capa", "rodape", "rodapé"};
        //String[] TEXTBODY = {"body"};
        //String[] METATEXT_AND_STYLE = {"front", "note", "orelha", "dedicatória", "índice", "back", "contracapa", "glossário","cabecalho","cabeçalho","contra capa","i","n","itálico","negrito"};
        String STYLE = "[/.]?(n|i)";
        String METATEXT_ATTRIBUTE_TAG = "([A-z0-9]*)\\s*(?:([A-z0-9]*)\\s*=?\\s*\\\"?([A-z0-9]*)\\s*\\\"?\\s*\\\"?\\s*?)?(?:([A-z0-9]*)\\s*=?\\s*\\\"?([A-z0-9]*)\\s*\\\"?\\s*\\\"?\\s*?)?(?:([A-z0-9]*)\\s*=?\\s*\\\"?([A-z0-9]*)\\s*\\\"?\\s*\\\"?\\s*?)?(?:([A-z0-9]*)\\s*=?\\s*\\\"?([A-z0-9]*)\\s*\\\"?\\s*\\\"?\\s*?)?";
        String SPECIAL = "([A-zÀ-ú]*)\\s*(\\d+)";
    }
}
