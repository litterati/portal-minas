package br.library.util;

/**
 * Class used to  get the execution time of a method or program in java
 * @author michelle
 */
public class Chronometer {
 private static long startTime;  
 private static long endTime;  
 private static long timeDiff;  
  
 /** 
  * Begin the chronometer
  */  
  public static void start() {  
    startTime = System.currentTimeMillis();  
    endTime = 0;  
    timeDiff = 0;  
  }  
  
  /** 
  * End the chronometer and calcule how long it was to finish
  */  
  public static void stop() {  
    endTime = System.currentTimeMillis();  
    timeDiff = endTime - startTime;  
  }  
  
  /** 
  * @return how long it was to finish the task in milliseconds 
  */  
  public static long elapsedTime() {  
    return timeDiff;  
  }  
}
