/*
 * Fornece todos os arquivos de Propriedades necessárias para o sitema.
 */
package br.library.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Config.java
 *
 * @author Thiago Vieira
 * @since 15/07/2014
 */
public class Config {

    private static Properties dbProps = null;
    private static final String dbConf = "../conf/DBConfig.properties";
    private static Properties pathProps = null;
    private static final String pathConf = "../conf/PathConfig.properties";

    private Config() {
        // Apenas para não permitir instânciar a classe
    }

    public static Properties getDBProperties() throws IOException, URISyntaxException {
        if (dbProps == null) {
            String path = Thread.currentThread()
                    .getContextClassLoader()
                    .getResource("/")
                    .toURI()
                    .resolve(dbConf)
                    .getPath();

            dbProps = new Properties();
            dbProps.load(new FileInputStream(path));
        }

        return dbProps;
    }

    public static Properties getPathProperties() throws IOException, URISyntaxException {
        if (pathProps == null) {
            String path = Thread.currentThread()
                    .getContextClassLoader()
                    .getResource("/")
                    .toURI()
                    .resolve(pathConf)
                    .getPath();

            pathProps = new Properties();
            pathProps.load(new FileInputStream(path));
        }

        return pathProps;
    }
}
