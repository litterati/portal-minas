/*
 * Contrala o armazenamento de eventos do sistema.
 */
package br.library.util;

import br.project.entity.User;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Logger
 *
 * @author Thiago Vieira
 * @since 04/09/2014
 */
public class Logger {

    private static Logger instance;
    private static final String EVENT = "EVENT";
    private static final String ERROR = "ERROR";

    private Logger() {
        // Apenas para não permitir instânciar a classe
    }

    private static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public static void controllerEvent(Object where, HttpServletRequest request, String message) {
        try {
            HttpSession session = request.getSession();
            String user = request.getRemoteHost();
            if (session.getAttribute("userLoged") != null) {
                user = user + "|" + ((User) session.getAttribute("userLoged")).getId();
            }
            Logger.getInstance().register(
                    EVENT,
                    user,
                    where.getClass().getName(),
                    message);
        } catch (Exception ex) {
            System.out.print("[ERROR] " + ex.getMessage());
        }
    }

    public static void controllerError(Object where, HttpServletRequest request, Exception ex) {
        try {
            HttpSession session = request.getSession();
            String user = request.getRemoteHost();
            if (session.getAttribute("userLoged") != null) {
                user = user + "|" + ((User) session.getAttribute("userLoged")).getId();
            }
            Logger.getInstance().register(
                    ERROR,
                    user,
                    where.getClass().getName(),
                    ex.getMessage());
        } catch (Exception e) {
            System.out.print("[ERROR] " + e.getMessage());
        }
    }

    public static void event(Object where, String message) {
        try {
            Logger.getInstance().register(
                    EVENT,
                    null,
                    where.toString(),
                    message);
        } catch (Exception ex) {
            System.out.print("[ERROR] " + ex.getMessage());
        }
    }

    public static void error(Object where, Exception ex) {
        try {
            Logger.getInstance().register(
                    ERROR,
                    null,
                    where.toString(),
                    ex.getMessage());
        } catch (Exception e) {
            System.out.print("[ERROR] " + e.getMessage());
        }
    }

    private boolean register(String type, String user, String where, String message) {
        try {
            //time
            DateFormat arqDateFormt = new SimpleDateFormat("yyyy/MM");
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
            Calendar cal = Calendar.getInstance();

            String dir_log = Config.getPathProperties().getProperty("path.logs") + arqDateFormt.format(cal.getTime());
            File dl = new File(dir_log);
            //Verifica se o caminho existe, senão cria
            if (!dl.exists()) {
                dl.mkdirs();
            }

            String arq_log = cal.get(Calendar.DAY_OF_MONTH) + ".log";
            String line = "[" + type + "]" + "[" + user + "]" + "[" + dateFormat.format(cal.getTime()) + "] " + "[" + where + "]" + message.replace('\n', ' ') + "\n";

            // Create file 
            FileWriter fstream = new FileWriter(dir_log + "/" + arq_log, true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(line);
            out.flush();
            //Close the output stream
            out.close();

            System.out.print(line);
            return true;
        } catch (Exception e) {
            System.out.println("[ERROR] Evento não registrado: [" + type + "] " + message);
            System.out.println("[ERROR] Motivo: " + e.getMessage());
            return false;
        }
    }

}
