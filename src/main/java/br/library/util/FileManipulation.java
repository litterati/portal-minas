package br.library.util;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 * Class used to manipulate file: upload a file, download a file and delete a
 * file or directory
 * @author Michelle
 */
public class FileManipulation {

  private String uploadPath;;
  private final int memoryThreshold = 1024 * 1024 * 3; // 3MB
  private final int maxFileSize = 1024 * 1024 * 40; // 40MB
  private final int maxReqSize = 1024 * 1024 * 50; // 50MB
  private Map<String, String> variableMap = new HashMap<>();
  private String[] contentTypeAllowed = {"application/zip",
                                         "application/x-zip-compressed",
                                         "application/x-zip", "text/plain", 
                                         "application/pdf", "application/msword", 
                                         "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};

  public FileManipulation() throws URISyntaxException{
    this.uploadPath = Thread.currentThread().getContextClassLoader().getResource("/").toURI().resolve("../../upload").getPath();
  }
  
  /**
   * Method to remove a file or a directory and all the files inside it
   * @param file File
   */
  private static void delete(File file) {
    
    // Check if file is directory/folder
    if (file.isDirectory()) {
      
      // Get all files in the folder
      File[] files = file.listFiles();

      for (int i = 0; i < files.length; i++) {
        // Delete each file in the folder
        delete(files[i]);
      }

      // Delete the folder
      file.delete();
    } else {
      // Delete the file if it is not a folder
      file.delete();
    }
  }

  /**
   * @return the variableMap, that contain the screen's inputs with their
   * values. For example: Wen we have the fallow code in the screen: 
   * <input type="text" name="path" value="/home/test/file.pdf"> the variableMap will
   * have the key 'path' whith the value '/home/test/file.pdf'. This variable
   * also contain error message when it exist. The error message will have the
   * key 'error_mensage'
   */
  public Map<String, String> getVariableMap() {
    return variableMap;
  }
  
    /**
   * Method to verify if a content type is allowed
   * @param content The content type of a file
   * @return <code>boolean</code>
   */
  private boolean isConntentypeAllowed(String contentType){
    
    boolean isAllowed = false;
    
    for(int i = 0; i < this.contentTypeAllowed.length; i++){
      if((contentType.compareTo(this.contentTypeAllowed[i]) == 0)){
        return true;
      }
    }
            
    return isAllowed;
  }
  
  public static void saveFile(String fileName, String text){
    BufferedWriter writer = null;
    try{
      writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
      writer.write(text);

    }catch ( IOException e){
    }
    finally{
      try
      {
          if (writer != null)
          writer.close( );
      }
      catch ( IOException e)
      {
      }
    }
  }

  /**
   * Method to remove a file or a directory
   * @param filePath Project path of the file or directory
   */
  public static void removeFile(String filePath) {
    File file = new File(filePath);
    FileManipulation.delete(file);
  }

  /**
   * Method to rename a file or directory
   * @param fileName The file or directory name
   * @param fileNewName The file or directory new name
   */
  public static void renameFile(File fileName, File fileNewName) {
    
    if (fileName.exists()) {

      boolean ok = fileName.renameTo(fileNewName);

      if (!ok) {
        //It wasn't possible to rename the file/directory
        throw new RuntimeException("Problemas para renomear diretório");
      }
    }
  }

  /**
   * Method to upload a file
   *
   * @param request Servlet request
   * @param response Servlet response
   */
  public void uploadFile(HttpServletRequest request, HttpServletResponse response) throws IOException, FileUploadException, Exception {

    // Configures upload settings
    DiskFileItemFactory factory = new DiskFileItemFactory();
    // Sets memory threshold - beyond which files are stored in disk
    factory.setSizeThreshold(this.memoryThreshold);
    // Sets temporary location to store files
    factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

    ServletFileUpload upload = new ServletFileUpload(factory);

    // Sets maximum size of upload file
    upload.setFileSizeMax(this.maxFileSize);

    // Sets maximum size of request (include file + form data)
    upload.setSizeMax(this.maxReqSize);

    // Creates the directory if it does not exist
    File uploadDir = new File(this.uploadPath);
    if (!uploadDir.exists()) {
      uploadDir.mkdir();
    }

    // parses the request's content to extract file data
    @SuppressWarnings("unchecked")
    List<FileItem> formItems = upload.parseRequest(request);

    if (formItems != null && formItems.size() > 0) {

      // Iterates over form's fields
      for (FileItem item : formItems) {
        // Processes only fields that are not form fields
        if (!item.isFormField()) {
          if ( this.isConntentypeAllowed(item.getContentType())) {

            String fileName = new File(item.getName()).getName();
            String filePath = this.uploadPath + File.separator + fileName;
            File storeFile = new File(filePath);

            this.variableMap.put("path", filePath);

            /*
             * if ((new File(filePath)).exists()) { //File exists
             * this.variableMap.put("error_message", "Arquivo já" + "
             * existe, tente novamente"); continue;
                        }
             */

            // Saves the file on disk
            item.write(storeFile);
            this.variableMap.put("error_message", "");
          } else {
            //It's oly possible upload .txt file
            this.variableMap.put("error_message", "Só é possível "
                    + "fazer upload de arquivos com extensões .txt, .doc, .docx .pdf"); // e .zip");
          }
        } else {
          this.variableMap.put(item.getFieldName(), item.getString());
        }
      }
    }
  }
}
