package br.project.facade;

import br.project.dao.CorpusTagValuesDAO;
import br.project.dao.CorpusTagsDAO;
import br.project.dao.FieldsTypeValuesDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTagValues;
import br.project.entity.CorpusTags;
import br.project.entity.FieldsTypeValues;
import br.project.exception.CorpusTagsFacadeException;
import br.project.exception.DAOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CorpusTagsFacade
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class CorpusTagsFacade {

    public static boolean existTag(CorpusTags corpusTags) throws CorpusTagsFacadeException {

        CorpusTagsDAO corpusTagsDAO = new CorpusTagsDAO();

        try {
            return corpusTagsDAO.existTag(corpusTags);
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
    }

    public static boolean existTagValues(CorpusTags corpusTags) throws CorpusTagsFacadeException {

        CorpusTagValues corpusTagValues = new CorpusTagValues();
        corpusTagValues.setIdCorpusTag(corpusTags.getId());
        corpusTagValues.setIdCorpus(corpusTags.getIdCorpus());

        try {
            CorpusTagValuesDAO corpusTagValuesDAO = new CorpusTagValuesDAO();
            corpusTagValuesDAO.selectById(corpusTagValues);

            if (corpusTagValues.getId() != 0) {
                return true;
            }
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
        return false;
    }

    public static void fillCorpusTagsWithoutValue(Corpus corpus) throws CorpusTagsFacadeException {

        CorpusTagValues corpusTagValues;
        FieldsTypeValues fieldsTypeValues = new FieldsTypeValues();

        try {
            List<CorpusTags> listCorpusMetadata = CorpusTagsDAO.selectAllByIdCorpus(corpus.getId());

            if (listCorpusMetadata.size() > 0) {
                corpus.setCorpusTagValues(new ArrayList<CorpusTagValues>());
                for (int i = listCorpusMetadata.size() - 1; i >= 0; i--) {

                    corpusTagValues = new CorpusTagValues();

                    corpusTagValues.setIdCorpus(corpus.getId());
                    corpusTagValues.setIdCorpusTag(listCorpusMetadata.get(i).getId());

                    fieldsTypeValues.setIdType(listCorpusMetadata.get(i).getIdFieldType());

                    corpus.addCorpusTagValues(corpusTagValues);
                }
            }
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
    }

    /*public static void fillCorpusMetadataValue(Corpus corpus) throws CorpusTagsFacadeException {

        CorpusTagValuesDAO corpusTagValuesDAO = new CorpusTagValuesDAO();

        try {
            CorpusTagsFacade.fillCorpusTagsWithoutValue(corpus);

            for (int i = 0; i < corpus.getCorpusTagValues().size(); i++) {
                corpusTagValuesDAO.selectById(corpus.getCorpusTagValues().get(i));
            }
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
    }*/

    public static void insertEmptyTagValues(CorpusTags corpusTags) throws CorpusTagsFacadeException {

        CorpusTagValues corpusTagValues = new CorpusTagValues();
        corpusTagValues.setIdCorpus(corpusTags.getIdCorpus());
        corpusTagValues.setIdCorpusTag(corpusTags.getId());
        corpusTagValues.setValue("");

        CorpusTagValuesDAO corpusTagValuesDAO = new CorpusTagValuesDAO();
        try {
            corpusTagValuesDAO.insert(corpusTagValues);
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
    }

    public static void insertTags(Corpus corpus) throws CorpusTagsFacadeException {
        try {
            CorpusTagValuesDAO.insert(corpus.getCorpusTagValues());
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
    }

    public static void updateTags(Corpus corpus) throws CorpusTagsFacadeException {
        try {
            CorpusTagValuesDAO.updateByMetadataId(corpus.getCorpusTagValues());
        } catch (DAOException ex) {
            throw new CorpusTagsFacadeException(ex);
        }
    }

}
