package br.project.facade;

import br.library.connection.BuildConnection;
import br.library.util.Config;
import br.library.util.Constant;
import br.library.util.FileManipulation;
import br.library.util.FileReaderManipulation;
import br.library.util.FileZip;
import br.library.util.Logger;
import br.library.util.decontract.ContractionEn;
import br.library.util.decontract.ContractionEs;
import br.library.util.decontract.ContractionLanguage;
import br.library.util.decontract.ContractionPt;
import br.library.util.sentence.Tokenizer;
import br.project.dao.MetaWordTagLabelDAO;
import br.project.dao.MetaWordTagsDAO;
import br.project.dao.MetaWordsDAO;
import br.project.dao.SentenceDAO;
import br.project.dao.TextAudioDAO;
import br.project.dao.TextDAO;
import br.project.dao.TextImagesDAO;
import br.project.dao.TextTagsDAO;
import br.project.dao.WordDAO;
import br.project.dao.WordTagLabelDAO;
import br.project.dao.WordTagsDAO;
import br.project.entity.MetaWordTags;
import br.project.entity.Sentence;
import br.project.entity.Text;
import br.project.entity.TextAudio;
import br.project.entity.TextImages;
import br.project.entity.TextTags;
import br.project.entity.WordTagAttribute;
import br.project.entity.WordTagLabel;
import br.project.entity.WordTags;
import br.project.entity.Words;
import br.project.exception.DAOException;
import br.project.exception.TextFacadeException;
import br.project.exception.TextTagsFacadeException;
import br.project.exception.TokenizerException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Stack;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextFacade {

    protected static final String[] specialCharacter = new String[]{"‘", "’", "“", "”"};
    protected List<String> filesNotExtracted = new ArrayList<>();
    private static Properties prop;

    public List<String> insertText(Text text) throws TextFacadeException {
        //DECONTRATOR ADICIONADO POR MARCEL
        ContractionLanguage decontractor;
        List<String> textMetadataProblems = new ArrayList<>();

        try (Connection c = BuildConnection.getConnection()) {

            //Definindo qual será a linguagem do decontrator por hora será sempre portugues
            switch (text.getLanguage()) {
                case "PORTUGUESE":
                    decontractor = new ContractionPt();
                    break;
                case "SPANISH":
                    decontractor = new ContractionEs();
                    break;
                case "ENGLISH":
                    decontractor = new ContractionEn();
                    break;
                default:
                    decontractor = new ContractionPt();
            }

            try {
                //Insert the texts in the database
                TextDAO.insert(text, c);

                if (text.getFile().compareTo("automatic") == 0) {
                    text.setTextString(FileReaderManipulation.returnTextString(text.getPath()));
                    FileManipulation.removeFile(text.getPath()); //Remove the file from the server
                }

                // Processamento do Header
                textMetadataProblems.addAll(new TextTagsFacade().insertHeaders(text, c));

                List<List<String>> lstWordsString = Tokenizer.tokenize(text.getTextString().toString(), text.getLanguage());

                List<WordTagLabel> lstMetaWordTagLabels = new ArrayList<>();
                List<WordTagLabel> lstWordTagLabels = new ArrayList<>();

                Stack<WordTagLabel> stkMetaWordTag = new Stack<>();
                Stack<WordTagLabel> stkWordTag = new Stack<>();

                List<String> lstWrongTags;

                int sentencePosition = 0;
                int wordPosition = 0;
                int lastWordId = -1;
                Sentence sentence;
                WordTags currentTag = new WordTags();
                WordTagLabel currentSpecialTag = null;

                for (List<String> sentenceStr : lstWordsString) {
                    sentence = new Sentence();
                    sentence.setPosition(sentencePosition);
                    sentence.setIdText(text.getId());
                    if (stkMetaWordTag.empty()) {
                        sentence.setType(Sentence.TYPE_TEXT_SENTENCE);
                    } else {
                        sentence.setType(Sentence.TYPE_META_SENTENCE);
                    }
                    sentencePosition++;
                    SentenceDAO.insert(sentence, c);

                    for (String buffer : sentenceStr) {
                        if (buffer.matches("<(\"[^\"]*\"|'[^']*'|[^'\">])*>")) {
                            WordTags wordTag = new WordTags(cleanTag(buffer));
                            WordTagLabel wordTagLabel = new WordTagLabel(wordTag);
                            wordTagLabel.setIdText(text.getId());

                            if (isMetaWordAttributeTag(wordTagLabel)) {
                                fillMetaWordAttributeTagValues(wordTagLabel, text);
                            }

                            if (isMetaWordsTag(wordTagLabel, text) && !isStyleTag(wordTagLabel)) // Tag metaword processável
                            {
                                if (!stkMetaWordTag.empty()) {
                                    currentTag = stkMetaWordTag.peek().getWordTag();
                                }
                                if (!buffer.startsWith("</")
                                        && wordTagLabel.getWordTag().getId() != currentTag.getId()) {
                                    stkMetaWordTag.push(wordTagLabel);
                                } else {
                                    if (!stkMetaWordTag.empty()) {
                                        if (wordTagLabel.getWordTag().getId() == stkMetaWordTag.peek().getWordTag().getId()) {
                                            wordTagLabel = stkMetaWordTag.pop();
                                            if (lastWordId >= 0) {
                                                wordTagLabel.setIdWordClose(lastWordId);
                                                lstMetaWordTagLabels.add(wordTagLabel);
                                            }
                                        } else {
                                            if (existUnclosedLabelInStack(wordTagLabel.getWordTag(), stkMetaWordTag)) {
                                                WordTagLabel unclosedWordTagLabel = wordTagLabel;
                                                do {
                                                    wordTagLabel = stkMetaWordTag.pop();
                                                    if (lastWordId >= 0) {
                                                        wordTagLabel.setIdWordClose(lastWordId);
                                                        lstMetaWordTagLabels.add(wordTagLabel);
                                                    }
                                                } while (!stkMetaWordTag.empty() && (wordTagLabel.getWordTag().getId() != unclosedWordTagLabel.getWordTag().getId()));
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (isStyleTag(wordTagLabel)) {
                                    if (!buffer.startsWith("</")
                                            || (!stkMetaWordTag.empty() && !wordTagLabel.getWordTag().getTag().equals(stkMetaWordTag.peek().getWordTag().getTag()))
                                            || (!stkWordTag.empty() && !wordTagLabel.getWordTag().getTag().equals(stkWordTag.peek().getWordTag().getTag()))) {
                                        if (!stkMetaWordTag.empty()) {
                                            fillMetaWordTagByLabel(wordTagLabel, text);
                                            stkMetaWordTag.push(wordTagLabel);
                                        } else {
                                            fillWordTagByLabel(wordTagLabel, text);
                                            stkWordTag.push(wordTagLabel);
                                        }
                                    } else {
                                        if (!stkMetaWordTag.empty()) {
                                            if (wordTagLabel.getWordTag().getId() == stkMetaWordTag.peek().getWordTag().getId()) {
                                                wordTagLabel = stkMetaWordTag.pop();
                                                if (lastWordId >= 0) {
                                                    wordTagLabel.setIdWordClose(lastWordId);
                                                    lstMetaWordTagLabels.add(wordTagLabel);
                                                }
                                            } else {
                                                if (existUnclosedLabelInStack(wordTagLabel.getWordTag(), stkMetaWordTag)) {
                                                    WordTagLabel unclosedWordTagLabel = wordTagLabel;
                                                    do {
                                                        wordTagLabel = stkMetaWordTag.pop();
                                                        if (lastWordId >= 0) {
                                                            wordTagLabel.setIdWordClose(lastWordId);
                                                            lstMetaWordTagLabels.add(wordTagLabel);
                                                        }
                                                    } while (!stkMetaWordTag.empty() && (wordTagLabel.getWordTag().getId() != unclosedWordTagLabel.getWordTag().getId()));
                                                }
                                            }
                                        } else {
                                            if (!stkWordTag.empty()) {
                                                if (wordTagLabel.getWordTag().getId() == stkWordTag.peek().getWordTag().getId()) {
                                                    wordTagLabel = stkWordTag.pop();
                                                    if (lastWordId >= 0) {
                                                        wordTagLabel.setIdWordClose(lastWordId);
                                                        lstWordTagLabels.add(wordTagLabel);
                                                    }
                                                } else {
                                                    if (existUnclosedLabelInStack(wordTagLabel.getWordTag(), stkWordTag)) {
                                                        WordTagLabel unclosedWordTagLabel = wordTagLabel;
                                                        do {
                                                            wordTagLabel = stkWordTag.pop();
                                                            if (lastWordId >= 0) {
                                                                wordTagLabel.setIdWordClose(lastWordId);
                                                                lstWordTagLabels.add(wordTagLabel);
                                                            }
                                                        } while (!stkWordTag.empty() && (wordTagLabel.getWordTag().getId() != unclosedWordTagLabel.getWordTag().getId()));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (isSpecialTag(wordTagLabel) && stkMetaWordTag.empty()) // Tag sem fechamento fora de metatexto (<Página x>, <Capítulo X>, ...)
                            {
                                fillSpecialTagAttributeValues(wordTagLabel);
                                if (isWordsTag(wordTagLabel, text) || isMetaWordsTag(wordTagLabel, text)) {
                                    if (currentSpecialTag == null) {
                                        currentSpecialTag = wordTagLabel;
                                        currentSpecialTag.setIdWordOpen(lastWordId);
                                    } else {
                                        currentSpecialTag.setIdWordClose(lastWordId);
                                        if (stkMetaWordTag.empty()) {
                                            lstWordTagLabels.add(currentSpecialTag);
                                        } else {
                                            lstMetaWordTagLabels.add(currentSpecialTag);
                                        }
                                        currentSpecialTag = wordTagLabel;
                                        currentSpecialTag.setIdWordOpen(lastWordId);
                                    }
                                }
                            }
                        } else {
                            Words word = new Words();
                            word.setIdSentence(sentence.getId());
                            word.setIdCorpus(text.getIdCorpus());
                            word.setIdText(text.getId());
                            word.setWord(buffer);
                            word.setPosition(wordPosition);
                            wordPosition++;

                            if (!stkMetaWordTag.empty()) // Metatexto
                            {
                                MetaWordsDAO.insert(word, c);
                                //System.out.println(word.getWord());
                                lastWordId = word.getId();

                                Stack<WordTagLabel> stkMetaWordsTagLabelsAux = new Stack<>();
                                while (!stkMetaWordTag.empty() && stkMetaWordTag.peek().getIdWordOpen() <= 0) // Tags de formatação
                                {
                                    WordTagLabel topMetaWordTag = stkMetaWordTag.pop();
                                    topMetaWordTag.setIdWordOpen(lastWordId);
                                    stkMetaWordsTagLabelsAux.push(topMetaWordTag);
                                }
                                while (!stkMetaWordsTagLabelsAux.empty()) {
                                    stkMetaWordTag.push(stkMetaWordsTagLabelsAux.pop());
                                }

                            } else {

                                    //ADICIONANDO DECONTRATOR
                                //CÓDIGO ALTERADO POR MARCEL -- INICIO
                                if (!decontractor.hasWord(word.getWord())) {
                                    WordDAO.insert(word, c);
                                    //System.out.println(word.getWord());
                                    lastWordId = word.getId();
                                } else {
                                    String[] decontracted = decontractor.expandWord(word.getWord());
                                    Words word1 = new Words();
                                    word1.setIdSentence(sentence.getId());
                                    word1.setIdCorpus(text.getIdCorpus());
                                    word1.setIdText(text.getId());
                                    word1.setWord(decontracted[0]);
                                    word1.setPosition(wordPosition);
                                    word1.setContraction(decontracted[2]);
                                    wordPosition++;
                                    WordDAO.insert(word1, c);

                                    Words word2 = new Words();
                                    word2.setIdSentence(sentence.getId());
                                    word2.setIdCorpus(text.getIdCorpus());
                                    word2.setIdText(text.getId());
                                    word2.setWord(decontracted[1]);
                                    word2.setPosition(wordPosition);
                                    word2.setContraction(decontracted[2]);
                                    wordPosition++;
                                    WordDAO.insert(word2, c);
                                    lastWordId = word.getId();
                                }
                                    //FIM DO CODIGO ALTERADO POR MARCEL
                                //CASO ESTE CODIGO ESTIVER ATRAPALHANDO BASTA RETIRAR E DESCOMENTAR 		
                            }

                            Stack<WordTagLabel> stkWordTagLabelsAux = new Stack<>();
                            while (!stkWordTag.empty() && stkWordTag.peek().getIdWordOpen() <= 0) // Tags de formatação
                            {
                                WordTagLabel topStyleTag = stkWordTag.pop();
                                topStyleTag.setIdWordOpen(lastWordId);  // set  the last word inserted as open_word_id of the open styleTag(s).
                                stkWordTagLabelsAux.push(topStyleTag);
                            }
                            while (!stkWordTagLabelsAux.empty()) {
                                stkWordTag.push(stkWordTagLabelsAux.pop());
                            }

                            if (currentSpecialTag != null && currentSpecialTag.getIdWordOpen() <= 0) {
                                currentSpecialTag.setIdWordOpen(lastWordId); // set  the last word inserted as open_word_id of the open specialTag(s).
                            }

                        }
                    }
                }
                c.commit();
                if (!lstWordTagLabels.isEmpty()) {
                    for (int i = 0; i < lstWordTagLabels.size(); i++) {
                        if (lstWordTagLabels.get(i).getIdWordClose() <= 0) {
                            if (isSpecialTag(lstWordTagLabels.get(i))) {
                                WordTagLabel wtl = lstWordTagLabels.get(i);
                                wtl.setIdWordClose(lastWordId);
                                lstWordTagLabels.set(i, wtl);
                            } else {
                                lstWordTagLabels.remove(i);
                                i--;
                            }
                        }
                    }
                    validateLabels(lstWordTagLabels);
                    WordTagLabelDAO.insert(lstWordTagLabels, c);
                }
                if (!lstMetaWordTagLabels.isEmpty()) {
                    validateLabels(lstMetaWordTagLabels);
                    MetaWordTagLabelDAO.insert(lstMetaWordTagLabels, c);
                }

                //Inserindo capa
                if (text.getImagePath() != null && !text.getImagePath().isEmpty()) {
                    TextImages textImage = new TextImages();
                    textImage.setIdText(text.getId());
                    textImage.setFileName(text.getImagePath());
                    textImage.setText(text);
                    TextImagesDAO.insert(textImage);
                }

                //Inserindo áudio
                if (text.getAudioPath() != null && !text.getAudioPath().isEmpty()) {
                    TextAudio textAudio = new TextAudio();
                    textAudio.setIdText(text.getId());
                    textAudio.setFileName(text.getAudioPath());
                    textAudio.setText(text);
                    TextAudioDAO.insert(textAudio);
                }
            } catch (DAOException | IOException | TokenizerException | SQLException e) {
                TextDAO.delete(text, c);
                throw new RuntimeException("[" + text.getTitle() + "] Erro em TextFacade.insertText(). " + e.getMessage());
            } catch (TextTagsFacadeException ex) {
                TextDAO.delete(text, c);
                throw ex;
            }
            try {
                c.commit();
            } catch (SQLException ex) {
                throw new TextFacadeException(ex);
            }

        } catch (Exception ex) {
            Logger.error(TextFacade.class, ex);
            throw new RuntimeException("Erro no primeiro try TextFacade.insert(). " + ex.getMessage());
        }
        return textMetadataProblems;
    }

    private static boolean isMetaWordsTag(WordTagLabel wordTagLabel, Text text) {
        searchWordTagObj(wordTagLabel, text.getMetaWordTagsList());  // caso encontre a WordTags, preenche wordTagLabel.wordTag com sua correspondente.
        return (wordTagLabel.getWordTag().getId() > 0);          // caso contrário retorna false, com o objeto wordTagLabel.wordTag == null.
    }

    public static Words searchWord(int id, List<Words> words) {
        for (Words w : words) {
            if (w.getId() == id) {
                return w;
            }
        }
        return null;
    }

    private static boolean isWordsTag(WordTagLabel wordTagLabel, Text text) {
        searchWordTagObj(wordTagLabel, text.getWordTags());  // caso encontre a WordTags, preenche wordTagLabel.wordTag com sua correspondente.
        return (wordTagLabel.getWordTag().getId() > 0);          // caso contrário retorna false, com o objeto wordTagLabel.wordTag == null.
    }

    private static boolean isStyleTag(WordTagLabel wordTagLabel) {
        if (wordTagLabel.getWordTag() != null && wordTagLabel.getWordTag().getXces() != null) {
            return wordTagLabel.getWordTag().getXces().equals("xces.style");
        } else {
            return false;
        }
    }

    private static boolean isSpecialTag(WordTagLabel wordTagLabel) {
        return wordTagLabel.getWordTag().getTag().matches(Constant.TAGS.SPECIAL);
    }

    private static boolean isMetaWordAttributeTag(WordTagLabel wordTagLabel) {
        return wordTagLabel.getWordTag().getTag().matches(Constant.TAGS.METATEXT_ATTRIBUTE_TAG);
    }

    //  Se for uma tag com atributos, a função a seguir preenche wordTagLabel.wordTag.attributes 
    //  com a lista de atributos e seus valores
    private static void fillMetaWordAttributeTagValues(WordTagLabel wordTagLabel, Text text) {
        List<WordTagAttribute> lstAttributes = new ArrayList<>();
        Pattern p = Pattern.compile(Constant.TAGS.METATEXT_ATTRIBUTE_TAG);
        Matcher m = p.matcher(wordTagLabel.getWordTag().getTag());
        if (m.find()) {
            attributeLoop:
            for (int i = 1; i < m.groupCount(); i++) {
                switch (i) {
                    case 1:
                        wordTagLabel.setWordTag(new WordTags(m.group(1))); //m.group(0) contem o identificador da etiqueta
                        if (!isMetaWordsTag(wordTagLabel, text)) // no caso de não ser uma metaWordTag, sai do lop e não pega os atributos
                        {
                            break attributeLoop;
                        }
                        break;
                    default:
                        if ((i % 2) == 0) //i par
                        {
                            if (m.group(i).length() > 0) {
                                lstAttributes.add(new WordTagAttribute(m.group(i), m.group(i + 1)));    //m.group(i_IMPAR) contem um atributo da anotação, m.group(i_PAR) contem o valor do atributo na anotação
                            }                                                                           //Exemplo ATRIBUTO=VALOR: place=bottom
                        }
                        break;
                }
            }
        }
        wordTagLabel.setAttributes(lstAttributes);
    }

    //Se for uma tag especial, então é uma tag com propriedades. A função a seguir retorna um objeto WordTags contendo a lista de propriedades
    private static void fillSpecialTagAttributeValues(WordTagLabel wordTagLabel) {
        List<WordTagAttribute> lstAttributes = new ArrayList<>();
        Pattern p = Pattern.compile(Constant.TAGS.SPECIAL);
        Matcher m = p.matcher(wordTagLabel.getWordTag().getTag());
        if (m.find()) {
            WordTags wt = new WordTags(m.group(1));
            lstAttributes.add(new WordTagAttribute(m.group(1), m.group(2)));    //m.group(1) contem a tag especial, m.group(2) contem sua contagem
            wordTagLabel.setWordTag(wt);
        }
        wordTagLabel.setAttributes(lstAttributes);
    }

    private static boolean existUnclosedLabelInStack(WordTags wordTag, Stack<WordTagLabel> stkTag) {
        for (WordTagLabel wtl : stkTag) {
            if (wtl.getWordTag().getId() == wordTag.getId()) {
                if (wtl.getIdWordClose() <= 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static File[] returnFilesFromZip(Text text) throws IOException, URISyntaxException {
        FileZip fileZip = new FileZip();
        prop = Config.getPathProperties();

        fileZip.setPathZip(text.getPath()); //Path to the .zip
        fileZip.setDestinationDirectory(prop.getProperty("path.tmp") + text.getPath().replace(".zip", "").substring(text.getPath().lastIndexOf("/") + 1, text.getPath().lastIndexOf("."))); //Path where the files inside the .zip will be extract 
        fileZip.unzip(); //Extract the .zip
        //  this.filesNotExtracted = fileZip.getFilesNotExtracted();
        FileManipulation.removeFile(fileZip.getPathZip());

        return new File(fileZip.getDestinationDirectory()).listFiles();
    }

    /*public static void update(Text text, TextDAO textDAO) {
     try {
     //Update the sentences and words in the database
     TextFacade.updateSentencesAndWords(text);

     TextTagsFacade.updateTagValues(text.getLstTextsMetadataValues()); //Update metadata
     TextDAO.update(text); //Update the text

     } catch (DAOException | TextTagsFacadeException ex) {
     //throw new TextFacadeException(ex);
     throw new RuntimeException("Erro em TextFacade.update." + ex.getMessage());
     }
     }*/
    public static String uploadFile(Text text, HttpServletRequest request, HttpServletResponse response) {
        try {
            FileManipulation fileUpload = new FileManipulation();
            fileUpload.uploadFile(request, response); //Uploading the file (.zip, .txt, .doc, .docx or .pdf)

            text.setPath(fileUpload.getVariableMap().get("path"));
            text.setLanguage(fileUpload.getVariableMap().get("language"));

            return fileUpload.getVariableMap().get("error_message");
        } catch (Exception ex) {
            throw new RuntimeException("Erro em TextFacede.uploadFile." + ex.getMessage());
        }
    }

    private static void fillMetaWordTagByLabel(WordTagLabel wordTagLabel, Text text) {
        for (WordTags wordTagCompare : text.getMetaWordTags()) {
            if (wordTagLabel.getWordTag().getTag().equals(wordTagCompare.getTag())) {
                wordTagLabel.setWordTag(wordTagCompare);
            }
        }
    }

    private static void fillWordTagByLabel(WordTagLabel wordTagLabel, Text text) {
        for (WordTags wordTagCompare : text.getWordTags()) {
            if (wordTagLabel.getWordTag().getTag().equals(wordTagCompare.getTag())) {
                wordTagLabel.setWordTag(wordTagCompare);
            }
        }
    }

    public List<String> getFilesNotExtracted() {
        return filesNotExtracted;
    }

    public void setFilesNotExtracted(List<String> filesNotExtracted) {
        this.filesNotExtracted = filesNotExtracted;
    }

    // -------------------------------------------------------------------------
    // SentencesAndWordsService
    // -------------------------------------------------------------------------
    public static void deleteSentencesAndWords(Text text) throws TextFacadeException {
        try {
            //Delete words from text
            WordDAO.deleteByIdText(text);

            //Delete sentences from text
            SentenceDAO.deleteByIdText(text);

            //Delete sentences from text
            MetaWordsDAO.deleteByIdText(text);
        } catch (DAOException ex) {
            throw TextFacadeException.deleteSentencesAndWords(text.getTitle());
        }
    }

    public static void updateSentencesAndWords(Text text) {
        /*deleteSentencesAndWords(text);
         insertSentencesAndWords(text);*/
    }

    private static String cleanTag(String tag) {
        return Normalizer.normalize(tag, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").replaceAll("[^A-zÀ-ú0-9\\=\\,]", " ").trim().toUpperCase();
    }

    public static void searchWordTagObj(WordTagLabel wordTagLabel, List<WordTags> lstTags) {

        if (!lstTags.isEmpty()) {
            int indexFound = binaryWordTagSearch(lstTags, 0, lstTags.size() - 1, cleanTag(wordTagLabel.getWordTag().getTag()).trim());
            WordTags wt = new WordTags();
            if (indexFound >= 0) {
                wt.setId(lstTags.get(indexFound).getId());
                wt.setIdCorpus(lstTags.get(indexFound).getIdCorpus());
                wt.setXces(lstTags.get(indexFound).getXces());
            } else {
                wt.setId(-1); // não encontrado
            }
            wt.setTag(wordTagLabel.getWordTag().getTag());
            wordTagLabel.setWordTag(wt);
            wordTagLabel.setIdWordTag(wt.getId());
        } else {
            WordTags wt2 = new WordTags();
            wt2.setId(-1); // não encontrado
            wt2.setTag(wordTagLabel.getWordTag().getTag());
            wordTagLabel.setWordTag(wt2);
            wordTagLabel.setIdWordTag(wt2.getId());
        }
    }

    public static int binaryWordTagSearch(List<WordTags> lstTags, int ini, int fim, String chave) {
        int media = (fim + ini) / 2;
        String valorMeio = lstTags.get(media).getTag().toUpperCase();

        if (ini > fim) {
            return -1;
        } else if (valorMeio.equals(chave)) {
            return media;
        } else if (valorMeio.compareTo(chave) < 0) {
            return binaryWordTagSearch(lstTags, media + 1, fim, chave);
        } else {
            return binaryWordTagSearch(lstTags, ini, media - 1, chave);
        }
    }

    private void validateLabels(List<WordTagLabel> lstWordTagLabels) {
        for (int i = 0; i < lstWordTagLabels.size(); i++) {
            if (lstWordTagLabels.get(i).getIdWordOpen() <= 0 || lstWordTagLabels.get(i).getIdWordClose() <= 0) {
                lstWordTagLabels.remove(i);
                i--;
            }
        }
    }
}
