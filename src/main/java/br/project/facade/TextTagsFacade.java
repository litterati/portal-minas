package br.project.facade;

import br.library.util.Constant;
import br.library.util.UtilFunctions;
import br.project.dao.TextDAO;
import br.project.dao.TextTagValuesDAO;
import br.project.dao.TextTagsDAO;
import br.project.entity.Text;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.entity.WordTags;
import br.project.entity.WordTagLabel;
import br.project.exception.DAOException;
import br.project.exception.TextTagsFacadeException;
import java.sql.Connection;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TextTagsFacade
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextTagsFacade {

    public static boolean existTag(TextTags textTags) throws TextTagsFacadeException {
        try {
            if (TextTagsDAO.existTag(textTags)) {
                return true;
            }
        } catch (DAOException e) {
            throw new TextTagsFacadeException(e);
        }
        return false;
    }

    public static boolean existMetadataValue(TextTags textTags) throws TextTagsFacadeException {

        TextTagValues textTagValues = new TextTagValues();
        textTagValues.setIdTextTag(textTags.getId());

        try {
            return TextTagValuesDAO.existValue(textTagValues);
        } catch (DAOException ex) {
            throw new TextTagsFacadeException(ex);
        }
    }

    public static void insertEmptyTagValues(int idCorpus, TextTags textTag) throws TextTagsFacadeException {
        try {
            List<Text> lstText = TextDAO.selectAllByIdCorpus(idCorpus, TextDAO.TITLE);

            TextTagValues textTagValues = new TextTagValues();
            textTagValues.setTextTag(textTag);
            for (Text text : lstText) {
                textTagValues.setIdText(text.getId());
                textTagValues.setValue("");
                textTagValues.setIdTextTag(textTag.getId());
                TextTagValuesDAO.insert(textTagValues);
            }
        } catch (DAOException ex) {
            throw new TextTagsFacadeException(ex);
        }
    }

    /*public static void fillTextTagValues(Text text) throws TextTagsFacadeException {
     try {
     text.setTextTags(TextTagsDAO.selectByIdCorpus(text.getIdCorpus()));
     text.setTextTagsValues(TextTagValuesDAO.selectTextTagsWithValue(text.getId()));
     } catch (DAOException ex) {
     throw new TextTagsFacadeException(ex);
     }
     }*/
    public static void fillTextTagsWithoutValue(Text text) throws TextTagsFacadeException, DAOException {
        text.setTextTags(TextTagsDAO.selectByIdCorpus(text.getIdCorpus()));
        for (TextTags textTag : text.getTextTags()) {
            TextTagValues textTagValues = new TextTagValues();
            textTagValues.setIdText(text.getId());
            textTagValues.setValue("");
            textTagValues.setIdTextTag(textTag.getId());
            textTagValues.setTextTag(textTag);
            text.addTextTagsValues(textTagValues);
        }
    }

    public List<String> insertHeader(Text text, WordTagLabel headerTagLabel, Connection c) throws TextTagsFacadeException {
        List<String> headerMetadataErrors = new ArrayList<>();
        List<TextTagValues> lstTextMetadata = new ArrayList<>();
        List<TextTags> textTagsRequired = getTextTagsRequired(text);

        TextTagValues textTagValues;
        TextTags textTag = new TextTags();
        textTag.setIdCorpus(text.getIdCorpus());

        try {
            String textHeaderString = text.getTextString().substring(headerTagLabel.getIdWordOpen(), headerTagLabel.getIdWordClose());

            //Verify if the metadata fields are defined to the corpus
            if (!TextTagsDAO.existTagCorpus(textTag)) {
                headerMetadataErrors.add("<p> Texto <b>" + text.getFile_name() + "</b>: Os metadados contidos no texto "
                        + " não foram salvos, pois eles precisam ser cadastrados na tela de gerenciamento  de metadados</p>");
                TextTagValuesDAO.insert(text.getTextTagsValues()); //Insert empty metadata in the database
                return headerMetadataErrors;
            }

            String[] tagsAray = UtilFunctions.splitParagraph(textHeaderString);

            for (String data : tagsAray) {
                data = data.trim();
                if (data.contains(":")) {
                    textTag = new TextTags();
                    textTag.setTag(data.substring(0, data.indexOf(":")).trim());
                    textTag = searchTextTagObj(textTag, text);
                    if (textTag.getId() > 0) {
                        textTagValues = new TextTagValues();
                        textTagValues.setIdText(text.getId());
                        textTagValues.setIdTextTag(textTag.getId());
                        textTagValues.setValue(data.substring(data.indexOf(":") + 1).trim()); // setValue com tudo que aparecer depois (+1) da primeira ocorrência de ":" 
                        textTagValues.setTextTag(textTag);

//                        if (!existMetadataValueInFieldTypeValues(textTagValues)) {
//                            //Metadata value wasn't find in the filds_type_values
//                            headerMetadataErrors.add("<p> Texto <b>" + text.getTitle() + "</b>: Valor \"" + textTagValues.getValue() + "\" do metadado \""
//                                    + textTagValues.getTextTag().getTag() + "\" não encontrado na lista de tipos de valor disponíveis para o "
//                                    + "corpus " + text.getNameCorpus() + ".</p>");
//                        } else {
                        lstTextMetadata.add(textTagValues);
//                        }
                        if (textTag.isIsRequired()) {
                            textTagsRequired.remove(textTag);
                        }
                    } else {
                        //Metadata wasn't find in the table
                        headerMetadataErrors.add("<p> Texto <b>" + text.getTitle() + "</b>:  Etiqueta \"" + data.split(":")[0] + "\" não encontrada na base de dados para o corpus " + text.getIdCorpus() + ".</p>");
                    }
                } else {
                    if (!data.trim().isEmpty()) {
                        //Problems to insert metadata
                        headerMetadataErrors.add("<p> Texto <b>" + text.getTitle() + "</b>: Problemas para leitura de seguinte linha do cabeçalho: " + data + "</p>");
                    }
                }
            }

            if (!textTagsRequired.isEmpty()) {
                for (TextTags ttv : textTagsRequired) {
                    headerMetadataErrors.add("<p> Texto <b>" + text.getTitle() + "</b>: Informação sobre '" + ttv.getTag() + "' não encontrada no cabeçalho.</p>");
                }
            }

            TextTagValuesDAO.insert(lstTextMetadata, c);
        } catch (DAOException ex) {
            throw new TextTagsFacadeException(ex);
        }
        return headerMetadataErrors;
    }

    public List<String> insertHeaders(Text text, Connection c) throws TextTagsFacadeException {

        List<String> headerMetadataErrors = new ArrayList<>();

        String textString = text.getTextString().toString();

        WordTags headerTag;
        WordTagLabel headerTagLabel = new WordTagLabel();
        Stack<WordTagLabel> stkHeaders = new Stack<>();

        Pattern pattern = Pattern.compile(Constant.TAGS.TEXTHEADER);
        Matcher matcher = pattern.matcher(textString);
        while (matcher.find()) {
            headerTag = new WordTags(textString.substring(matcher.start(), matcher.end()));
            if (!headerTag.getTag().startsWith("</")) {
                headerTagLabel.setWordTag(headerTag);
                headerTagLabel.setIdText(text.getId());
                headerTagLabel.setIdWordOpen(matcher.end());
            } else {
                headerTagLabel.setIdWordClose(matcher.start());
                stkHeaders.push(headerTagLabel);
            }
        }

        for (WordTagLabel htl : stkHeaders) {
            if (htl.getIdWordClose() < 0) {
                headerMetadataErrors.add("<p>Erro no texto <b>" + text.getFile_name() + "</b>: Os dados sobre o texto não estão anotados corretamente. Verifique se há a anotação delimitando o fim do cabeçalho.</p>");
            } else if (htl.getIdWordOpen() < 0) {
                headerMetadataErrors.add("<p>Erro no texto <b>" + text.getFile_name() + "</b>: Os dados sobre o texto não estão anotados corretamente. Verifique se há a anotação delimitando o início do cabeçalho.</p>");
            } else {
                headerMetadataErrors.addAll(insertHeader(text, htl, c));
            }
        }

        while (!stkHeaders.empty()) {
            textString = removeHeader(textString, stkHeaders.peek());
            stkHeaders.pop();
        }

        text.setTextString(textString);
        return headerMetadataErrors;
    }

    public void insertTagValuesManual(Text text) throws TextTagsFacadeException {
        TextTagValuesDAO textTagValuesDAO = new TextTagValuesDAO();

        for (TextTagValues textTagValues : text.getTextTagsValues()) {
            textTagValues.setIdText(text.getId());
        }

        try {
            TextTagValuesDAO.insert(text.getTextTagsValues());
        } catch (DAOException ex) {
            throw new TextTagsFacadeException(ex);
        }
    }

    private static String removeHeader(String textString, WordTagLabel headerTag) {
        if (headerTag.getIdWordOpen() > 0 && headerTag.getIdWordClose() > 0) {
            return textString.replace(textString.substring(headerTag.getIdWordOpen(), headerTag.getIdWordClose()), "").replaceAll(headerTag.getWordTag().getTag() + "|" + headerTag.getWordTag().getTag().replace("<", "</"), "");
        } else {
            return textString;
        }
    }

    private List<TextTags> getTextTagsRequired(Text text) {
        List<TextTags> textTagsRequired = new ArrayList<>();
        for (TextTags tt : text.getTextTags()) {
            if (tt.isIsRequired()) {
                textTagsRequired.add(tt);
            }
        }
        return textTagsRequired;
    }

    public static TextTags searchTextTagObj(TextTags textTag, Text text) {
        for (int i = 0; i < text.getTextTags().size(); i++) {
            if (cleanTag(text.getTextTags().get(i).getTag()).equals(cleanTag(textTag.getTag()))) {
                return text.getTextTags().get(i);
            }
        }
        return textTag;
    }

    private static String cleanTag(String tag) {
        return Normalizer.normalize(tag, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").trim().toUpperCase();
    }
}
