package br.project.facade;

import br.project.dao.WordTagLabelDAO;
import br.project.entity.WordTags;
import br.project.entity.WordTagLabel;
import br.project.exception.DAOException;
import br.project.exception.WordTagsFacadeException;

/**
 * WordTagsFacade
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class WordTagsFacade {

    public static boolean existWordTagLabel(WordTags wordTag) throws WordTagsFacadeException {
        WordTagLabel wordTagLabel = new WordTagLabel(wordTag);
        try {
            return WordTagLabelDAO.existValue(wordTagLabel);
        } catch (DAOException ex) {
            throw new WordTagsFacadeException(ex);
        }
    }
}
