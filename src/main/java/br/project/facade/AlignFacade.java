/*
 * Em padrões de projeto de software, um façade ("fachada") é um objeto que 
 * disponibiliza uma interface simplificada para uma das funcionalidades de uma 
 * API, por exemplo. Um façade pode:
 * - tornar uma biblioteca de software mais fácil de entender e usar;
 * - tornar o código que utiliza esta biblioteca mais fácil de entender;
 * - reduzir as dependências em relação às características internas de uma 
 *   biblioteca, trazendo flexibilidade no desenvolvimento do sistema;
 * - envolver uma interface mal desenhada, com uma interface melhor definida.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Fa%C3%A7ade)
 */
package br.project.facade;

import br.library.util.Config;
import br.library.util.align.ReferenceAlign;
import br.library.util.align.ReferenceWord;
import br.project.dao.SentenceAlignDAO;
import br.project.dao.WordDAO;
import br.project.dao.WordsAlignDAO;
import br.project.entity.SentenceAlign;
import br.project.entity.Text;
import br.project.entity.Words;
import br.project.entity.WordsAlign;
import br.project.exception.AlignFacadeException;
import br.project.exception.DAOException;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * AlignFacade
 *
 *
 * @author Thiago Vieira
 * @since 12/08/2014
 */
public class AlignFacade {

    public static final String INTERVANTION_CONVERT_TO_LOWCASE = "-m";
    public static final String INTERVANTION_SPLIT_TOKENS = "-c";
    public static final String INTERVANTION_ALL = "-m -c";
    public static final String INTERVANTION_NONE = "";

    public static final String MODE_SOURCE_TO_TARGET = "fa";
    public static final String MODE_TARGET_TO_SOURCE = "af";
    public static final String MODE_UNION = "u";
    public static final String MODE_INTERSECTION = "i";

    public static final int DICTIONARY_PT_EN = 1;
    public static final int DICTIONARY_EN_PT = 2;
    public static final int DICTIONARY_PT_ES = 3;
    public static final int DICTIONARY_ES_PT = 4;

    public static final String LANGUAGE_PT = "pt";
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_ES = "es";

    public static final String LANGUAGE_PORTUGUESE = "PORTUGUESE";
    public static final String LANGUAGE_ENGLISH = "ENGLISH";
    public static final String LANGUAGE_SPANISH = "SPANISH";

    private AlignFacade() {
        // no instances allow
    }

    public static String alignment(Text source, Text target) throws AlignFacadeException {

        String tmpPath = "/tmp/";
        try {
            tmpPath = Config.getPathProperties().getProperty("path.tmp");
        } catch (IOException | URISyntaxException ex) {
            throw new AlignFacadeException(ex);
        }

        //remover os arquivos que existem
        AlignFacade.deleteFiles(new String[]{
            tmpPath + source.getId() + ".ref",
            tmpPath + target.getId() + ".ref",
            tmpPath + source.getId() + ".txt",
            tmpPath + target.getId() + ".txt",
            tmpPath + source.getId() + ".align",
            tmpPath + target.getId() + ".align"
        });

        //exportar textos em arquivos de refêrencia
        AlignFacade.textToReference(
                tmpPath + source.getId() + ".ref",
                source.getId());
        AlignFacade.textToReference(
                tmpPath + target.getId() + ".ref",
                target.getId());

        //exportar textos em arquivos
        AlignFacade.textToFile(
                tmpPath + source.getId() + ".txt",
                source.getId());
        AlignFacade.textToFile(
                tmpPath + target.getId() + ".txt",
                target.getId());

        //alinhar por palavra (GIZA)
        String response = AlignFacade.wordAlign(
                tmpPath + source.getId() + ".txt",
                tmpPath + target.getId() + ".txt",
                tmpPath + source.getId() + ".align",
                tmpPath + target.getId() + ".align",
                source.getLanguage(),
                target.getLanguage());

        //importar os alinhamentos para o BD
        AlignFacade.saveAlign(
                tmpPath + source.getId() + ".ref",
                tmpPath + target.getId() + ".ref",
                tmpPath + source.getId() + ".align",
                tmpPath + target.getId() + ".align");

        return response;
    }

    private static boolean textToFile(String text_file_path_out, int id) {
        try {
            FileOutputStream fos = new FileOutputStream(new File(text_file_path_out), true);// to write in file

            List<Words> lws = WordDAO.selectAllByIdText(id);
            for (int i = 0; i < lws.size(); i++) {
                Words word = lws.get(i);
                if (word.getWord().equals("_NEWLINE_")) {
                    // quebra de linha
                    fos.write(("\n").getBytes());// write file
                } else {
                    // palavra
                    word.setWord(word.getWord().replaceAll("&", "ampersand"));
                    word.setWord(word.getWord().replaceAll("<", "chevron"));
                    word.setWord(word.getWord().replaceAll(">", "chevron"));
                    fos.write((word.getWord() + " ").getBytes());// write file
                }
            }

            fos.close();

            return true;
        } catch (FileNotFoundException ex) {
            //
        } catch (IOException ex) {
            //
        }
        return false;
    }

    private static boolean textToReference(String text_file_path_out, int id) {
        try {
            FileOutputStream fos = new FileOutputStream(new File(text_file_path_out), true);// to write in file

            List<Words> lws = WordDAO.selectAllByIdText(id);
            for (Words word : lws) {
                if (word.getWord().equals("_NEWLINE_")) {
                    // quebra de linha
                } else {
                    word.setWord(word.getWord().replaceAll("&", "ampersand"));
                    word.setWord(word.getWord().replaceAll("<", "chevron"));
                    word.setWord(word.getWord().replaceAll(">", "chevron"));
                    // id_word|id_sentence|position|word
                    fos.write((word.getId() + "|" + word.getIdSentence() + "|" + word.getPosition() + "|" + word.getWord() + "\n").getBytes());// write file
                }
            }

            fos.close();

            return true;
        } catch (FileNotFoundException ex) {
            //
        } catch (IOException ex) {
            //
        }
        return false;
    }

    /**
     * Word Align
     *
     * O Alinhador de Palavras processa um par de textos paralelos para
     * determinar o alinhamento entre as palavras fonte e as palavras alvo, ou
     * seja, determinar quais palavras são a tradução umas das outras.
     *
     * @param source_file_path_in
     * @param target_file_path_in
     * @param source_file_path_out
     * @param target_file_path_out
     * @param source_language
     * @param source_dictionary
     * @param intervantion
     * @param mode
     * @param target_language
     * @param target_dictionary
     *
     * @return
     */
    private static String wordAlign(String source_file_path_in, String target_file_path_in, String source_file_path_out, String target_file_path_out, String source_language, String target_language) {

        switch (source_language) {
            case AlignFacade.LANGUAGE_ENGLISH:
                source_language = AlignFacade.LANGUAGE_EN;
                break;
            case AlignFacade.LANGUAGE_SPANISH:
                source_language = AlignFacade.LANGUAGE_ES;
                break;
            default://LANGUAGE_PORTUGUESE
                source_language = AlignFacade.LANGUAGE_PT;
        }

        switch (target_language) {
            case AlignFacade.LANGUAGE_ENGLISH:
                target_language = AlignFacade.LANGUAGE_EN;
                break;
            case AlignFacade.LANGUAGE_SPANISH:
                target_language = AlignFacade.LANGUAGE_ES;
                break;
            default://LANGUAGE_PORTUGUESE
                target_language = AlignFacade.LANGUAGE_PT;
        }

        String intervantion = INTERVANTION_ALL;
        String mode = MODE_SOURCE_TO_TARGET;
        int source_dic = 0;
        int target_dic = 0;

//        //MODE_SOURCE_TO_TARGET
//        if (source_language.equals(LANGUAGE_PT) && target_language.equals(LANGUAGE_EN)) {
//            source_dic = DICTIONARY_PT_EN;
//            //target_dic = DICTIONARY_EN_PT;
//        } else if (source_language.equals(LANGUAGE_EN) && target_language.equals(LANGUAGE_PT)) {
//            source_dic = DICTIONARY_EN_PT;
//            //target_dic = DICTIONARY_PT_EN;
//        } else if (source_language.equals(LANGUAGE_PT) && target_language.equals(LANGUAGE_ES)) {
//            source_dic = DICTIONARY_PT_ES;
//            //target_dic = DICTIONARY_ES_PT;
//        } else if (source_language.equals(LANGUAGE_ES) && target_language.equals(LANGUAGE_PT)) {
//            source_dic = DICTIONARY_ES_PT;
//            //target_dic = DICTIONARY_PT_ES;
//        }
        String source_dictionary;
        switch (source_dic) {
            case AlignFacade.DICTIONARY_PT_EN:
                //dicionario_pt-en_v2_159814_freqmin_1.txt
                source_dictionary = "-j 1";
                break;
            case AlignFacade.DICTIONARY_EN_PT:
                //dicionario_en-pt_v2_158037_freqmin_1.txt
                source_dictionary = "-j 2";
                break;
            case AlignFacade.DICTIONARY_PT_ES:
                //dicionario_pt-es_v2_151024_freqmin_1.txt
                source_dictionary = "-j 3";
                break;
            case AlignFacade.DICTIONARY_ES_PT:
                //dicionario_es-pt_v2_141833_freqmin_1.txt
                source_dictionary = "-j 4";
                break;
            default://NONE
                source_dictionary = "";
        }

        String target_dictionary;
        switch (target_dic) {
            case AlignFacade.DICTIONARY_PT_EN:
                //dicionario_pt-en_v2_159814_freqmin_1.txt
                target_dictionary = "-k 1";
                break;
            case AlignFacade.DICTIONARY_EN_PT:
                //dicionario_en-pt_v2_158037_freqmin_1.txt
                target_dictionary = "-k 2";
                break;
            case AlignFacade.DICTIONARY_PT_ES:
                //dicionario_pt-es_v2_151024_freqmin_1.txt
                target_dictionary = "-k 3";
                break;
            case AlignFacade.DICTIONARY_ES_PT:
                //dicionario_es-pt_v2_141833_freqmin_1.txt
                target_dictionary = "-k 4";
                break;
            default://NONE
                target_dictionary = "";
        }

        StringBuilder response = new StringBuilder();
        try {
            ////./processa.sh -i <numero_tarefa> -p 4 -l <par_linguas> -f <entrada_fonte> -s <saida_fonte> -a <entrada_alvo> -t <saida_alvo> -d <sentido> [-j <dicionario_fonte-alvo> [-k <dicionario_alvo-fonte>]]
            Process process = Runtime.getRuntime().exec(
                    "timeout 3h " 
                    + "/bin/bash "
                    + Config.getPathProperties().getProperty("path.align.tool") + "processa.sh "
                    + "-i 1 " // task number is unimportant here
                    + "-p 4 " // 4 to word align
                    + "-l " + source_language + "-" + target_language + " " // language pair
                    + "-f " + source_file_path_in + " " // source in file
                    + "-a " + target_file_path_in + " " // target in file
                    + "-s " + source_file_path_out + " " // source out file
                    + "-t " + target_file_path_out + " " // target out file
                    + "-d " + mode + " " // mode
                    + source_dictionary + " " + target_dictionary + " " //dictionaries
                    + intervantion // intervantion
            );
            //output
            System.out.print("Output of running is: ");
            try (InputStream is = process.getInputStream();
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader br = new BufferedReader(isr)) {
                String line;
                while ((line = br.readLine()) != null) {
                    response.append(line).append("\n");
                    System.out.println(line);
                }
            } catch (IOException ex) {
                response.append(ex.getMessage()).append("\n");
                System.out.println(ex.getMessage());
            }
            //error
            System.out.print("Error of running is: ");
            try (InputStream is = process.getErrorStream();
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader br = new BufferedReader(isr)) {
                String line;
                while ((line = br.readLine()) != null) {
                    response.append(line).append("\n");
                    System.out.println(line);
                }
            } catch (IOException ex) {
                response.append(ex.getMessage()).append("\n");
                System.out.println(ex.getMessage());
            }
            System.out.print("Output/Error END");
            //wait
            process.waitFor();
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            response.append(ex.getMessage()).append("\n");
            System.out.println(ex.getMessage());
        }
        return response.toString();
    }

    private static boolean saveAlign(String source_file_path_ref, String target_file_path_ref, String source_file_path_align, String target_file_path_align) {
        try {
            //source
            //read source_file_path_ref to a list
            ArrayList<String[]> sourceReference = AlignFacade.readReferenceFile(source_file_path_ref);
            //read align files
            TreeMap<String, ReferenceAlign> sral = AlignFacade.getReferenceAlignList(sourceReference, source_file_path_align);
            //to garbage collector
            sourceReference = null;
            //call garbage collector
            System.gc();

            //target
            //read target_file_path_ref to a list
            ArrayList<String[]> targetReference = AlignFacade.readReferenceFile(target_file_path_ref);
            //read align files
            TreeMap<String, ReferenceAlign> tral = AlignFacade.getReferenceAlignList(targetReference, target_file_path_align);
            //to garbage collector
            targetReference = null;
            //call garbage collector
            System.gc();

            //compare the source_file_path_ref with source_file_path_align
            ArrayList<WordsAlign> wordsAligns = AlignFacade.getWordsAligns(sral, tral);
            ArrayList<SentenceAlign> sentencesAligns = AlignFacade.getSentencesAligns(sral, tral);

            //to garbage collector
            sral = null;
            tral = null;
            System.gc();//call garbage collector

            //add to DB
            WordsAlignDAO.insert(wordsAligns);

            SentenceAlignDAO.insert(sentencesAligns);

            return true;
        } catch (DAOException e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    private static ArrayList<String[]> readReferenceFile(String file_path) {
        ArrayList<String[]> reference = new ArrayList<>();
        try {
            FileInputStream fstream = new FileInputStream(file_path);
            try (DataInputStream in = new DataInputStream(fstream)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String strLine;

                while ((strLine = br.readLine()) != null) {
                    reference.add(strLine.split("\\|"));
                }
            } catch (IOException e) {
                throw e;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return reference;
    }

    private static TreeMap<String, ReferenceAlign> getReferenceAlignList(ArrayList<String[]> reference, String file_path_align) {
        TreeMap<String, ReferenceAlign> tree = new TreeMap<>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            //read source_file_path_align to a xml object
            Document alignFile = dBuilder.parse(new File(file_path_align));
            alignFile.getDocumentElement().normalize();

            NodeList words = alignFile.getElementsByTagName("w");
            for (int i = 0, j = 0; i < words.getLength(); i++) {
                Node nodeWord = words.item(i);
                if (nodeWord.getNodeType() == Node.ELEMENT_NODE) {
                    Element elementWord = (Element) nodeWord;

                    ReferenceAlign currentAlign = new ReferenceAlign(
                            elementWord.getAttribute("id"),// local id
                            elementWord.getAttribute("crr"),// align correspondent
                            elementWord.getAttribute("atype"),// align type
                            elementWord.getChildNodes().item(0).getNodeValue()// local superficial form
                    );

                    if (j < reference.size()) {
                        String[] aux = reference.get(j);
                        ReferenceWord currentWord = new ReferenceWord(
                                aux[0], // original id
                                aux[1], // original id sentence
                                aux[2], // original position
                                aux[3] // original superficial form
                        );

                        //System.out.println(currentAlign.getSuperficialForm() + " <> " + currentWord.getSuperficialForm());
                        if (currentWord.getSuperficialForm().contains(currentAlign.getSuperficialForm())) {
                            // if contains or equals
                            currentAlign.setWord(currentWord);
                            tree.put(currentAlign.getId(), currentAlign);
                            j++;
                        } else {
                            // if different
                            if (words.item(i + 1) != null) { // if has next
                                String nextAlign = ((Element) words.item(i + 1)).getChildNodes().item(0).getNodeValue();
                                String nextWord = reference.get(j + 1)[3];

                                if (nextWord.contains(nextAlign)) {
                                    // currentReference == currentAlign
                                    currentAlign.setWord(currentWord);
                                    tree.put(currentAlign.getId(), currentAlign);
                                    j++;
                                } else if (currentWord.getSuperficialForm().contains(nextAlign)) {
                                    nodeWord.getParentNode().removeChild(nodeWord);
                                    i--;
                                }
                            }
                        }
                    }
                }
            }
            //to garbage collector
            dbFactory = null;
            dBuilder = null;
            alignFile = null;
        } catch (SAXException | ParserConfigurationException | IOException e) {
            System.err.println(e.getMessage());
        }
        return tree;
    }

    private static ArrayList<WordsAlign> getWordsAligns(TreeMap<String, ReferenceAlign> sral, TreeMap<String, ReferenceAlign> tral) {
        TreeMap<String, WordsAlign> aligns = new TreeMap<>();

        //source to target
        for (ReferenceAlign referenceAlign : sral.values()) {
            //look for the correspondent id
            for (String crr : referenceAlign.getCrr()) {
                if (!crr.isEmpty()) {//if not crr empty
                    ReferenceAlign ra = tral.get(crr);
                    if (ra != null) {
                        WordsAlign wordAlign = new WordsAlign();
                        wordAlign.setIdSource(referenceAlign.getWord().getIntegerId()); // source id
                        wordAlign.setType(referenceAlign.getAtype()); // atype
                        wordAlign.setIdTarget(ra.getWord().getIntegerId()); // target id

                        String key = wordAlign.getIdSource() + "|" + wordAlign.getIdTarget();
                        aligns.put(key, wordAlign);
                    }
                }
            }
        }
        //target to source (maybe not necessary)
        for (ReferenceAlign referenceAlign : tral.values()) {
            //look for the correspondent id
            for (String crr : referenceAlign.getCrr()) {
                if (!crr.isEmpty()) {//if not crr empty
                    ReferenceAlign ra = sral.get(crr);
                    if (ra != null) {
                        WordsAlign wordAlign = new WordsAlign();
                        wordAlign.setIdTarget(referenceAlign.getWord().getIntegerId()); // target id
                        wordAlign.setType(referenceAlign.getAtype()); // atype
                        wordAlign.setIdSource(ra.getWord().getIntegerId()); // source id

                        String key = wordAlign.getIdSource() + "|" + wordAlign.getIdTarget();
                        aligns.put(key, wordAlign);
                    }
                }
            }
        }

        return new ArrayList<>(aligns.values());
    }

    private static ArrayList<SentenceAlign> getSentencesAligns(TreeMap<String, ReferenceAlign> sral, TreeMap<String, ReferenceAlign> tral) {
        TreeMap<String, SentenceAlign> aligns = new TreeMap<>();

        //source to target
        for (ReferenceAlign referenceAlign : sral.values()) {
            //look for the correspondent id
            for (String crr : referenceAlign.getCrr()) {
                if (!crr.isEmpty()) {//if not crr empty
                    ReferenceAlign ra = tral.get(crr);
                    if (ra != null) {
                        SentenceAlign sentenceAlign = new SentenceAlign();
                        sentenceAlign.setIdSource(referenceAlign.getWord().getIntegerIdSentence()); // source id
                        sentenceAlign.setType("none"); // atype
                        sentenceAlign.setIdTarget(ra.getWord().getIntegerIdSentence()); // target id

                        String key = sentenceAlign.getIdSource() + "|" + sentenceAlign.getIdTarget();
                        aligns.put(key, sentenceAlign);
                    }
                }
            }
        }
        //target to source (maybe not necessary)
        for (ReferenceAlign referenceAlign : tral.values()) {
            //look for the correspondent id
            for (String crr : referenceAlign.getCrr()) {
                if (!crr.isEmpty()) {//if not crr empty
                    ReferenceAlign ra = sral.get(crr);
                    if (ra != null) {
                        SentenceAlign sentenceAlign = new SentenceAlign();
                        sentenceAlign.setIdTarget(referenceAlign.getWord().getIntegerIdSentence()); // target id
                        sentenceAlign.setType("none"); // atype
                        sentenceAlign.setIdSource(ra.getWord().getIntegerIdSentence()); // source id

                        String key = sentenceAlign.getIdSource() + "|" + sentenceAlign.getIdTarget();
                        aligns.put(key, sentenceAlign);
                    }
                }
            }
        }

        return new ArrayList<>(aligns.values());
    }

    private static void deleteFiles(String[] files_paths) {
        for (String path : files_paths) {
            File f = new File(path);
            if (f.isFile()) {
                f.delete();
            }
        }
    }

}
