package br.project.task;

import br.library.util.Logger;
import br.project.dao.TaskDAO;
import br.project.entity.Task;
import br.project.entity.User;
import br.project.exception.DAOException;
import java.util.Date;
import java.util.List;

/**
 * TaskManagement
 *
 * @author Thiago Vieira
 * @since 16/09/2014
 */
public final class TaskManagement extends Thread {

    private static TaskManagement instance = null;
    private TaskInterface currentTask = null;
    private final Date dateStarted;//time when task management started
    private boolean continueRun = true;

    private TaskManagement() {
        super("TaskManagement");
        this.dateStarted = new Date(System.currentTimeMillis());
    }

    public static synchronized TaskManagement getInstance() {
        if (TaskManagement.instance == null) {
            TaskManagement.instance = new TaskManagement();
        } else if (!TaskManagement.instance.getState().equals(java.lang.Thread.State.NEW)
                && !TaskManagement.instance.getState().equals(java.lang.Thread.State.RUNNABLE)) {
            TaskManagement.instance = new TaskManagement();
        }
        return TaskManagement.instance;
    }

    public synchronized boolean addTask(TaskInterface task, int type, User user) {
        boolean resp = false;
        //update DB
        try {
            Task entity = new Task();
            entity.setIdUser(user.getId());
            entity.setType(type);
            entity.setProgress(0);
            entity.setStatus(Task.STATUS_WAITING);
            entity.setJavaObject(task);
            //insert
            TaskDAO.insert(entity);
            //update current object
            task.setTaskId(entity.getId());
            resp = true;
        } catch (DAOException ex) {
            Logger.error(this, ex);
        }
        //show log
        Logger.event(this, "Task inserted");
        this.startTaskManagement();
        return resp;
    }

    public synchronized boolean abortTask(TaskInterface task) {
        //update DB
        try {
            if ((this.currentTask != null)
                    && (this.currentTask.getTaskId() == task.getTaskId())) {
                this.currentTask.stop();
            }
            Task entity = TaskDAO.selectById(task.getTaskId());
            entity.setStatus(Task.STATUS_ABORTED);
            entity.setDateFinished(new Date(System.currentTimeMillis()));
            //update
            TaskDAO.update(entity);
            return true;
        } catch (DAOException ex) {
            Logger.error(this, ex);
        }
        //show log
        Logger.event(this, "Task aborted");
        return false;
    }

    public synchronized void startTaskManagement() {
        //to resolve locked problems
        if (this.isAlive() && numberOfProcessRunning() <= 0) {
            //if it is alive but there is no task running
            TaskManagement.instance = new TaskManagement();
        }
        this.continueRun = true;
        //run the thread
        if (!this.isAlive()) {
            this.start();
        }
    }

    public synchronized void stopTaskManagement() {
        this.continueRun = false;
        if (TaskManagement.instance == null) {
            TaskManagement.instance = new TaskManagement();
        } else {
            //abort current task
            if (this.currentTask != null) {
                this.abortTask(this.currentTask);
            }
            //interrupt task management
            try {
                TaskManagement.instance.interrupt();
                //TaskManagement.instance.stop();
                //TaskManagement.instance.finalize();
                TaskManagement.instance = new TaskManagement();
            } catch (Throwable ex) {
                Logger.event(this, ex.getMessage());
            }
        }
    }

    public int queueSize() {
        return TaskDAO.countByStatus(Task.STATUS_WAITING);
    }

    public int numberOfProcessRunning() {
        return TaskDAO.countByStatus(Task.STATUS_RUNNING);
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    @Override
    public void run() {
        List<Task> tasks = TaskDAO.selectQueue();//get tasks in data base
        while (this.continueRun && tasks.size() > 0) {
            Task entity = tasks.get(0);
            entity.getJavaObject().setTaskId(entity.getId());
            this.currentTask = entity.getJavaObject();
            //update DB
            try {
                entity.setProgress(1);
                entity.setStatus(Task.STATUS_RUNNING);
                entity.setDateStarted(new Date(System.currentTimeMillis()));
                //update
                TaskDAO.update(entity);
            } catch (DAOException ex) {
                Logger.error(this, ex);
            }
            //execute task
            String response = this.currentTask.execute();
            //update DB
            try {
                entity.setProgress(100);
                entity.setStatus(Task.STATUS_COMPLETED_SUCCESS);
                entity.setResponse(response);
                entity.setDateFinished(new Date(System.currentTimeMillis()));
                //update
                TaskDAO.update(entity);
            } catch (DAOException ex) {
                Logger.error(this, ex);
            }
            //show log
            Logger.event(this, "Task executed");
            this.currentTask = null;
            if (this.continueRun) {
                tasks = TaskDAO.selectQueue();//get new tasks in data base
            }
        }
        this.continueRun = false;
    }
}
