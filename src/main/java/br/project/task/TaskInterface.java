package br.project.task;

/**
 * TaskInterface
 *
 * @author Thiago Vieira
 * @since 26/09/2014
 */
public interface TaskInterface {

    /**
     * Deafult method which the TaskManagement will call to execute the task.
     *
     * @return Response from the method.
     */
    public String execute();
    
    public void stop();

    /**
     * Method which the TaskManagement will call to ckeck if the task is alive.
     *
     * @return If the task is running.
     */
    public boolean isAlive();

    public void setTaskId(int task_id);

    public int getTaskId();

}
