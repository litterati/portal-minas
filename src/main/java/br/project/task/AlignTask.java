package br.project.task;

import br.library.util.Logger;
import br.project.dao.TextAlignDAO;
import br.project.entity.Text;
import br.project.entity.TextAlign;
import br.project.exception.AlignFacadeException;
import br.project.exception.DAOException;
import br.project.facade.AlignFacade;
import java.io.Serializable;

/**
 * AlignTask
 *
 * @author Thiago Vieira
 * @since 26/09/2014
 */
public final class AlignTask implements TaskInterface, Serializable {

    private final TextAlign text_align;
    private boolean is_alive;
    public int task_id;

    public AlignTask(TextAlign text_align) {
        this.text_align = text_align;
        this.is_alive = false;
    }

    @Override
    public String execute() {
        this.is_alive = true;
        String response;
        try {
            if (TextAlignDAO.selectById(this.text_align.getIdSource(), this.text_align.getIdTarget()) == null){
                TextAlignDAO.insert(this.text_align);
                //texts
                Text source = this.text_align.getSource();
                Text target = this.text_align.getTarget();
                //align
                response = AlignFacade.alignment(source, target);
                //show log
                Logger.event(this, "Text Align");
            } else {
                response = "Esse par de textos já foi alinhado.";
            }
        } catch (AlignFacadeException | DAOException ex) {
            Logger.error(this, ex);
            response = ex.getMessage();
        }
        this.is_alive = false;
        return response;
    }

    @Override
    public boolean isAlive() {
        return this.is_alive;
    }

    @Override
    public void setTaskId(int task_id) {
        this.task_id = task_id;
    }

    @Override
    public int getTaskId() {
        return this.task_id;
    }

    @Override
    public void stop() {
        //
    }

}
