package br.project.task;

import br.library.util.Logger;
import br.project.entity.Text;
import br.project.exception.TextFacadeException;
import br.project.facade.TextFacade;
import java.io.Serializable;
import java.util.List;

/**
 * InsertTextTask
 *
 * @author Thiago Vieira
 * @since 16/09/2014
 */
public final class InsertTextTask implements TaskInterface, Serializable {

    private Text text;
    private boolean is_alive;
    public int task_id;

    public InsertTextTask(Text text) {
        this.text = text;
        this.is_alive = false;
    }

    @Override
    public String execute() {
        this.is_alive = true;
        String response = "";
        try {
            TextFacade textFacade = new TextFacade();
            List<String> textWarnings = textFacade.insertText(this.text);

            Logger.event(this, "Text Inserted");

            for (String error : textWarnings) {
                response = response.concat(error);
            }

        } catch (TextFacadeException ex) {
            Logger.error(this, ex);
            response = ex.getMessage();
        }
        this.is_alive = false;
        return response;
    }

    @Override
    public boolean isAlive() {
        return this.is_alive;
    }

    @Override
    public void setTaskId(int task_id) {
        this.task_id = task_id;
    }

    @Override
    public int getTaskId() {
        return this.task_id;
    }
    
    @Override
    public void stop() {
        //
    }

}
