package br.project.task;

import br.library.util.Logger;
import br.project.dao.TextDAO;
import br.project.entity.Text;
import br.project.exception.DAOException;
import java.io.Serializable;

/**
 * AlignTask
 *
 * @author Thiago Vieira
 * @since 26/09/2014
 */
public final class DeleteTextTask implements TaskInterface, Serializable {

    private final Text text;
    private boolean is_alive;
    public int task_id;

    public DeleteTextTask(Text text) {
        this.text = text;
        this.is_alive = false;
    }

    @Override
    public String execute() {
        this.is_alive = true;
        String response = "";
        try {

            TextDAO dao = new TextDAO();
            dao.delete(this.text);

            Logger.event(this, "Text Deleted");

        } catch (DAOException ex) {
            Logger.error(this, ex);
            response = ex.getMessage();
        }
        this.is_alive = false;
        return response;
    }

    @Override
    public boolean isAlive() {
        return this.is_alive;
    }

    @Override
    public void setTaskId(int task_id) {
        this.task_id = task_id;
    }

    @Override
    public int getTaskId() {
        return this.task_id;
    }
    
    @Override
    public void stop() {
        //
    }

}
