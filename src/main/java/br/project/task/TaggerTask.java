/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.task;

import br.library.util.Logger;
import br.library.util.tagger.MyTagger;
import java.io.Serializable;

/**
 *
 * @author marcel
 */
public class TaggerTask implements TaskInterface, Serializable {

    private boolean is_alive;
    public int task_id;
    public int idCorpus;
    
    
    public TaggerTask(int idCorpus){
        this.idCorpus = idCorpus;
        this.is_alive = false;
    }
    
    @Override
    public String execute() {
        this.is_alive = true;
        MyTagger myTagger = new MyTagger();
        String response = "";
        try {
            myTagger.taggerCorpus(idCorpus);
            Logger.event(this, "Corpus Tagged.");
        } catch (Exception ex) {
            Logger.error(this, ex);
            response = ex.getMessage();
        }
        this.is_alive = false;
        return response;
    }

    @Override
    public void stop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isAlive() {
        return this.is_alive;
    }

    @Override
    public void setTaskId(int task_id) {
        this.task_id = task_id;
    }

    @Override
    public int getTaskId() {
        return this.task_id;
    }
    
}
