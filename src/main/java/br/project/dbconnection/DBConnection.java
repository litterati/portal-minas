/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.dbconnection;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marcel
 */
public class DBConnection {
    public static ComboPooledDataSource cpds = new ComboPooledDataSource();   
    
    public void DBConnection(){
        try {
            cpds.setDriverClass( "org.postgresql.Driver" ); //loads the jdbc driver 
            cpds.setJdbcUrl( "jdbc:postgresql://localhost:5433/projeto" ); 
            cpds.setUser("portalminas"); 
            cpds.setPassword("NcRQ+7nC");
            
        } catch (PropertyVetoException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection getConnection() throws SQLException {
    	return cpds.getConnection();
    }

}
