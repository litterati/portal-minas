package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import static br.project.dao.CorpusTagsDAO.ID_FIELDS_TYPE;
import br.project.entity.CorpusTagValues;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CorpusTagValuesDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class CorpusTagValuesDAO {

    public static final String TABLE_NAME = "corpus_tag_values";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String ID_CORPUS_TAG = "id_corpus_tag";
    public static final String VALUE = "value";

    public static CorpusTagValues load(ResultSet rs) throws SQLException {
        CorpusTagValues ctv = new CorpusTagValues();
        ctv.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ctv.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        ctv.setIdCorpusTag(rs.getInt(TABLE_NAME + "_" + ID_CORPUS_TAG));
        ctv.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        return ctv;
    }

    public static List<CorpusTagValues> loadList(ResultSet rs) throws SQLException {
        List<CorpusTagValues> ctvList = new ArrayList<>();
        while (rs.next()) {
            CorpusTagValues ctv = new CorpusTagValues();
            ctv.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ctv.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            ctv.setIdCorpusTag(rs.getInt(TABLE_NAME + "_" + ID_CORPUS_TAG));
            ctv.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            // add into the list
            ctvList.add(ctv);
        }
        return ctvList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + ID_CORPUS_TAG + " AS " + TABLE_NAME + "_" + ID_CORPUS_TAG + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE;
    }

    public static void delete(CorpusTagValues corpusMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpusMetadataValues.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(CorpusTagValues corpusMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = ID_CORPUS + ", " + ID_CORPUS_TAG + ", " + VALUE;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpusMetadataValues.getIdCorpus());
                ps.setInt(2, corpusMetadataValues.getIdCorpusTag());
                ps.setString(3, corpusMetadataValues.getValue());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<CorpusTagValues> lstCorpusMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = ID_CORPUS + ", " + ID_CORPUS_TAG + ", " + VALUE;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //disable auto commit
                c.setAutoCommit(false);
                for (CorpusTagValues corpusMetadataValues : lstCorpusMetadataValues) {
                    ps.setInt(1, corpusMetadataValues.getIdCorpus());
                    ps.setInt(2, corpusMetadataValues.getIdCorpusTag());
                    ps.setString(3, corpusMetadataValues.getValue());
                    ps.addBatch();
                }
                //execute
                ps.executeBatch();
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void selectById(CorpusTagValues corpusMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  v." + ID + ", "
                    + "  v." + VALUE + ", "
                    + "  m." + CorpusTagsDAO.TAG + " AS tag, "
                    + "  m." + CorpusTagsDAO.IS_REQUIRED + " AS is_required "
                    + "FROM "
                    + "  " + TABLE_NAME + " v, "
                    + "  " + CorpusTagsDAO.TABLE_NAME + " m "
                    + "WHERE "
                    + "  v." + ID_CORPUS + " = ? "
                    + "  AND v." + ID_CORPUS_TAG + " = ? "
                    + "  AND trim(v." + VALUE + ") != '' "
                    + "  AND v." + ID_CORPUS_TAG + " = m.id";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpusMetadataValues.getIdCorpus());
                ps.setInt(2, corpusMetadataValues.getIdCorpusTag());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        corpusMetadataValues.setId(rs.getInt(ID));
                        corpusMetadataValues.setValue(rs.getString(VALUE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void updateByMetadataId(List<CorpusTagValues> lstCorpusMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String table = TABLE_NAME;
            String fields = VALUE + " = ? ";
            String where = ID_CORPUS + " = ? AND " + ID_CORPUS_TAG + " = ? ";
            String sql = "UPDATE " + table + " SET " + fields + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //disable auto commit
                c.setAutoCommit(false);
                for (CorpusTagValues corpusMetadataValues : lstCorpusMetadataValues) {
                    ps.setString(1, corpusMetadataValues.getValue());
                    ps.setInt(2, corpusMetadataValues.getIdCorpus());
                    ps.setInt(3, corpusMetadataValues.getIdCorpusTag());
                    ps.addBatch();
                }
                //execute
                ps.executeBatch();
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
