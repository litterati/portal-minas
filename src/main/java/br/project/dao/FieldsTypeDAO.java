package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.FieldsType;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * FieldsTypeDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class FieldsTypeDAO {

    public static final String TABLE_NAME = "fields_type";
    public static final String ID = "id";
    public static final String VALUE = "value";

    public static FieldsType load(ResultSet rs) throws SQLException {
        FieldsType ft = new FieldsType();
        ft.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ft.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        return ft;
    }

    public static List<FieldsType> loadList(ResultSet rs) throws SQLException {
        List<FieldsType> ftList = new ArrayList<>();
        while (rs.next()) {
            FieldsType ft = new FieldsType();
            ft.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ft.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            // add into the list
            ftList.add(ft);
        }
        return ftList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE;
    }

    public static List<FieldsType> selectAll() throws DAOException {
        List<FieldsType> lstFieldsType = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    FieldsType fieldsType;
                    while (rs.next()) {
                        fieldsType = new FieldsType();
                        fieldsType.setId(rs.getInt(ID));
                        fieldsType.setValue(rs.getString(VALUE));
                        //add to list
                        lstFieldsType.add(fieldsType);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstFieldsType;
    }

    public static FieldsType selectById(int id) throws DAOException {
        FieldsType fields_type = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        fields_type = new FieldsType();
                        fields_type.setId(rs.getInt(ID));
                        fields_type.setValue(rs.getString(VALUE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeDAO.class, ex);
            //throw new DAOException(ex);
        }
        return fields_type;
    }

    public static void insert(FieldsType field_type) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + VALUE + ") "
                    + "VALUES (?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setString(1, field_type.getValue());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        //Set the text object with the id created when the text was inserted
                        field_type.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(FieldsType field_type) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + VALUE + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, field_type.getValue());
                ps.setInt(2, field_type.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(FieldsType field_type) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, field_type.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
