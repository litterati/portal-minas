package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Text;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TextDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextDAO {

    public static final String TABLE_NAME = "texts";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String LANGUAGE = "language";
    public static final String TITLE = "title";

    public static Text load(ResultSet rs) throws SQLException {
        Text text = new Text();
        text.setId(rs.getInt(TABLE_NAME + "_" + ID));
        text.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        text.setLanguage(rs.getString(TABLE_NAME + "_" + LANGUAGE));
        text.setTitle(rs.getString(TABLE_NAME + "_" + TITLE));
        return text;
    }

    public static List<Text> loadList(ResultSet rs) throws SQLException {
        List<Text> textList = new ArrayList<>();
        while (rs.next()) {
            Text text = new Text();
            text.setId(rs.getInt(TABLE_NAME + "_" + ID));
            text.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            text.setLanguage(rs.getString(TABLE_NAME + "_" + LANGUAGE));
            text.setTitle(rs.getString(TABLE_NAME + "_" + TITLE));
            // add into the list
            textList.add(text);
        }
        return textList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + LANGUAGE + " AS " + TABLE_NAME + "_" + LANGUAGE + ", "
                + TABLE_NAME + "." + TITLE + " AS " + TABLE_NAME + "_" + TITLE;
    }

    /**
     * Delete a specific Text in the database.
     *
     * @param text The text to delete
     * @throws br.project.exception.DAOException
     */
    public static void delete(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            TextDAO.delete(text, c);
            //c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(Text text, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Delete a group of Text in the database.
     *
     * @param texts The text group to delete.
     * @throws br.project.exception.DAOException
     */
    public static void delete(List<Text> texts) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                c.setAutoCommit(false);
                for (Text text : texts) {
                    ps.setInt(1, text.getId());
                    //add to batch
                    ps.addBatch();
                }
                //execute
                ps.executeBatch();
                //commit
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Insert a specific Text in the database.
     *
     * @param text The text to insert
     * @throws br.project.exception.DAOException
     */
    public static void insert(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            TextDAO.insert(text, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(Text text, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_CORPUS + ", " + LANGUAGE + ", " + TITLE + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                ps.setInt(1, text.getIdCorpus());
                ps.setString(2, text.getLanguage());
                ps.setString(3, text.getTitle());
                //execute
                ps.execute();
                //generete keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        text.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Insert a group of Text in the database.
     *
     * @param texts The text group to insert
     * @throws br.project.exception.DAOException
     */
    public static void insert(List<Text> texts) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            TextDAO.insert(texts, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Insert a group of Text in the database.
     *
     * @param texts The text group to insert
     * @param c Connection
     * @throws br.project.exception.DAOException
     */
    public static void insert(List<Text> texts, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_CORPUS + ", " + LANGUAGE + ", " + TITLE + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                for (Text text : texts) {
                    ps.setInt(1, text.getIdCorpus());
                    ps.setString(2, text.getLanguage());
                    ps.setString(3, text.getTitle());
                    ps.addBatch();
                }
                //execute batch
                ps.executeBatch();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    for (int i = 0; i < texts.size() && rs.next(); i++) {
                        texts.get(i).setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Get all Texts by the Corpus ID.
     *
     * @param id ID of the Corpus.
     * @param order The name of the table to order by.
     * @return List of all Texts.
     * @throws br.project.exception.DAOException
     */
    public static List<Text> selectAllByIdCorpus(int id, String order) throws DAOException {
        List<Text> listTexts = new ArrayList<>();
        //order
        if (order == null) {
            order = ID;
        } else if (!order.equals(ID) && !order.equals(ID_CORPUS) && !order.equals(LANGUAGE) && !order.equals(TITLE)) {
            order = ID;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TextDAO.getFields()
                    + " FROM " + TextDAO.TABLE_NAME + " "
                    + " WHERE " + TextDAO.TABLE_NAME + "." + TextDAO.ID_CORPUS + " = ? "
                    + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    //add to list
                    listTexts = loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new RuntimeException("Erro em TextDAO. " + ex.getMessage());
        }
        return listTexts;
    }

    /**
     * Get all Texts from the database by the Corpus ID.
     *
     * @param id ID of the Corpus
     * @param offset Start from which register
     * @param limit Number of registers to get
     * @param order
     * @return List of all Texts
     */
    public static List<Text> selectByIdCorpus(int id, int limit, int offset, String order) {
        List<Text> listTexts = new ArrayList<>();
        //order
        if (order == null) {
            order = ID;
        } else if (!order.equals(ID) && !order.equals(ID_CORPUS) && !order.equals(LANGUAGE) && !order.equals(TITLE)) {
            order = ID;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TextDAO.getFields()
                    + " FROM " + TextDAO.TABLE_NAME + " "
                    + " WHERE " + TextDAO.TABLE_NAME + "." + TextDAO.ID_CORPUS + " = ? "
                    + " ORDER BY " + order + " ASC "
                    + " LIMIT ? "
                    + " OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                ps.setInt(2, limit);
                ps.setInt(3, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    //add to list
                    listTexts = loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //Erro ao fillTextTagValues
            Logger.error(TextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listTexts;
    }

    public static List<Text> selectByIdCorpusWithTagValues(int id, int limit, int offset, String order) {
        List<Text> listTexts = new ArrayList<>();
        //order
        if (order == null) {
            order = ID;
        } else if (!order.equals(ID) && !order.equals(ID_CORPUS) && !order.equals(LANGUAGE) && !order.equals(TITLE)) {
            order = ID;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String innerSql = "("
                    + "SELECT " + TextDAO.ID
                    + " FROM " + TextDAO.TABLE_NAME
                    + " WHERE " + TextDAO.ID_CORPUS + " = ? "
                    + " ORDER BY " + TextDAO.TITLE
                    + " LIMIT ? "
                    + " OFFSET ? "
                    + ")";
            String fields = TextDAO.getFields() + ", "
                    + TextTagsDAO.getFields() + ", "
                    + TextTagValuesDAO.getFields();
            String from = TextDAO.TABLE_NAME + ", "
                    + TextTagsDAO.TABLE_NAME + ", "
                    + TextTagValuesDAO.TABLE_NAME;
            String where = TextDAO.TABLE_NAME + "." + TextDAO.ID + " IN " + innerSql
                    + " AND " + TextDAO.TABLE_NAME + "." + TextDAO.ID + " = " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT
                    + " AND " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT_TAG + " = " + TextTagsDAO.TABLE_NAME + "." + TextTagsDAO.ID;
            String order_by = TextDAO.TABLE_NAME + "." + order
                    + " , " + TextDAO.TABLE_NAME + "." + TextDAO.ID
                    + " , " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT_TAG
                    + " ASC ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where
                    + " ORDER BY " + order_by;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                ps.setInt(2, limit);
                ps.setInt(3, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    int textTagId = -1;
                    int textId = -1;
                    Text text = null;
                    TextTags textTag = null;
                    while (rs.next()) {
                        //text tag value
                        TextTagValues textTagValue = TextTagValuesDAO.load(rs);
                        //text tag
                        if (textTagId != textTagValue.getIdTextTag()) {
                            textTag = TextTagsDAO.load(rs);
                            textTagId = textTag.getId();
                        }
                        textTagValue.setTextTag(textTag);
                        textTagValue.setIdTextTag(textTag.getId());
                        textTag.addTextTagsValues(textTagValue);
                        //text
                        if (textId != textTagValue.getIdText()) {
                            text = TextDAO.load(rs);
                            textId = text.getId();
                            listTexts.add(text);
                        }
                        text.addTextTags(textTag);
                        text.addTextTagsValues(textTagValue);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //Erro ao fillTextTagValues
            Logger.error(TextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listTexts;
    }

    public static List<Text> selectAllByIdCorpusWithTagValues(int id, String order) {
        List<Text> listTexts = new ArrayList<>();
        //order
        if (order == null) {
            order = ID;
        } else if (!order.equals(ID) && !order.equals(ID_CORPUS) && !order.equals(LANGUAGE) && !order.equals(TITLE)) {
            order = ID;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = TextDAO.getFields() + ", "
                    + TextTagsDAO.getFields() + ", "
                    + TextTagValuesDAO.getFields();
            String from = TextDAO.TABLE_NAME + ", "
                    + TextTagsDAO.TABLE_NAME + ", "
                    + TextTagValuesDAO.TABLE_NAME;
            String sql = "SELECT " + fields + " FROM " + from
                    + " WHERE " + TextDAO.TABLE_NAME + "." + TextDAO.ID_CORPUS + " = ? "
                    + " AND " + TextDAO.TABLE_NAME + "." + TextDAO.ID + " = " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT
                    + " AND " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT_TAG + " = " + TextTagsDAO.TABLE_NAME + "." + TextTagsDAO.ID
                    + " ORDER BY " + TextDAO.TABLE_NAME + "." + order
                    + " , " + TextDAO.TABLE_NAME + "." + TextDAO.ID
                    + " , " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT_TAG
                    + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    int textTagId = -1;
                    int textId = -1;
                    Text text = null;
                    TextTags textTag = null;
                    while (rs.next()) {
                        //text tag value
                        TextTagValues textTagValue = TextTagValuesDAO.load(rs);
                        //text tag
                        if (textTagId != textTagValue.getIdTextTag()) {
                            textTag = TextTagsDAO.load(rs);
                            textTagId = textTag.getId();
                        }
                        textTagValue.setTextTag(textTag);
                        textTagValue.setIdTextTag(textTag.getId());
                        textTag.addTextTagsValues(textTagValue);
                        //text
                        if (textId != textTagValue.getIdText()) {
                            text = TextDAO.load(rs);
                            textId = text.getId();
                            listTexts.add(text);
                        }
                        text.addTextTags(textTag);
                        text.addTextTagsValues(textTagValue);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //Erro ao fillTextTagValues
            Logger.error(TextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listTexts;
    }

    /**
     * Complete a Text object. Fulfill the variables query by the ID.
     * Deprecated! The right way is the method TextDAO.selectById(int id).
     *
     * @param text The text object with the ID.
     * @throws br.project.exception.DAOException
     */
    @Deprecated
    public static void fillById(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + ", " + ID_CORPUS + ", " + LANGUAGE + ", " + TITLE + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        text.setIdCorpus(rs.getInt(ID_CORPUS));
                        text.setLanguage(rs.getString(LANGUAGE));
                        text.setTitle(rs.getString(TITLE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Count the number of texts in the database.
     *
     * @param idCorpus The corpus ID to query.
     * @return The number of text or -1.
     */
    public static int countByIdCorpus(int idCorpus) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_CORPUS + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            //throw new DAOException(e);
        }
        return value;
    }

    /**
     * Get all words from a specific text and fulfill the text object.
     * Deprecated! The right way is get the list of words.
     *
     * @param text The text to get the ID.
     * @throws br.project.exception.DAOException
     */
    @Deprecated
    public static void selectWordsByText(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT w." + WordDAO.WORD + " "
                    + "FROM " + WordDAO.TABLE_NAME + " w "
                    + "WHERE w." + WordDAO.ID_TEXT + " = ? "
                    + "ORDER BY w." + WordDAO.POSITION + ";";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        text.addTextString(rs.getString("word"));
                        text.addTextString(" ");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Update the text data in the database.
     *
     * @param text The corpus ID to query.
     * @throws br.project.exception.DAOException
     */
    public static void update(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + LANGUAGE + " = ? , " + TITLE + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, text.getLanguage());
                ps.setString(2, text.getTitle());
                ps.setInt(3, text.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Get all words from a specific text and fulfill the text object.
     * Deprecated! The right way is get the list of words.
     *
     * @param text The text to get the ID.
     * @throws java.sql.SQLException
     * @throws br.project.exception.DAOException
     */
    @Deprecated
    public static void setWordsWithIdByText(Text text) throws SQLException, Exception {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT w." + WordDAO.WORD + ", w." + WordDAO.ID + " "
                    + "FROM " + WordDAO.TABLE_NAME + " w "
                    + "WHERE w." + WordDAO.ID_TEXT + " = ? "
                    + "ORDER BY w." + WordDAO.POSITION + ";";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        text.addTextString(rs.getString("word"));
                        text.addTextString("_");
                        Integer i = rs.getInt("id");
                        text.addTextString(i.toString());
                        text.addTextString(" ");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    /**
     * Get a specific text in the database by the text id.
     *
     * @param id The corpus ID to query.
     * @return The text if success or null.
     */
    public static Text selectById(int id) {
        Text text = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TextDAO.getFields() + " "
                    + "FROM " + TextDAO.TABLE_NAME + " "
                    + "WHERE " + TextDAO.TABLE_NAME + "." + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        text = TextDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //Erro ao fillTextTagValues
            Logger.error(TextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return text;
    }

    public static Text selectByIdWithTextTagValues(int id) {
        Text text = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = TextDAO.getFields() + ", "
                    + TextTagsDAO.getFields() + ", "
                    + TextTagValuesDAO.getFields();
            String from = TextDAO.TABLE_NAME + ", "
                    + TextTagsDAO.TABLE_NAME + ", "
                    + TextTagValuesDAO.TABLE_NAME;
            String sql = "SELECT " + fields + " FROM " + from
                    + " WHERE " + TextDAO.TABLE_NAME + "." + TextDAO.ID + " = ? "
                    + " AND " + TextDAO.TABLE_NAME + "." + TextDAO.ID + " = " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT
                    + " AND " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT_TAG + " = " + TextTagsDAO.TABLE_NAME + "." + TextTagsDAO.ID
                    + " ORDER BY " + TextDAO.TABLE_NAME + "." + TextDAO.ID + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    int textTagId = -1;
                    int textId = -1;
                    TextTags textTag = null;
                    while (rs.next()) {
                        //text tag value
                        TextTagValues textTagValue = TextTagValuesDAO.load(rs);
                        //text tag
                        if (textTagId != textTagValue.getIdTextTag()) {
                            textTag = TextTagsDAO.load(rs);
                            textTagId = textTag.getId();
                        }
                        textTagValue.setTextTag(textTag);
                        textTagValue.setIdTextTag(textTag.getId());
                        textTag.addTextTagsValues(textTagValue);
                        //text
                        if (textId != textTagValue.getIdText()) {
                            text = TextDAO.load(rs);
                            textId = text.getId();
                        }
                        text.addTextTags(textTag);
                        text.addTextTagsValues(textTagValue);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //Erro ao fillTextTagValues
            Logger.error(TextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return text;
    }
}
