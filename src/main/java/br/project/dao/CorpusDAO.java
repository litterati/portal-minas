package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Corpus;
import br.project.entity.CorpusTagValues;
import br.project.entity.CorpusTags;
import br.project.exception.CorpusTagsFacadeException;
import br.project.exception.DAOException;
import br.project.facade.CorpusTagsFacade;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CorpusDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class CorpusDAO {

    public static final String TABLE_NAME = "corpus";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String TYPE = "type";
    public static final String USERTERMS = "user_terms";

    public static Corpus load(ResultSet rs) throws SQLException {
        Corpus corpus = new Corpus();
        corpus.setId(rs.getInt(TABLE_NAME + "_" + ID));
        corpus.setName(rs.getString(TABLE_NAME + "_" + NAME));
        corpus.setDescription(rs.getString(TABLE_NAME + "_" + DESCRIPTION));
        corpus.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
        corpus.setUserTerms(rs.getString(TABLE_NAME + "_" + USERTERMS));
        return corpus;
    }

    public static List<Corpus> loadList(ResultSet rs) throws SQLException {
        List<Corpus> corpusList = new ArrayList<>();
        while (rs.next()) {
            Corpus corpus = new Corpus();
            corpus.setId(rs.getInt(TABLE_NAME + "_" + ID));
            corpus.setName(rs.getString(TABLE_NAME + "_" + NAME));
            corpus.setDescription(rs.getString(TABLE_NAME + "_" + DESCRIPTION));
            corpus.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
            corpus.setUserTerms(rs.getString(TABLE_NAME + "_" + USERTERMS));
            // add into the list
            corpusList.add(corpus);
        }
        return corpusList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + NAME + " AS " + TABLE_NAME + "_" + NAME + ", "
                + TABLE_NAME + "." + DESCRIPTION + " AS " + TABLE_NAME + "_" + DESCRIPTION + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE + ", "
                + TABLE_NAME + "." + USERTERMS + " AS " + TABLE_NAME + "_" + USERTERMS;
    }

    public static void delete(Corpus corpus) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpus.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(Corpus corpus) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = NAME + ", " + DESCRIPTION + ", " + TYPE;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setString(1, corpus.getName());
                ps.setString(2, corpus.getDescription());
                ps.setInt(3, corpus.getType());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        //Set the text object with the id created when the text was inserted
                        corpus.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            throw new DAOException(ex);
            //throw new RuntimeException("Problemas para inserir corpus. Provavelmente nome do corpus a ser inserido já existe");
        }
    }

    public static Corpus selectById(int corpus_id) throws DAOException {
        Corpus corpus = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusDAO.getFields();
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "SELECT " + fields + " FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpus_id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        corpus = CorpusDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return corpus;
    }

    public static Corpus selectByIdWithTagValues(int corpus_id) throws DAOException {
        Corpus corpus = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusDAO.getFields() + ", "
                    + CorpusTagsDAO.getFields() + ", "
                    + CorpusTagValuesDAO.getFields();
            String from = CorpusDAO.TABLE_NAME + ", "
                    + CorpusTagsDAO.TABLE_NAME + ", "
                    + CorpusTagValuesDAO.TABLE_NAME;
            String where = CorpusDAO.TABLE_NAME + "." + CorpusDAO.ID + " = ? "
                    + " AND " + CorpusDAO.TABLE_NAME + "." + CorpusDAO.ID + " = " + CorpusTagsDAO.TABLE_NAME + "." + CorpusTagsDAO.ID_CORPUS
                    + " AND " + CorpusTagsDAO.TABLE_NAME + "." + CorpusTagsDAO.ID + " = " + CorpusTagValuesDAO.TABLE_NAME + "." + CorpusTagValuesDAO.ID_CORPUS_TAG;
            String order = CorpusDAO.TABLE_NAME + "." + CorpusDAO.NAME + ", "
                    + CorpusDAO.TABLE_NAME + "." + CorpusDAO.ID
                    + " ASC ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where
                    + " ORDER BY " + order;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpus_id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    int corpusTagId = -1;
                    int corpusId = -1;
                    CorpusTags corpusTag = null;
                    while (rs.next()) {
                        //corpus tag value
                        CorpusTagValues corpusTagValue = CorpusTagValuesDAO.load(rs);
                        //corpus tag
                        if (corpusTagId != corpusTagValue.getIdCorpusTag()) {
                            corpusTag = CorpusTagsDAO.load(rs);
                            corpusTagId = corpusTag.getId();
                        }
                        corpusTagValue.setCorpusTags(corpusTag);
                        corpusTagValue.setIdCorpusTag(corpusTag.getId());
                        corpusTag.addCorpusTagValues(corpusTagValue);
                        //corpus
                        if (corpusId != corpusTagValue.getIdCorpus()) {
                            corpus = CorpusDAO.load(rs);
                            corpusId = corpus.getId();
                        }
                        corpus.addCorpusTags(corpusTag);
                        corpus.addCorpusTagValues(corpusTagValue);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return corpus;
    }

    public static List<Corpus> selectAll(int type) throws DAOException {
        List<Corpus> listCorpus = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusDAO.getFields();
            String from = TABLE_NAME;
            String order = NAME + " ASC ";
            String sql = "SELECT " + fields + " FROM " + from + " ORDER BY " + order;
            PreparedStatement ps = c.prepareStatement(sql);
            if (type == Corpus.WORK || type == Corpus.REFERENCE) {
                String where = TYPE + " = ? ";
                sql = "SELECT " + fields + " FROM " + from + " WHERE " + where + " ORDER BY " + order;
                ps = c.prepareStatement(sql);
                ps.setInt(1, type);
            }
            //execute
            try (ResultSet rs = ps.executeQuery()) {
                listCorpus = CorpusDAO.loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
            ps.close();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listCorpus;
    }

    public static List<Corpus> selectAllWithTagValues(int type) throws DAOException {
        List<Corpus> listCorpus = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusDAO.getFields() + ", "
                    + CorpusTagsDAO.getFields() + ", "
                    + CorpusTagValuesDAO.getFields();
            String from = CorpusDAO.TABLE_NAME + ", "
                    + CorpusTagsDAO.TABLE_NAME + ", "
                    + CorpusTagValuesDAO.TABLE_NAME;
            String where = CorpusDAO.TABLE_NAME + "." + CorpusDAO.ID + " = " + CorpusTagsDAO.TABLE_NAME + "." + CorpusTagsDAO.ID_CORPUS
                    + " AND " + CorpusTagsDAO.TABLE_NAME + "." + CorpusTagsDAO.ID + " = " + CorpusTagValuesDAO.TABLE_NAME + "." + CorpusTagValuesDAO.ID_CORPUS_TAG;
            String order = CorpusDAO.TABLE_NAME + "." + CorpusDAO.NAME + ", "
                    + CorpusDAO.TABLE_NAME + "." + CorpusDAO.ID
                    + " ASC ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where
                    + " ORDER BY " + order;
            PreparedStatement ps = c.prepareStatement(sql);
            if (type == Corpus.WORK || type == Corpus.REFERENCE) {
                where = TYPE + " = ? " + " AND " + where;
                sql = "SELECT " + fields
                        + " FROM " + from
                        + " WHERE " + where
                        + " ORDER BY " + order;
                ps = c.prepareStatement(sql);
                ps.setInt(1, type);
            }
            //execute
            try (ResultSet rs = ps.executeQuery()) {
                int corpusTagId = -1;
                int corpusId = -1;
                Corpus corpus = null;
                CorpusTags corpusTag = null;
                while (rs.next()) {
                    //corpus tag value
                    CorpusTagValues corpusTagValue = CorpusTagValuesDAO.load(rs);
                    //corpus tag
                    if (corpusTagId != corpusTagValue.getIdCorpusTag()) {
                        corpusTag = CorpusTagsDAO.load(rs);
                        corpusTagId = corpusTag.getId();
                    }
                    corpusTagValue.setCorpusTags(corpusTag);
                    corpusTagValue.setIdCorpusTag(corpusTag.getId());
                    corpusTag.addCorpusTagValues(corpusTagValue);
                    //corpus
                    if (corpusId != corpusTagValue.getIdCorpus()) {
                        corpus = CorpusDAO.load(rs);
                        corpusId = corpus.getId();
                        listCorpus.add(corpus);
                    }
                    corpus.addCorpusTags(corpusTag);
                    corpus.addCorpusTagValues(corpusTagValue);
                }
            } catch (SQLException ex) {
                throw ex;
            }
            ps.close();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listCorpus;
    }

    public static void update(Corpus corpus) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String table = TABLE_NAME;
            String fields = NAME + " = ?, " + DESCRIPTION + " = ?,  " + TYPE + " = ?, " + USERTERMS + " = ? ";
            String where = ID + " = ? ";
            String sql = "UPDATE " + table + " SET " + fields + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, corpus.getName());
                ps.setString(2, corpus.getDescription());
                ps.setInt(3, corpus.getType());
                ps.setString(4, corpus.getUserTerms());
                ps.setInt(5, corpus.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusDAO.class, ex);
            throw new DAOException(ex);
            //throw new RuntimeException("Problemas para atualizar corpus. Provavelmente nome do corpus a ser inserido já existe");
        }
    }
}
