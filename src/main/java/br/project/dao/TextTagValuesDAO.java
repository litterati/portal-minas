package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TextTagValuesDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextTagValuesDAO {

    public static final String TABLE_NAME = "text_tag_values";
    public static final String ID = "id";
    public static final String ID_TEXT = "id_text";
    public static final String ID_TEXT_TAG = "id_text_tag";
    public static final String VALUE = "value";

    public static TextTagValues load(ResultSet rs) throws SQLException {
        TextTagValues ttv = new TextTagValues();
        ttv.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ttv.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        ttv.setIdTextTag(rs.getInt(TABLE_NAME + "_" + ID_TEXT_TAG));
        ttv.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        return ttv;
    }

    public static List<TextTagValues> loadList(ResultSet rs) throws SQLException {
        List<TextTagValues> ttvList = new ArrayList<>();
        while (rs.next()) {
            TextTagValues ttv = new TextTagValues();
            ttv.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ttv.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            ttv.setIdTextTag(rs.getInt(TABLE_NAME + "_" + ID_TEXT_TAG));
            ttv.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            // add into the list
            ttvList.add(ttv);
        }
        return ttvList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + ID_TEXT_TAG + " AS " + TABLE_NAME + "_" + ID_TEXT_TAG + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE;
    }

    public static List<TextTagValues> selectTextTagsWithValue(int idText) throws DAOException {
        List<TextTagValues> textTagsValues = new ArrayList<>();
        TextTagValues textTagsValue;
        TextTags textTag;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  ttv." + ID + " AS id_text_tag_value, "
                    + "  ttv." + ID_TEXT + ", "
                    + "  ttv." + VALUE + ", "
                    + "  tt." + TextTagsDAO.ID + " AS id_text_tag, "
                    + "  tt." + TextTagsDAO.TAG + ", "
                    + "  tt." + TextTagsDAO.IS_REQUIRED + ", "
                    + "  tt." + TextTagsDAO.ID_FIELDS_TYPE + ", "
                    + "  tt." + TextTagsDAO.ID_CORPUS
                    + " FROM "
                    + "  " + TABLE_NAME + " ttv, "
                    + "  " + TextTagsDAO.TABLE_NAME + " tt "
                    + "WHERE "
                    + "  ttv." + ID_TEXT + " = ?"
                    + "  AND ttv." + ID_TEXT_TAG + " = tt." + TextTagsDAO.ID + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idText);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        textTagsValue = new TextTagValues();
                        textTag = new TextTags();
                        //Text Tags Value
                        textTagsValue.setId(rs.getInt("id_text_tag_value"));
                        textTagsValue.setIdText(rs.getInt(ID_TEXT));
                        textTagsValue.setIdTextTag(rs.getInt("id_text_tag"));
                        textTagsValue.setValue(rs.getString(VALUE));
                        //auxiliary
                        //textTagsValue.setTag(rs.getString(TextTagsDAO.TAG));
                        //textTagsValue.setIsRequired(rs.getBoolean(TextTagsDAO.IS_REQUIRED));
                        //textTagsValue.setIdCorpus(rs.getInt(TextTagsDAO.ID_CORPUS));
                        //textTagsValue.setIdFieldType(rs.getInt(TextTagsDAO.ID_FIELDS_TYPE));
                        //textTagsValue.setLstFieldsTypeValues(FieldsTypeValuesDAO.selectAllByType(rs.getInt(TextTagsDAO.ID_FIELDS_TYPE)));
                        //Text Tags
                        textTag.setId(rs.getInt("id_text_tag"));
                        textTag.setIdCorpus(rs.getInt(TextTagsDAO.ID_CORPUS));
                        textTag.setTag(rs.getString(TextTagsDAO.TAG));
                        textTag.setIsRequired(rs.getBoolean(TextTagsDAO.IS_REQUIRED));
                        textTag.setIdFieldType(rs.getInt(TextTagsDAO.ID_FIELDS_TYPE));
                        //add
                        textTagsValue.setTextTag(textTag);
                        textTagsValues.add(textTagsValue);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
        return textTagsValues;
    }

    public static void delete(TextTagValues textMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, textMetadata.getIdText());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static boolean existValue(TextTagValues textMetadataValues) throws DAOException {
        boolean exist = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  v.id, "
                    + "  v.id_text, "
                    + "  v.value, "
                    + "  m.tag AS metadata, "
                    + "  m.is_required AS is_required "
                    + "FROM "
                    + "  " + TABLE_NAME + " v, "
                    + "  " + TextTagsDAO.TABLE_NAME + " m "
                    + "WHERE "
                    + "  v.id_text_tag = ? "
                    + "  AND m.id_corpus = ? "
                    + "  AND trim(v.value) != '' "
                    + "  AND v.id_text_tag = m.id";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, textMetadataValues.getIdTextTag());
                ps.setInt(2, textMetadataValues.getTextTag().getIdCorpus());
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        exist = true;
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            //throw new DAOException(ex);
        }
        return exist;
    }

    public static void insert(TextTagValues textMetadatalues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + ID_TEXT_TAG + ", " + VALUE + ") "
                    + "VALUE (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, textMetadatalues.getIdText());
                ps.setInt(2, textMetadatalues.getIdTextTag());
                ps.setString(3, textMetadatalues.getValue());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<TextTagValues> lstTextMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            TextTagValuesDAO.insert(lstTextMetadataValues, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<TextTagValues> lstTextMetadataValues, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + ID_TEXT_TAG + ", " + VALUE + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                c.setAutoCommit(false);
                for (TextTagValues textMetadataValues : lstTextMetadataValues) {
                    ps.setInt(1, textMetadataValues.getIdText());
                    ps.setInt(2, textMetadataValues.getIdTextTag());
                    ps.setString(3, textMetadataValues.getValue());
                    //add to batch
                    ps.addBatch();
                }
                //execute batch
                ps.executeBatch();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void selectById(TextTagValues textsMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  v." + ID + ", "
                    + "  v." + ID_TEXT + ", "
                    + "  v." + ID_TEXT_TAG + ", "
                    + "  v." + VALUE + ", "
                    + "  m." + TextTagsDAO.TAG + " AS tag, "
                    + "  m." + TextTagsDAO.IS_REQUIRED + " AS is_required "
                    + "FROM "
                    + "  " + TABLE_NAME + " v, "
                    + "  " + TextTagsDAO.TABLE_NAME + " m "
                    + "WHERE "
                    + "  v." + ID_TEXT + " = ?"
                    + "  AND v." + ID_TEXT_TAG + " = ? "
                    + "  AND v." + ID_TEXT_TAG + " = m." + TextTagsDAO.ID + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, textsMetadataValues.getIdText());
                ps.setInt(2, textsMetadataValues.getIdTextTag());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        textsMetadataValues.setId(rs.getInt(ID));
                        textsMetadataValues.setIdText(rs.getInt(ID_TEXT));
                        textsMetadataValues.setValue(rs.getString(VALUE));
                        textsMetadataValues.setIdTextTag(rs.getInt(ID_TEXT_TAG));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void updateByMetadataId(List<TextTagValues> lstTextMetadataValues) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + VALUE + " = ? "
                    + "WHERE " + ID_TEXT + " = ? AND " + ID_TEXT_TAG + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                c.setAutoCommit(false);
                for (TextTagValues textMetadataValues : lstTextMetadataValues) {
                    ps.setString(1, textMetadataValues.getValue());
                    ps.setInt(2, textMetadataValues.getIdText());
                    ps.setInt(3, textMetadataValues.getIdTextTag());
                    //add to batch
                    ps.addBatch();
                }
                //execute batch
                ps.executeBatch();
                //commit
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }
    
}
