package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Cryptography;
import br.library.util.Logger;
import br.project.entity.User;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * UserDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class UserDAO {

    public static final String TABLE_NAME = "users";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String TYPE = "type";

    public static User load(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt(TABLE_NAME + "_" + ID));
        user.setName(rs.getString(TABLE_NAME + "_" + NAME));
        user.setLogin(rs.getString(TABLE_NAME + "_" + LOGIN));
        user.setPassword(rs.getString(TABLE_NAME + "_" + PASSWORD));
        user.setEmail(rs.getString(TABLE_NAME + "_" + EMAIL));
        user.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
        return user;
    }

    public static List<User> loadList(ResultSet rs) throws SQLException {
        List<User> userList = new ArrayList<>();
        while (rs.next()) {
            User user = new User();
            user.setId(rs.getInt(TABLE_NAME + "_" + ID));
            user.setName(rs.getString(TABLE_NAME + "_" + NAME));
            user.setLogin(rs.getString(TABLE_NAME + "_" + LOGIN));
            user.setPassword(rs.getString(TABLE_NAME + "_" + PASSWORD));
            user.setEmail(rs.getString(TABLE_NAME + "_" + EMAIL));
            user.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
            // add into the list
            userList.add(user);
        }
        return userList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + NAME + " AS " + TABLE_NAME + "_" + NAME + ", "
                + TABLE_NAME + "." + LOGIN + " AS " + TABLE_NAME + "_" + LOGIN + ", "
                + TABLE_NAME + "." + PASSWORD + " AS " + TABLE_NAME + "_" + PASSWORD + ", "
                + TABLE_NAME + "." + EMAIL + " AS " + TABLE_NAME + "_" + EMAIL + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE;
    }

    public static void delete(User user) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, user.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static User authenticate(String login, String password) throws DAOException {
        User user = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + LOGIN + " = ? AND " + PASSWORD + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, login);
                ps.setString(2, Cryptography.encryptography(password));
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        user = new User();
                        user.setId(rs.getInt(ID));
                        user.setLogin(rs.getString(LOGIN));
                        user.setPassword(rs.getString(PASSWORD));
                        user.setName(rs.getString(NAME));
                        user.setEmail(rs.getString(EMAIL));
                        user.setType(rs.getInt(TYPE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException | NoSuchAlgorithmException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
        return user;
    }

    public static void insert(User user) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + NAME + ", " + EMAIL + ", " + LOGIN + ", " + PASSWORD + ", " + TYPE + ") "
                    + "VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setString(1, user.getName());
                ps.setString(2, user.getEmail());
                ps.setString(3, user.getLogin());
                ps.setString(4, Cryptography.encryptography(user.getPassword()));
                ps.setInt(5, user.getType());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        user.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException | NoSuchAlgorithmException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<User> selectAll() throws DAOException {
        List<User> listUsers = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    User user;
                    while (rs.next()) {
                        user = new User();
                        user.setId(rs.getInt(ID));
                        user.setLogin(rs.getString(LOGIN));
                        user.setPassword(rs.getString(PASSWORD));
                        user.setName(rs.getString(NAME));
                        user.setEmail(rs.getString(EMAIL));
                        user.setType(rs.getInt(TYPE));
                        //add to list
                        listUsers.add(user);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listUsers;
    }

    public static List<User> selectAll(String order) throws DAOException {
        List<User> listUsers = new ArrayList<>();
        //order
        if (order == null) {
            order = UserDAO.ID;
        } else if (!order.equals(UserDAO.ID) && !order.equals(UserDAO.NAME) && !order.equals(UserDAO.LOGIN) && !order.equals(UserDAO.EMAIL) && !order.equals(UserDAO.TYPE)) {
            order = UserDAO.ID;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    User user;
                    while (rs.next()) {
                        user = new User();
                        user.setId(rs.getInt(ID));
                        user.setLogin(rs.getString(LOGIN));
                        user.setPassword(rs.getString(PASSWORD));
                        user.setName(rs.getString(NAME));
                        user.setEmail(rs.getString(EMAIL));
                        user.setType(rs.getInt(TYPE));
                        //add to list
                        listUsers.add(user);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listUsers;
    }

    public static void selectById(User user) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, user.getId());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        user.setId(rs.getInt(ID));
                        user.setLogin(rs.getString(LOGIN));
                        user.setPassword(rs.getString(PASSWORD));
                        user.setName(rs.getString(NAME));
                        user.setEmail(rs.getString(EMAIL));
                        user.setType(rs.getInt(TYPE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(User user) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + NAME + " = ?, " + EMAIL + " = ?, " + LOGIN + " = ?, " + PASSWORD + " = ?, " + TYPE + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, user.getName());
                ps.setString(2, user.getEmail());
                ps.setString(3, user.getLogin());
                ps.setString(4, Cryptography.encryptography(user.getPassword()));
                ps.setInt(5, user.getType());
                ps.setInt(6, user.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException | NoSuchAlgorithmException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void updatePassword(User user) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + PASSWORD + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, Cryptography.encryptography(user.getPassword()));
                ps.setInt(2, user.getId());
                //execute
                ps.execute();
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException | NoSuchAlgorithmException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    //Just update Name Email and Type
    public static void updateNoPassword(User user) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + NAME + " = ?, " + EMAIL + " = ?, " + TYPE + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, user.getName());
                ps.setString(2, user.getEmail());
                ps.setInt(3, user.getType());
                ps.setInt(4, user.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static boolean existLogin(String login) {
        boolean user = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + LOGIN + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, login);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        user = true;
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            //throw new DAOException(e);
        }
        return user;
    }

    public static boolean existEmail(String email) {
        boolean user = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + EMAIL + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, email);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        user = true;
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(UserDAO.class, ex);
            //throw new DAOException(e);
        }
        return user;
    }
}
