package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Concordancer;
import br.project.entity.ConcordancerEntry;
import br.project.entity.Text;
import br.project.entity.TextTagValues;
import br.project.entity.Words;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains methods that concern the DB concordancer operations.
 *
 * @author Michelle
 * @since 2013
 * @modified and commented by: Marcel Akira Serikawa - marcel.serikawa@gmail.com
 */
public class ConcordancerDAO {

    public static void search(Concordancer concordancer) {
        try (Connection c = BuildConnection.getConnection()) {
            StringBuilder sql = new StringBuilder();
            //sql
            sql.append("SELECT window1.id AS window_id, window1.id_corpus AS window_id_corpus, window1.id_text AS window_id_text, window1.id_sentence AS window_id_sentence, window1.position AS window_position, window1.word AS window_word, window1.contraction AS window_contraction, w1.id AS word_id ")
                    .append(" FROM words w1, words window1 ")
                    .append(" WHERE w1.id IN (")
                    .append(ConcordancerDAO.buildSQL(concordancer.getTerm(), concordancer.getIdCorpus(), concordancer.getTolerance(), concordancer.getLstTextTagValues(), concordancer.getResultsByPage(), concordancer.getInterval()))
                    .append(") AND (window1.position BETWEEN w1.position - ? AND w1.position + ? ) ")
                    .append(" AND window1.id_text = w1.id_text ");
            //order by
            sql.append(" ORDER BY w1.id, window1.position ASC ");
            //prepare sql
            try (PreparedStatement ps = c.prepareStatement(sql.toString())) {
                //windows size
                ps.setInt(1, concordancer.getLeftTokensNumber());
                ps.setInt(2, concordancer.getRightTokensNumber());
                //Show
                //System.out.println(sql.toString());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    List<ConcordancerEntry> entries = new ArrayList<>();
                    ConcordancerEntry entry = null;
                    while (rs.next()) {
                        //word
                        Words w = new Words();
                        w.setId(rs.getInt("window_id"));
                        w.setIdCorpus(rs.getInt("window_id_corpus"));
                        w.setIdText(rs.getInt("window_id_text"));
                        w.setIdSentence(rs.getInt("window_id_sentence"));
                        w.setPosition(rs.getInt("window_position"));
                        w.setWord(rs.getString("window_word"));
                        w.setContraction(rs.getString("window_contraction"));
                        //if the term is from the same entry
                        if (entry == null || entry.getIdMainTerm() != rs.getInt("word_id")) {
                            entry = new ConcordancerEntry();
                            entry.setIdMainTerm(rs.getInt("word_id"));
                            entries.add(entry);
                        }
                        entry.addWords(w);
                        if (rs.getInt("window_id") == rs.getInt("word_id")) {
                            entry.setTerm(w);
                            //Text
                            Text text = TextDAO.selectById(rs.getInt("window_id_text"));
                            w.setText(text);
                        }
                    }
                    //sizes
                    int count = 0;
                    String sql2 = ConcordancerDAO.buildSQL(concordancer.getTerm(), concordancer.getIdCorpus(), concordancer.getTolerance(), concordancer.getLstTextTagValues(), 0, 0);
                    try (PreparedStatement ps2 = c.prepareStatement(sql2)) {
                        try (ResultSet rs2 = ps2.executeQuery()) {
                            while (rs2.next()) {
                                count++;
                            }
                        }
                    }
                    if (count <= 0) {//if query error
                        count = entries.size();
                    }
                    concordancer.setTotalConcordances(count);
                    concordancer.setTotalPages((count + (concordancer.getResultsByPage() - 1)) / concordancer.getResultsByPage());
                    concordancer.setEntries(entries);
                }
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(ConcordancerDAO.class, ex);
        }
    }

    private static String buildSQL(String search, int id_corpus, int tolerance, List<TextTagValues> metadata, int limit, int offset) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT w0.id ")
                .append(buildFromSQL(search)).append(" ")//from
                .append(buildWhereSQL(search, id_corpus, tolerance, metadata));//where
        if (limit > 0) {
            sql.append(" LIMIT ").append(limit)
                    .append(" OFFSET ").append(offset);
        }
        return sql.toString();
    }

    private static String buildFromSQL(String search) {
        String[] terms = search.trim().split(" ");//AND
        StringBuilder sql = new StringBuilder();
        sql.append("FROM ");
        for (int i = 0; i < terms.length; i++) {
            sql.append("words w").append(i);
            if ((i + 1) < terms.length) {//has next
                sql.append(", ");
            }
        }
        return sql.toString();
    }

    private static String buildWhereSQL(String search, int id_corpus, int tolerance, List<TextTagValues> metadata) {
        StringBuilder sql = new StringBuilder();
        sql.append("WHERE ")
                .append(whereTerms(search, 0, tolerance))
                .append(" AND w0.id_corpus = ").append(id_corpus);
        if (metadata != null && metadata.size() > 0) {
            sql.append(" AND w0.id_text IN (").append(whereMeta(metadata)).append(")");
        }
        return sql.toString();
    }

    private static String whereTerms(String search, int count, int tolerance) {
        search = search.trim();
        if (search.contains(" ")) {//AND
            String[] terms = search.split(" ");
            StringBuilder sql = new StringBuilder();
            for (int i = 0; i < terms.length; i++) {
                sql.append(whereTerms(terms[i], count + i, tolerance));
                if (i > 0) {//conect the terms to the order into the same text
                    if (tolerance > 0) {//tolerance
                        sql.append(" AND (w").append(i).append(".position BETWEEN w0.position AND w0.position + ").append(i + tolerance).append(") AND w0.id_text = w").append(i).append(".id_text ");
                    } else {
                        sql.append(" AND w").append(i).append(".position = (w0.position + ").append(i).append(") AND w0.id_text = w").append(i).append(".id_text ");
                    }
                }
                if ((i + 1) < terms.length) {//has next
                    sql.append(" AND ");
                }
            }
            return sql.toString();
        } else if (search.contains("+")) {//OR
            String[] terms = search.split("\\+");
            StringBuilder sql = new StringBuilder();
            sql.append("(");
            for (int i = 0; i < terms.length; i++) {
                sql.append(whereTerms(terms[i], count, tolerance));
                if ((i + 1) < terms.length) {//has next
                    sql.append(" OR ");
                }
            }
            sql.append(")");
            return sql.toString();
        } else if (search.contains(",")) {//multi modifiers
            String[] terms = search.split("\\,");
            StringBuilder sql = new StringBuilder();
            sql.append("(");
            for (int i = 0; i < terms.length; i++) {
                sql.append(whereTerms(terms[i], count, tolerance));
                if ((i + 1) < terms.length) {//has next
                    sql.append(" AND ");
                }
            }
            sql.append(")");
            return sql.toString();
        } else if (search.toLowerCase().contains("prefix:")) {//prefix
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".word) LIKE lower('" + terms[1] + "%')";
        } else if (search.toLowerCase().contains("infix:")) {//infix
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".word) LIKE lower('%" + terms[1] + "%')";
        } else if (search.toLowerCase().contains("suffix:")) {//suffix
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".word) LIKE lower('%" + terms[1] + "')";
        } else if (search.toLowerCase().contains("pos:")) {//pos
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".pos) = lower('" + terms[1] + "')";
        } else if (search.toLowerCase().contains("lemma:")) {//lemma
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".lemma) = lower('" + terms[1] + "')";
        }
        return "lower(w" + count + ".word) = lower('" + search + "')";
    }

    public static String whereMeta(List<TextTagValues> metadata) {
        StringBuilder sql = new StringBuilder();
        //Texts metadata
        if (metadata != null && metadata.size() > 0) {
            sql.append("SELECT m.id_text FROM text_tag_values m WHERE ");
            for (int i = 0; i < metadata.size(); i++) {
                sql.append("(m.id_text_tag = ").append(metadata.get(i).getIdTextTag()).append(" AND lower(m.value) = lower('").append(metadata.get(i).getValue()).append("'))");
                if ((i + 1) < metadata.size()) {//has next
                    sql.append(" OR ");
                }
            }
        }
        return sql.toString();
    }
}
