/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Permission;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * PermissionDAO
 *
 * @author Thiago Vieira
 * @since 14/07/2014
 */
public class PermissionDAO {

    public static final String TABLE_NAME = "permission";
    public static final String ID_USER = "id_user";
    public static final String ID_CORPUS = "id_corpus";
    public static final String LEVEL = "level";

    public static Permission load(ResultSet rs) throws SQLException {
        Permission permission = new Permission();
        permission.setIdUser(rs.getInt(TABLE_NAME + "_" + ID_USER));
        permission.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        permission.setLevel(rs.getInt(TABLE_NAME + "_" + LEVEL));
        return permission;
    }

    public static List<Permission> loadList(ResultSet rs) throws SQLException {
        List<Permission> permissionList = new ArrayList<>();
        while (rs.next()) {
            Permission permission = new Permission();
            permission.setIdUser(rs.getInt(TABLE_NAME + "_" + ID_USER));
            permission.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            permission.setLevel(rs.getInt(TABLE_NAME + "_" + LEVEL));
            // add into the list
            permissionList.add(permission);
        }
        return permissionList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID_USER + " AS " + TABLE_NAME + "_" + ID_USER + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + LEVEL + " AS " + TABLE_NAME + "_" + LEVEL;
    }

    public static void insert(Permission object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_USER + ", " + ID_CORPUS + ", " + LEVEL + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdUser());
                ps.setInt(2, object.getIdCorpus());
                ps.setInt(3, object.getLevel());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(PermissionDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(Permission object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + LEVEL + " = ?  "
                    + "WHERE " + ID_USER + " = ? AND " + ID_CORPUS + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getLevel());
                ps.setInt(2, object.getIdUser());
                ps.setInt(3, object.getIdCorpus());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(PermissionDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(Permission object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_USER + " = ? AND " + ID_CORPUS + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdUser());
                ps.setInt(2, object.getIdCorpus());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(PermissionDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<Permission> selectAll(int id_user) throws DAOException {
        List<Permission> listCorpusTools = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_USER + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_user);
                try (ResultSet rs = ps.executeQuery()) {
                    Permission p;
                    while (rs.next()) {
                        p = new Permission();
                        p.setIdUser(rs.getInt(ID_USER));
                        p.setIdCorpus(rs.getInt(ID_CORPUS));
                        p.setLevel(rs.getInt(LEVEL));
                        //add to list
                        listCorpusTools.add(p);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(PermissionDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listCorpusTools;
    }
}
