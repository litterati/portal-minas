/*
 * Em padrões de projeto de software, um façade ("fachada") é um objeto que 
 * disponibiliza uma interface simplificada para uma das funcionalidades de uma 
 * API, por exemplo. Um façade pode:
 * - tornar uma biblioteca de software mais fácil de entender e usar;
 * - tornar o código que utiliza esta biblioteca mais fácil de entender;
 * - reduzir as dependências em relação às características internas de uma 
 *   biblioteca, trazendo flexibilidade no desenvolvimento do sistema;
 * - envolver uma interface mal desenhada, com uma interface melhor definida.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Fa%C3%A7ade)
 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.ConcordancerAlign;
import br.project.entity.ConcordancerAlignEntry;
import br.project.entity.Text;
import br.project.entity.TextTagValues;
import br.project.entity.Words;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ConcordancerAlignDAO.
 *
 * It was a ConcordancerDAO
 *
 * @author Thiago Vieira, Marcel Akira Serikawa
 * @since 10/11/2014
 */
public class ConcordancerAlignDAO {

    public static void search(ConcordancerAlign concordancer) {
        try (Connection c = BuildConnection.getConnection()) {
            StringBuilder sql = new StringBuilder();
            //sql 1 ( all sentence)
            sql.append("SELECT w1.id AS w1_id, w1.id_sentence AS w1_id_sentence, w2.id AS w2_id, w2.id_sentence AS w2_id_sentence ")
                    .append(" FROM words w1, words w2, words_align ")
                    .append(" WHERE w1.id IN (")
                    .append(ConcordancerAlignDAO.buildSQL(concordancer.getTerm(), concordancer.getIdCorpus(), concordancer.getTolerance(), concordancer.getLstTextTagValues(), concordancer.getResultsByPage(), concordancer.getInterval()))
                    .append(") AND (w1.id = words_align.id_source OR w1.id = words_align.id_target) ")
                    .append(" AND w2.id = (CASE w1.id WHEN words_align.id_source THEN words_align.id_target WHEN words_align.id_target THEN words_align.id_source END) ");
            //prepare sql 1
            try (PreparedStatement ps = c.prepareStatement(sql.toString())) {
                //Show sql 1
                //System.out.println(sql.toString());
                //execute sql 1
                try (ResultSet rs = ps.executeQuery()) {
                    //sql 2
                    StringBuilder sql_where = new StringBuilder();
                    StringBuilder sql_word_type = new StringBuilder();
                    StringBuilder sql_align_group = new StringBuilder();
                    StringBuilder sql_search_term = new StringBuilder();
                    while (rs.next()) {
                        sql_where.append(" w.id_sentence = ").append(rs.getInt("w1_id_sentence"))
                                .append(" OR w.id_sentence = ").append(rs.getInt("w2_id_sentence"));
                        if (!rs.isLast()) {
                            sql_where.append(" OR ");
                        }
                        sql_word_type.append(" WHEN ").append(rs.getInt("w1_id_sentence")).append(" THEN ").append(" 'source' ")
                                .append(" WHEN ").append(rs.getInt("w2_id_sentence")).append(" THEN ").append(" 'target' ");
                        sql_align_group.append(" WHEN ").append(rs.getInt("w1_id_sentence")).append(" THEN '").append(rs.getInt("w1_id_sentence")).append("-").append(rs.getInt("w2_id_sentence")).append("' ")
                                .append(" WHEN ").append(rs.getInt("w2_id_sentence")).append(" THEN '").append(rs.getInt("w1_id_sentence")).append("-").append(rs.getInt("w2_id_sentence")).append("' ");
                        sql_search_term.append(" WHEN ").append(rs.getInt("w1_id")).append(" THEN ").append(" 'term' ")
                                .append(" WHEN ").append(rs.getInt("w2_id")).append(" THEN ").append(" 'term' ");
                    }
                    StringBuilder sql2 = new StringBuilder();
                    sql2.append("SELECT w.id AS w_id, w.id_corpus AS w_id_corpus, w.id_text AS w_id_text, w.id_sentence AS w_id_sentence, w.position AS w_position, w.word AS w_word, w.contraction AS w_contraction \n")
                            .append(", (CASE w.id_sentence ").append(sql_word_type).append("END) AS word_type \n")
                            .append(", (CASE w.id_sentence ").append(sql_align_group).append("END) AS align_group \n")
                            .append(", (CASE w.id ").append(sql_search_term).append("END) AS search_term \n")
                            .append(" FROM words w ")
                            .append(" WHERE ")
                            .append(sql_where)
                            .append(" ORDER BY align_group, w.id_sentence, w.position ASC ");
                    //entries
                    List<ConcordancerAlignEntry> entries = new ArrayList<>();
                    ConcordancerAlignEntry entry = null;
                    try (PreparedStatement ps2 = c.prepareStatement(sql2.toString())) {
                        //Show
                        //System.out.println(sql2.toString());
                        //execute sql 2
                        try (ResultSet rs2 = ps2.executeQuery()) {
                            while (rs2.next()) {
                                //word source
                                Words w = new Words();
                                w.setId(rs2.getInt("w_id"));
                                w.setIdCorpus(rs2.getInt("w_id_corpus"));
                                w.setIdText(rs2.getInt("w_id_text"));
                                w.setIdSentence(rs2.getInt("w_id_sentence"));
                                w.setWord(rs2.getString("w_word"));
                                w.setContraction(rs2.getString("w_contraction"));
                                if (entry == null || !entry.getAlignGroup().equals(rs2.getString("align_group"))) {
                                    entry = new ConcordancerAlignEntry();
                                    entry.setAlignGroup(rs2.getString("align_group"));
                                    entries.add(entry);
                                }
                                if (rs2.getString("word_type").equals("source")) {
                                    entry.addSourceList(w);
                                    if (rs2.getString("search_term") != null) {
                                        entry.setSourceTerm(w);
                                        //Text
                                        Text text = TextDAO.selectById(rs2.getInt("w_id_text"));
                                        w.setText(text);
                                    }
                                } else {
                                    entry.addTargetList(w);
                                    if (rs2.getString("search_term") != null) {
                                        entry.setTargetTerm(w);
                                        //Text
                                        Text text = TextDAO.selectById(rs2.getInt("w_id_text"));
                                        w.setText(text);
                                    }
                                }
                            }
                        }
                    }
                    //sizes
                    int count = 0;
                    String sql3 = ConcordancerAlignDAO.buildSQL(concordancer.getTerm(), concordancer.getIdCorpus(), concordancer.getTolerance(), concordancer.getLstTextTagValues(), 0, 0);
                    try (PreparedStatement ps3 = c.prepareStatement(sql3)) {
                        try (ResultSet rs3 = ps3.executeQuery()) {
                            while (rs3.next()) {
                                count++;
                            }
                        }
                    }
                    if (count <= 0) {//if query error
                        count = entries.size();
                    }
                    concordancer.setTotalConcordances(count);
                    concordancer.setTotalPages((count + (concordancer.getResultsByPage() - 1)) / concordancer.getResultsByPage());
                    concordancer.setEntries(entries);
                }
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(ConcordancerAlignDAO.class, ex);
        }
    }

    public static Map<Words, Words> match(ConcordancerAlign concordancer) {
        Map<Words, Words> entries = new HashMap<>();
        try (Connection c = BuildConnection.getConnection()) {
            StringBuilder sql = new StringBuilder();
            //sql
            sql.append("SELECT ")
                    .append(" w1.id AS w1_id, w1.id_corpus AS w1_id_corpus, w1.id_text AS w1_id_text, w1.id_sentence AS w1_id_sentence, w1.position AS w1_position, w1.word AS w1_word, w1.contraction AS w1_contraction, w1.pos AS w1_pos, ")
                    .append(" w2.id AS w2_id, w2.id_corpus AS w2_id_corpus, w2.id_text AS w2_id_text, w2.id_sentence AS w2_id_sentence, w2.position AS w2_position, w2.word AS w2_word, w2.contraction AS w2_contraction, w2.pos AS w2_pos ")
                    .append(" FROM words w1, words w2, words_align ")
                    .append(" WHERE w1.id IN (")
                    .append(ConcordancerAlignDAO.buildSQL(concordancer.getTerm(), concordancer.getIdCorpus(), concordancer.getTolerance(), concordancer.getLstTextTagValues(), 0, 0))
                    .append(") AND (w1.id = words_align.id_source OR w1.id = words_align.id_target) ")
                    .append(" AND w2.id = (CASE w1.id WHEN words_align.id_source THEN words_align.id_target WHEN words_align.id_target THEN words_align.id_source END) ");
            //prepare sql
            try (PreparedStatement ps = c.prepareStatement(sql.toString())) {
                //Show
                //System.out.println(sql.toString());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        //word source
                        Words w1 = new Words();
                        w1.setId(rs.getInt("w1_id"));
                        w1.setIdCorpus(rs.getInt("w1_id_corpus"));
                        w1.setIdText(rs.getInt("w1_id_text"));
                        w1.setIdSentence(rs.getInt("w1_id_sentence"));
                        w1.setWord(rs.getString("w1_word"));
                        w1.setContraction(rs.getString("w1_contraction"));
                        w1.setPos(rs.getString("w1_pos"));
                        //word target
                        Words w2 = new Words();
                        w2.setId(rs.getInt("w2_id"));
                        w2.setIdCorpus(rs.getInt("w2_id_corpus"));
                        w2.setIdText(rs.getInt("w2_id_text"));
                        w2.setIdSentence(rs.getInt("w2_id_sentence"));
                        w2.setWord(rs.getString("w2_word"));
                        w2.setContraction(rs.getString("w2_contraction"));
                        w2.setPos(rs.getString("w2_pos"));
                        //Text source
                        w1.setText(TextDAO.selectById(rs.getInt("w1_id_text")));
                        //Text target
                        w2.setText(TextDAO.selectById(rs.getInt("w2_id_text")));
                        //add
                        entries.put(w1, w2);
                    }
                }
                //sizes
                concordancer.setTotalConcordances(entries.size());
                concordancer.setTotalPages((entries.size() + (concordancer.getResultsByPage() - 1)) / concordancer.getResultsByPage());
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(ConcordancerAlignDAO.class, ex);
        }
        return entries;
    }

    private static String buildSQL(String search, int id_corpus, int tolerance, List<TextTagValues> metadata, int limit, int offset) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT w0.id ")
                .append(buildFromSQL(search)).append(" ")//from
                .append(buildWhereSQL(search, id_corpus, tolerance, metadata));//where
        if (limit > 0) {
            sql.append(" LIMIT ").append(limit)
                    .append(" OFFSET ").append(offset);
        }
        return sql.toString();
    }

    private static String buildFromSQL(String search) {
        String[] terms = search.trim().split(" ");//AND
        StringBuilder sql = new StringBuilder();
        sql.append("FROM words_align, ");
        for (int i = 0; i < terms.length; i++) {
            sql.append("words w").append(i);
            if ((i + 1) < terms.length) {//has next
                sql.append(", ");
            }
        }
        return sql.toString();
    }

    private static String buildWhereSQL(String search, int id_corpus, int tolerance, List<TextTagValues> metadata) {
        StringBuilder sql = new StringBuilder();
        sql.append("WHERE ")
                .append(whereTerms(search, 0, tolerance))
                .append(" AND w0.id_corpus = ").append(id_corpus)
                .append(" AND (w0.id = words_align.id_source or w0.id = words_align.id_target)");
        if (metadata != null && metadata.size() > 0) {
            sql.append(" AND w0.id_text IN (").append(whereMeta(metadata)).append(")");
        }
        return sql.toString();
    }

    private static String whereTerms(String search, int count, int tolerance) {
        search = search.trim();
        if (search.contains(" ")) {//AND
            String[] terms = search.split(" ");
            StringBuilder sql = new StringBuilder();
            for (int i = 0; i < terms.length; i++) {
                sql.append(whereTerms(terms[i], count + i, tolerance));
                if (i > 0) {//conect the terms to the order into the same text
                    if (tolerance > 0) {//tolerance
                        sql.append(" AND (w").append(i).append(".position BETWEEN w0.position AND w0.position + ").append(i + tolerance).append(") AND w0.id_text = w").append(i).append(".id_text ");
                    } else {
                        sql.append(" AND w").append(i).append(".position = (w0.position + ").append(i).append(") AND w0.id_text = w").append(i).append(".id_text ");
                    }
                }
                if ((i + 1) < terms.length) {//has next
                    sql.append(" AND ");
                }
            }
            return sql.toString();
        } else if (search.contains("+")) {//OR
            String[] terms = search.split("\\+");
            StringBuilder sql = new StringBuilder();
            sql.append("(");
            for (int i = 0; i < terms.length; i++) {
                sql.append(whereTerms(terms[i], count, tolerance));
                if ((i + 1) < terms.length) {//has next
                    sql.append(" OR ");
                }
            }
            sql.append(")");
            return sql.toString();
        } else if (search.contains(",")) {//multi modifiers
            String[] terms = search.split("\\,");
            StringBuilder sql = new StringBuilder();
            sql.append("(");
            for (int i = 0; i < terms.length; i++) {
                sql.append(whereTerms(terms[i], count, tolerance));
                if ((i + 1) < terms.length) {//has next
                    sql.append(" AND ");
                }
            }
            sql.append(")");
            return sql.toString();
        } else if (search.toLowerCase().contains("prefix:")) {//prefix
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".word) LIKE lower('" + terms[1] + "%')";
        } else if (search.toLowerCase().contains("infix:")) {//infix
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".word) LIKE lower('%" + terms[1] + "%')";
        } else if (search.toLowerCase().contains("suffix:")) {//suffix
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".word) LIKE lower('%" + terms[1] + "')";
        } else if (search.toLowerCase().contains("pos:")) {//pos
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".pos) = lower('" + terms[1] + "')";
        } else if (search.toLowerCase().contains("lemma:")) {//lemma
            String[] terms = search.split(":", 2);
            return "lower(w" + count + ".lemma) = lower('" + terms[1] + "')";
        }
        return "lower(w" + count + ".word) = lower('" + search + "')";
    }

    public static String whereMeta(List<TextTagValues> metadata) {
        StringBuilder sql = new StringBuilder();
        //Texts metadata
        if (metadata != null && metadata.size() > 0) {
            sql.append("SELECT m.id_text FROM text_tag_values m WHERE ");
            for (int i = 0; i < metadata.size(); i++) {
                sql.append("(m.id_text_tag = ").append(metadata.get(i).getIdTextTag()).append(" AND lower(m.value) = lower('").append(metadata.get(i).getValue()).append("'))");
                if ((i + 1) < metadata.size()) {//has next
                    sql.append(" OR ");
                }
            }
        }
        return sql.toString();
    }
}
