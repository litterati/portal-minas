package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Subcorpus;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * SubcorpusDAO
 *
 * @author Thiago Vieira
 * @since 16/01/2015
 */
public class SubcorpusDAO {

    public static final String TABLE_NAME = "subcorpus";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";

    public static Subcorpus load(ResultSet rs) throws SQLException {
        Subcorpus obj = new Subcorpus();
        obj.setId(rs.getInt(TABLE_NAME + "_" + ID));
        obj.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        obj.setName(rs.getString(TABLE_NAME + "_" + NAME));
        obj.setDescription(rs.getString(TABLE_NAME + "_" + DESCRIPTION));
        return obj;
    }

    public static List<Subcorpus> loadList(ResultSet rs) throws SQLException {
        List<Subcorpus> list = new ArrayList<>();
        while (rs.next()) {
            Subcorpus obj = new Subcorpus();
            obj.setId(rs.getInt(TABLE_NAME + "_" + ID));
            obj.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            obj.setName(rs.getString(TABLE_NAME + "_" + NAME));
            obj.setDescription(rs.getString(TABLE_NAME + "_" + DESCRIPTION));
            // add into the list
            list.add(obj);
        }
        return list;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + NAME + " AS " + TABLE_NAME + "_" + NAME + ", "
                + TABLE_NAME + "." + DESCRIPTION + " AS " + TABLE_NAME + "_" + DESCRIPTION;
    }

    public static List<Subcorpus> selectAll(String order) throws DAOException {
        List<Subcorpus> list = new ArrayList<>();
        //order
        if (order == null) {
            order = SubcorpusDAO.NAME;
        } else if (!order.equals(SubcorpusDAO.ID) && !order.equals(SubcorpusDAO.DESCRIPTION)) {
            order = SubcorpusDAO.NAME;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = SubcorpusDAO.getFields();
            String from = TABLE_NAME;
            String sql = "SELECT " + fields 
                    + " FROM " + from 
                    + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery()) {
                list = SubcorpusDAO.loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static Subcorpus selectById(int id) throws DAOException {
        Subcorpus obj = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = SubcorpusDAO.getFields();
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "SELECT " + fields 
                    + " FROM " + from 
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        obj = SubcorpusDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return obj;
    }
    
    public static List<Subcorpus> selectAllByIdCorpus(int id_corpus, String order) throws DAOException {
        List<Subcorpus> list = new ArrayList<>();
        //order
        if (order == null) {
            order = SubcorpusDAO.NAME;
        } else if (!order.equals(SubcorpusDAO.ID) && !order.equals(SubcorpusDAO.DESCRIPTION)) {
            order = SubcorpusDAO.NAME;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = SubcorpusDAO.getFields();
            String from = TABLE_NAME;
            String where = ID_CORPUS + " = ? ";
            String sql = "SELECT " + fields 
                    + " FROM " + from 
                    + " WHERE " + where 
                    + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                try (ResultSet rs = ps.executeQuery()) {
                    list = SubcorpusDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static void insert(Subcorpus obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = ID_CORPUS + ", " + NAME + ", " + DESCRIPTION;
            String sql = "INSERT INTO " + into 
                    + " ( " + fields + " ) VALUES (?, ?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setInt(1, obj.getIdCorpus());
                ps.setString(2, obj.getName());
                ps.setString(3, obj.getDescription());
                //execute
                ps.execute();
                //generete keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        obj.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(Subcorpus obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String table = TABLE_NAME;
            String fields = ID_CORPUS + " = ? , " + NAME + " = ? , " + DESCRIPTION + " = ? ";
            String where = ID + " = ? ";
            String sql = "UPDATE " + table 
                    + " SET " + fields 
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, obj.getIdCorpus());
                ps.setString(2, obj.getName());
                ps.setString(3, obj.getDescription());
                ps.setInt(4, obj.getId());
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(Subcorpus obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "DELETE FROM " + from 
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, obj.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
