/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Task;
import br.project.exception.DAOException;
import br.project.task.TaskInterface;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * TaskDAO
 *
 * @author Thiago Vieira
 * @since 28/09/2014
 */
public class TaskDAO {

    public static final String TABLE_NAME = "task";
    public static final String ID = "id";
    public static final String ID_USER = "id_user";
    public static final String TYPE = "type";
    public static final String PROGRESS = "progress";
    public static final String STATUS = "status";
    public static final String RESPONSE = "response";
    public static final String DATE_INSERTED = "date_inserted";
    public static final String DATE_STARTED = "date_started";
    public static final String DATE_FINISHED = "date_finished";
    public static final String JAVA_OBJECT = "java_object";

    public static Task load(ResultSet rs) throws SQLException {
        Task task = new Task();
        task.setId(rs.getInt(TABLE_NAME + "_" + ID));
        task.setIdUser(rs.getInt(TABLE_NAME + "_" + ID_USER));
        task.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
        task.setProgress(rs.getInt(TABLE_NAME + "_" + PROGRESS));
        task.setStatus(rs.getInt(TABLE_NAME + "_" + STATUS));
        task.setResponse(rs.getString(TABLE_NAME + "_" + RESPONSE));
        task.setDateInserted(rs.getTimestamp(TABLE_NAME + "_" + DATE_INSERTED) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_INSERTED).getTime()));
        task.setDateStarted(rs.getTimestamp(TABLE_NAME + "_" + DATE_STARTED) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_STARTED).getTime()));
        task.setDateFinished(rs.getTimestamp(TABLE_NAME + "_" + DATE_FINISHED) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_FINISHED).getTime()));
        //java object from a blob
        Object java_object = rs.getObject(TABLE_NAME + "_" + JAVA_OBJECT);
        if (java_object != null) {
            try {
                byte[] st = (byte[]) java_object;
                ByteArrayInputStream baip = new ByteArrayInputStream(st);
                ObjectInputStream ois = new ObjectInputStream(baip);
                task.setJavaObject((TaskInterface) ois.readObject());
            } catch (IOException | ClassNotFoundException ex) {
                task.setJavaObject(null);
            }
        } else {
            task.setJavaObject(null);
        }
        return task;
    }

    public static List<Task> loadList(ResultSet rs) throws SQLException {
        List<Task> taskList = new ArrayList<>();
        while (rs.next()) {
            Task task = new Task();
            task.setId(rs.getInt(TABLE_NAME + "_" + ID));
            task.setIdUser(rs.getInt(TABLE_NAME + "_" + ID_USER));
            task.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
            task.setProgress(rs.getInt(TABLE_NAME + "_" + PROGRESS));
            task.setStatus(rs.getInt(TABLE_NAME + "_" + STATUS));
            task.setResponse(rs.getString(TABLE_NAME + "_" + RESPONSE));
            task.setDateInserted(rs.getTimestamp(TABLE_NAME + "_" + DATE_INSERTED) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_INSERTED).getTime()));
            task.setDateStarted(rs.getTimestamp(TABLE_NAME + "_" + DATE_STARTED) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_STARTED).getTime()));
            task.setDateFinished(rs.getTimestamp(TABLE_NAME + "_" + DATE_FINISHED) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_FINISHED).getTime()));
            //java object from a blob
            Object java_object = rs.getObject(TABLE_NAME + "_" + JAVA_OBJECT);
            if (java_object != null) {
                try {
                    byte[] st = (byte[]) java_object;
                    ByteArrayInputStream baip = new ByteArrayInputStream(st);
                    ObjectInputStream ois = new ObjectInputStream(baip);
                    task.setJavaObject((TaskInterface) ois.readObject());
                } catch (IOException | ClassNotFoundException ex) {
                    task.setJavaObject(null);
                }
            } else {
                task.setJavaObject(null);
            }
            // add into the list
            taskList.add(task);
        }
        return taskList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_USER + " AS " + TABLE_NAME + "_" + ID_USER + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE + ", "
                + TABLE_NAME + "." + PROGRESS + " AS " + TABLE_NAME + "_" + PROGRESS + ", "
                + TABLE_NAME + "." + RESPONSE + " AS " + TABLE_NAME + "_" + RESPONSE + ", "
                + TABLE_NAME + "." + STATUS + " AS " + TABLE_NAME + "_" + STATUS + ", "
                + TABLE_NAME + "." + DATE_INSERTED + " AS " + TABLE_NAME + "_" + DATE_INSERTED + ", "
                + TABLE_NAME + "." + DATE_STARTED + " AS " + TABLE_NAME + "_" + DATE_STARTED + ", "
                + TABLE_NAME + "." + DATE_FINISHED + " AS " + TABLE_NAME + "_" + DATE_FINISHED + ", "
                + TABLE_NAME + "." + JAVA_OBJECT + " AS " + TABLE_NAME + "_" + JAVA_OBJECT;
    }

    public static List<Task> selectQueue() {
        List<Task> lstTasks = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TaskDAO.getFields()
                    + " FROM " + TABLE_NAME + " "
                    + " WHERE " + TABLE_NAME + "." + STATUS + " = ? "
                    + " ORDER BY " + TABLE_NAME + "." + DATE_INSERTED + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, Task.STATUS_WAITING);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    lstTasks = TaskDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstTasks;
    }
    
    public static List<Task> selectAllByIdUser(int id_user) {
        List<Task> lstTasks = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TaskDAO.getFields()
                    + " FROM " + TABLE_NAME + " "
                    + " WHERE " + TABLE_NAME + "." + ID_USER + " = ? "
                    + " ORDER BY " + TABLE_NAME + "." + DATE_INSERTED + " DESC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_user);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    lstTasks = TaskDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstTasks;
    }

    public static List<Task> selectByIdUser(int id_user, int offset, int limit) {
        List<Task> lstTasks = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TaskDAO.getFields()
                    + " FROM " + TABLE_NAME + " "
                    + " WHERE " + TABLE_NAME + "." + ID_USER + " = ? "
                    + " ORDER BY " + TABLE_NAME + "." + DATE_INSERTED + " DESC "
                    + " LIMIT ? "
                    + " OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_user);
                ps.setInt(2, limit);
                ps.setInt(3, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    lstTasks = TaskDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstTasks;
    }

    public static Task selectById(int id) {
        Task task = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TaskDAO.getFields()
                    + " FROM " + TABLE_NAME + " "
                    + " WHERE " + TABLE_NAME + "." + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        task = TaskDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            //throw new DAOException(ex);
        }
        return task;
    }

    /**
     * Count the number of tasks in the database.
     *
     * @param idUser The user ID to query.
     * @return The number of text or -1.
     */
    public static int countByIdUser(int idUser) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_USER + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idUser);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            //throw new DAOException(e);
        }
        return value;
    }
    
    public static int countByStatus(int status) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + STATUS + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, status);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            //throw new DAOException(e);
        }
        return value;
    }

    public static void insert(Task task) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_USER + ", " + TYPE + ", " + PROGRESS + ", " + STATUS + ", " + RESPONSE + ", " + DATE_INSERTED + ", " + JAVA_OBJECT + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setInt(1, task.getIdUser());
                ps.setInt(2, task.getType());
                ps.setInt(3, task.getProgress());
                ps.setInt(4, task.getStatus());
                ps.setString(5, task.getResponse());
                java.util.Date date = new java.util.Date();
                ps.setTimestamp(6, new Timestamp(date.getTime()));
                //java object to blob
                if (task.getJavaObject() != null) {
                    try {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ObjectOutputStream oos = new ObjectOutputStream(baos);
                        oos.writeObject(task.getJavaObject());
                        byte[] taskAsBytes = baos.toByteArray();
                        ByteArrayInputStream bais = new ByteArrayInputStream(taskAsBytes);
                        ps.setBinaryStream(7, bais, taskAsBytes.length);
                    } catch (IOException ex) {
                        ps.setNull(7, java.sql.Types.BLOB);
                    }
                } else {
                    ps.setNull(7, java.sql.Types.BLOB);
                }
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        //Set the task object with the id created when the task was inserted
                        task.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(Task task) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + TYPE + " = ?, " + PROGRESS + " = ?, " + STATUS + " = ?, " + RESPONSE + " = ?, " + DATE_STARTED + " = ?, " + DATE_FINISHED + " = ?, " + JAVA_OBJECT + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, task.getType());
                //not update id_user
                ps.setInt(2, task.getProgress());
                ps.setInt(3, task.getStatus());
                ps.setString(4, task.getResponse());
                //not necessary update date_inserted
                //time started
                if (task.getDateStarted() != null){
                    ps.setTimestamp(5, new Timestamp(task.getDateStarted().getTime()));
                } else {
                    ps.setNull(5, java.sql.Types.TIMESTAMP);
                }
                //time finished
                if (task.getDateFinished() != null){
                    ps.setTimestamp(6, new Timestamp(task.getDateFinished().getTime()));
                } else {
                    ps.setNull(6, java.sql.Types.TIMESTAMP);
                }
                //java object to blob
                if (task.getJavaObject() != null) {
                    try {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ObjectOutputStream oos = new ObjectOutputStream(baos);
                        oos.writeObject(task.getJavaObject());
                        byte[] taskAsBytes = baos.toByteArray();
                        ByteArrayInputStream bais = new ByteArrayInputStream(taskAsBytes);
                        ps.setBinaryStream(7, bais, taskAsBytes.length);
                    } catch (IOException ex) {
                        ps.setNull(7, java.sql.Types.BLOB);
                    }
                } else {
                    ps.setNull(7, java.sql.Types.BLOB);
                }
                ps.setInt(8, task.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(Task task) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, task.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TaskDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
