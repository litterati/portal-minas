/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.WordTags;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * WordTagsDAO
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class WordTagsDAO {

    public static final String TABLE_NAME = "word_tags";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String TAG = "tag";
    public static final String XCES = "xces";

    public static WordTags load(ResultSet rs) throws SQLException {
        WordTags wt = new WordTags();
        wt.setId(rs.getInt(TABLE_NAME + "_" + ID));
        wt.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        wt.setTag(rs.getString(TABLE_NAME + "_" + TAG));
        wt.setXces(rs.getString(TABLE_NAME + "_" + XCES));
        return wt;
    }

    public static List<WordTags> loadList(ResultSet rs) throws SQLException {
        List<WordTags> wtList = new ArrayList<>();
        while (rs.next()) {
            WordTags wt = new WordTags();
            wt.setId(rs.getInt(TABLE_NAME + "_" + ID));
            wt.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            wt.setTag(rs.getString(TABLE_NAME + "_" + TAG));
            wt.setXces(rs.getString(TABLE_NAME + "_" + XCES));
            // add into the list
            wtList.add(wt);
        }
        return wtList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + TAG + " AS " + TABLE_NAME + "_" + TAG + ", "
                + TABLE_NAME + "." + XCES + " AS " + TABLE_NAME + "_" + XCES;
    }

    public static void insert(WordTags wordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            WordTagsDAO.insert(wordTag, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(WordTags wordTag, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_CORPUS + ", " + TAG + ", " + XCES + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                ps.setInt(1, wordTag.getIdCorpus());
                ps.setString(2, wordTag.getTag());
                ps.setString(3, wordTag.getXces());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        wordTag.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(WordTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<WordTags> selectAllByIdCorpus(int idCorpus) {
        List<WordTags> lstWordTag = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID_CORPUS + " = " + idCorpus
                    + " ORDER BY " + TAG + " ASC";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    WordTags wordTag;
                    while (rs.next()) {
                        wordTag = new WordTags();
                        wordTag.setId(rs.getInt(ID));
                        wordTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        wordTag.setTag(rs.getString(TAG));
                        wordTag.setXces(rs.getString(XCES));
                        lstWordTag.add(wordTag);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagsDAO.class, ex);
            //throw new RuntimeException("Erro em WordsTagsDAO.listWordTagsByCorpus(). " + e.getMessage());
        }
        return lstWordTag;
    }

    public static List<WordTags> selectWordTags(int idCorpus) throws DAOException {
        List<WordTags> lstWordTag = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID_CORPUS + " = " + idCorpus + " AND xces <> 'xces.style'"
                    + " ORDER BY " + TAG + " ASC";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                try (ResultSet rs = ps.executeQuery()) {

                    WordTags wordTag;
                    while (rs.next()) {
                        wordTag = new WordTags();
                        wordTag.setId(rs.getInt(ID));
                        wordTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        wordTag.setTag(rs.getString(TAG));
                        wordTag.setXces(rs.getString(XCES));
                        lstWordTag.add(wordTag);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagsDAO.class, ex);
            //throw new RuntimeException("Erro em WordsTagsDAO.selectStyleWordTags(). " + e.getMessage());
        }
        return lstWordTag;
    }

    public static List<WordTags> selectStyleWordTags(int idCorpus) throws DAOException {
        List<WordTags> lstWordTag = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME 
                    + " WHERE " + ID_CORPUS + " = " + idCorpus + " AND xces = 'xces.style'"
                    + " ORDER BY " + TAG + " ASC";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    WordTags wordTag;
                    while (rs.next()) {
                        wordTag = new WordTags();
                        wordTag.setId(rs.getInt(ID));
                        wordTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        wordTag.setTag(rs.getString(TAG));
                        wordTag.setXces(rs.getString(XCES));
                        //add to list
                        lstWordTag.add(wordTag);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagsDAO.class, ex);
            //throw new RuntimeException("Erro em WordsTagsDAO.selectStyleWordTags(). " + e.getMessage());
        }
        return lstWordTag;
    }

    public static WordTags find(WordTags wordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID_CORPUS + " = " + wordTag.getIdCorpus() + " AND " + TAG + " = '" + wordTag.getTag() + "'";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        wordTag = new WordTags();
                        wordTag.setId(rs.getInt(ID));
                        wordTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        wordTag.setTag(rs.getString(TAG));
                        wordTag.setXces(rs.getString(XCES));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagsDAO.class, ex);
            throw new DAOException(ex);
            //throw new RuntimeException("Erro em WordsTagsDAO.find(). " + ex.getMessage());
        }
        if (wordTag.getId() > 0) {
            return wordTag;
        } else {
            return null;
        }
    }

    public static void deleteByTag(WordTags wordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE LOWER(" + TAG + ") = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, wordTag.getTag());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            throw new DAOException(ex);
        }
    }

    public static void deleteById(WordTags wordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, wordTag.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.close();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            throw new DAOException(ex);
        }
    }
}
