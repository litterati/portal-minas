/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.CorpusTools;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CorpusToolsDAO.java
 *
 * @author Thiago Vieira
 * @since 14/07/2014
 */
public class CorpusToolsDAO {

    public static final String TABLE_NAME = "corpus_tools";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String TOOL = "tool";

    public static CorpusTools load(ResultSet rs) throws SQLException {
        CorpusTools ct = new CorpusTools();
        ct.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ct.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        ct.setTool(rs.getString(TABLE_NAME + "_" + TOOL));
        return ct;
    }

    public static List<CorpusTools> loadList(ResultSet rs) throws SQLException {
        List<CorpusTools> ctList = new ArrayList<>();
        while (rs.next()) {
            CorpusTools ct = new CorpusTools();
            ct.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ct.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            ct.setTool(rs.getString(TABLE_NAME + "_" + TOOL));
            // add into the list
            ctList.add(ct);
        }
        return ctList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + TOOL + " AS " + TABLE_NAME + "_" + TOOL;
    }

    public static void insert(CorpusTools object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_CORPUS + ", " + TOOL + ") "
                    + "VALUES (?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdCorpus());
                ps.setString(2, object.getTool());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusToolsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(CorpusTools object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + ID_CORPUS + " = ?, " + TOOL + " = ?  "
                    + "WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdCorpus());
                ps.setString(2, object.getTool());
                ps.setInt(3, object.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusToolsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(CorpusTools object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusToolsDAO.class, ex);
            throw new DAOException(ex);
        }
    }
    
    public static void deleteAllByIdCorpus(int id_corpus) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_CORPUS + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusToolsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<CorpusTools> selectAll(int id_corpus) throws DAOException {
        List<CorpusTools> listCorpusTools = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + CorpusToolsDAO.getFields() 
                    + " FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_CORPUS + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    listCorpusTools = CorpusToolsDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusToolsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listCorpusTools;
    }

}
