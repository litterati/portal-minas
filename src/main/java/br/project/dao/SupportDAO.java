/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Support;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TextAlignDAO
 *
 * @author Thiago Vieira
 * @since 28/08/2014
 */
public class SupportDAO {

    public static final String TABLE_NAME = "support";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String SUBJECT = "subject";
    public static final String MESSAGE = "message";
    public static final String STATUS = "status";

    public static Support load(ResultSet rs) throws SQLException {
        return new Support(
                rs.getInt(TABLE_NAME + "_" + ID),
                rs.getString(TABLE_NAME + "_" + NAME),
                rs.getString(TABLE_NAME + "_" + EMAIL),
                rs.getString(TABLE_NAME + "_" + SUBJECT),
                rs.getString(TABLE_NAME + "_" + MESSAGE),
                rs.getInt(TABLE_NAME + "_" + STATUS)
        );
    }

    public static List<Support> loadList(ResultSet rs) throws SQLException {
        List<Support> supportList = new ArrayList<>();
        while (rs.next()) {
            Support support = new Support(
                    rs.getInt(TABLE_NAME + "_" + ID),
                    rs.getString(TABLE_NAME + "_" + NAME),
                    rs.getString(TABLE_NAME + "_" + EMAIL),
                    rs.getString(TABLE_NAME + "_" + SUBJECT),
                    rs.getString(TABLE_NAME + "_" + MESSAGE),
                    rs.getInt(TABLE_NAME + "_" + STATUS)
            );
            // add into the list
            supportList.add(support);
        }
        return supportList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + NAME + " AS " + TABLE_NAME + "_" + NAME + ", "
                + TABLE_NAME + "." + EMAIL + " AS " + TABLE_NAME + "_" + EMAIL + ", "
                + TABLE_NAME + "." + SUBJECT + " AS " + TABLE_NAME + "_" + SUBJECT + ", "
                + TABLE_NAME + "." + MESSAGE + " AS " + TABLE_NAME + "_" + MESSAGE + ", "
                + TABLE_NAME + "." + STATUS + " AS " + TABLE_NAME + "_" + STATUS;
    }

    public static void insert(Support object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + NAME + ", " + EMAIL + ", " + SUBJECT + ", " + MESSAGE + ", " + STATUS + ") "
                    + "VALUES (?, ?, ?, ? ,?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, object.getName());
                ps.setString(2, object.getEmail());
                ps.setString(3, object.getSubject());
                ps.setString(4, object.getMessage());
                ps.setInt(5, object.getStatus());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SupportDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(Support object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + NAME + " = ? , " + EMAIL + " = ? , " + SUBJECT + " = ? , " + MESSAGE + " = ? , " + STATUS + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, object.getName());
                ps.setString(2, object.getEmail());
                ps.setString(3, object.getSubject());
                ps.setString(4, object.getMessage());
                ps.setInt(5, object.getStatus());
                ps.setInt(6, object.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SupportDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(Support object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SupportDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static Support selectById(int id) {
        Support support = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + SupportDAO.getFields()
                    + " FROM " + TABLE_NAME 
                    + " WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()){
                        support = SupportDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SupportDAO.class, ex);
            //throw new DAOException(ex);
        }
        return support;
    }

    public static List<Support> selectAll() {
        List<Support> supportList = new ArrayList();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + SupportDAO.getFields()
                    + " FROM " + TABLE_NAME + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    supportList = SupportDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SupportDAO.class, ex);
            //throw new DAOException(ex);
        }
        return supportList;
    }
}
