package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.FieldsType;
import br.project.entity.Text;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TextTagsDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextTagsDAO {

    public static final String TABLE_NAME = "text_tags";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String TAG = "tag";
    public static final String IS_REQUIRED = "is_required";
    public static final String ID_FIELDS_TYPE = "id_fields_type";

    public static TextTags load(ResultSet rs) throws SQLException {
        TextTags tg = new TextTags();
        tg.setId(rs.getInt(TABLE_NAME + "_" + ID));
        tg.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        tg.setTag(rs.getString(TABLE_NAME + "_" + TAG));
        tg.setIsRequired(rs.getBoolean(TABLE_NAME + "_" + IS_REQUIRED));
        tg.setIdFieldType(rs.getInt(TABLE_NAME + "_" + ID_FIELDS_TYPE));
        return tg;
    }

    public static List<TextTags> loadList(ResultSet rs) throws SQLException {
        List<TextTags> tgList = new ArrayList<>();
        while (rs.next()) {
            TextTags tg = new TextTags();
            tg.setId(rs.getInt(TABLE_NAME + "_" + ID));
            tg.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            tg.setTag(rs.getString(TABLE_NAME + "_" + TAG));
            tg.setIsRequired(rs.getBoolean(TABLE_NAME + "_" + IS_REQUIRED));
            tg.setIdFieldType(rs.getInt(TABLE_NAME + "_" + ID_FIELDS_TYPE));
            // add into the select
            tgList.add(tg);
        }
        return tgList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + TAG + " AS " + TABLE_NAME + "_" + TAG + ", "
                + TABLE_NAME + "." + IS_REQUIRED + " AS " + TABLE_NAME + "_" + IS_REQUIRED + ", "
                + TABLE_NAME + "." + ID_FIELDS_TYPE + " AS " + TABLE_NAME + "_" + ID_FIELDS_TYPE;
    }

    public static void deleteById(TextTags textsMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, textsMetadata.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static boolean existTag(TextTags corpusMetadata) throws DAOException {
        boolean exist = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + " "
                    + "FROM " + TABLE_NAME + " c "
                    + "WHERE c." + ID_CORPUS + " = ? AND lower(c." + TAG + ") = lower(?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpusMetadata.getIdCorpus());
                ps.setString(2, corpusMetadata.getTag());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        exist = true;
                    }
                } catch (SQLException ex) {
                    c.rollback();
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return exist;
    }

    public static boolean existTagCorpus(TextTags corpusMetadata) throws DAOException {
        boolean exist = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + " "
                    + "FROM " + TABLE_NAME + " c "
                    + "WHERE c." + ID_CORPUS + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpusMetadata.getIdCorpus());
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        exist = true;
                    }
                } catch (SQLException ex) {
                    c.rollback();
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return exist;
    }

    public static void insert(TextTags textMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_CORPUS + ", " + TAG + ", " + IS_REQUIRED + ", " + ID_FIELDS_TYPE + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setInt(1, textMetadata.getIdCorpus());
                ps.setString(2, textMetadata.getTag());
                ps.setBoolean(3, textMetadata.isIsRequired());
                ps.setInt(4, textMetadata.getIdFieldType());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        textMetadata.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<TextTags> select(TextTags textMetadata) throws DAOException {
        List<TextTags> listTextsMetadata = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            int idCorpus = textMetadata.getIdCorpus();
            //prepare sql
            String sql = "SELECT " + ID + ", " + TAG + ", " + IS_REQUIRED + ", " + ID_FIELDS_TYPE + ", "
                    + "  ("
                    + "    SELECT " + FieldsTypeDAO.VALUE + " "
                    + "    FROM " + FieldsTypeDAO.TABLE_NAME + " f "
                    + "    WHERE f." + FieldsTypeDAO.ID + " = c." + ID_FIELDS_TYPE + " "
                    + "  ) AS name_field_type "
                    + "FROM " + TABLE_NAME + " c "
                    + "WHERE c." + ID_CORPUS + " = ? "
                    + "ORDER BY " + ID + " DESC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        //fields type
                        FieldsType fieldsType = new FieldsType();
                        fieldsType.setId(rs.getInt(ID_FIELDS_TYPE));
                        fieldsType.setValue(rs.getString("name_field_type"));
                        //text Metadata
                        TextTags textMetadata2 = new TextTags();
                        textMetadata2.setIdCorpus(idCorpus);
                        textMetadata2.setId(rs.getInt(ID));
                        textMetadata2.setTag(rs.getString(TAG));
                        textMetadata2.setIsRequired(rs.getBoolean(IS_REQUIRED));
                        textMetadata2.setIdFieldType(rs.getInt(ID_FIELDS_TYPE));
                        textMetadata2.setFieldType(fieldsType);
                        //add to list
                        listTextsMetadata.add(textMetadata2);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return listTextsMetadata;
    }

    public static void selectIdMetadata(TextTags textsMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + " "
                    + "FROM " + TABLE_NAME + " c "
                    + "WHERE c." + ID_CORPUS + " = ? AND lower(c." + TAG + ") = lower(?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, textsMetadata.getIdCorpus());
                ps.setString(2, textsMetadata.getTag());
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        textsMetadata.setId(rs.getInt(ID));
                    } else {
                        textsMetadata.setId(0); //Metadata doesn't exist
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<Integer> selectByIdCorpus(Text text) throws DAOException {
        List<Integer> lstIdMetadata = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + " "
                    + "FROM " + TABLE_NAME + " c "
                    + "WHERE c." + ID_CORPUS + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getIdCorpus());
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        lstIdMetadata.add(rs.getInt(ID));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstIdMetadata;
    }

    public static List<TextTags> selectByIdCorpus(int idCorpus) throws DAOException {
        List<TextTags> lstTextTags = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME
                    + " WHERE " + ID_CORPUS + " = ? "
                    + " ORDER BY " + TAG + " ASC";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                try (ResultSet rs = ps.executeQuery()) {
                    TextTags textTag;
                    while (rs.next()) {
                        textTag = new TextTags();
                        textTag.setId(rs.getInt(ID));
                        textTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        textTag.setTag(rs.getString(TAG));
                        textTag.setIsRequired(rs.getBoolean(IS_REQUIRED));
                        textTag.setIdFieldType(rs.getInt(ID_FIELDS_TYPE));
                        lstTextTags.add(textTag);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new RuntimeException("Erro em TextTagsDAO.selectByIdCorpus(). " + e.getMessage());
            //throw new DAOException(ex);
        }
        return lstTextTags;
    }

    public static void update(TextTags textsMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + TAG + " = ?, " + IS_REQUIRED + " = ?, " + ID_FIELDS_TYPE + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, textsMetadata.getTag());
                ps.setBoolean(2, textsMetadata.isIsRequired());
                ps.setInt(3, textsMetadata.getIdFieldType());
                ps.setInt(4, textsMetadata.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static TextTags selectById(int id) throws DAOException {
        TextTags textMetadata = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        textMetadata = new TextTags();
                        textMetadata.setId(rs.getInt(ID));
                        textMetadata.setTag(rs.getString(TAG));
                        textMetadata.setIsRequired(rs.getBoolean(IS_REQUIRED));
                        textMetadata.setIdFieldType(rs.getInt(ID_FIELDS_TYPE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return textMetadata;
    }
    
    public static List<TextTags> selectDistinctWithTagValuesByIdCorpus(int id_corpus) throws DAOException {
        List<TextTags> list = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = " DISTINCT ON (" + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.VALUE + ") " 
                    + TextTagsDAO.getFields()
                    +  ", " + TextTagValuesDAO.getFields();
            String from = TextTagsDAO.TABLE_NAME
                    + ", " + TextTagValuesDAO.TABLE_NAME;
            String where = TextTagsDAO.TABLE_NAME + "." + TextTagsDAO.ID_CORPUS  + " = ? "
                    + " AND " + TextTagsDAO.TABLE_NAME + "." + TextTagsDAO.ID + " = " + TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.ID_TEXT_TAG;
            String order = TextTagValuesDAO.TABLE_NAME + "." + TextTagValuesDAO.VALUE 
                    + ", " + TextTagsDAO.TABLE_NAME + "." + TextTagsDAO.ID + " ASC ";
            String sql = "SELECT " + fields 
                    + " FROM " + from 
                    + " WHERE " + where 
                    + " ORDER BY " + order;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    TextTags textTag = null;
                    while (rs.next()) {
                        TextTags aux = TextTagsDAO.load(rs);
                        if ((textTag == null) || (textTag.getId() != aux.getId())){
                            textTag = aux;
                            //add to list
                            boolean flag = false;
                            for (TextTags tt : list) {
                                if (tt.getId() == aux.getId()){
                                    textTag = tt;
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag){
                                list.add(textTag);
                            }
                        }
                        TextTagValues textTagValue = TextTagValuesDAO.load(rs);
                        //complete
                        textTagValue.setTextTag(textTag);
                        textTag.addTextTagsValues(textTagValue);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }
}
