package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.AlignTag;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AlignTagDAO
 *
 * @author Thiago Vieira
 * @since 22/10/2014
 */
public class AlignTagDAO {

    public static final String TABLE_NAME = "align_tag";
    public static final String ID = "id";
    public static final String VALUE = "value";
    public static final String ID_CORPUS = "id_corpus";

    public static AlignTag load(ResultSet rs) throws SQLException {
        AlignTag tag = new AlignTag();
        tag.setId(rs.getInt(TABLE_NAME + "_" + ID));
        tag.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        tag.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        return tag;
    }

    public static List<AlignTag> loadList(ResultSet rs) throws SQLException {
        List<AlignTag> tagList = new ArrayList<>();
        while (rs.next()) {
            AlignTag tag = new AlignTag();
            tag.setId(rs.getInt(TABLE_NAME + "_" + ID));
            tag.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            tag.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            // add into the list
            tagList.add(tag);
        }
        return tagList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS;
    }

    public static List<AlignTag> selectAll() throws DAOException {
        List<AlignTag> tagList = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AlignTagDAO.getFields();
            String from = TABLE_NAME;
            String sql = "SELECT " + fields + " FROM " + from;
            try (PreparedStatement ps = c.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery()) {
                tagList = AlignTagDAO.loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AlignTagDAO.class, ex);
            //throw new DAOException(ex);
        }
        return tagList;
    }

    public static List<AlignTag> selectAllByIdCorpus(int id_corpus) throws DAOException {
        List<AlignTag> tagList = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AlignTagDAO.getFields();
            String from = TABLE_NAME;
            String where = TABLE_NAME + "." + ID_CORPUS + " = ? ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                try (ResultSet rs = ps.executeQuery()) {
                    tagList = AlignTagDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AlignTagDAO.class, ex);
            //throw new DAOException(ex);
        }
        return tagList;
    }

    public static Map<Integer, AlignTag> selectAllMap() throws DAOException {
        Map<Integer, AlignTag> tagMap = new HashMap<>();
        List<AlignTag> tagList = selectAll();
        for (AlignTag alignTag : tagList) {
            tagMap.put(alignTag.getId(), alignTag);
        }
        return tagMap;
    }

    public static AlignTag selectById(int id) throws DAOException {
        AlignTag tag = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AlignTagDAO.getFields();
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "SELECT " + fields + " FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        tag = AlignTagDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AlignTagDAO.class, ex);
            //throw new DAOException(ex);
        }
        return tag;
    }

    public static void insert(AlignTag tag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = VALUE + ", " + ID_CORPUS;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setString(1, tag.getValue());
                ps.setInt(2, tag.getIdCorpus());
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        //Set the text object with the id created when the text was inserted
                        tag.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AlignTagDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(AlignTag tag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String table = TABLE_NAME;
            String fields = VALUE + " = ? ";
            String where = ID + " = ? ";
            String sql = "UPDATE " + table + " SET " + fields + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, tag.getValue());
                ps.setInt(2, tag.getId());
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AlignTagDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(AlignTag tag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, tag.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AlignTagDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
