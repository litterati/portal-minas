package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.MetaWordTags;
import br.project.entity.WordTags;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * MetaWordTagsDAO
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class MetaWordTagsDAO {

    public static final String TABLE_NAME = "meta_word_tags";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String TAG = "tag";
    public static final String XCES = "xces";

    public static WordTags load(ResultSet rs) throws SQLException {
        WordTags wt = new WordTags();
        wt.setId(rs.getInt(TABLE_NAME + "_" + ID));
        wt.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        wt.setTag(rs.getString(TABLE_NAME + "_" + TAG));
        wt.setXces(rs.getString(TABLE_NAME + "_" + XCES));
        return wt;
    }

    public static List<WordTags> loadList(ResultSet rs) throws SQLException {
        List<WordTags> wtList = new ArrayList<>();
        while (rs.next()) {
            WordTags wt = new WordTags();
            wt.setId(rs.getInt(TABLE_NAME + "_" + ID));
            wt.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            wt.setTag(rs.getString(TABLE_NAME + "_" + TAG));
            wt.setXces(rs.getString(TABLE_NAME + "_" + XCES));
            // add into the selectAll
            wtList.add(wt);
        }
        return wtList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + TAG + " AS " + TABLE_NAME + "_" + TAG + ", "
                + TABLE_NAME + "." + XCES + " AS " + TABLE_NAME + "_" + XCES;
    }

    public static List<MetaWordTags> selectByIdCorpus(int idCorpus) {
        List<MetaWordTags> lstMetaWordTag = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME 
                    + " WHERE " + ID_CORPUS + " = " + idCorpus
                    + " ORDER BY " + TAG + " ASC";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    MetaWordTags metaWordTag;
                    while (rs.next()) {
                        metaWordTag = new MetaWordTags();
                        metaWordTag.setId(rs.getInt(ID));
                        metaWordTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        metaWordTag.setTag(rs.getString(TAG));
                        metaWordTag.setXces(rs.getString(XCES));
                        //add to list
                        lstMetaWordTag.add(metaWordTag);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagsDAO.class, ex);
            //throw new RuntimeException("Erro em MetaWordTagsDAO.listMetaWordTagsByCorpus(). " + ex.getMessage());
        }
        return lstMetaWordTag;
    }

    public static void insert(MetaWordTags metaWordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_CORPUS + ", " + TAG + ", " + XCES + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                ps.setInt(1, metaWordTag.getIdCorpus());
                ps.setString(2, metaWordTag.getTag());
                ps.setString(3, metaWordTag.getXces());
                //execute
                ps.execute();
                //commit
                c.commit();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        metaWordTag.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static MetaWordTags find(MetaWordTags metaWordTag) {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID_CORPUS + " = " + metaWordTag.getIdCorpus() + " AND " + TAG + " = '" + metaWordTag.getTag() + "'";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        metaWordTag = new MetaWordTags();
                        metaWordTag.setId(rs.getInt(ID));
                        metaWordTag.setIdCorpus(rs.getInt(ID_CORPUS));
                        metaWordTag.setTag(rs.getString(TAG));
                        metaWordTag.setXces(rs.getString(XCES));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagsDAO.class, ex);
            //throw new RuntimeException("Erro em MetaWordsTagsDAO.find(). " + ex.getMessage());
        }
        if (metaWordTag.getId() > 0) {
            return metaWordTag;
        } else {
            return null;
        }

    }

    public static void deleteById(MetaWordTags metaWordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, metaWordTag.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void deleteByTag(MetaWordTags metaWordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE LOWER(" + TAG + ") = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, metaWordTag.getTag());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
