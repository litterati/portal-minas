/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.MetaWordTagAttribute;
import br.project.entity.WordTagAttribute;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * MetaWordTagAttributesDAO
 *
 * @author Thiago Vieira
 * @since 14/07/2014
 */
public class MetaWordTagAttributesDAO {

    public static final String TABLE_NAME = "meta_word_tag_attributes";
    public static final String ID = "id";
    public static final String ID_WORD_TAG_LABEL = "id_meta_word_tag_label";
    public static final String ATTRIBUTE = "attribute";
    public static final String VALUE = "value";

    public static MetaWordTagAttribute load(ResultSet rs) throws SQLException {
        MetaWordTagAttribute mwta = new MetaWordTagAttribute();
        mwta.setId(rs.getInt(TABLE_NAME + "_" + ID));
        mwta.setIdWordTagLabel(rs.getInt(TABLE_NAME + "_" + ID_WORD_TAG_LABEL));
        mwta.setAttribute(rs.getString(TABLE_NAME + "_" + ATTRIBUTE));
        mwta.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        return mwta;
    }

    public static List<MetaWordTagAttribute> loadList(ResultSet rs) throws SQLException {
        List<MetaWordTagAttribute> mwtaList = new ArrayList<>();
        while (rs.next()) {
            MetaWordTagAttribute mwta = new MetaWordTagAttribute();
            mwta.setId(rs.getInt(TABLE_NAME + "_" + ID));
            mwta.setIdWordTagLabel(rs.getInt(TABLE_NAME + "_" + ID_WORD_TAG_LABEL));
            mwta.setAttribute(rs.getString(TABLE_NAME + "_" + ATTRIBUTE));
            mwta.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            // add into the list
            mwtaList.add(mwta);
        }
        return mwtaList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_WORD_TAG_LABEL + " AS " + TABLE_NAME + "_" + ID_WORD_TAG_LABEL + ", "
                + TABLE_NAME + "." + ATTRIBUTE + " AS " + TABLE_NAME + "_" + ATTRIBUTE + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE;
    }
    
    public static void insert(List<WordTagAttribute> metaWordTagAttributes, int idMetaWordTagLabel, Connection c) throws DAOException {
        for (WordTagAttribute metaWordTagAttribute : metaWordTagAttributes) {
            metaWordTagAttribute.setIdWordTagLabel(idMetaWordTagLabel);
            MetaWordTagAttributesDAO.insert(metaWordTagAttribute, c);
        }
    }

    public static void insert(WordTagAttribute metaWordTagAttribute) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            MetaWordTagAttributesDAO.insert(metaWordTagAttribute, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagAttributesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(WordTagAttribute metaWordTagAttribute, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_WORD_TAG_LABEL + ", " + ATTRIBUTE + ", " + VALUE + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, metaWordTagAttribute.getIdWordTagLabel());
                ps.setString(2, metaWordTagAttribute.getAttribute());
                ps.setString(3, metaWordTagAttribute.getValue());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        metaWordTagAttribute.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(MetaWordTagAttributesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

}
