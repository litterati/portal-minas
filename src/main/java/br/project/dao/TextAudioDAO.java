/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Config;
import br.library.util.Logger;
import br.project.entity.TextAudio;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * TextAudioDAO
 *
 * @author Thiago Vieira
 * @since 14/07/2014
 */
public class TextAudioDAO {

    public static final String TABLE_NAME = "text_images";
    public static final String ID = "id";
    public static final String ID_TEXT = "id_text";
    public static final String FILE_NAME = "file_name";
    public static final String COMMENT = "comment";

    public static TextAudio load(ResultSet rs) throws SQLException {
        TextAudio ti = new TextAudio();
        ti.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ti.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        ti.setFileName(rs.getString(TABLE_NAME + "_" + FILE_NAME));
        ti.setComment(rs.getString(TABLE_NAME + "_" + COMMENT));
        return ti;
    }

    public static List<TextAudio> loadList(ResultSet rs) throws SQLException {
        List<TextAudio> tiList = new ArrayList<>();
        while (rs.next()) {
            TextAudio ti = new TextAudio();
            ti.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ti.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            ti.setFileName(rs.getString(TABLE_NAME + "_" + FILE_NAME));
            ti.setComment(rs.getString(TABLE_NAME + "_" + COMMENT));
            // add into the list
            tiList.add(ti);
        }
        return tiList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + FILE_NAME + " AS " + TABLE_NAME + "_" + FILE_NAME + ", "
                + TABLE_NAME + "." + COMMENT + " AS " + TABLE_NAME + "_" + COMMENT;
    }

    public static void insert(TextAudio object) {
        try {
            File imageDirTarget = new File(Config.getPathProperties().getProperty("path.images") + "/" + object.getIdText());
            File file = new File(object.getFileName());
            if (!imageDirTarget.exists()) {
                imageDirTarget.mkdir();
            }
            object.setFileName(imageDirTarget.getPath() + "/" + file.getName());
            //Move the image to the proper directory
            FileUtils.copyFileToDirectory(file, imageDirTarget);
            //Remove image from temp directory
            FileUtils.forceDelete(file);
            //connection
            try (Connection c = BuildConnection.getConnection()) {
                //prepare sql
                String sql = "INSERT INTO " + TABLE_NAME + " "
                        + "(" + ID_TEXT + ", " + FILE_NAME + ", " + COMMENT + ") "
                        + "VALUES (?, ?, ?)";
                try (PreparedStatement ps = c.prepareStatement(sql)) {
                    ps.setInt(1, object.getIdText());
                    ps.setString(2, object.getFileName());
                    ps.setString(3, object.getComment());
                    //execute
                    ps.execute();
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAudioDAO.class, ex);
            throw new RuntimeException("Erro em TextAudioDAO.insert(). " + ex.getMessage());
        }
    }

    public static void update(TextAudio object) {
        try {
            if (object != null) {
                File imageDirTarget = new File(Config.getPathProperties().getProperty("path.images") + "/" + object.getIdText());
                File file = new File(object.getFileName());
                if (imageDirTarget.exists()) {
                    imageDirTarget.delete();
                    imageDirTarget.mkdir();
                    object.setFileName(imageDirTarget.getPath() + "/" + file.getName());
                    //Move the image to the proper directory
                    FileUtils.copyFileToDirectory(file, imageDirTarget);
                    //Remove image from temp directory
                    FileUtils.forceDelete(file);
                    //connection
                    try (Connection c = BuildConnection.getConnection()) {
                        //prepare sql
                        String sql = "UPDATE " + TABLE_NAME + " "
                                + "SET " + ID_TEXT + " = ?, " + FILE_NAME + " = ?, " + COMMENT + " = ?"
                                + "WHERE " + ID + " = ? ";
                        try (PreparedStatement ps = c.prepareStatement(sql)) {
                            ps.setInt(1, object.getIdText());
                            ps.setString(2, object.getFileName());
                            ps.setString(3, object.getComment());
                            ps.setInt(4, object.getId());
                            //execute
                            ps.execute();
                        } catch (SQLException ex) {
                            throw ex;
                        }
                    } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
                        throw ex;
                    }
                } else {
                    insert(object);
                }
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAudioDAO.class, ex);
            throw new RuntimeException("Erro em TextAudioDAO.update(). " + ex.getMessage());
        }
    }

    public static void delete(TextAudio object) {
        //delete the local file
        File file = new File(object.getFileName());
        file.delete();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAudioDAO.class, ex);
            throw new RuntimeException("Erro em TextAudioDAO.delete()." + ex.getMessage());
        }
    }

    public static void selectById(TextAudio object) {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getId());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        object.setFileName(rs.getString(FILE_NAME));
                        object.setId(rs.getInt(ID));
                        object.setIdText(rs.getInt(ID_TEXT));
                        object.setComment(rs.getString(COMMENT));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAudioDAO.class, ex);
            throw new RuntimeException("Erro em TextAudioDAO.select()." + ex.getMessage());
        }
    }
    
    public static TextAudio selectByIdText(int id_text) {
        TextAudio textAudio = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + TextAudioDAO.getFields()
                    + " FROM " + TABLE_NAME + " "
                    + " WHERE " + TABLE_NAME + "." + ID_TEXT + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        textAudio = TextAudioDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAudioDAO.class, ex);
            //throw new RuntimeException("Erro em TextAudioDAO.selectByTextId()." + ex.getMessage());
        }
        return textAudio;
    }
}
