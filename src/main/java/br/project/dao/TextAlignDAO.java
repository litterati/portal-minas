/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Text;
import br.project.entity.TextAlign;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TextAlignDAO
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 */
public class TextAlignDAO {

    public static final String TABLE_NAME = "text_align";
    public static final String ID_SOURCE = "id_source";
    public static final String ID_TARGET = "id_target";
    public static final String TYPE = "type";

    public static TextAlign load(ResultSet rs) throws SQLException {
        TextAlign ta = new TextAlign();
        ta.setIdSource(rs.getInt(TABLE_NAME + "_" + ID_SOURCE));
        ta.setIdTarget(rs.getInt(TABLE_NAME + "_" + ID_TARGET));
        ta.setType(rs.getString(TABLE_NAME + "_" + TYPE));
        return ta;
    }

    public static List<TextAlign> loadList(ResultSet rs) throws SQLException {
        List<TextAlign> taList = new ArrayList<>();
        while (rs.next()) {
            TextAlign ta = new TextAlign();
            ta.setIdSource(rs.getInt(TABLE_NAME + "_" + ID_SOURCE));
            ta.setIdTarget(rs.getInt(TABLE_NAME + "_" + ID_TARGET));
            ta.setType(rs.getString(TABLE_NAME + "_" + TYPE));
            // add into the list
            taList.add(ta);
        }
        return taList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID_SOURCE + " AS " + TABLE_NAME + "_" + ID_SOURCE + ", "
                + TABLE_NAME + "." + ID_TARGET + " AS " + TABLE_NAME + "_" + ID_TARGET + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE;
    }

    public static void insert(TextAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ") "
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdSource());
                ps.setInt(2, object.getIdTarget());
                ps.setString(3, object.getType());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(TextAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + TYPE + " = ? "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, object.getType());
                ps.setInt(2, object.getIdSource());
                ps.setInt(3, object.getIdTarget());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(TextAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdSource());
                ps.setInt(2, object.getIdTarget());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(TextAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<TextAlign> selectAllByIdCorpus(int id) throws DAOException {
        List<TextAlign> listAlign = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  " + TABLE_NAME + "." + ID_SOURCE + " as align_id_source, "
                    + "  " + TABLE_NAME + "." + ID_TARGET + " as align_id_target, "
                    + "  " + TABLE_NAME + "." + TYPE + " as align_type, "
                    + "  source." + TextDAO.ID + " as source_id, "
                    + "  source." + TextDAO.ID_CORPUS + " as source_id_corpus, "
                    + "  source." + TextDAO.LANGUAGE + " as source_language, "
                    + "  source." + TextDAO.TITLE + " as source_title, "
                    + "  target." + TextDAO.ID + " as target_id, "
                    + "  target." + TextDAO.ID_CORPUS + " as target_id_corpus, "
                    + "  target." + TextDAO.LANGUAGE + " as target_language, "
                    + "  target." + TextDAO.TITLE + " as target_title "
                    + "FROM "
                    + "  public." + TABLE_NAME + ", "
                    + "  public." + TextDAO.TABLE_NAME + " as source, "
                    + "  public." + TextDAO.TABLE_NAME + " as target "
                    + "WHERE "
                    + "  source.id_corpus = ? AND "
                    + "  source.id = " + TABLE_NAME + "." + ID_SOURCE + " AND "
                    + "  target.id = " + TABLE_NAME + "." + ID_TARGET + ";";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    TextAlign aux;
                    Text source, target;
                    while (rs.next()) {
                        aux = new TextAlign();
                        aux.setIdSource(rs.getInt("align_id_source"));
                        aux.setIdTarget(rs.getInt("align_id_target"));
                        aux.setType(rs.getString("align_type"));
                        //source
                        source = new Text();
                        source.setId(rs.getInt("source_id"));
                        source.setIdCorpus(rs.getInt("source_id_corpus"));
                        source.setLanguage(rs.getString("source_language"));
                        source.setTitle(rs.getString("source_title"));
                        //target
                        target = new Text();
                        target.setId(rs.getInt("target_id"));
                        target.setIdCorpus(rs.getInt("target_id_corpus"));
                        target.setLanguage(rs.getString("target_language"));
                        target.setTitle(rs.getString("target_title"));
                        //add to list
                        aux.setSource(source);
                        aux.setTarget(target);
                        listAlign.add(aux);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(ex);
            Logger.error(TextAlignDAO.class, ex);
        }
        return listAlign;
    }

    public static List<TextAlign> selectByIdCorpus(int id, int offset, int limit, String order) {
        List<TextAlign> listAlign = new ArrayList<>();
        //order
        if (order == null) {
            order = "source_title";
        } else if (order.equals("target")) {
            order = "target_title";
        } else if (order.equals("type")) {
            order = "align_type";
        } else { // order.equals("source")
            order = "source_title";
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  " + TABLE_NAME + "." + ID_SOURCE + " as align_id_source, "
                    + "  " + TABLE_NAME + "." + ID_TARGET + " as align_id_target, "
                    + "  " + TABLE_NAME + "." + TYPE + " as align_type, "
                    + "  source." + TextDAO.ID + " as source_id, "
                    + "  source." + TextDAO.ID_CORPUS + " as source_id_corpus, "
                    + "  source." + TextDAO.LANGUAGE + " as source_language, "
                    + "  source." + TextDAO.TITLE + " as source_title, "
                    + "  target." + TextDAO.ID + " as target_id, "
                    + "  target." + TextDAO.ID_CORPUS + " as target_id_corpus, "
                    + "  target." + TextDAO.LANGUAGE + " as target_language, "
                    + "  target." + TextDAO.TITLE + " as target_title "
                    + "FROM "
                    + "  public." + TABLE_NAME + ", "
                    + "  public." + TextDAO.TABLE_NAME + " as source, "
                    + "  public." + TextDAO.TABLE_NAME + " as target "
                    + "WHERE "
                    + "  source.id_corpus = ? AND "
                    + "  source.id = " + TABLE_NAME + "." + ID_SOURCE + " AND "
                    + "  target.id = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "ORDER BY " + order + " ASC "
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                ps.setInt(2, limit);
                ps.setInt(3, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    TextAlign aux;
                    Text source, target;
                    while (rs.next()) {
                        aux = new TextAlign();
                        aux.setIdSource(rs.getInt("align_id_source"));
                        aux.setIdTarget(rs.getInt("align_id_target"));
                        aux.setType(rs.getString("align_type"));
                        //source
                        source = new Text();
                        source.setId(rs.getInt("source_id"));
                        source.setIdCorpus(rs.getInt("source_id_corpus"));
                        source.setLanguage(rs.getString("source_language"));
                        source.setTitle(rs.getString("source_title"));
                        //target
                        target = new Text();
                        target.setId(rs.getInt("target_id"));
                        target.setIdCorpus(rs.getInt("target_id_corpus"));
                        target.setLanguage(rs.getString("target_language"));
                        target.setTitle(rs.getString("target_title"));
                        //add to list
                        aux.setSource(source);
                        aux.setTarget(target);
                        listAlign.add(aux);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(ex);
            Logger.error(TextAlignDAO.class, ex);
        }
        return listAlign;
    }

    public static int countByIdCorpus(int idCorpus) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + ", " + CorpusDAO.TABLE_NAME + ", " + TextDAO.TABLE_NAME + " "
                    + "WHERE "
                    + "corpus.id = texts.id_corpus "
                    + "AND texts.id = text_align.id_source "
                    + "AND corpus.id = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(ex);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }

    public static TextAlign selectById(int id_source, int id_target) {
        TextAlign align = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  " + TABLE_NAME + "." + ID_SOURCE + " as align_id_source, "
                    + "  " + TABLE_NAME + "." + ID_TARGET + " as align_id_target, "
                    + "  " + TABLE_NAME + "." + TYPE + " as align_type, "
                    + "  source." + TextDAO.ID + " as source_id, "
                    + "  source." + TextDAO.ID_CORPUS + " as source_id_corpus, "
                    + "  source." + TextDAO.LANGUAGE + " as source_language, "
                    + "  source." + TextDAO.TITLE + " as source_title, "
                    + "  target." + TextDAO.ID + " as target_id, "
                    + "  target." + TextDAO.ID_CORPUS + " as target_id_corpus, "
                    + "  target." + TextDAO.LANGUAGE + " as target_language, "
                    + "  target." + TextDAO.TITLE + " as target_title "
                    + "FROM "
                    + "  public." + TABLE_NAME + ", "
                    + "  public." + TextDAO.TABLE_NAME + " as source, "
                    + "  public." + TextDAO.TABLE_NAME + " as target "
                    + "WHERE "
                    + "  " + TABLE_NAME + "." + ID_SOURCE + " = ? AND "
                    + "  " + TABLE_NAME + "." + ID_TARGET + " = ? AND "
                    + "  source.id = " + TABLE_NAME + "." + ID_SOURCE + " AND "
                    + "  target.id = " + TABLE_NAME + "." + ID_TARGET + ";";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source);
                ps.setInt(2, id_target);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    Text source, target;
                    while (rs.next()) {
                        align = new TextAlign();
                        align.setIdSource(rs.getInt("align_id_source"));
                        align.setIdTarget(rs.getInt("align_id_target"));
                        align.setType(rs.getString("align_type"));
                        //source
                        source = new Text();
                        source.setId(rs.getInt("source_id"));
                        source.setIdCorpus(rs.getInt("source_id_corpus"));
                        source.setLanguage(rs.getString("source_language"));
                        source.setTitle(rs.getString("source_title"));
                        //target
                        target = new Text();
                        target.setId(rs.getInt("target_id"));
                        target.setIdCorpus(rs.getInt("target_id_corpus"));
                        target.setLanguage(rs.getString("target_language"));
                        target.setTitle(rs.getString("target_title"));
                        //add to list
                        align.setSource(source);
                        align.setTarget(target);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(ex);
            Logger.error(TextAlignDAO.class, ex);
        }
        return align;
    }
}
