package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Text;
import br.project.entity.Words;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * WordDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class WordDAO {

    public static final String TABLE_NAME = "words";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String ID_TEXT = "id_text";
    public static final String ID_SENTENCE = "id_sentence";
    public static final String POSITION = "position";
    public static final String WORD = "word";
    public static final String LEMMA = "lemma";
    public static final String POS = "pos";
    public static final String NORM = "norm";
    public static final String TRANSKRIPTION = "transkription";
    public static final String DEP_HEAD = "dep_head";
    public static final String DEP_FUNCTION = "dep_function";
    public static final String CONTRACTION = "contraction";

    public static Words load(ResultSet rs) throws SQLException {
        Words word = new Words();
        word.setId(rs.getInt(TABLE_NAME + "_" + ID));
        word.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        word.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        word.setIdSentence(rs.getInt(TABLE_NAME + "_" + ID_SENTENCE));
        word.setPosition(rs.getInt(TABLE_NAME + "_" + POSITION));
        word.setWord(rs.getString(TABLE_NAME + "_" + WORD));
        word.setLemma(rs.getString(TABLE_NAME + "_" + LEMMA));
        word.setPos(rs.getString(TABLE_NAME + "_" + POS));
        word.setNorm(rs.getString(TABLE_NAME + "_" + NORM));
        word.setTranskription(rs.getString(TABLE_NAME + "_" + TRANSKRIPTION));
        word.setDepHead(rs.getString(TABLE_NAME + "_" + DEP_HEAD));
        word.setDepFunction(rs.getString(TABLE_NAME + "_" + DEP_FUNCTION));
        word.setContraction(rs.getString(TABLE_NAME + "_" + CONTRACTION));
        return word;
    }

    public static List<Words> loadList(ResultSet rs) throws SQLException {
        List<Words> wordList = new ArrayList<>();
        while (rs.next()) {
            Words word = new Words();
            word.setId(rs.getInt(TABLE_NAME + "_" + ID));
            word.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            word.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            word.setIdSentence(rs.getInt(TABLE_NAME + "_" + ID_SENTENCE));
            word.setPosition(rs.getInt(TABLE_NAME + "_" + POSITION));
            word.setWord(rs.getString(TABLE_NAME + "_" + WORD));
            word.setLemma(rs.getString(TABLE_NAME + "_" + LEMMA));
            word.setPos(rs.getString(TABLE_NAME + "_" + POS));
            word.setNorm(rs.getString(TABLE_NAME + "_" + NORM));
            word.setTranskription(rs.getString(TABLE_NAME + "_" + TRANSKRIPTION));
            word.setDepHead(rs.getString(TABLE_NAME + "_" + DEP_HEAD));
            word.setDepFunction(rs.getString(TABLE_NAME + "_" + DEP_FUNCTION));
            word.setContraction(rs.getString(TABLE_NAME + "_" + CONTRACTION));
            // add into the list
            wordList.add(word);
        }
        return wordList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + ID_SENTENCE + " AS " + TABLE_NAME + "_" + ID_SENTENCE + ", "
                + TABLE_NAME + "." + POSITION + " AS " + TABLE_NAME + "_" + POSITION + ", "
                + TABLE_NAME + "." + WORD + " AS " + TABLE_NAME + "_" + WORD + ", "
                + TABLE_NAME + "." + LEMMA + " AS " + TABLE_NAME + "_" + LEMMA + ", "
                + TABLE_NAME + "." + POS + " AS " + TABLE_NAME + "_" + POS + ", "
                + TABLE_NAME + "." + NORM + " AS " + TABLE_NAME + "_" + NORM + ", "
                + TABLE_NAME + "." + TRANSKRIPTION + " AS " + TABLE_NAME + "_" + TRANSKRIPTION + ", "
                + TABLE_NAME + "." + DEP_HEAD + " AS " + TABLE_NAME + "_" + DEP_HEAD + ", "
                + TABLE_NAME + "." + DEP_FUNCTION + " AS " + TABLE_NAME + "_" + DEP_FUNCTION + ", "
                + TABLE_NAME + "." + CONTRACTION + " AS " + TABLE_NAME + "_" + CONTRACTION;
    }

    public static void deleteByIdText(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static Map<String, String> frequencyDistinctWords(int idCorpus) throws DAOException {
        Map<String, String> frequenciesMap = new LinkedHashMap<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT DISTINCT "
                    + "  lower(w." + WORD + ") AS word, "
                    + "  count(*) AS frequency "
                    + "FROM " + TABLE_NAME + " w "
                    + "WHERE w." + ID_CORPUS + " = ? "
                    + "GROUP BY lower(w." + WORD + ") "
                    + "ORDER BY frequency DESC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        frequenciesMap.put(rs.getString("word"), rs.getString("frequency"));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(ex);
            Logger.error(WordDAO.class, ex);
        }
        return frequenciesMap;
    }

    public static Map<String, String> frequencyDistinctWords(int idCorpus, int offset, int limit, String order) throws DAOException {
        Map<String, String> frequenciesMap = new LinkedHashMap<>();
        //order
        if (order == null) {
            order = "frequency DESC ";
        } else if (order.equals("word")) {
            order = "word ASC ";
        } else {// (!order.equals("word") && !order.equals("frequency"))
            order = "frequency DESC ";
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT DISTINCT "
                    + "  lower(w." + WORD + ") AS word, "
                    + "  count(*) AS frequency "
                    + "FROM " + TABLE_NAME + " w "
                    + "WHERE w." + ID_CORPUS + " = ? "
                    + "GROUP BY lower(w." + WORD + ") "
                    + "ORDER BY " + order
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                ps.setInt(2, limit);
                ps.setInt(3, offset);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        frequenciesMap.put(rs.getString("word"), rs.getString("frequency"));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordDAO.class, ex);
            //throw new DAOException(ex);
        }
        return frequenciesMap;
    }

    public static int countByIdText(int idText) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " w "
                    + "WHERE w." + ID_TEXT + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idText);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }

    public static int countByIdCorpus(int idCorpus) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "count(*) AS number "
                    + "FROM " + TABLE_NAME + " w "
                    + "WHERE w." + ID_CORPUS + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                try ( //execute
                        ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }

    public static void insert(List<Words> lstWords) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + ", " + WORD + ", " + ID_CORPUS + ", " + ID_SENTENCE + ") "
                    + "VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                for (Words words : lstWords) {
                    ps.setInt(1, words.getIdText());
                    ps.setInt(2, words.getPosition());
                    ps.setString(3, words.getWord());
                    ps.setInt(4, words.getIdCorpus());
                    ps.setInt(5, words.getIdSentence());
                    //add to batch
                    ps.addBatch();
                }
                //execute batch
                ps.executeBatch();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    int cont = 0;
                    while (rs.next()) {
                        lstWords.get(cont).setId(rs.getInt(1));
                        cont++;
                    }
                    //commit
                    c.commit();
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(Words word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            WordDAO.insert(word, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(Words word, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + ", " + WORD + ", " + ID_CORPUS + ", " + ID_SENTENCE + ", " + CONTRACTION + ") "
                    + "VALUES (?, ?, ?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                ps.setInt(1, word.getIdText());
                ps.setInt(2, word.getPosition());
                ps.setString(3, word.getWord());
                ps.setInt(4, word.getIdCorpus());
                ps.setInt(5, word.getIdSentence());
                ps.setString(6, word.getContraction());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        word.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(WordDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(List<Words> lstWords) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            c.setAutoCommit(false);
            //prepare sql to delete
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ?";
            try ( //Delete words
                    PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, lstWords.get(0).getIdText());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
            //prepare sql to insert
            String sql2 = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + ", " + WORD + ", " + ID_CORPUS + ") "
                    + "VALUES (?, ?, ?, ?)";
            try ( //Insert words
                    PreparedStatement ps2 = c.prepareStatement(sql2)) {
                for (Words words : lstWords) {
                    ps2.setInt(1, words.getIdText());
                    ps2.setInt(2, words.getPosition());
                    ps2.setString(3, words.getWord());
                    ps2.setInt(4, words.getIdCorpus());
                    //add to batch
                    ps2.addBatch();
                }
                //execute batch
                ps2.executeBatch();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
            //Execute
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(Words word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + WORD + " = ?, " + LEMMA + " = ?, " + POS + " = ?, " + NORM + " = ?, " + TRANSKRIPTION + " = ?, " + DEP_HEAD + " = ?, " + DEP_FUNCTION + " = ?, " + CONTRACTION + " = ?, " + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, word.getWord());
                ps.setString(2, word.getLemma());
                ps.setString(3, word.getPos());
                ps.setString(4, word.getNorm());
                ps.setString(5, word.getTranskription());
                ps.setString(6, word.getDepHead());
                ps.setString(7, word.getDepFunction());
                ps.setBoolean(8, !word.getContraction().isEmpty());
                ps.setInt(9, word.getId());
                // not change: ID, ID_CORPUS, ID_TEXT, ID_SENTENCE, POSITION
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<Words> selectByIdText(int id_text, int offset, int limit) {
        List<Words> words = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + getFields() + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ? "
                    + "ORDER BY " + POSITION + " ASC "
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                ps.setInt(2, limit);
                ps.setInt(3, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    words = loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return words;
    }

    public static List<Words> selectAllByIdSentence(int id_sentence) {
        List<Words> words = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            words = WordDAO.selectAllByIdSentence(c, id_sentence);
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return words;
    }

    public static List<Words> selectAllByIdSentence(Connection c, int id_sentence) {
        List<Words> words = new ArrayList<>();
        //prepare sql
        String sql = "SELECT " + getFields() + " "
                + "FROM " + TABLE_NAME + " "
                + "WHERE " + ID_SENTENCE + " = ? "
                + "ORDER BY " + POSITION + " ASC ";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, id_sentence);
            //execute
            try (ResultSet rs = ps.executeQuery()) {
                words = loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return words;
    }

    public static List<Words> selectAllByIdText(int id_text) {
        List<Words> words = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + getFields() + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ? "
                    + "ORDER BY " + POSITION + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                try (ResultSet rs = ps.executeQuery()) {
                    words = loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return words;
    }

    public static StringBuilder getOnlyWordsFromText(int id_text) {

        StringBuilder words = new StringBuilder();

        try (Connection c = BuildConnection.getConnection(); PreparedStatement ps = c.prepareStatement(
                "SELECT * "
                + "FROM " + TABLE_NAME + " "
                + "WHERE " + ID_TEXT + " = ? "
                + "ORDER BY " + POSITION + " ASC ")) {
            ps.setInt(1, id_text);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    if (rs.getString(WORD).endsWith("_NEWLINE_")) {
                        words.append("");
                    } else if (rs.getString(CONTRACTION) != null) {
                        words.append(rs.getString(CONTRACTION)).append(" ");
                        rs.next();
                    }else if(rs.getString(WORD).matches("[a-zA-Z0-9]+")){
                        words.append(rs.getString(WORD)).append(" ");
                    }
                }
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            throw new RuntimeException("Erro em WordDAO.getAllWorldsFromCorpus(). " + ex.getMessage());
        }

        return words;
    }

    public static String transformASCII(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    public static Words selectById(int id_word) {
        Words w = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + getFields() + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_word);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        w = load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return w;
    }
}
