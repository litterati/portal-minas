package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Access;
import br.library.util.access.AccessAnalysis;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * AccessDAO
 *
 * @author Thiago Vieira
 * @since 13/01/2015
 */
public class AccessDAO {

    public static final String TABLE_NAME = "access";
    public static final String ID_USER = "id_user";
    public static final String ID_CORPUS = "id_corpus";
    public static final String DATE_ACCESS = "date_access";
    public static final String TOOL = "tool";

    public static Access load(ResultSet rs) throws SQLException {
        Access obj = new Access();
        obj.setIdUser(rs.getInt(TABLE_NAME + "_" + ID_USER));
        obj.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        obj.setTool(rs.getString(TABLE_NAME + "_" + TOOL));
        obj.setDateAccess(rs.getTimestamp(TABLE_NAME + "_" + DATE_ACCESS) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_ACCESS).getTime()));
        return obj;
    }

    public static List<Access> loadList(ResultSet rs) throws SQLException {
        List<Access> list = new ArrayList<>();
        while (rs.next()) {
            Access obj = new Access();
            obj.setIdUser(rs.getInt(TABLE_NAME + "_" + ID_USER));
            obj.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            obj.setTool(rs.getString(TABLE_NAME + "_" + TOOL));
            obj.setDateAccess(rs.getTimestamp(TABLE_NAME + "_" + DATE_ACCESS) == null ? null : new java.util.Date(rs.getTimestamp(TABLE_NAME + "_" + DATE_ACCESS).getTime()));
            // add into the list
            list.add(obj);
        }
        return list;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID_USER + " AS " + TABLE_NAME + "_" + ID_USER + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + TOOL + " AS " + TABLE_NAME + "_" + TOOL + ", "
                + TABLE_NAME + "." + DATE_ACCESS + " AS " + TABLE_NAME + "_" + DATE_ACCESS;
    }

    public static List<Access> selectAll(String order) throws DAOException {
        List<Access> list = new ArrayList<>();
        //order
        if (order == null) {
            order = AccessDAO.ID_CORPUS;
        } else if (!order.equals(AccessDAO.ID_USER) && !order.equals(AccessDAO.TOOL) && !order.equals(AccessDAO.DATE_ACCESS)) {
            order = AccessDAO.ID_CORPUS;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AccessDAO.getFields();
            String from = TABLE_NAME;
            String sql = "SELECT " + fields + " FROM " + from + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery()) {
                list = AccessDAO.loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }
    
    public static List<Access> selectAllByDate(String start, String end) throws DAOException {
        List<Access> list = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AccessDAO.getFields();
            String from = TABLE_NAME;
            String where = DATE_ACCESS + " BETWEEN '" + start + "' AND '" + end + "  23:59:59' ";
            String sql = "SELECT " + fields + " FROM " + from;
            if (start != null && end != null){
                sql = sql + " WHERE " + where;
            }
            try (PreparedStatement ps = c.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery()) {
                list = AccessDAO.loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static List<Access> selectAllByIdUser(int id_user) throws DAOException {
        List<Access> list = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AccessDAO.getFields();
            String from = TABLE_NAME;
            String where = TABLE_NAME + "." + ID_USER + " = ? ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_user);
                try (ResultSet rs = ps.executeQuery()) {
                    list = AccessDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static List<Access> selectAllByIdCorpus(int id_corpus) throws DAOException {
        List<Access> list = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AccessDAO.getFields();
            String from = TABLE_NAME;
            String where = TABLE_NAME + "." + ID_CORPUS + " = ? ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                try (ResultSet rs = ps.executeQuery()) {
                    list = AccessDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static List<Access> selectAllByTool(String tool) throws DAOException {
        List<Access> list = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AccessDAO.getFields();
            String from = TABLE_NAME;
            String where = TABLE_NAME + "." + TOOL + " = ? ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, tool);
                try (ResultSet rs = ps.executeQuery()) {
                    list = AccessDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }
    
    public static List<AccessAnalysis<Integer>> selectAndCountByDate(String start, String end) throws DAOException {
        List<AccessAnalysis<Integer>> list = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = "id_corpus, to_char(date_access, 'YYYY-MM-DD') as date_access, count(*)";
            String from = TABLE_NAME;
            String where = DATE_ACCESS + " BETWEEN '" + start + "' AND '" + end + "  23:59:59' ";
            String group_by = "id_corpus, to_char(date_access, 'YYYY-MM-DD')";
            String order_by = "date_access";
            String sql = "SELECT " + fields + " FROM " + from;
            if (start != null && end != null){
                sql = sql + " WHERE " + where;
            }
            sql = sql + " GROUP BY " + group_by + " ORDER BY " + order_by + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    AccessAnalysis<Integer> aa = new AccessAnalysis<>(
                            rs.getString("date_access"),
                            rs.getInt("count"),
                            rs.getInt("id_corpus")
                    );
                    list.add(aa);
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static Access select(int id_user, int id_corpus, String tool) throws DAOException {
        Access obj = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = AccessDAO.getFields();
            String from = TABLE_NAME;
            String where = ID_USER + " = ? AND " + ID_CORPUS + " = ? AND " + TOOL + " = ? ";
            String sql = "SELECT " + fields + " FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_user);
                ps.setInt(2, id_corpus);
                ps.setString(3, tool);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        obj = AccessDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            //throw new DAOException(ex);
        }
        return obj;
    }

    public static void insert(Access obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = ID_USER + ", " + ID_CORPUS + ", " + TOOL + ", " + DATE_ACCESS;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?, ?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, obj.getIdUser());
                ps.setInt(2, obj.getIdCorpus());
                ps.setString(3, obj.getTool());
                java.util.Date date = new java.util.Date();
                ps.setTimestamp(4, new Timestamp(date.getTime()));
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(Access obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String table = TABLE_NAME;
            String fields = TOOL + " = ? ";
            String where = ID_USER + " = ? AND " + ID_CORPUS + " = ? AND " + DATE_ACCESS + " = ? ";
            String sql = "UPDATE " + table + " SET " + fields + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, obj.getTool());
                ps.setInt(2, obj.getIdUser());
                ps.setInt(3, obj.getIdCorpus());
                ps.setTimestamp(4, new Timestamp(obj.getDateAccess().getTime()));
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(Access obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID_USER + " = ? AND " + ID_CORPUS + " = ? AND " + DATE_ACCESS + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, obj.getIdUser());
                ps.setInt(2, obj.getIdCorpus());
                ps.setTimestamp(3, new Timestamp(obj.getDateAccess().getTime()));
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(AccessDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
