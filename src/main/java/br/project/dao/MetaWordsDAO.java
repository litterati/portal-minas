package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.MetaWords;
import br.project.entity.Text;
import br.project.entity.Words;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * MetaWordTagsDAO
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class MetaWordsDAO {

    public static final String TABLE_NAME = "meta_words";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String ID_TEXT = "id_text";
    public static final String ID_SENTENCE = "id_sentence";
    public static final String POSITION = "position";
    public static final String WORD = "word";
    public static final String LEMMA = "lemma";
    public static final String POS = "pos";
    public static final String CONTRACTION = "contraction";

    public static MetaWords load(ResultSet rs) throws SQLException {
        MetaWords mw = new MetaWords();
        mw.setId(rs.getInt(TABLE_NAME + "_" + ID));
        mw.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        mw.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        mw.setIdSentence(rs.getInt(TABLE_NAME + "_" + ID_SENTENCE));
        mw.setPosition(rs.getInt(TABLE_NAME + "_" + POSITION));
        mw.setWord(rs.getString(TABLE_NAME + "_" + WORD));
        mw.setLemma(rs.getString(TABLE_NAME + "_" + LEMMA));
        mw.setPos(rs.getString(TABLE_NAME + "_" + POS));
        mw.setContraction(rs.getString(TABLE_NAME + "_" + CONTRACTION));
        return mw;
    }

    public static List<MetaWords> loadList(ResultSet rs) throws SQLException {
        List<MetaWords> mwList = new ArrayList<>();
        while (rs.next()) {
            MetaWords mw = new MetaWords();
            mw.setId(rs.getInt(TABLE_NAME + "_" + ID));
            mw.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            mw.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            mw.setIdSentence(rs.getInt(TABLE_NAME + "_" + ID_SENTENCE));
            mw.setPosition(rs.getInt(TABLE_NAME + "_" + POSITION));
            mw.setWord(rs.getString(TABLE_NAME + "_" + WORD));
            mw.setLemma(rs.getString(TABLE_NAME + "_" + LEMMA));
            mw.setPos(rs.getString(TABLE_NAME + "_" + POS));
            mw.setContraction(rs.getString(TABLE_NAME + "_" + CONTRACTION));
            // add to list
            mwList.add(mw);
        }
        return mwList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + ID_SENTENCE + " AS " + TABLE_NAME + "_" + ID_SENTENCE + ", "
                + TABLE_NAME + "." + POSITION + " AS " + TABLE_NAME + "_" + POSITION + ", "
                + TABLE_NAME + "." + WORD + " AS " + TABLE_NAME + "_" + WORD + ", "
                + TABLE_NAME + "." + LEMMA + " AS " + TABLE_NAME + "_" + LEMMA + ", "
                + TABLE_NAME + "." + POS + " AS " + TABLE_NAME + "_" + POS + ", "
                + TABLE_NAME + "." + CONTRACTION + " AS " + TABLE_NAME + "_" + CONTRACTION;
    }

    public static void deleteByIdText(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static Map<String, String> frequencyMetaWords(int idCorpus) throws DAOException {
        Map<String, String> frequenciesMap = new LinkedHashMap<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT DISTINCT "
                    + "  lower(mw." + WORD + ") AS word, "
                    + "  count(*) AS frequency "
                    + "FROM " + TABLE_NAME + " mw "
                    + "WHERE mw." + ID_CORPUS + " = ? "
                    + "GROUP BY lower(mw." + WORD + ") "
                    + "ORDER BY frequency DESC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        frequenciesMap.put(rs.getString("word"), rs.getString("frequency"));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordsDAO.class, ex);
            throw new DAOException(ex);
        }
        return frequenciesMap;
    }

    public static void insert(List<Words> lstWords) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + ", " + WORD + ", " + ID_CORPUS + ", " + ID_SENTENCE + ") "
                    + "VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                for (Words words : lstWords) {
                    ps.setInt(1, words.getIdText());
                    ps.setInt(2, words.getPosition());
                    ps.setString(3, words.getWord());
                    ps.setInt(4, words.getIdCorpus());
                    ps.setInt(5, words.getIdSentence());
                    //add to batch
                    ps.addBatch();
                }
                //execute
                ps.executeBatch();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    int cont = 0;
                    while (rs.next()) {
                        lstWords.get(cont).setId(rs.getInt(1));
                        cont++;
                    }
                    //commit
                    c.commit();
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(List<Words> lstWords) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            c.setAutoCommit(false);
            //prepare sql to delete
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ?";
            //Delete words
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, lstWords.get(0).getIdText());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
            //prepare sql to insert
            String sql2 = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + ", " + WORD + ", " + ID_CORPUS + ") "
                    + "VALUES (?, ?, ?, ?)";
            //Insert words
            try (PreparedStatement ps2 = c.prepareStatement(sql2)) {
                for (Words words : lstWords) {
                    ps2.setInt(1, words.getIdText());
                    ps2.setInt(2, words.getPosition());
                    ps2.setString(3, words.getWord());
                    ps2.setInt(4, words.getIdCorpus());
                    //add to batch
                    ps2.addBatch();
                }
                ps2.executeBatch();
                //Execute
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(Words word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            MetaWordsDAO.insert(word, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(Words word, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + ", " + WORD + ", " + ID_CORPUS + ", " + ID_SENTENCE + ") "
                    + "VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{"id"})) {
                c.setAutoCommit(false);
                ps.setInt(1, word.getIdText());
                ps.setInt(2, word.getPosition());
                ps.setString(3, word.getWord());
                ps.setInt(4, word.getIdCorpus());
                ps.setInt(5, word.getIdSentence());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        word.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(MetaWordsDAO.class, ex);
            throw new DAOException(ex);
        }
    }
    
    public static List<MetaWords> selectAllByIdText(int id_text) {
        List<MetaWords> words = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + getFields() + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ? "
                    + "ORDER BY " + POSITION + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    words = loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(MetaWordsDAO.class, ex);
        }
        return words;
    }
    
    public static int countByIdText(int idText) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " w "
                    + "WHERE w." + ID_TEXT + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idText);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }
}
