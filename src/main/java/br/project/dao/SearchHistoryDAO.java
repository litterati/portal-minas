/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.dao;

import br.library.connection.BuildConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author marcel
 */
public class SearchHistoryDAO {
    public static final String TABLE_NAME = "search_history";
    public static final String ID_USER = "id_user";
    public static final String ID_HISTORY = "id_history";
    public static final String TERM = "term";
    
    public void updateSearch(int userId, String term){
        try(Connection c = BuildConnection.getConnection()){
            ArrayList<String> lastSearches = lastSearches(userId, c);
            boolean exist = false;
            
            if(lastSearches!=null){
                for(String search : lastSearches){
                    if(search.equals(term)){
                        exist = true;
                    }else if(lastSearches.size() > 11){
                        String sql = "SELECT * FROM " + TABLE_NAME
                               + " WHERE " + ID_USER + " = ?"
                               + " ORDER BY " + ID_HISTORY + " ASC;";
                    PreparedStatement ps = c.prepareStatement(sql);
                    ps.setInt(1, userId);
                    ResultSet rs = ps.executeQuery();
                    
                    rs.first();
                    int oldId = rs.getInt(ID_HISTORY);
                        sql = "DELETE " + TABLE_NAME
                            + " WHERE " + ID_HISTORY + " = ?";
                        ps = c.prepareStatement(sql);
                    ps.setInt(1, oldId);
                    ps.execute();
                    }
                }
            }
            
            if(!exist){
                String sql = "INSERT INTO " + TABLE_NAME
                        + "(" + ID_USER + "," + TERM + ") VALUES (?,?);";
                PreparedStatement ps = c.prepareStatement(sql);
                ps.setInt(1, userId);
                ps.setString(2, term);
                ps.execute();
            }
            c.close();
        }catch(Exception ex){
            throw new RuntimeException("Erro em SearchHistoryDAO.updateSearch. "+ex.getMessage());
        }
    }
    
    public ArrayList lastSearches(int userId, Connection c){
        ArrayList<String> lastSearches = new ArrayList<>();
        try{
            String sql = "SELECT * from " + TABLE_NAME
                    + " WHERE " + ID_USER + "= ?;";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                lastSearches.add(rs.getString(TERM));
            }
            
        }catch(SQLException ex){
            throw new RuntimeException("Erro em SearchHistoryDAO.lastSearches. "+ex.getMessage());            
        }
        return lastSearches;
    }
    
    public ArrayList lastSearches(int userId){
        ArrayList<String> lastSearches = new ArrayList<>();
         try(Connection c = BuildConnection.getConnection()){
            String sql = "SELECT * from " + TABLE_NAME
                    + " WHERE " + ID_USER + "= ?;";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                lastSearches.add(rs.getString(TERM));
            }
            c.close();
        }catch(Exception ex){
            throw new RuntimeException("Erro em SearchHistoryDAO.lastSearches. "+ex.getMessage());            
        }
        return lastSearches;
    }
    
}
