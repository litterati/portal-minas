/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.WordTagAttribute;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * WordTagAttributesDAO
 *
 * @author Thiago Vieira
 * @since 14/07/2014
 */
public class WordTagAttributesDAO {

    public static final String TABLE_NAME = "word_tags_attributes";
    public static final String ID = "id";
    public static final String ID_WORD_TAGS_LABEL = "id_word_tag_label";
    public static final String ATTRIBUTE = "attribute";
    public static final String VALUE = "value";

    public static WordTagAttribute load(ResultSet rs) throws SQLException {
        WordTagAttribute wta = new WordTagAttribute();
        wta.setId(rs.getInt(TABLE_NAME + "_" + ID));
        wta.setIdWordTagLabel(rs.getInt(TABLE_NAME + "_" + ID_WORD_TAGS_LABEL));
        wta.setAttribute(rs.getString(TABLE_NAME + "_" + ATTRIBUTE));
        wta.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        return wta;
    }

    public static List<WordTagAttribute> loadList(ResultSet rs) throws SQLException {
        List<WordTagAttribute> wtaList = new ArrayList<>();
        while (rs.next()) {
            WordTagAttribute wta = new WordTagAttribute();
            wta.setId(rs.getInt(TABLE_NAME + "_" + ID));
            wta.setIdWordTagLabel(rs.getInt(TABLE_NAME + "_" + ID_WORD_TAGS_LABEL));
            wta.setAttribute(rs.getString(TABLE_NAME + "_" + ATTRIBUTE));
            wta.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            // add into the list
            wtaList.add(wta);
        }
        return wtaList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_WORD_TAGS_LABEL + " AS " + TABLE_NAME + "_" + ID_WORD_TAGS_LABEL + ", "
                + TABLE_NAME + "." + ATTRIBUTE + " AS " + TABLE_NAME + "_" + ATTRIBUTE + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE;
    }

    
    // O parâmetro idWordTagLabel foi adicionado pois no momento em que os atributos de uma tag são extraídos (fillMetaWordAttributeTagValues), o método 
    // ainda não sabe o id que será gerado para a wordTagLabel, mas no momento em que é feito o registro da wordTagLabel o seu id se torna conhecido, sendo passado então como parametro para preencher os ids da lista uma a um
    public static void insert(List<WordTagAttribute> wordTagAttributes, int idWordTagLabel, Connection c) throws DAOException {
        for (WordTagAttribute wordTagAttribute : wordTagAttributes) {
            wordTagAttribute.setIdWordTagLabel(idWordTagLabel);
            WordTagAttributesDAO.insert(wordTagAttribute, c);
        }
    }

    public static void insert(WordTagAttribute wordTagAttribute) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            WordTagAttributesDAO.insert(wordTagAttribute, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagAttributesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(WordTagAttribute wordTagAttribute, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID + ", " + ID_WORD_TAGS_LABEL + ", " + ATTRIBUTE + ", " + VALUE + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, wordTagAttribute.getId());
                ps.setInt(2, wordTagAttribute.getIdWordTagLabel());
                ps.setString(3, wordTagAttribute.getAttribute());
                ps.setString(3, wordTagAttribute.getValue());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        wordTagAttribute.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(WordTagAttributesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(WordTagAttribute object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagAttributesDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
