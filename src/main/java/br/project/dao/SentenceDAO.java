package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Sentence;
import br.project.entity.Text;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * SentenceDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class SentenceDAO {

    public static final String TABLE_NAME = "sentences";
    public static final String ID = "id";
    public static final String ID_TEXT = "id_text";
    public static final String POSITION = "position";
    public static final String TYPE = "type";

    public static Sentence load(ResultSet rs) throws SQLException {
        Sentence sentence = new Sentence();
        sentence.setId(rs.getInt(TABLE_NAME + "_" + ID));
        sentence.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        sentence.setPosition(rs.getInt(TABLE_NAME + "_" + POSITION));
        sentence.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
        return sentence;
    }

    public static List<Sentence> loadList(ResultSet rs) throws SQLException {
        List<Sentence> sentenceList = new ArrayList<>();
        while (rs.next()) {
            Sentence sentence = new Sentence();
            sentence.setId(rs.getInt(TABLE_NAME + "_" + ID));
            sentence.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            sentence.setPosition(rs.getInt(TABLE_NAME + "_" + POSITION));
            sentence.setType(rs.getInt(TABLE_NAME + "_" + TYPE));
            // add into the list
            sentenceList.add(sentence);
        }
        return sentenceList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + POSITION + " AS " + TABLE_NAME + "_" + POSITION + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE;
    }

    public static void deleteByIdText(Text text) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, text.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SentenceDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static int countByIdText(int idText, int type) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " w "
                    + "WHERE w." + ID_TEXT + " = ? ";
            if (type == Sentence.TYPE_TEXT_SENTENCE || type == Sentence.TYPE_META_SENTENCE){
                sql = sql + " AND w." + TYPE + " = ? ";
            }
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idText);
                if (type == Sentence.TYPE_TEXT_SENTENCE || type == Sentence.TYPE_META_SENTENCE){
                    ps.setInt(2, type);
                }
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }
    
    public static void insert(Sentence sentence, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + POSITION + "," + TYPE + ")"
                    + "VALUES (?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                ps.setInt(1, sentence.getIdText());
                ps.setInt(2, sentence.getPosition());
                ps.setInt(3, sentence.getType());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    rs.next();
                    sentence.setId(rs.getInt(1));
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(SentenceDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<Sentence> selectAllByIdText(int id_text) {
        List<Sentence> sentences = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + SentenceDAO.getFields() + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TEXT + " = ? "
                    + "ORDER BY " + POSITION + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    sentences = SentenceDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return sentences;
    }
}
