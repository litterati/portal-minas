/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Words;
import br.project.entity.WordsAlign;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * WordsAlignDAO
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 */
public class WordsAlignDAO {

    public static final String TABLE_NAME = "words_align";
    public static final String ID_SOURCE = "id_source";
    public static final String ID_TARGET = "id_target";
    public static final String TYPE = "type";
    public static final String ID_ALIGN_TAG = "id_align_tag";

    public static WordsAlign load(ResultSet rs) throws SQLException {
        WordsAlign wa = new WordsAlign();
        wa.setIdSource(rs.getInt(TABLE_NAME + "_" + ID_SOURCE));
        wa.setIdTarget(rs.getInt(TABLE_NAME + "_" + ID_TARGET));
        wa.setType(rs.getString(TABLE_NAME + "_" + TYPE));
        wa.setIdAlignTag(rs.getInt(TABLE_NAME + "_" + ID_ALIGN_TAG));
        return wa;
    }

    public static List<WordsAlign> loadList(ResultSet rs) throws SQLException {
        List<WordsAlign> waList = new ArrayList<>();
        while (rs.next()) {
            WordsAlign wa = new WordsAlign();
            wa.setIdSource(rs.getInt(TABLE_NAME + "_" + ID_SOURCE));
            wa.setIdTarget(rs.getInt(TABLE_NAME + "_" + ID_TARGET));
            wa.setType(rs.getString(TABLE_NAME + "_" + TYPE));
            wa.setIdAlignTag(rs.getInt(TABLE_NAME + "_" + ID_ALIGN_TAG));
            // add into the list
            waList.add(wa);
        }
        return waList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID_SOURCE + " AS " + TABLE_NAME + "_" + ID_SOURCE + ", "
                + TABLE_NAME + "." + ID_TARGET + " AS " + TABLE_NAME + "_" + ID_TARGET + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE + ", "
                + TABLE_NAME + "." + ID_ALIGN_TAG + " AS " + TABLE_NAME + "_" + ID_ALIGN_TAG;
    }

    public static void insert(WordsAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdSource());
                ps.setInt(2, object.getIdTarget());
                ps.setString(3, object.getType());
                if (object.getIdAlignTag() > 0){
                    ps.setInt(4, object.getIdAlignTag());
                } else {
                    ps.setNull(4, java.sql.Types.INTEGER);
                }
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<WordsAlign> lstObjects) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                c.setAutoCommit(false);
                for (int i = 0; i < lstObjects.size(); i++) {
                    WordsAlign object = lstObjects.get(i);
                    //set values
                    ps.setInt(1, object.getIdSource());
                    ps.setInt(2, object.getIdTarget());
                    ps.setString(3, object.getType());
                    if (object.getIdAlignTag() > 0){
                        ps.setInt(4, object.getIdAlignTag());
                    } else {
                        ps.setNull(4, java.sql.Types.INTEGER);
                    }
                    //add to batch
                    ps.addBatch();
                    //executa de mil em mil
                    if ((i > 0) && ((i % 1000) == 0)){
                        //execute batch
                        ps.executeBatch();
                    }
                }
                //execute batch
                ps.executeBatch();
                //commit
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(WordsAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + TYPE + " = ?, " + ID_ALIGN_TAG + " = ? "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, object.getType());
                if (object.getIdAlignTag() > 0){
                    ps.setInt(2, object.getIdAlignTag());
                } else {
                    ps.setNull(2, java.sql.Types.INTEGER);
                }
                ps.setInt(3, object.getIdSource());
                ps.setInt(4, object.getIdTarget());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void delete(WordsAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdSource());
                ps.setInt(2, object.getIdTarget());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<WordsAlign> selectAll(int id_source_text, int id_target_text) {
        List<WordsAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + " "
                    + "FROM " + TABLE_NAME + ", " + WordDAO.TABLE_NAME + " source, " + WordDAO.TABLE_NAME + " target "
                    + "WHERE source." + WordDAO.ID_TEXT + " = ? "
                    + "AND target." + WordDAO.ID_TEXT + " = ? "
                    + "AND source." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_text);
                ps.setInt(2, id_target_text);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        WordsAlign obj = new WordsAlign();
                        obj.setIdSource(rs.getInt(ID_SOURCE));
                        obj.setIdTarget(rs.getInt(ID_TARGET));
                        obj.setType(rs.getString(TYPE));
                        obj.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                        //add to list
                        aligns.add(obj);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static WordsAlign selectById(int id_source_word, int id_target_word) {
        WordsAlign align = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_word);
                ps.setInt(2, id_target_word);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        align = new WordsAlign();
                        align.setIdSource(rs.getInt(ID_SOURCE));
                        align.setIdTarget(rs.getInt(ID_TARGET));
                        align.setType(rs.getString(TYPE));
                        align.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return align;
    }

    public static List<WordsAlign> selectAll(int id_source_text, int id_target_text, int offset, int limit) {
        List<WordsAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + ", "
                    + "source." + WordDAO.ID + " AS source_id, source." + WordDAO.ID_CORPUS + " AS source_id_corpus, source." + WordDAO.ID_TEXT + " AS source_id_text, source." + WordDAO.ID_SENTENCE + " AS source_id_sentence, source." + WordDAO.POSITION + " AS source_position, source." + WordDAO.WORD + " AS source_word, "
                    + "target." + WordDAO.ID + " AS target_id, target." + WordDAO.ID_CORPUS + " AS target_id_corpus, target." + WordDAO.ID_TEXT + " AS target_id_text, target." + WordDAO.ID_SENTENCE + " AS target_id_sentence, target." + WordDAO.POSITION + " AS target_position, target." + WordDAO.WORD + " AS target_word "
                    + "FROM " + TABLE_NAME + ", " + WordDAO.TABLE_NAME + " source, " + WordDAO.TABLE_NAME + " target "
                    + "WHERE source." + WordDAO.ID_TEXT + " = ? "
                    + "AND target." + WordDAO.ID_TEXT + " = ? "
                    + "AND source." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_text);
                ps.setInt(2, id_target_text);
                ps.setInt(3, limit);
                ps.setInt(4, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        WordsAlign obj = new WordsAlign();
                        obj.setIdSource(rs.getInt(ID_SOURCE));
                        obj.setIdTarget(rs.getInt(ID_TARGET));
                        obj.setType(rs.getString(TYPE));
                        obj.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                        //source
                        Words sw = new Words();
                        sw.setId(rs.getInt("source_id"));
                        sw.setIdCorpus(rs.getInt("source_id_corpus"));
                        sw.setIdText(rs.getInt("source_id_text"));
                        sw.setIdSentence(rs.getInt("source_id_sentence"));
                        sw.setPosition(rs.getInt("source_position"));
                        sw.setWord(rs.getString("source_word"));
                        //target
                        Words tw = new Words();
                        tw.setId(rs.getInt("target_id"));
                        tw.setIdCorpus(rs.getInt("target_id_corpus"));
                        tw.setIdText(rs.getInt("target_id_text"));
                        tw.setIdSentence(rs.getInt("target_id_sentence"));
                        tw.setPosition(rs.getInt("target_position"));
                        tw.setWord(rs.getString("target_word"));
                        //align
                        obj.setSource(sw);
                        obj.setTarget(tw);
                        //add to list
                        aligns.add(obj);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }
    
    public static List<WordsAlign> selectAllWithTagByIdCorpus(int id_corpus, int offset, int limit) {
        List<WordsAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + ", "
                    + "source." + WordDAO.ID + " AS source_id, source." + WordDAO.ID_CORPUS + " AS source_id_corpus, source." + WordDAO.ID_TEXT + " AS source_id_text, source." + WordDAO.ID_SENTENCE + " AS source_id_sentence, source." + WordDAO.POSITION + " AS source_position, source." + WordDAO.WORD + " AS source_word, "
                    + "target." + WordDAO.ID + " AS target_id, target." + WordDAO.ID_CORPUS + " AS target_id_corpus, target." + WordDAO.ID_TEXT + " AS target_id_text, target." + WordDAO.ID_SENTENCE + " AS target_id_sentence, target." + WordDAO.POSITION + " AS target_position, target." + WordDAO.WORD + " AS target_word "
                    + "FROM " + TABLE_NAME + ", " + WordDAO.TABLE_NAME + " source, " + WordDAO.TABLE_NAME + " target "
                    + "WHERE source." + WordDAO.ID_CORPUS + " = ? "
                    + "AND target." + WordDAO.ID_CORPUS + " = ? "
                    + "AND source." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "AND " + ID_ALIGN_TAG + " IS NOT NULL "
                    + "AND " + ID_ALIGN_TAG + " > 0 "
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                ps.setInt(2, id_corpus);
                ps.setInt(3, limit);
                ps.setInt(4, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        WordsAlign obj = new WordsAlign();
                        obj.setIdSource(rs.getInt(ID_SOURCE));
                        obj.setIdTarget(rs.getInt(ID_TARGET));
                        obj.setType(rs.getString(TYPE));
                        obj.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                        //source
                        Words sw = new Words();
                        sw.setId(rs.getInt("source_id"));
                        sw.setIdCorpus(rs.getInt("source_id_corpus"));
                        sw.setIdText(rs.getInt("source_id_text"));
                        sw.setIdSentence(rs.getInt("source_id_sentence"));
                        sw.setPosition(rs.getInt("source_position"));
                        sw.setWord(rs.getString("source_word"));
                        //target
                        Words tw = new Words();
                        tw.setId(rs.getInt("target_id"));
                        tw.setIdCorpus(rs.getInt("target_id_corpus"));
                        tw.setIdText(rs.getInt("target_id_text"));
                        tw.setIdSentence(rs.getInt("target_id_sentence"));
                        tw.setPosition(rs.getInt("target_position"));
                        tw.setWord(rs.getString("target_word"));
                        //align
                        obj.setSource(sw);
                        obj.setTarget(tw);
                        //add to list
                        aligns.add(obj);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static int countByIdText(int id_source_text, int id_target_text) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + ", " + WordDAO.TABLE_NAME + " source, " + WordDAO.TABLE_NAME + " target "
                    + "WHERE source." + WordDAO.ID_TEXT + " = ? "
                    + "AND target." + WordDAO.ID_TEXT + " = ? "
                    + "AND source." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_text);
                ps.setInt(2, id_target_text);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }
    
    public static int countAllWithTagByIdCorpus(int id_corpus) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + ", " + WordDAO.TABLE_NAME + " source, " + WordDAO.TABLE_NAME + " target "
                    + "WHERE source." + WordDAO.ID_CORPUS + " = ? "
                    + "AND target." + WordDAO.ID_CORPUS + " = ? "
                    + "AND source." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + WordDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "AND " + ID_ALIGN_TAG + " IS NOT NULL "
                    + "AND " + ID_ALIGN_TAG + " > 0 ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                ps.setInt(2, id_corpus);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }
    
    public static int countByIdAlignTag(int id_align_tag) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_ALIGN_TAG + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_align_tag);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }

    public static List<WordsAlign> selectAllByIdSource(int id_source_word) {
        List<WordsAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_word);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        WordsAlign a = new WordsAlign();
                        a.setIdSource(rs.getInt(ID_SOURCE));
                        a.setIdTarget(rs.getInt(ID_TARGET));
                        a.setType(rs.getString(TYPE));
                        a.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                        //add to list
                        aligns.add(a);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static void deleteAllByIdSource(int id_source_word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_word);
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<WordsAlign> selectAllByIdTarget(int id_target_word) {
        List<WordsAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TARGET + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_target_word);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        WordsAlign a = new WordsAlign();
                        a.setIdSource(rs.getInt(ID_SOURCE));
                        a.setIdTarget(rs.getInt(ID_TARGET));
                        a.setType(rs.getString(TYPE));
                        a.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                        //add to list
                        aligns.add(a);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static void deleteAllByIdTarget(int id_target_word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TARGET + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_target_word);
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
