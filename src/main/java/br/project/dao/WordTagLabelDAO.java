/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.WordTagLabel;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * WordTagsDAO
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class WordTagLabelDAO {

    public static final String TABLE_NAME = "word_tag_label";
    public static final String ID = "id";
    public static final String ID_TEXT = "id_text";
    public static final String ID_WORD_OPEN = "id_word_open";
    public static final String ID_WORD_CLOSE = "id_word_close";
    public static final String ID_WORD_TAG = "id_word_tag";

    public static WordTagLabel load(ResultSet rs) throws SQLException {
        WordTagLabel wtl = new WordTagLabel();
        wtl.setId(rs.getInt(TABLE_NAME + "_" + ID));
        wtl.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        wtl.setIdWordOpen(rs.getInt(TABLE_NAME + "_" + ID_WORD_OPEN));
        wtl.setIdWordClose(rs.getInt(TABLE_NAME + "_" + ID_WORD_CLOSE));
        wtl.setIdWordTag(rs.getInt(TABLE_NAME + "_" + ID_WORD_TAG));
        return wtl;
    }

    public static List<WordTagLabel> loadList(ResultSet rs) throws SQLException {
        List<WordTagLabel> wtlList = new ArrayList<>();
        while (rs.next()) {
            WordTagLabel wtl = new WordTagLabel();
            wtl.setId(rs.getInt(TABLE_NAME + "_" + ID));
            wtl.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            wtl.setIdWordOpen(rs.getInt(TABLE_NAME + "_" + ID_WORD_OPEN));
            wtl.setIdWordClose(rs.getInt(TABLE_NAME + "_" + ID_WORD_CLOSE));
            wtl.setIdWordTag(rs.getInt(TABLE_NAME + "_" + ID_WORD_TAG));
            // add into the list
            wtlList.add(wtl);
        }
        return wtlList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + ID_WORD_OPEN + " AS " + TABLE_NAME + "_" + ID_WORD_OPEN + ", "
                + TABLE_NAME + "." + ID_WORD_CLOSE + " AS " + TABLE_NAME + "_" + ID_WORD_CLOSE + ", "
                + TABLE_NAME + "." + ID_WORD_TAG + " AS " + TABLE_NAME + "_" + ID_WORD_TAG;
    }

    public static void insert(WordTagLabel wordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + ID_WORD_TAG + ", " + ID_WORD_OPEN + ", " + ID_WORD_CLOSE + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setInt(1, wordTag.getIdText());
                ps.setInt(2, wordTag.getWordTag().getId());
                ps.setInt(3, wordTag.getIdWordOpen());
                ps.setInt(4, wordTag.getIdWordClose());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        wordTag.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<WordTagLabel> lstWordTagLabel) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            WordTagLabelDAO.insert(lstWordTagLabel);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<WordTagLabel> lstWordTagLabel, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + ID_WORD_TAG + ", " + ID_WORD_OPEN + ", " + ID_WORD_CLOSE + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                for (WordTagLabel wordTagLabel : lstWordTagLabel) {
                    ps.setInt(1, wordTagLabel.getIdText());
                    ps.setInt(2, wordTagLabel.getWordTag().getId());
                    ps.setInt(3, wordTagLabel.getIdWordOpen());
                    ps.setInt(4, wordTagLabel.getIdWordClose());
                    //add to batch
                    ps.addBatch();
                }
                //execute batch
                ps.executeBatch();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    int cont = 0;
                    while (rs.next()) {
                        lstWordTagLabel.get(cont).setId(rs.getInt(1));
                        if (!lstWordTagLabel.get(cont).getAttributes().isEmpty()) {
                            WordTagAttributesDAO.insert(lstWordTagLabel.get(cont).getAttributes(),
                                    lstWordTagLabel.get(cont).getId(), c);
                        }
                        cont++;
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(WordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static boolean existValue(WordTagLabel wordTagLabel) throws DAOException {
        boolean exist = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, wordTagLabel.getWordTag().getId());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        exist = true;
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
        return exist;
    }
}
