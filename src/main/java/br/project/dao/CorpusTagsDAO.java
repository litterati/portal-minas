package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.CorpusTags;
import br.project.entity.FieldsType;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CorpusTagsDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class CorpusTagsDAO {

    public static final String TABLE_NAME = "corpus_tags";
    public static final String ID = "id";
    public static final String ID_CORPUS = "id_corpus";
    public static final String TAG = "tag";
    public static final String IS_REQUIRED = "is_required";
    public static final String ID_FIELDS_TYPE = "id_fields_type";

    public static CorpusTags load(ResultSet rs) throws SQLException {
        CorpusTags ct = new CorpusTags();
        ct.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ct.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
        ct.setTag(rs.getString(TABLE_NAME + "_" + TAG));
        ct.setIsRequired(rs.getBoolean(TABLE_NAME + "_" + IS_REQUIRED));
        ct.setIdFieldType(rs.getInt(TABLE_NAME + "_" + ID_FIELDS_TYPE));
        return ct;
    }

    public static List<CorpusTags> loadList(ResultSet rs) throws SQLException {
        List<CorpusTags> ctList = new ArrayList<>();
        while (rs.next()) {
            CorpusTags ct = new CorpusTags();
            ct.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ct.setIdCorpus(rs.getInt(TABLE_NAME + "_" + ID_CORPUS));
            ct.setTag(rs.getString(TABLE_NAME + "_" + TAG));
            ct.setIsRequired(rs.getBoolean(TABLE_NAME + "_" + IS_REQUIRED));
            ct.setIdFieldType(rs.getInt(TABLE_NAME + "_" + ID_FIELDS_TYPE));
            // add into the selectAllByIdCorpus
            ctList.add(ct);
        }
        return ctList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_CORPUS + " AS " + TABLE_NAME + "_" + ID_CORPUS + ", "
                + TABLE_NAME + "." + TAG + " AS " + TABLE_NAME + "_" + TAG + ", "
                + TABLE_NAME + "." + IS_REQUIRED + " AS " + TABLE_NAME + "_" + IS_REQUIRED + ", "
                + TABLE_NAME + "." + ID_FIELDS_TYPE + " AS " + TABLE_NAME + "_" + ID_FIELDS_TYPE;
    }

    public static void delete(CorpusTags copusMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, copusMetadata.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static boolean existTag(CorpusTags corpusMetadata) throws DAOException {
        boolean exist = false;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusTagsDAO.getFields();
            String from = TABLE_NAME;
            String where = TABLE_NAME + "." + ID_CORPUS + " = ? AND " 
                    + TABLE_NAME + "." + TAG + " = ? ";
            String sql = "SELECT " + fields 
                    + " FROM " + from 
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, corpusMetadata.getIdCorpus());
                ps.setString(2, corpusMetadata.getTag());
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        exist = true;
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return exist;
    }

    public static CorpusTags selectById(int id) throws DAOException {
        CorpusTags corpusMetadata = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusTagsDAO.getFields();
            String from = TABLE_NAME;
            String where = TABLE_NAME + "." + ID + " = ? ";
            String sql = "SELECT " + fields 
                    + " FROM " + from 
                    + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        corpusMetadata = CorpusTagsDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return corpusMetadata;
    }

    public static void insert(CorpusTags corpusMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = ID_CORPUS + ", " + TAG + ", " + IS_REQUIRED + ", " + ID_FIELDS_TYPE;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?, ?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setInt(1, corpusMetadata.getIdCorpus());
                ps.setString(2, corpusMetadata.getTag());
                ps.setBoolean(3, corpusMetadata.isIsRequired());
                ps.setInt(4, corpusMetadata.getIdFieldType());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        corpusMetadata.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<CorpusTags> selectAllByIdCorpus(int idCorpus) throws DAOException {
        List<CorpusTags> lstCorpusMetadata = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  " + ID + ", "
                    + "  " + TAG + ", "
                    + "  " + IS_REQUIRED + ", "
                    + "  " + ID_FIELDS_TYPE + ", "
                    + "  ("
                    + "    SELECT value "
                    + "    FROM " + FieldsTypeDAO.TABLE_NAME + " f "
                    + "    WHERE f." + FieldsTypeDAO.ID + " = c." + ID_FIELDS_TYPE + " "
                    + "  ) AS name_field_type "
                    + "FROM " + TABLE_NAME + " c "
                    + "WHERE c." + ID_CORPUS + " = ? "
                    + "ORDER BY " + ID + " DESC";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, idCorpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        //fields type
                        FieldsType fieldsType = new FieldsType();
                        fieldsType.setId(rs.getInt(ID_FIELDS_TYPE));
                        fieldsType.setValue(rs.getString("name_field_type"));
                        //corpus Metadata
                        CorpusTags corpusMetadata = new CorpusTags();
                        corpusMetadata.setIdCorpus(idCorpus);
                        corpusMetadata.setId(rs.getInt(ID));
                        corpusMetadata.setTag(rs.getString(TAG));
                        corpusMetadata.setIsRequired(rs.getBoolean(IS_REQUIRED));
                        corpusMetadata.setIdFieldType(rs.getInt(ID_FIELDS_TYPE));
                        corpusMetadata.setFieldsType(fieldsType);
                        //add to list
                        lstCorpusMetadata.add(corpusMetadata);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstCorpusMetadata;
    }
    
    public static List<CorpusTags> selectAll() throws DAOException {
        List<CorpusTags> lstCorpusMetadata = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = CorpusTagsDAO.getFields();
            String sql = "SELECT " + fields + "FROM " + TABLE_NAME;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    lstCorpusMetadata = CorpusTagsDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstCorpusMetadata;
    }

    public static void update(CorpusTags corpusMetadata) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String table = TABLE_NAME;
            String fields = TAG + " = ?, " + IS_REQUIRED + " = ?, " + ID_FIELDS_TYPE + " = ? ";
            String where = ID + " = ? ";
            String sql = "UPDATE " + table + " SET " + fields + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, corpusMetadata.getTag());
                ps.setBoolean(2, corpusMetadata.isIsRequired());
                ps.setInt(3, corpusMetadata.getIdFieldType());
                ps.setInt(4, corpusMetadata.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(CorpusTagsDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
