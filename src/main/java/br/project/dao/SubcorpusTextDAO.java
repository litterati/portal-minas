package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.SubcorpusText;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * SubcorpusTextDAO
 *
 * @author Thiago Vieira
 * @since 16/01/2015
 */
public class SubcorpusTextDAO {

    public static final String TABLE_NAME = "subcorpus_texts";
    public static final String ID_TEXT = "id_text";
    public static final String ID_SUBCORPUS = "id_subcorpus";
    

    public static SubcorpusText load(ResultSet rs) throws SQLException {
        SubcorpusText obj = new SubcorpusText();
        obj.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        obj.setIdSubcorpus(rs.getInt(TABLE_NAME + "_" + ID_SUBCORPUS));
        return obj;
    }

    public static List<SubcorpusText> loadList(ResultSet rs) throws SQLException {
        List<SubcorpusText> list = new ArrayList<>();
        while (rs.next()) {
            SubcorpusText obj = new SubcorpusText();
            obj.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            obj.setIdSubcorpus(rs.getInt(TABLE_NAME + "_" + ID_SUBCORPUS));
            // add into the list
            list.add(obj);
        }
        return list;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + ID_SUBCORPUS + " AS " + TABLE_NAME + "_" + ID_SUBCORPUS;
    }

    public static List<SubcorpusText> selectAll(String order) throws DAOException {
        List<SubcorpusText> list = new ArrayList<>();
        //order
        if (order == null) {
            order = SubcorpusTextDAO.ID_SUBCORPUS;
        } else if (!order.equals(SubcorpusTextDAO.ID_TEXT)) {
            order = SubcorpusTextDAO.ID_SUBCORPUS;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = SubcorpusTextDAO.getFields();
            String from = TABLE_NAME;
            String sql = "SELECT " + fields + " FROM " + from + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql);
                    ResultSet rs = ps.executeQuery()) {
                list = SubcorpusTextDAO.loadList(rs);
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusTextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static List<SubcorpusText> selectAllByIdSubcorpus(int id_subcorpus, String order) throws DAOException {
        List<SubcorpusText> list = new ArrayList<>();
        //order
        if (order == null) {
            order = SubcorpusTextDAO.ID_SUBCORPUS;
        } else if (!order.equals(SubcorpusTextDAO.ID_TEXT)) {
            order = SubcorpusTextDAO.ID_SUBCORPUS;
        }
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = SubcorpusTextDAO.getFields();
            String from = TABLE_NAME;
            String where = ID_SUBCORPUS + " = ? ";
            String sql = "SELECT " + fields
                    + " FROM " + from
                    + " WHERE " + where
                    + " ORDER BY " + order + " ASC ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_subcorpus);
                try (ResultSet rs = ps.executeQuery()) {
                    list = SubcorpusTextDAO.loadList(rs);
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusTextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return list;
    }

    public static SubcorpusText select(int id_text, int id_subcorpus) throws DAOException {
        SubcorpusText obj = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String fields = SubcorpusTextDAO.getFields();
            String from = TABLE_NAME;
            String where = ID_TEXT + " = ? AND " + ID_SUBCORPUS + " = ? ";
            String sql = "SELECT " + fields + " FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                ps.setInt(2, id_subcorpus);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        obj = SubcorpusTextDAO.load(rs);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusTextDAO.class, ex);
            //throw new DAOException(ex);
        }
        return obj;
    }

    public static void insert(SubcorpusText obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String into = TABLE_NAME;
            String fields = ID_TEXT + ", " + ID_SUBCORPUS;
            String sql = "INSERT INTO " + into + " ( " + fields + " ) VALUES (?, ?) ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, obj.getIdText());
                ps.setInt(2, obj.getIdSubcorpus());
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusTextDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    //There is no update
    //public static void update(SubcorpusText obj) throws DAOException {}
    
    public static void delete(SubcorpusText obj) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID_TEXT + " = ? AND " + ID_SUBCORPUS + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, obj.getIdText());
                ps.setInt(2, obj.getIdSubcorpus());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusTextDAO.class, ex);
            throw new DAOException(ex);
        }
    }
    
    public static void deleteAllByIdSubcorpus(int id_subcorpus) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String from = TABLE_NAME;
            String where = ID_SUBCORPUS + " = ? ";
            String sql = "DELETE FROM " + from + " WHERE " + where;
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_subcorpus);
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SubcorpusTextDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
