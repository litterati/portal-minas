package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.FieldsTypeValues;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * FieldsTypeValuesDAO
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class FieldsTypeValuesDAO {

    public static final String TABLE_NAME = "fields_type_values";
    public static final String ID = "id";
    public static final String ID_TYPE = "id_type";
    public static final String VALUE = "value";

    public static FieldsTypeValues load(ResultSet rs) throws SQLException {
        FieldsTypeValues ftv = new FieldsTypeValues();
        ftv.setId(rs.getInt(TABLE_NAME + "_" + ID));
        ftv.setIdType(rs.getInt(TABLE_NAME + "_" + ID_TYPE));
        ftv.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
        return ftv;
    }

    public static List<FieldsTypeValues> loadList(ResultSet rs) throws SQLException {
        List<FieldsTypeValues> ftvList = new ArrayList<>();
        while (rs.next()) {
            FieldsTypeValues ftv = new FieldsTypeValues();
            ftv.setId(rs.getInt(TABLE_NAME + "_" + ID));
            ftv.setIdType(rs.getInt(TABLE_NAME + "_" + ID_TYPE));
            ftv.setValue(rs.getString(TABLE_NAME + "_" + VALUE));
            // add into the list
            ftvList.add(ftv);
        }
        return ftvList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_TYPE + " AS " + TABLE_NAME + "_" + ID_TYPE + ", "
                + TABLE_NAME + "." + VALUE + " AS " + TABLE_NAME + "_" + VALUE;
    }

    public static List<FieldsTypeValues> selectAllByType(FieldsTypeValues fieldsTypeValues) throws DAOException {
        return FieldsTypeValuesDAO.selectAllByType(fieldsTypeValues.getIdType());
    }

    public static List<FieldsTypeValues> selectAllByType(int type) throws DAOException {
        FieldsTypeValues fieldsTypeValues2;
        List<FieldsTypeValues> lstFieldsTypeValues = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + ", " + VALUE + " "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TYPE + " = ? "
                    + "ORDER BY " + VALUE + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, type);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        fieldsTypeValues2 = new FieldsTypeValues();
                        fieldsTypeValues2.setId(rs.getInt(ID));
                        fieldsTypeValues2.setValue(rs.getString(VALUE));
                        fieldsTypeValues2.setIdType(type);
                        //add to list
                        lstFieldsTypeValues.add(fieldsTypeValues2);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeValuesDAO.class, ex);
            throw new DAOException(ex);
        }
        return lstFieldsTypeValues;
    }

    public static List<FieldsTypeValues> selectAll() throws DAOException {
        List<FieldsTypeValues> lstFieldsTypeValues = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID + ", " + VALUE + ", " + ID_TYPE + " "
                    + "FROM " + TABLE_NAME + " "
                    + "ORDER BY " + VALUE + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    FieldsTypeValues fieldsTypeValues;
                    while (rs.next()) {
                        fieldsTypeValues = new FieldsTypeValues();
                        fieldsTypeValues.setId(rs.getInt(ID));
                        fieldsTypeValues.setValue(rs.getString(VALUE));
                        fieldsTypeValues.setIdType(rs.getInt(ID_TYPE));
                        //add to list
                        lstFieldsTypeValues.add(fieldsTypeValues);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeValuesDAO.class, ex);
            //throw new DAOException(ex);
        }
        return lstFieldsTypeValues;
    }

    public static FieldsTypeValues selectById(int id) throws DAOException {
        FieldsTypeValues fields_type_values = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        fields_type_values = new FieldsTypeValues();
                        fields_type_values.setId(rs.getInt(ID));
                        fields_type_values.setValue(rs.getString(VALUE));
                        fields_type_values.setIdType(rs.getInt(ID_TYPE));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeValuesDAO.class, ex);
            //throw new DAOException(ex);
        }
        return fields_type_values;
    }

    public static void insert(FieldsTypeValues fields_type_values) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + VALUE + ", " + ID_TYPE + ") "
                    + "VALUES (?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setString(1, fields_type_values.getValue());
                ps.setInt(2, fields_type_values.getIdType());
                //execute
                ps.execute();
                //generate keys
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        //Set the text object with the id created when the text was inserted
                        fields_type_values.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(FieldsTypeValues fields_type_values) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + VALUE + " = ? , " + ID_TYPE + " = ? "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, fields_type_values.getValue());
                ps.setInt(2, fields_type_values.getIdType());
                ps.setInt(3, fields_type_values.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            throw new DAOException(ex);
        }
    }

    public static void delete(FieldsTypeValues fields_type_values) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, fields_type_values.getId());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(FieldsTypeValuesDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
