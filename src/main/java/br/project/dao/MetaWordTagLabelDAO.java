/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.MetaWordTagLabel;
import br.project.entity.WordTagLabel;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * WordTagsDAO
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class MetaWordTagLabelDAO {

    public static final String TABLE_NAME = "meta_word_tag_label";
    public static final String ID = "id";
    public static final String ID_TEXT = "id_text";
    public static final String ID_WORD_OPEN = "id_word_open";
    public static final String ID_WORD_CLOSE = "id_word_close";
    public static final String ID_META_WORD_TAG = "id_meta_word_tag";

    public static MetaWordTagLabel load(ResultSet rs) throws SQLException {
        MetaWordTagLabel mwtl = new MetaWordTagLabel();
        mwtl.setId(rs.getInt(TABLE_NAME + "_" + ID));
        mwtl.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
        mwtl.setIdWordOpen(rs.getInt(TABLE_NAME + "_" + ID_WORD_OPEN));
        mwtl.setIdWordClose(rs.getInt(TABLE_NAME + "_" + ID_WORD_CLOSE));
        mwtl.setIdMetaWordTag(rs.getInt(TABLE_NAME + "_" + ID_META_WORD_TAG));
        return mwtl;
    }

    public static List<MetaWordTagLabel> loadList(ResultSet rs) throws SQLException {
        List<MetaWordTagLabel> mwtlList = new ArrayList<>();
        while (rs.next()) {
            MetaWordTagLabel mwtl = new MetaWordTagLabel();
            mwtl.setId(rs.getInt(TABLE_NAME + "_" + ID));
            mwtl.setIdText(rs.getInt(TABLE_NAME + "_" + ID_TEXT));
            mwtl.setIdWordOpen(rs.getInt(TABLE_NAME + "_" + ID_WORD_OPEN));
            mwtl.setIdWordClose(rs.getInt(TABLE_NAME + "_" + ID_WORD_CLOSE));
            mwtl.setIdMetaWordTag(rs.getInt(TABLE_NAME + "_" + ID_META_WORD_TAG));
            // add into the list
            mwtlList.add(mwtl);
        }
        return mwtlList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID + " AS " + TABLE_NAME + "_" + ID + ", "
                + TABLE_NAME + "." + ID_TEXT + " AS " + TABLE_NAME + "_" + ID_TEXT + ", "
                + TABLE_NAME + "." + ID_WORD_OPEN + " AS " + TABLE_NAME + "_" + ID_WORD_OPEN + ", "
                + TABLE_NAME + "." + ID_WORD_CLOSE + " AS " + TABLE_NAME + "_" + ID_WORD_CLOSE + ", "
                + TABLE_NAME + "." + ID_META_WORD_TAG + " AS " + TABLE_NAME + "_" + ID_META_WORD_TAG;
    }

    public static void insert(WordTagLabel wordTag) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + ID_META_WORD_TAG + ", " + ID_WORD_OPEN + ", " + ID_WORD_CLOSE + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                ps.setInt(1, wordTag.getIdText());
                ps.setInt(2, wordTag.getWordTag().getId());
                ps.setInt(3, wordTag.getIdWordOpen());
                ps.setInt(4, wordTag.getIdWordClose());
                //execute
                ps.execute();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs != null && rs.next()) {
                        wordTag.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<WordTagLabel> lstWordTagLabel) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            MetaWordTagLabelDAO.insert(lstWordTagLabel, c);
            c.commit();
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(MetaWordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<WordTagLabel> lstMetaWordTagLabel, Connection c) throws DAOException {
        try {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_TEXT + ", " + ID_META_WORD_TAG + ", " + ID_WORD_OPEN + ", " + ID_WORD_CLOSE + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql, new String[]{ID})) {
                c.setAutoCommit(false);
                for (WordTagLabel metaWordTagLabel : lstMetaWordTagLabel) {
                    ps.setInt(1, metaWordTagLabel.getIdText());
                    ps.setInt(2, metaWordTagLabel.getWordTag().getId());
                    ps.setInt(3, metaWordTagLabel.getIdWordOpen());
                    ps.setInt(4, metaWordTagLabel.getIdWordClose());
                    ps.addBatch();
                }
                //execute
                ps.executeBatch();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    int cont = 0;
                    while (rs.next()) {
                        lstMetaWordTagLabel.get(cont).setId(rs.getInt(1));
                        if (!lstMetaWordTagLabel.get(cont).getAttributes().isEmpty()) {
                            MetaWordTagAttributesDAO.insert(lstMetaWordTagLabel.get(cont).getAttributes(),
                                    lstMetaWordTagLabel.get(cont).getId(), c);
                        }
                        cont++;
                    }

                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException ex) {
            Logger.error(MetaWordTagLabelDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
