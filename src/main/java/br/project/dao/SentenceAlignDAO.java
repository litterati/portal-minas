/*

 Objeto de acesso a dados (ou simplesmente DAO, acrônimo de Data Access Object), 
 é um padrão para persistência de dados que permite separar regras de negócio das 
 regras de acesso a banco de dados. Numa aplicação que utilize a arquitetura MVC, 
 todas as funcionalidades de bancos de dados, tais como obter as conexões, mapear 
 objetos Java para tipos de dados SQL ou executar comandos SQL, devem ser feitas 
 por classes de DAO. (Wikipédia, http://pt.wikipedia.org/wiki/Data_Access_Object)

 */
package br.project.dao;

import br.library.connection.BuildConnection;
import br.library.util.Logger;
import br.project.entity.Sentence;
import br.project.entity.SentenceAlign;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * SentenceAlignDAO
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 */
public class SentenceAlignDAO {

    public static final String TABLE_NAME = "sentence_align";
    public static final String ID_SOURCE = "id_source";
    public static final String ID_TARGET = "id_target";
    public static final String TYPE = "type";
    public static final String ID_ALIGN_TAG = "id_align_tag";

    public static SentenceAlign load(ResultSet rs) throws SQLException {
        SentenceAlign sa = new SentenceAlign();
        sa.setIdSource(rs.getInt(TABLE_NAME + "_" + ID_SOURCE));
        sa.setIdTarget(rs.getInt(TABLE_NAME + "_" + ID_TARGET));
        sa.setType(rs.getString(TABLE_NAME + "_" + TYPE));
        sa.setIdAlignTag(rs.getInt(TABLE_NAME + "_" + ID_ALIGN_TAG));
        return sa;
    }

    public static List<SentenceAlign> loadList(ResultSet rs) throws SQLException {
        List<SentenceAlign> saList = new ArrayList<>();
        while (rs.next()) {
            SentenceAlign sa = new SentenceAlign();
            sa.setIdSource(rs.getInt(TABLE_NAME + "_" + ID_SOURCE));
            sa.setIdTarget(rs.getInt(TABLE_NAME + "_" + ID_TARGET));
            sa.setType(rs.getString(TABLE_NAME + "_" + TYPE));
            sa.setIdAlignTag(rs.getInt(TABLE_NAME + "_" + ID_ALIGN_TAG));
            // add into the list
            saList.add(sa);
        }
        return saList;
    }

    public static String getFields() {
        return TABLE_NAME + "." + ID_SOURCE + " AS " + TABLE_NAME + "_" + ID_SOURCE + ", "
                + TABLE_NAME + "." + ID_TARGET + " AS " + TABLE_NAME + "_" + ID_TARGET + ", "
                + TABLE_NAME + "." + TYPE + " AS " + TABLE_NAME + "_" + TYPE+ ", "
                + TABLE_NAME + "." + ID_ALIGN_TAG + " AS " + TABLE_NAME + "_" + ID_ALIGN_TAG;
    }

    public static void insert(SentenceAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdSource());
                ps.setInt(2, object.getIdTarget());
                ps.setString(3, object.getType());
                if (object.getIdAlignTag() > 0){
                    ps.setInt(4, object.getIdAlignTag());
                } else {
                    ps.setNull(4, java.sql.Types.INTEGER);
                }
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SentenceAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void insert(List<SentenceAlign> lstObjects) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "INSERT INTO " + TABLE_NAME + " "
                    + "(" + ID_SOURCE + ", " + ID_TARGET + ", " + TYPE + ", " + ID_ALIGN_TAG + ") "
                    + "VALUES (?, ?, ?, ?)";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                c.setAutoCommit(false);
                for (int i = 0; i < lstObjects.size(); i++) {
                    SentenceAlign object = lstObjects.get(i);
                    //set values
                    ps.setInt(1, object.getIdSource());
                    ps.setInt(2, object.getIdTarget());
                    ps.setString(3, object.getType());
                    if (object.getIdAlignTag() > 0){
                        ps.setInt(4, object.getIdAlignTag());
                    } else {
                        ps.setNull(4, java.sql.Types.INTEGER);
                    }
                    //add to batch
                    ps.addBatch();
                    //executa de mil em mil
                    if ((i > 0) && ((i % 1000) == 0)){
                        //execute batch
                        ps.executeBatch();
                    }
                }
                //execute batch
                ps.executeBatch();
                //commit
                c.commit();
            } catch (SQLException ex) {
                c.rollback();
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SentenceAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static void update(SentenceAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "UPDATE " + TABLE_NAME + " "
                    + "SET " + TYPE + " = ?, " + ID_ALIGN_TAG + " = ? "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setString(1, object.getType());
                if (object.getIdAlignTag() > 0){
                    ps.setInt(2, object.getIdAlignTag());
                } else {
                    ps.setNull(2, java.sql.Types.INTEGER);
                }
                ps.setInt(3, object.getIdSource());
                ps.setInt(4, object.getIdTarget());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SentenceAlignDAO.class, ex);
            throw new DAOException(ex);
        }

    }

    public static void delete(SentenceAlign object) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ?";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, object.getIdSource());
                ps.setInt(2, object.getIdTarget());
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(SentenceAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<SentenceAlign> selectByIdText(int id_text) {
        List<SentenceAlign> listAlign = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT "
                    + "  " + TABLE_NAME + "." + ID_SOURCE + " as align_id_source, "
                    + "  " + TABLE_NAME + "." + ID_TARGET + " as align_id_target, "
                    + "  " + TABLE_NAME + "." + TYPE + " as align_type, "
                    + "  " + TABLE_NAME + "." + ID_ALIGN_TAG + " as align_id_tag, "
                    + "  source." + SentenceDAO.ID + " as source_id, "
                    + "  source." + SentenceDAO.ID_TEXT + " as source_id_text, "
                    + "  source." + SentenceDAO.POSITION + " as source_position, "
                    + "  target." + SentenceDAO.ID + " as target_id, "
                    + "  target." + SentenceDAO.ID_TEXT + " as target_id_text, "
                    + "  target." + SentenceDAO.POSITION + " as target_position "
                    + "FROM "
                    + "  public." + TABLE_NAME + ", "
                    + "  public." + SentenceDAO.TABLE_NAME + " as source, "
                    + "  public." + SentenceDAO.TABLE_NAME + " as target "
                    + "WHERE "
                    + "  source.id_text = ? AND "
                    + "  source.id = " + TABLE_NAME + "." + ID_SOURCE + " AND "
                    + "  target.id = " + TABLE_NAME + "." + ID_TARGET + ";";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_text);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    SentenceAlign aux;
                    Sentence source, target;
                    while (rs.next()) {
                        aux = new SentenceAlign();
                        aux.setIdSource(rs.getInt("align_id_source"));
                        aux.setIdTarget(rs.getInt("align_id_target"));
                        aux.setType(rs.getString("align_type"));
                        aux.setIdAlignTag(rs.getInt("align_id_tag"));

                        source = new Sentence();
                        source.setId(rs.getInt("source_id"));
                        source.setIdText(rs.getInt("source_id_text"));
                        source.setPosition(rs.getInt("source_position"));

                        target = new Sentence();
                        target.setId(rs.getInt("target_id"));
                        target.setIdText(rs.getInt("target_id_text"));
                        target.setPosition(rs.getInt("target_position"));

                        aux.setSource(source);
                        aux.setTarget(target);
                        listAlign.add(aux);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(SentenceAlignDAO.class, ex);
        }
        return listAlign;
    }

    public static List<SentenceAlign> selectAll(int id_source_text, int id_target_text) {
        List<SentenceAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID_SOURCE + ", " + ID_TARGET + ", " + TABLE_NAME + "." + TYPE + " AS align_type, " + TABLE_NAME + "." + ID_ALIGN_TAG + " AS align_id_tag, "
                    + "source." + SentenceDAO.ID + " AS source_id, source." + SentenceDAO.ID_TEXT + " AS source_id_text, source." + SentenceDAO.POSITION + " AS source_position, source." + SentenceDAO.TYPE + " AS source_type, "
                    + "target." + SentenceDAO.ID + " AS target_id, target." + SentenceDAO.ID_TEXT + " AS target_id_text, target." + SentenceDAO.POSITION + " AS target_position, target." + SentenceDAO.TYPE + " AS target_type "
                    + "FROM " + TABLE_NAME + ", " + SentenceDAO.TABLE_NAME + " source, " + SentenceDAO.TABLE_NAME + " target "
                    + "WHERE source." + SentenceDAO.ID_TEXT + " = ? "
                    + "AND target." + SentenceDAO.ID_TEXT + " = ? "
                    + "AND source." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_text);
                ps.setInt(2, id_target_text);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        SentenceAlign obj = new SentenceAlign();
                        obj.setIdSource(rs.getInt(ID_SOURCE));
                        obj.setIdTarget(rs.getInt(ID_TARGET));
                        obj.setType(rs.getString("align_type"));
                        obj.setIdAlignTag(rs.getInt("align_id_tag"));

                        Sentence ss = new Sentence();
                        ss.setId(rs.getInt("source_id"));
                        ss.setIdText(rs.getInt("source_id_text"));
                        ss.setPosition(rs.getInt("source_position"));
                        ss.setType(rs.getInt("target_type"));

                        Sentence ts = new Sentence();
                        ts.setId(rs.getInt("target_id"));
                        ts.setIdText(rs.getInt("target_id_text"));
                        ts.setPosition(rs.getInt("target_position"));
                        ts.setType(rs.getInt("target_type"));

                        obj.setSource(ss);
                        obj.setTarget(ts);

                        aligns.add(obj);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static List<SentenceAlign> selectByIdText(int id_source_text, int id_target_text, int offset, int limit) {
        List<SentenceAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID_SOURCE + ", " + ID_TARGET + ", " + TABLE_NAME + "." + TYPE + " AS align_type, " + TABLE_NAME + "." + ID_ALIGN_TAG + " AS align_id_tag, "
                    + "source." + SentenceDAO.ID + " AS source_id, source." + SentenceDAO.ID_TEXT + " AS source_id_text, source." + SentenceDAO.POSITION + " AS source_position, source." + SentenceDAO.TYPE + " AS source_type, "
                    + "target." + SentenceDAO.ID + " AS target_id, target." + SentenceDAO.ID_TEXT + " AS target_id_text, target." + SentenceDAO.POSITION + " AS target_position, target." + SentenceDAO.TYPE + " AS target_type "
                    + "FROM " + TABLE_NAME + ", " + SentenceDAO.TABLE_NAME + " source, " + SentenceDAO.TABLE_NAME + " target "
                    + "WHERE source." + SentenceDAO.ID_TEXT + " = ? "
                    + "AND target." + SentenceDAO.ID_TEXT + " = ? "
                    + "AND source." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_text);
                ps.setInt(2, id_target_text);
                ps.setInt(3, limit);
                ps.setInt(4, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        SentenceAlign obj = new SentenceAlign();
                        obj.setIdSource(rs.getInt(ID_SOURCE));
                        obj.setIdTarget(rs.getInt(ID_TARGET));
                        obj.setType(rs.getString("align_type"));
                        obj.setIdAlignTag(rs.getInt("align_id_tag"));

                        Sentence ss = new Sentence();
                        ss.setId(rs.getInt("source_id"));
                        ss.setIdText(rs.getInt("source_id_text"));
                        ss.setPosition(rs.getInt("source_position"));
                        ss.setType(rs.getInt("target_type"));

                        Sentence ts = new Sentence();
                        ts.setId(rs.getInt("target_id"));
                        ts.setIdText(rs.getInt("target_id_text"));
                        ts.setPosition(rs.getInt("target_position"));
                        ts.setType(rs.getInt("target_type"));
                        
                        //too slow
                        if (rs.getRow() <= 10){
                            ss.setWords(WordDAO.selectAllByIdSentence(c, rs.getInt("source_id")));//to show the sentence text (words)
                            ts.setWords(WordDAO.selectAllByIdSentence(c, rs.getInt("target_id")));//to show the sentence text (words)
                        }

                        obj.setSource(ss);
                        obj.setTarget(ts);

                        aligns.add(obj);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }
    
    public static List<SentenceAlign> selectAllWithTagByIdCorpus(int id_corpus, int offset, int limit) {
        List<SentenceAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT " + ID_SOURCE + ", " + ID_TARGET + ", " + TABLE_NAME + "." + TYPE + " AS align_type, " + TABLE_NAME + "." + ID_ALIGN_TAG + " AS align_id_tag, "
                    + "source." + SentenceDAO.ID + " AS source_id, source." + SentenceDAO.ID_TEXT + " AS source_id_text, source." + SentenceDAO.POSITION + " AS source_position, source." + SentenceDAO.TYPE + " AS source_type, "
                    + "target." + SentenceDAO.ID + " AS target_id, target." + SentenceDAO.ID_TEXT + " AS target_id_text, target." + SentenceDAO.POSITION + " AS target_position, target." + SentenceDAO.TYPE + " AS target_type "
                    + "FROM " + TABLE_NAME + ", " + SentenceDAO.TABLE_NAME + " source, " + SentenceDAO.TABLE_NAME + " target, " + TextDAO.TABLE_NAME + " source_text, " + TextDAO.TABLE_NAME + " target_text "
                    + "WHERE source_text." + TextDAO.ID_CORPUS + " = ? "
                    + "AND target_text." + TextDAO.ID_CORPUS + " = ? "
                    + "AND source." + SentenceDAO.ID_TEXT + " = source_text." + TextDAO.ID + " "
                    + "AND target." + SentenceDAO.ID_TEXT + " = target_text." + TextDAO.ID + " "
                    + "AND source." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "AND " + ID_ALIGN_TAG + " IS NOT NULL "
                    + "AND " + ID_ALIGN_TAG + " > 0 "
                    + "LIMIT ? "
                    + "OFFSET ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                ps.setInt(2, id_corpus);
                ps.setInt(3, limit);
                ps.setInt(4, offset);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        SentenceAlign obj = new SentenceAlign();
                        obj.setIdSource(rs.getInt(ID_SOURCE));
                        obj.setIdTarget(rs.getInt(ID_TARGET));
                        obj.setType(rs.getString("align_type"));
                        obj.setIdAlignTag(rs.getInt("align_id_tag"));

                        Sentence ss = new Sentence();
                        ss.setId(rs.getInt("source_id"));
                        ss.setIdText(rs.getInt("source_id_text"));
                        ss.setPosition(rs.getInt("source_position"));
                        ss.setType(rs.getInt("target_type"));

                        Sentence ts = new Sentence();
                        ts.setId(rs.getInt("target_id"));
                        ts.setIdText(rs.getInt("target_id_text"));
                        ts.setPosition(rs.getInt("target_position"));
                        ts.setType(rs.getInt("target_type"));
                        
                        //too slow
                        if (rs.getRow() <= 10){
                            ss.setWords(WordDAO.selectAllByIdSentence(c, rs.getInt("source_id")));//to show the sentence text (words)
                            ts.setWords(WordDAO.selectAllByIdSentence(c, rs.getInt("target_id")));//to show the sentence text (words)
                        }

                        obj.setSource(ss);
                        obj.setTarget(ts);

                        aligns.add(obj);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static int countByIdText(int id_source_text, int id_target_text) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + ", " + SentenceDAO.TABLE_NAME + " source, " + SentenceDAO.TABLE_NAME + " target "
                    + "WHERE source." + SentenceDAO.ID_TEXT + " = ? "
                    + "AND target." + SentenceDAO.ID_TEXT + " = ? "
                    + "AND source." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_text);
                ps.setInt(2, id_target_text);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }
    
    public static int countAllWithTagByIdCorpus(int id_corpus) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + ", " + SentenceDAO.TABLE_NAME + " source, " + SentenceDAO.TABLE_NAME + " target, " + TextDAO.TABLE_NAME + " source_text, " + TextDAO.TABLE_NAME + " target_text "
                    + "WHERE source_text." + TextDAO.ID_CORPUS + " = ? "
                    + "AND target_text." + TextDAO.ID_CORPUS + " = ? "
                    + "AND source." + SentenceDAO.ID_TEXT + " = source_text." + TextDAO.ID + " "
                    + "AND target." + SentenceDAO.ID_TEXT + " = target_text." + TextDAO.ID + " "
                    + "AND source." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_SOURCE + " "
                    + "AND target." + SentenceDAO.ID + " = " + TABLE_NAME + "." + ID_TARGET + " "
                    + "AND " + ID_ALIGN_TAG + " IS NOT NULL "
                    + "AND " + ID_ALIGN_TAG + " > 0 ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_corpus);
                ps.setInt(2, id_corpus);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }
    
    public static int countByIdAlignTag(int id_align_tag) {
        int value = -1;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT count(*) AS number "
                    + "FROM " + TABLE_NAME + " " 
                    + "WHERE " + ID_ALIGN_TAG + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_align_tag);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        value = rs.getInt("number");
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            //throw new DAOException(e);
            Logger.error(WordDAO.class, ex);
        }
        return value;
    }

    public static SentenceAlign selectById(int id_source_sentence, int id_target_sentence) {
        SentenceAlign align = null;
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? AND " + ID_TARGET + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_sentence);
                ps.setInt(2, id_target_sentence);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        align = new SentenceAlign();
                        align.setIdSource(rs.getInt(ID_SOURCE));
                        align.setIdTarget(rs.getInt(ID_TARGET));
                        align.setType(rs.getString(TYPE));
                        align.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return align;
    }

    public static List<SentenceAlign> selectAllByIdSource(int id_source_word) {
        List<SentenceAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_word);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        SentenceAlign a = new SentenceAlign();
                        a.setIdSource(rs.getInt(ID_SOURCE));
                        a.setIdTarget(rs.getInt(ID_TARGET));
                        a.setType(rs.getString(TYPE));
                        a.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));

                        aligns.add(a);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static void deleteAllByIdSource(int id_source_word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_SOURCE + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_source_word);
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }

    public static List<SentenceAlign> selectAllByIdTarget(int id_target_word) {
        List<SentenceAlign> aligns = new ArrayList<>();
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "SELECT * "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TARGET + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_target_word);
                //execute
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        SentenceAlign a = new SentenceAlign();
                        a.setIdSource(rs.getInt(ID_SOURCE));
                        a.setIdTarget(rs.getInt(ID_TARGET));
                        a.setType(rs.getString(TYPE));
                        a.setIdAlignTag(rs.getInt(ID_ALIGN_TAG));
                        //add to list
                        aligns.add(a);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            //throw new DAOException(e);
        }
        return aligns;
    }

    public static void deleteAllByIdTarget(int id_target_word) throws DAOException {
        try (Connection c = BuildConnection.getConnection()) {
            //prepare sql
            String sql = "DELETE "
                    + "FROM " + TABLE_NAME + " "
                    + "WHERE " + ID_TARGET + " = ? ";
            try (PreparedStatement ps = c.prepareStatement(sql)) {
                ps.setInt(1, id_target_word);
                //execute
                ps.execute();
            } catch (SQLException ex) {
                throw ex;
            }
        } catch (SQLException | ClassNotFoundException | IOException | URISyntaxException ex) {
            Logger.error(WordsAlignDAO.class, ex);
            throw new DAOException(ex);
        }
    }
}
