package br.project.controller.support;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.SupportDAO;
import br.project.entity.Support;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SupportViewController
 *
 * <pre>
 * pages:
 * support/view.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/08/2014
 *
 */
@WebServlet(
        name = "SupportViewController",
        description = "Portal Min@s",
        urlPatterns = {
            "/support/view"
        }
)
public class SupportViewController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            Support s = SupportDAO.selectById(Integer.valueOf(request.getParameter("id")));
            s.setStatus(Support.READ);

            SupportDAO.update(s);
            
            request.setAttribute("support", s);

            //Page viewer
            template.pageViewer(
                    null, //"menu/support.jsp", //page_menu
                    "breadcrumb/support.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "support", //page_menu_option_sub
                    "view", //page_menu_tools
                    "view", //page_menu_tools_sub
                    "Mensagem", //page_title
                    null, //page_subtitle
                    "support/view.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/support/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
