package br.project.controller.support;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.SupportDAO;
import br.project.entity.Support;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SupportSelectController
 *
 * <pre>
 * pages:
 * support/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/08/2014
 *
 */
@WebServlet(
        name = "SupportSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/support/select",
            "/support/list"
        }
)
public class SupportSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            List<Support> list = SupportDAO.selectAll();

            request.setAttribute("supportList", list);

            //Page viewer
            template.pageViewer(
                    null, //"menu/support.jsp", //page_menu
                    "breadcrumb/support.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "support", //page_menu_option_sub
                    "select", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Mensagens", //page_title
                    null, //page_subtitle
                    "support/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
