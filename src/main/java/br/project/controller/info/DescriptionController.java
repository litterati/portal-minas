/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.info;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.entity.Corpus;
import br.project.task.AlignTask;
import br.project.task.DeleteTextTask;
import br.project.task.TaskManagement;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextDeleteController
 *
 * <pre>
 * pages:
 * info/description.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "DescriptionController",
        description = "Portal Min@s",
        urlPatterns = {
            "/info/description"
        }
)
public class DescriptionController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            request.setAttribute("corpus", corpus);

            //Page viewer
            template.pageViewer(
                    "menu/info.jsp", //page_menu
                    "breadcrumb/info.jsp", //page_breadcrumb
                    "" + corpus.getId(), //page_menu_id_corpus
                    corpus.getName(), //page_menu_name_corpus
                    "corpus", //page_menu_option
                    "" + corpus.getId(), //page_menu_option_sub
                    "info", //page_menu_tools
                    "description", //page_menu_tools_sub
                    corpus.getName(), //page_title
                    null, //page_subtitle
                    "info/description.jsp"); //page

            template.forward();

        } catch (Exception ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/info", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }

    }

}
