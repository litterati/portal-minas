/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.info;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.dao.MetaWordsDAO;
import br.project.dao.TextDAO;
import br.project.dao.WordDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextDeleteController
 *
 * <pre>
 * pages:
 * info/info.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "InfoController",
        description = "Portal Min@s",
        urlPatterns = {
            "/info"
        }
)
public class InfoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.INFO);
           
            String order = request.getParameter("order");
            if ( order != null && order.equals(TextDAO.LANGUAGE)){
                    order = TextDAO.LANGUAGE;
            } else {
                order = TextDAO.TITLE;
            }

            // Text list
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(corpus.getId(), order);

            // Count languages
            HashMap<String, Integer> languages = new HashMap<>();
            int total_languages = 0;
            for (Text text : listTexts) {
                if (languages.containsKey(text.getLanguage())) {
                    languages.put(text.getLanguage(),
                            languages.get(text.getLanguage()) + 1);
                } else {
                    languages.put(text.getLanguage(), 1);
                }
                total_languages++;
            }

            // Count words in each text
            int[][] size = new int[listTexts.size()][2];
            int[] total_size = {0, 0};
            for (int i = 0; i < listTexts.size(); i++) {
                size[i][0] = WordDAO.countByIdText(listTexts.get(i).getId());
                size[i][1] = MetaWordsDAO.countByIdText(listTexts.get(i).getId());
                total_size[0] += size[i][0];
                total_size[1] += size[i][1];
            }

            request.setAttribute("texts", listTexts);
            request.setAttribute("size", size);
            request.setAttribute("total_size", total_size);
            request.setAttribute("languages", languages);
            request.setAttribute("total_languages", total_languages);

            //Page viewer
            template.pageViewer(
                    "menu/info.jsp", //page_menu
                    "breadcrumb/info.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "info", //page_menu_tools
                    "info", //page_menu_tools_sub
                    "Estatísticas", //page_title
                    "info/info.jsp"); //page

            template.forward();

        } catch (Exception ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }

    }

}
