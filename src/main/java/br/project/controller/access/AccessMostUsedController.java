package br.project.controller.access;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AccessDAO;
import br.project.dao.CorpusDAO;
import br.project.dao.UserDAO;
import br.project.entity.Access;
import br.project.entity.Corpus;
import br.project.entity.User;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AccessMostUsedController
 *
 * <pre>
 * pages:
 * access/most-used.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 13/01/2015
 *
 */
@WebServlet(
        name = "AccessMostUsedController",
        description = "Portal Min@s",
        urlPatterns = {
            "/access/most-used"
        }
)
public class AccessMostUsedController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            
            //period
            String start = request.getParameter("start");
            String end = request.getParameter("end");
            
            if (start == null){
                start = getCurrentDate();
            }
            if (end == null){
                end = getCurrentDate();
            }
            
            validateDate(start);
            validateDate(end);
            
            request.setAttribute("end", end);
            request.setAttribute("start", start);
            
            List<Access> accessList = AccessDAO.selectAllByDate(start, end);
            List<Corpus> corpusList = CorpusDAO.selectAll(Corpus.ALL);
            List<User> userList = UserDAO.selectAll();
            
            Map<String, Integer> mostUsedTool = new TreeMap<>();
            Map<String, Integer> mostUsedCorpus = new TreeMap<>();
            Map<String, Integer> mostActiveUser = new TreeMap<>();

            //complete the access list
            for (Access access : accessList) {
                for (Corpus corpus : corpusList) {
                    if (corpus.getId() == access.getIdCorpus()) {
                        access.setCorpus(corpus);
                        break;
                    }
                }
                for (User user : userList) {
                    if (user.getId() == access.getIdUser()) {
                        access.setUser(user);
                        break;
                    }
                }
                //cont the most used tool
                if (mostUsedTool.containsKey(access.getTool())){
                    mostUsedTool.put(access.getTool(), 
                            mostUsedTool.get(access.getTool()) + 1);
                } else {
                    mostUsedTool.put(access.getTool(), 1);
                }
                //count the most used corpus
                if (mostUsedCorpus.containsKey(access.getCorpus().getName())){
                    mostUsedCorpus.put(access.getCorpus().getName(), 
                            mostUsedCorpus.get(access.getCorpus().getName()) + 1);
                } else {
                    mostUsedCorpus.put(access.getCorpus().getName(), 1);
                }
                //count the most active user
                if (mostActiveUser.containsKey(access.getUser().getName())){
                    mostActiveUser.put(access.getUser().getName(), 
                            mostActiveUser.get(access.getUser().getName()) + 1);
                } else {
                    mostActiveUser.put(access.getUser().getName(), 1);
                }
            }

            request.setAttribute("mostUsedTool", mostUsedTool);
            request.setAttribute("mostUsedCorpus", mostUsedCorpus);
            request.setAttribute("mostActiveUser", mostActiveUser);

            //Page viewer
            template.pageViewer(
                    "menu/access.jsp", //page_menu
                    "breadcrumb/access.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "access", //page_menu_option_sub
                    "most-used", //page_menu_tools
                    "most-used", //page_menu_tools_sub
                    "Informações de acesso", //page_title
                    "Mais usado", //page_subtitle
                    "access/most-used.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/access/analysis", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private Date validateDate (String dateToValidate) throws ParseException {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        //if not valid, it will throw ParseException
        return sdf.parse(dateToValidate);
    }
    
    private String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
