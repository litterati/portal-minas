package br.project.controller.access;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AccessDAO;
import br.project.dao.CorpusDAO;
import br.project.dao.UserDAO;
import br.project.entity.Access;
import br.library.util.access.AccessAnalysis;
import br.project.entity.Corpus;
import br.project.entity.User;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AccessAnalysisController
 *
 * <pre>
 * pages:
 * access/analysis.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 27/01/2015
 *
 */
@WebServlet(
        name = "AccessAnalysisController",
        description = "Portal Min@s",
        urlPatterns = {
            "/access/analysis"
        }
)
public class AccessAnalysisController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            
            //period
            String start = request.getParameter("start");
            String end = request.getParameter("end");
            
            if (start == null){
                start = getCurrentDate();
            }
            if (end == null){
                end = getCurrentDate();
                
            }
            
            validateDate(start);
            validateDate(end);
            
            request.setAttribute("end", end);
            request.setAttribute("start", start);
            
            List<AccessAnalysis<Integer>> accessList = AccessDAO.selectAndCountByDate(start, end);
            List<Corpus> corpusList = CorpusDAO.selectAll(Corpus.ALL);
            //List<User> userList = UserDAO.selectAll();
            
            // <date|corpus_name, count>
            Map<String, Integer> accessMap = new TreeMap<>();
            
            List<String> corpusNameList = new ArrayList<>();
            List<String> dateList = new ArrayList<>();

            //convert access list into a map by date
            for (AccessAnalysis<Integer> aa : accessList) {
                String corpusName = aa.getAnalyzed().toString();
                String date = aa.getDateAccess();
                //find corpus name
                for (Corpus corpus : corpusList) {
                    if (corpus.getId() == aa.getAnalyzed()) {
                        corpusName = corpus.getName();
                        break;
                    }
                }
                //list corpus name
                if (!corpusNameList.contains(corpusName)){
                    corpusNameList.add(corpusName);
                }
                //list date
                if (!dateList.contains(date)){
                    dateList.add(date);
                }
                accessMap.put(date + "|" + corpusName, aa.getCount());
            }

            request.setAttribute("accessMap", accessMap);
            request.setAttribute("corpusNameList", corpusNameList);
            request.setAttribute("dateList", dateList);

            //Page viewer
            template.pageViewer(
                    "menu/access.jsp", //page_menu
                    "breadcrumb/access.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "access", //page_menu_option_sub
                    "analysis", //page_menu_tools
                    "analysis", //page_menu_tools_sub
                    "Informações de acesso", //page_title
                    "Análise", //page_subtitle
                    "access/analysis.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private Date validateDate (String dateToValidate) throws ParseException {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        //if not valid, it will throw ParseException
        return sdf.parse(dateToValidate);
    }
    
    private String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
