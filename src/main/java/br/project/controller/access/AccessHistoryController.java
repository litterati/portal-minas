package br.project.controller.access;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AccessDAO;
import br.project.dao.CorpusDAO;
import br.project.dao.UserDAO;
import br.project.entity.Access;
import br.project.entity.Corpus;
import br.project.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AccessHistoryController
 *
 * <pre>
 * pages:
 * access/history.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 24/01/2015
 *
 */
@WebServlet(
        name = "AccessHistoryController",
        description = "Portal Min@s",
        urlPatterns = {
            "/access/history"
        }
)
public class AccessHistoryController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //order by
            String order = request.getParameter("order");
            if (order != null && order.equals("user")) {
                order = AccessDAO.ID_USER;
            } else if (order != null && order.equals("tool")) {
                order = AccessDAO.TOOL;
            } else if (order != null && order.equals("date")) {
                order = AccessDAO.DATE_ACCESS;
            } else {
                order = AccessDAO.ID_CORPUS;
            }
            request.setAttribute("order", order);

            List<Access> accessList = AccessDAO.selectAll(order);
            List<Corpus> corpusList = CorpusDAO.selectAll(Corpus.ALL);
            List<User> userList = UserDAO.selectAll();

            //complete the access list
            for (Access access : accessList) {
                for (Corpus corpus : corpusList) {
                    if (corpus.getId() == access.getIdCorpus()) {
                        access.setCorpus(corpus);
                        break;
                    }
                }
                for (User user : userList) {
                    if (user.getId() == access.getIdUser()) {
                        access.setUser(user);
                        break;
                    }
                }
            }

            request.setAttribute("accessList", accessList);

            //Page viewer
            template.pageViewer(
                    "menu/access.jsp", //page_menu
                    "breadcrumb/access.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "access", //page_menu_option_sub
                    "history", //page_menu_tools
                    "history", //page_menu_tools_sub
                    "Informações de acesso", //page_title
                    "Histórico", //page_subtitle
                    "access/history.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/access/analysis", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
