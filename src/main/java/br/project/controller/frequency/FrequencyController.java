/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.frequency;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.dao.WordDAO;
import br.project.entity.Corpus;
import br.project.exception.DAOException;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FrequencyController.java
 *
 * <pre>
 * pages:
 * frequency/frequency.jsp
 * </pre>
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 *
 */
@WebServlet(
        name = "FrequencyController",
        description = "Portal Min@s",
        urlPatterns = {
            "/frequency"
        }
)
public class FrequencyController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.FREQUENCY);
            
            Map<String, String> frequenciesMap = this.pagination(request, corpus);

            request.setAttribute("frequencies", frequenciesMap);
            
            //Page viewer
            template.pageViewer(
                    null, //"menu/frequency.jsp", //page_menu
                    "breadcrumb/frequency.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "frequency", //page_menu_tools
                    "frequency", //page_menu_tools_sub
                    "Contador de frequência", //page_title
                    "frequency/frequency.jsp"); //page

            template.forward();

        } catch (Exception ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }
    }
    
    private Map<String, String> pagination(HttpServletRequest request, Corpus corpus) throws DAOException {
        // Paginação
        int start = 0;
        int range = 3000;
        int page = 1;
        
        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }
        
        int total_tokens = WordDAO.frequencyDistinctWords(corpus.getId()).size();
        int size = (int) Math.ceil((double) total_tokens / (double) range);
        
        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total_tokens", total_tokens);
        request.setAttribute("pagination_range", range);
        
        String order = request.getParameter("order");
        if ( order != null && order.equals("word")){
                order = "word";
        } else {
            order = "frequency";
        }
        request.setAttribute("order", order);
        
        return WordDAO.frequencyDistinctWords(corpus.getId(), start, range, order);
    }

}
