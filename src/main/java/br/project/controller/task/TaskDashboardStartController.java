/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.task;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.task.TaskManagement;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TaskDashboardStartController
 *
 * @author Thiago Vieira
 * @since 18/12/2014
 *
 */
@WebServlet(
        name = "TaskDashboardStartController",
        description = "Portal Min@s",
        urlPatterns = {
            "/task/dashboard/start"
        }
)
public class TaskDashboardStartController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            
            TaskManagement taskManagement = TaskManagement.getInstance();
            taskManagement.startTaskManagement();

            template.redirect(
                    this, //servlet
                    "/task/dashboard", //url
                    "Gerenciador de Tarefas iniciado...", //success
                    null, //warning
                    null //error
            );
        } catch (Exception ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/task/dashboard", //url
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }
    }

}
