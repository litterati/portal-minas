/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.task;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TaskDAO;
import br.project.entity.Task;
import br.project.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * TaskSelectController
 *
 * <pre>
 * pages:
 * task/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/09/2014
 *
 */
@WebServlet(
        name = "TaskSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/task/select",
            "/task/list"
        }
)
public class TaskSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Select all tasks
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("userLoged");
            
            List<Task> tasks = this.pagination(request, user);
            request.setAttribute("tasks_list", tasks);

            //Page viewer
            template.pageViewer(
                    "menu/task.jsp", //page_menu
                    "breadcrumb/task.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "task", //page_menu_option
                    null, //page_menu_option_sub
                    "select", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Tarefas", //page_title
                    null, //page_subtitle
                    "task/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private List<Task> pagination(HttpServletRequest request, User user) {
        // Paginação
        int start = 0;
        int range = 10;
        int page = 1;
        
        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }
        
        int total = TaskDAO.countByIdUser(user.getId());
        int size = (int) Math.ceil((double) total / (double) range);
        
        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total", total);
        request.setAttribute("pagination_range", range);
        
        return TaskDAO.selectByIdUser(user.getId(), start, range);
    }

}
