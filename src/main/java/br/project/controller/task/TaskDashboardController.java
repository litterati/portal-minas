/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.task;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.task.TaskManagement;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TaskDashboardController
 *
 * <pre>
 * pages:
 * task/dashboard.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 18/12/2014
 *
 */
@WebServlet(
        name = "TaskDashboardController",
        description = "Portal Min@s",
        urlPatterns = {
            "/task/dashboard"
        }
)
public class TaskDashboardController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            
            TaskManagement taskManagement = TaskManagement.getInstance();

            request.setAttribute("queueSize", taskManagement.queueSize());
            request.setAttribute("numberOfProcessRunning", taskManagement.numberOfProcessRunning());
            request.setAttribute("stateName", taskManagement.getState().name());
            request.setAttribute("dateStarted", taskManagement.getDateStarted());
            //request.setAttribute("taskManagement", taskManagement);

            //Page viewer
            template.pageViewer(
                    "menu/task.jsp", //page_menu
                    "breadcrumb/task.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "task", //page_menu_option
                    null, //page_menu_option_sub
                    "dashboard", //page_menu_tools
                    "dashboard", //page_menu_tools_sub
                    "Tarefas", //page_title
                    "Painel de Controle", //page_subtitle
                    "task/dashboard.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
