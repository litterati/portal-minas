/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.captcha;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.channels.DatagramChannel;

import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CaptchaController.java
 *
 *
 * @author Thiago Vieira
 * @since 31/10/2014
 * @see http://zetcode.com/tutorials/jeetutorials/captcha/
 */
@WebServlet(
        name = "CaptchaController",
        description = "Portal Min@s",
        urlPatterns = {"/captcha/*"}
)
public class CaptchaController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int width = 150;
        int height = 50;

//        char data[][] = {
//            {'z', 'e', 't', 'c', 'o', 'd', 'e'},
//            {'l', 'i', 'n', 'u', 'x'},
//            {'f', 'r', 'e', 'e', 'b', 's', 'd'},
//            {'u', 'b', 'u', 'n', 't', 'u'},
//            {'j', 'e', 'e'}
//        };

        BufferedImage bufferedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

        Graphics2D g2d = bufferedImage.createGraphics();

        Font font = new Font("Georgia", Font.BOLD, 18);
        g2d.setFont(font);

        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        rh.put(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        g2d.setRenderingHints(rh);

        GradientPaint gp = new GradientPaint(
                0, //x1
                0, //y1
                getRandomColor(), //color1
                0, //x2
                height / 2, //y2
                getRandomColor(), //color2 
                true);//cyclic

        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);

        g2d.setColor(getRandomColor());//text color

//        Random r = new Random();
//        int index = Math.abs(r.nextInt()) % 5;
//
//        String captcha = String.copyValueOf(data[index]);
//        request.getSession().setAttribute("captcha", captcha);
//
//        int x = 0;
//        int y = 0;
//
//        for (int i = 0; i < data[index].length; i++) {
//            x += 10 + (Math.abs(r.nextInt()) % 15);
//            y = 20 + Math.abs(r.nextInt()) % 20;
//            g2d.drawChars(data[index], i, 1, x, y);
//        }
        
        //-----------------------------
        Random r = new Random();
        
        String captcha = generateRandomString();
        
        int x = 0;
        int y = 0;
        
        request.getSession().setAttribute("captcha", captcha);
        
        for (int i = 0; i < captcha.length(); i++) {
            x += 10 + (Math.abs(r.nextInt()) % 15);
            y = 20 + Math.abs(r.nextInt()) % 20;
            g2d.drawChars(captcha.toCharArray(), i, 1, x, y);
        }
        //-----------------------------

        g2d.dispose();

        response.setContentType("image/png");
        OutputStream os = response.getOutputStream();
        ImageIO.write(bufferedImage, "png", os);
        os.close();
    }

    protected Color getRandomColor() {
        int R = (int) (Math.random() * 256);
        int G = (int) (Math.random() * 256);
        int B = (int) (Math.random() * 256);
        return new Color(R, G, B); //random color, but can be bright or dull
    }

    /**
     * This method generates random string
     *
     * @return
     */
    protected String generateRandomString() {
        String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int RANDOM_STRING_LENGTH = 6;
        StringBuilder randStr = new StringBuilder();
        for (int i = 0;
                i < RANDOM_STRING_LENGTH;
                i++) {
            int number = getRandomNumber(CHAR_LIST.length());
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }

        return randStr.toString();
    }

    /**
     * This method generates random numbers
     *
     * @param char_list_lenght
     * @return int
     */
    protected int getRandomNumber(int char_list_lenght) {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(char_list_lenght);
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
