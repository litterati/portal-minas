/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.dao.TextAlignDAO;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.entity.TextAlign;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ParallelAlignSelectController
 *
 * <pre>
 * pages:
 * align/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 *
 */
@WebServlet(
        name = "ParallelAlignSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/select",
            "/align/list"
        }
)
public class ParallelAlignSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.ALIGN);

            // List TextAligns
            //ArrayList<TextAlign> list = TextAlignDAO.selectByIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            List<TextAlign> list = this.pagination(request, corpus);

            // List texts
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(corpus.getId(), TextDAO.TITLE);

            request.setAttribute("text_align", list);
            request.setAttribute("list_texts", listTexts);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Alinhamentos", //page_title
                    "align/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e); //log
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private List<TextAlign> pagination(HttpServletRequest request, Corpus corpus) {
        // Paginação
        int start = 0;
        int range = 50;
        int page = 1;
        
        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }
        
        int total_tokens = TextAlignDAO.countByIdCorpus(corpus.getId());
        int size = (int) Math.ceil((double) total_tokens / (double) range);
        
        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total", total_tokens);
        request.setAttribute("pagination_range", range);
        
        String order = request.getParameter("order");
        if ( order != null && order.equals("target") ){
            order = TextAlignDAO.ID_TARGET;
        } else if ( order != null && order.equals(TextAlignDAO.TYPE) ){
            order = TextAlignDAO.TYPE;
        } else {
            // order.equals("source")
            order = TextAlignDAO.ID_SOURCE;
        }
        request.setAttribute("order", order);
        
        return TextAlignDAO.selectByIdCorpus(corpus.getId(), start, range, order);
    }

}
