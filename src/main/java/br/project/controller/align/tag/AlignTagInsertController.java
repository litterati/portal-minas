/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.tag;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AlignTagDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AlignTagInsertController
 *
 * <pre>
 * pages:
 * align/tag/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 22/10/2014
 *
 */
@WebServlet(
        name = "AlignTagInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/tag/insert",
            "/align/tag/create"
        }
)
public class AlignTagInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align-tag", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Etiquetas de Alinhamento", //page_title
                    "align/tag/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/tag/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new Align Tag
            AlignTag tag = new AlignTag();
            tag.setValue(request.getParameter("value"));
            tag.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));

            //insert
            AlignTagDAO.insert(tag);

            Logger.controllerEvent(this, request, "Align Tag Inserted");

            // Success
            template.redirect(
                    this, //servlet
                    "/align/tag/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Etiqueta de Alinhamento inserida com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/tag/select", //url
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
