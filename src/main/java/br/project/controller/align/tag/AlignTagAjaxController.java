/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.tag;

import br.library.util.Logger;
import br.project.dao.SentenceAlignDAO;
import br.project.dao.WordsAlignDAO;
import br.project.entity.SentenceAlign;
import br.project.entity.WordsAlign;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * AlignTagAjaxController
 *
 *
 * @author Thiago Vieira
 * @since 09/12/2014
 *
 */
@WebServlet(
        name = "AlignTagAjaxController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/tag/ajax"
        }
)
public class AlignTagAjaxController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            int id_source = Integer.parseInt(request.getParameter("id_source"));
            int id_target = Integer.parseInt(request.getParameter("id_target"));
            int id_align_tag = Integer.parseInt(request.getParameter("id_align_tag"));
            
            //Create new Align
            String log;
            if (request.getParameter("sentence") != null) {
                SentenceAlign align = SentenceAlignDAO.selectById(id_source, id_target);
                if (align == null){
                    throw new Exception("Alinhamento de Sentença não existe");
                }
                align.setIdAlignTag(id_align_tag);
                //update database
                SentenceAlignDAO.update(align);
                log = "Sentence Align Updated by Ajax";
            } else {//if (request.getParameter("word") != null) {
                WordsAlign align = WordsAlignDAO.selectById(id_source, id_target);
                if (align == null){
                    throw new Exception("Alinhamento de Palavras não existe");
                }
                align.setIdAlignTag(id_align_tag);
                //update database
                WordsAlignDAO.update(align);
                log = "Word Align Updated by Ajax";
            }

            Logger.controllerEvent(this, request, log);

            // Success
            // Ajax response
            JSONObject obj = new JSONObject();
            obj.put("alert_success", log);
            obj.put("id_source", id_source);
            obj.put("id_target", id_target);
            obj.put("id_align_tag", id_align_tag);
            response.setContentType("application/json");
            response.getWriter().write(obj.toString());
            //return;
        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            // Ajax response
            JSONObject obj = new JSONObject();
            obj.put("alert_danger", e.getMessage());
            response.setContentType("application/json");
            response.getWriter().write(obj.toString());
            //return;
        }
    }
}