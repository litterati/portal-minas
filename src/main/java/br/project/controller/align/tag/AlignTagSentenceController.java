/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.tag;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AlignTagDAO;
import br.project.dao.SentenceAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import br.project.entity.SentenceAlign;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AlignTagSentenceController
 *
 * <pre>
 * pages:
 * align/tag/sentence.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 14/12/2014
 *
 */
@WebServlet(
        name = "AlignTagSentenceController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/tag/sentence"
        }
)
public class AlignTagSentenceController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            // List SentenceAligns from the TextAlign
            List<SentenceAlign> list = this.pagination(request, corpus);

            Map<Integer, AlignTag> align_tags = AlignTagDAO.selectAllMap();

            request.setAttribute("sentence_align", list);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("align_tags", align_tags);

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align-tag", //page_menu_tools
                    "align-tag-sentence", //page_menu_tools_sub
                    "Lista de Alinhamentos com Etiquetas", //page_title
                    "align/tag/sentence.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e); //log
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private List<SentenceAlign> pagination(HttpServletRequest request, Corpus corpus) {
        // Paginação
        int start = 0;
        int range = 500;
        int page = 1;

        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }

        int total = SentenceAlignDAO.countAllWithTagByIdCorpus(corpus.getId());
        int size = (int) Math.ceil((double) total / (double) range);

        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total", total);
        request.setAttribute("pagination_range", range);

        return SentenceAlignDAO.selectAllWithTagByIdCorpus(corpus.getId(), start, range);
    }

}
