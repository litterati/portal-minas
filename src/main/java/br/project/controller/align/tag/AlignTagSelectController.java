/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.tag;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AlignTagDAO;
import br.project.dao.SentenceAlignDAO;
import br.project.dao.WordsAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AlignTagSelectController
 *
 * <pre>
 * pages:
 * align/tag/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 22/10/2014
 *
 */
@WebServlet(
        name = "AlignTagSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/tag/select",
            "/align/tag/list"
        }
)
public class AlignTagSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            List<AlignTag> tag_list = AlignTagDAO.selectAllByIdCorpus(corpus.getId());
            //to count the number of tags
            if (tag_list.size() > 0) {
                Map<AlignTag, Integer[]> tag_map = new HashMap<>();
                for (AlignTag alignTag : tag_list) {
                    Integer[] size = new Integer[2];
                    size[0] = SentenceAlignDAO.countByIdAlignTag(alignTag.getId());
                    size[1] = WordsAlignDAO.countByIdAlignTag(alignTag.getId());
                    //add
                    tag_map.put(alignTag, size);
                }
                request.setAttribute("tag_map", tag_map);
            }

            //Select all align tags
            request.setAttribute("tag_list", tag_list);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align-tag", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Listar Etiquetas de Alinhamento", //page_title
                    "align/tag/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //url
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
