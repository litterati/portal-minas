/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AlignTagDAO;
import br.project.dao.SentenceAlignDAO;
import br.project.dao.TextAlignDAO;
import br.project.dao.WordDAO;
import br.project.dao.WordsAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import br.project.entity.SentenceAlign;
import br.project.entity.TextAlign;
import br.project.entity.Words;
import br.project.entity.WordsAlign;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ParallelAlignSelectController
 *
 * <pre>
 * pages:
 * align/view.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 *
 */
@WebServlet(
        name = "ParallelAlignShowController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/show",
            "/align/view"
        }
)
public class ParallelAlignViewController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            // Select TextAlign
            TextAlign align = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source")),
                    Integer.parseInt(request.getParameter("id_target")));

            ParallelAlignViewController.pagination(request, align);
            
            List<AlignTag> align_tags = AlignTagDAO.selectAll();

            request.setAttribute("text_align", align);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("align_tags", align_tags);

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "view", //page_menu_tools_sub
                    "Visualizar alinhamento", //page_title
                    "align/view.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    public static void pagination(HttpServletRequest request, TextAlign align) {
        // Paginação
        int start = 0;
        int range = 5000;
        int page = 1;
        int bonus = 200;
        
        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }
        
        int total = WordDAO.countByIdText(align.getIdSource());
        int size = (int) Math.ceil((double) total / (double) range);
        
        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total_tokens", total);
        request.setAttribute("pagination_range", range);
        
        List<Words> source = WordDAO.selectByIdText(align.getIdSource(), start, range + bonus);
        List<Words> target = WordDAO.selectByIdText(align.getIdTarget(), start, range + bonus);
        List<WordsAlign> lstAligns = WordsAlignDAO.selectAll(align.getIdSource(), align.getIdTarget());
        
        // source
        for (WordsAlign wa : lstAligns) {
            for (Words w : source) {
                if (wa.getIdSource() == w.getId()){
                    if (w.getWordAlign() == null){
                        w.setWordAlign(new ArrayList<WordsAlign>());
                    }
                    w.getWordAlign().add(wa);
                }
            }
        }
        // target
        for (WordsAlign wa : lstAligns) {
            for (Words w : target) {
                if (wa.getIdTarget() == w.getId()){
                    if (w.getWordAlign() == null){
                        w.setWordAlign(new ArrayList<WordsAlign>());
                    }
                    w.getWordAlign().add(wa);
                }
            }
        }
        
        request.setAttribute("source", source);
        request.setAttribute("target", target);
        
        List<SentenceAlign> sentenceAlignList = SentenceAlignDAO.selectAll(align.getIdSource(), align.getIdTarget());
        HashMap<Integer, String> sentenceAlignCrrMap = new HashMap<>();
        HashMap<Integer, SentenceAlign> sentenceAlignMap = new HashMap<>();
        for (SentenceAlign sa : sentenceAlignList) {
            
            if (sentenceAlignCrrMap.containsKey(sa.getIdSource())){
                sentenceAlignCrrMap.put(sa.getIdSource(),  sentenceAlignCrrMap.get(sa.getIdSource()) + sa.getIdTarget() + " ");
                sentenceAlignMap.put(sa.getIdSource(),  sentenceAlignMap.get(sa.getIdSource()));
            } else {
                sentenceAlignCrrMap.put(sa.getIdSource(),  sa.getIdTarget() + " ");
                sentenceAlignMap.put(sa.getIdSource(),  sa);
            }

            if (sentenceAlignCrrMap.containsKey(sa.getIdTarget())){
                sentenceAlignCrrMap.put(sa.getIdSource(),  sentenceAlignCrrMap.get(sa.getIdTarget()) + sa.getIdSource() + " ");
                sentenceAlignMap.put(sa.getIdSource(),  sentenceAlignMap.get(sa.getIdTarget()));
            } else {
                sentenceAlignCrrMap.put(sa.getIdTarget(),  sa.getIdSource() + " ");
                sentenceAlignMap.put(sa.getIdTarget(),  sa);
            }
        }
        request.setAttribute("sentenceAlignCrrMap", sentenceAlignCrrMap);
        request.setAttribute("sentenceAlignMap", sentenceAlignMap);
    }

}
