/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextAlignDAO;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.entity.TextAlign;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * ParallelAlignDeleteController
 *
 * <pre>
 * pages:
 * align/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 *
 */
@WebServlet(
        name = "ParallelAlignDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/delete",
            "/align/remove"
        }
)
public class ParallelAlignDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // List texts
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(Integer.parseInt(request.getParameter("id_corpus")), TextDAO.TITLE);
            // Select TextAlign
            TextAlign align = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source")), 
                    Integer.parseInt(request.getParameter("id_target")));
            
            request.setAttribute("text_align", align);
            request.setAttribute("list_texts", listTexts);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover Alinhamento", //page_title
                    "align/form.jsp"); //page
            
            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            
            TextAlign text_align = new TextAlign();
            text_align.setIdSource(Integer.parseInt(request.getParameter("source")));
            text_align.setIdTarget(Integer.parseInt(request.getParameter("target")));
            //text_align.setType(request.getParameter("type"));
            
            TextAlignDAO dao = new TextAlignDAO();
            dao.delete(text_align);
            
            Logger.controllerEvent(this, request, "Parallel Align Deleted");
            
            // Ajax response
            if (request.getParameter("ajax") != null){
                JSONObject obj = new JSONObject();
                obj.put("alert_success","Alinhamento removido com sucesso");
                //obj.put("id_source", text_align.getIdSource());
                //obj.put("id_target", text_align.getIdTarget());
                //obj.put("type", text_align.getType());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }
            
            //Success
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Alinhamento removido com sucesso", //success
                    null, //warning
                    null //error
            );
            
        } catch (Exception ex) {
            Logger.controllerError(this, request, ex);
            
            // Ajax response
            if (request.getParameter("ajax") != null){
                JSONObject obj = new JSONObject();
                obj.put("alert_danger",ex.getMessage());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }
            
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }

    }

}
