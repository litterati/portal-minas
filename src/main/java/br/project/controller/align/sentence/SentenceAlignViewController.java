/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.sentence;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.controller.align.ParallelAlignViewController;
import br.project.dao.AlignTagDAO;
import br.project.dao.SentenceAlignDAO;
import br.project.dao.TextAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import br.project.entity.SentenceAlign;
import br.project.entity.TextAlign;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * SentenceAlignViewController
 *
 * <pre>
 * pages:
 * align/sentence/view.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 13/10/2014
 *
 */
@WebServlet(
        name = "SentenceAlignViewController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/sentence/show",
            "/align/sentence/view"
        }
)
public class SentenceAlignViewController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            // Select TextAlign
            TextAlign align = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source")),
                    Integer.parseInt(request.getParameter("id_target")));

            ParallelAlignViewController.pagination(request, align);
            
            List<AlignTag> align_tags = AlignTagDAO.selectAll();

            request.setAttribute("text_align", align);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("align_tags", align_tags);

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "view_sentence", //page_menu_tools_sub
                    "Visualizar alinhamento", //page_title
                    "align/sentence/view.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject obj = new JSONObject();// Ajax response

        try {
            String mode = request.getParameter("mode");
            if (mode != null) {
                if (mode.equals("remove_all_source")) {
                    String id_word = request.getParameter("id_sentence");
                    obj.put("id_sentence", id_word);

                    List<SentenceAlign> sa = SentenceAlignDAO.selectAllByIdSource(Integer.parseInt(id_word));
                    String old_align = "";
                    for (SentenceAlign object : sa) {
                        old_align += object.getIdTarget() + " ";
                    }
                    obj.put("old_aligns", old_align);

                    SentenceAlignDAO.deleteAllByIdSource(Integer.parseInt(id_word));
                } else if (mode.equals("remove_all_target")) {
                    String id_word = request.getParameter("id_sentence");
                    obj.put("id_sentence", id_word);

                    List<SentenceAlign> sa = SentenceAlignDAO.selectAllByIdTarget(Integer.parseInt(id_word));
                    String old_align = "";
                    for (SentenceAlign object : sa) {
                        old_align += object.getIdSource() + " ";
                    }
                    obj.put("old_aligns", old_align);

                    SentenceAlignDAO.deleteAllByIdTarget(Integer.parseInt(id_word));
                } else if (mode.equals("save")) {
                    String id_sentence_source = request.getParameter("id_sentence_source");
                    String id_sentence_target = request.getParameter("id_sentence_target");
                    String type = request.getParameter("type");
                    obj.put("id_sentence_source", id_sentence_source);
                    obj.put("id_sentence_target", id_sentence_target);
                    
                    SentenceAlign sa = new SentenceAlign();
                    sa.setIdSource(Integer.parseInt(id_sentence_source));
                    sa.setIdTarget(Integer.parseInt(id_sentence_target));
                    sa.setType((type == null) ? "t1:1" : type );
                    SentenceAlignDAO.insert(sa);
                    
                    List<SentenceAlign> sas = SentenceAlignDAO.selectAllByIdSource(Integer.parseInt(id_sentence_source));
                    String new_align_source = "";
                    for (SentenceAlign object : sas) {
                        new_align_source += object.getIdTarget() + " ";
                    }
                    obj.put("new_align_source", new_align_source);
                    
                    List<SentenceAlign> sat = SentenceAlignDAO.selectAllByIdTarget(Integer.parseInt(id_sentence_target));
                    String new_align_target = "";
                    for (SentenceAlign object : sat) {
                        new_align_target += object.getIdSource() + " ";
                    }
                    obj.put("new_align_target", new_align_target);
                }
                obj.put("alert_success", "OK");
            }
        } catch (Exception ex) {
            obj.put("alert_danger", ex.getMessage());
        }
        // Ajax response
        response.setContentType("application/json");
        response.getWriter().write(obj.toString());

    }

}
