/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.sentence;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AlignTagDAO;
import br.project.dao.SentenceAlignDAO;
import br.project.dao.SentenceDAO;
import br.project.dao.TextAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import br.project.entity.Sentence;
import br.project.entity.SentenceAlign;
import br.project.entity.TextAlign;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * SentenceAlignDeleteController
 *
 * <pre>
 * pages:
 * align/sentence/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 07/10/2014
 *
 */
@WebServlet(
        name = "SentenceAlignDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/sentence/delete",
            "/align/sentence/remove"
        }
)
public class SentenceAlignDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // Text Align
            TextAlign textAlign = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source_text")),
                    Integer.parseInt(request.getParameter("id_target_text")));

            List<Sentence> source = SentenceDAO.selectAllByIdText(textAlign.getIdSource());
            List<Sentence> target = SentenceDAO.selectAllByIdText(textAlign.getIdTarget());
            List<AlignTag> tags = AlignTagDAO.selectAllByIdCorpus(corpus.getId());

            SentenceAlign sentenceAlign = SentenceAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source_sentence")),
                    Integer.parseInt(request.getParameter("id_target_sentence")));

            request.setAttribute("textAlign", textAlign);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("source_sentences_list", source);
            request.setAttribute("target_sentences_list", target);
            request.setAttribute("sentenceAlign", sentenceAlign);
            request.setAttribute("tags", tags);

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "remove", //page_menu_tools_sub
                    "Remover Alinhamento", //page_title
                    "align/sentence/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/sentence/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage(), //error
                    new String[]{"id_source_text", request.getParameter("id_source_text")},
                    new String[]{"id_target_text", request.getParameter("id_target_text")}
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {

            SentenceAlign sentenceAlign = SentenceAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source_sentence")),
                    Integer.parseInt(request.getParameter("id_target_sentence")));

            SentenceAlignDAO.delete(sentenceAlign);

            Logger.controllerEvent(this, request, "Alinhamento de sentenças removido");

            // Ajax response
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                obj.put("alert_success", "Alinhamento de sentenças removido");
                obj.put("id_source_sentence", sentenceAlign.getIdSource());
                obj.put("id_target_sentence", sentenceAlign.getIdTarget());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            //Success
            template.redirect(
                    this, //servlet
                    "/align/sentence/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Alinhamento de sentenças removido", //success
                    null, //warning
                    null, //error
                    new String[]{"id_source_text", request.getParameter("id_source_text")},
                    new String[]{"id_target_text", request.getParameter("id_target_text")}
            );

        } catch (Exception ex) {
            // Ajax response
            Logger.controllerError(this, request, ex);
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                obj.put("alert_danger", ex.getMessage());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            // Error
            template.redirect(
                    this, //servlet
                    "/align/sentence/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage(), //error
                    new String[]{"id_source_text", request.getParameter("id_source_text")},
                    new String[]{"id_target_text", request.getParameter("id_target_text")}
            );
        }

    }

}
