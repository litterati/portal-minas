/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.word;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.AlignTagDAO;
import br.project.dao.TextAlignDAO;
import br.project.dao.WordsAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import br.project.entity.TextAlign;
import br.project.entity.WordsAlign;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * WordAlignSelectController
 *
 * <pre>
 * pages:
 * align/word/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 07/10/2014
 *
 */
@WebServlet(
        name = "WordAlignSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/word/select",
            "/align/word/list"
        }
)
public class WordAlignSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            // Text Align
            TextAlign textAlign = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source_text")),
                    Integer.parseInt(request.getParameter("id_target_text")));
            
            // List WordsAligns from the TextAlign
            List<WordsAlign> list = this.pagination(request, textAlign);
            
            Map<Integer, AlignTag> align_tags = AlignTagDAO.selectAllMap();

            request.setAttribute("word_align", list);
            request.setAttribute("id_source_text", textAlign.getIdSource());
            request.setAttribute("id_target_text", textAlign.getIdTarget());
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("align_tags", align_tags);

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Alinhamentos", //page_title
                    "align/word/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e); //log
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private List<WordsAlign> pagination(HttpServletRequest request, TextAlign align) {
        // Paginação
        int start = 0;
        int range = 500;
        int page = 1;
        
        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }
        
        int total = WordsAlignDAO.countByIdText(align.getIdSource(), align.getIdTarget());
        int size = (int) Math.ceil((double) total / (double) range);
        
        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total", total);
        request.setAttribute("pagination_range", range);
        
        return WordsAlignDAO.selectAll(align.getIdSource(), align.getIdTarget(), start, range);
    }

}
