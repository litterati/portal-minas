/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.word;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.controller.align.ParallelAlignViewController;
import br.project.dao.AlignTagDAO;
import br.project.dao.TextAlignDAO;
import br.project.dao.WordsAlignDAO;
import br.project.entity.AlignTag;
import br.project.entity.Corpus;
import br.project.entity.TextAlign;
import br.project.entity.WordsAlign;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * WordAlignViewController
 *
 * <pre>
 * pages:
 * align/sentence/view.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 13/10/2014
 *
 */
@WebServlet(
        name = "WordAlignViewController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/word/show",
            "/align/word/view"
        }
)
public class WordAlignViewController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            // Select TextAlign
            TextAlign align = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source")),
                    Integer.parseInt(request.getParameter("id_target")));

            ParallelAlignViewController.pagination(request, align);
            
            List<AlignTag> align_tags = AlignTagDAO.selectAll();

            request.setAttribute("text_align", align);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("align_tags", align_tags);

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "view_word", //page_menu_tools_sub
                    "Visualizar alinhamento", //page_title
                    "align/word/view.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject obj = new JSONObject();// Ajax response

        try {
            String mode = request.getParameter("mode");
            if (mode != null) {
                if (mode.equals("remove_all_source")) {
                    String id_word = request.getParameter("id_word");
                    obj.put("id_word", id_word);

                    List<WordsAlign> wa = WordsAlignDAO.selectAllByIdSource(Integer.parseInt(id_word));
                    String old_align = "";
                    for (WordsAlign object : wa) {
                        old_align += object.getIdTarget() + " ";
                    }
                    obj.put("old_aligns", old_align);

                    WordsAlignDAO.deleteAllByIdSource(Integer.parseInt(id_word));
                } else if (mode.equals("remove_all_target")) {
                    String id_word = request.getParameter("id_word");
                    obj.put("id_word", id_word);

                    List<WordsAlign> wa = WordsAlignDAO.selectAllByIdTarget(Integer.parseInt(id_word));
                    String old_align = "";
                    for (WordsAlign object : wa) {
                        old_align += object.getIdSource() + " ";
                    }
                    obj.put("old_aligns", old_align);

                    WordsAlignDAO.deleteAllByIdTarget(Integer.parseInt(id_word));
                } else if (mode.equals("save")) {
                    String id_word_source = request.getParameter("id_word_source");
                    String id_word_target = request.getParameter("id_word_target");
                    String type = request.getParameter("type");
                    obj.put("id_word_source", id_word_source);
                    obj.put("id_word_target", id_word_target);
                    
                    WordsAlign wa = new WordsAlign();
                    wa.setIdSource(Integer.parseInt(id_word_source));
                    wa.setIdTarget(Integer.parseInt(id_word_target));
                    wa.setType((type == null) ? "t1:1" : type );
                    WordsAlignDAO.insert(wa);
                    
                    List<WordsAlign> was = WordsAlignDAO.selectAllByIdSource(Integer.parseInt(id_word_source));
                    String new_align_source = "";
                    for (WordsAlign object : was) {
                        new_align_source += object.getIdTarget() + " ";
                    }
                    obj.put("new_align_source", new_align_source);
                    
                    List<WordsAlign> wat = WordsAlignDAO.selectAllByIdTarget(Integer.parseInt(id_word_target));
                    String new_align_target = "";
                    for (WordsAlign object : wat) {
                        new_align_target += object.getIdSource() + " ";
                    }
                    obj.put("new_align_target", new_align_target);
                }
                obj.put("alert_success", "OK");
            }
        } catch (Exception ex) {
            obj.put("alert_danger", ex.getMessage());
        }
        // Ajax response
        response.setContentType("application/json");
        response.getWriter().write(obj.toString());

    }

}
