/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextAlignDAO;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.entity.TextAlign;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ParallelAlignUpdateController
 *
 * <pre>
 * pages:
 * align/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 *
 */
@WebServlet(
        name = "ParallelAlignUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/update",
            "/align/edit"
        }
)
public class ParallelAlignUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // List texts
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(Integer.parseInt(request.getParameter("id_corpus")), TextDAO.TITLE);
            // Select TextAlign
            TextAlign align = TextAlignDAO.selectById(
                    Integer.parseInt(request.getParameter("id_source")), 
                    Integer.parseInt(request.getParameter("id_target")));
            
            request.setAttribute("text_align", align);
            request.setAttribute("list_texts", listTexts);
            request.setAttribute("id_corpus", corpus.getId());
            
            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Atualizar Alinhamento", //page_title
                    "align/form.jsp"); //page
            
            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            
            TextAlign text_align = new TextAlign();
            text_align.setIdSource(Integer.parseInt(request.getParameter("source")));
            text_align.setIdTarget(Integer.parseInt(request.getParameter("target")));
            text_align.setType(request.getParameter("type"));
            
            TextAlignDAO dao = new TextAlignDAO();
            dao.update(text_align);
            
            Logger.controllerEvent(this, request, "Parallel Align Updated");
            
            //Success
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Alinhamento alterado com sucesso", //success
                    null, //warning
                    null //error
            );
            
        } catch (Exception ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }

    }

}
