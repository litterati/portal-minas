/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Task;
import br.project.entity.Text;
import br.project.entity.TextAlign;
import br.project.entity.User;
import br.project.task.AlignTask;
import br.project.task.TaskManagement;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 * ParallelAlignInsertController
 *
 * <pre>
 * pages:
 * align/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 *
 */
@WebServlet(
        name = "ParallelAlignInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/insert",
            "/align/create"
        }
)
public class ParallelAlignInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // List texts
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(Integer.parseInt(request.getParameter("id_corpus")), TextDAO.TITLE);
            
            request.setAttribute("list_texts", listTexts);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Cadastro de Alinhamento", //page_title
                    "align/form.jsp"); //page
            
            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            
            TextAlign text_align = new TextAlign();
            text_align.setIdSource(Integer.parseInt(request.getParameter("source")));
            text_align.setIdTarget(Integer.parseInt(request.getParameter("target")));
            text_align.setType(request.getParameter("type"));
            
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("userLoged");
            
            TaskManagement tm = TaskManagement.getInstance();
            tm.addTask(new AlignTask(text_align), Task.TYPE_ALIGNER, user);
            
            Logger.controllerEvent(this, request, "Parallel Align Inserted [AlignTask]");
            
            // Ajax response
            if (request.getParameter("ajax") != null){
                JSONObject obj = new JSONObject();
                obj.put("alert_success","Textos na fila de alinhamento");
                obj.put("id_source", text_align.getIdSource());
                obj.put("id_target", text_align.getIdTarget());
                obj.put("source_title", text_align.getSource().getTitle());
                obj.put("target_title", text_align.getTarget().getTitle());
                obj.put("type", text_align.getType());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }
            
            //Success
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Textos na fila de alinhamento", //success
                    null, //warning
                    null //error
            );
            
        } catch (Exception ex) {
            // Ajax response
            Logger.controllerError(this, request, ex);
            if (request.getParameter("ajax") != null){
                JSONObject obj = new JSONObject();
                obj.put("alert_danger",ex.getMessage());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }
            
            // Error
            template.redirect(
                    this, //servlet
                    "/align/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }

    }

}
