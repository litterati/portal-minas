/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.align.concordancer;

import br.library.util.Chronometer;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextTagsDAO;
import br.project.entity.ConcordancerAlign;
import br.project.entity.Corpus;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.dao.ConcordancerAlignDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/* 
 * ConcordancerAlignSearchController
 *
 * <pre>
 * pages:
 * align/concordancer/search.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/11/2014
 *
 */
@WebServlet(
        name = "ConcordancerAlignSearchController",
        description = "Portal Min@s",
        urlPatterns = {
            "/align/concordancer/search"
        }
)
public class ConcordancerAlignSearchController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //if is a search
            if (request.getParameter("search") != null) {
                this.concordancer(request);
            }
            //List<TextTags> textTagsList = TextTagsDAO.selectByIdCorpus(corpus.getId());
            List<TextTags> textTagsList = TextTagsDAO.selectDistinctWithTagValuesByIdCorpus(corpus.getId());

            request.setAttribute("textTagsList", textTagsList);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "align-concordancer", //page_menu_tools
                    "simple", //page_menu_tools_sub
                    "Concordanciador de Alinhamentos", //page_title
                    "align/concordancer/search.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e); //log
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private void concordancer(HttpServletRequest request) {
        //Chronometer start
        Chronometer.start();
        //Concordancer
        ConcordancerAlign concordancer = new ConcordancerAlign();
        concordancer.setTerm(request.getParameter("search"));
        concordancer.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
        //tolerance
        if (request.getParameter("tolerance") != null && !request.getParameter("tolerance").isEmpty()) {
            concordancer.setTolerance(Integer.parseInt(request.getParameter("tolerance")));
        }
        //result by pages
        if (request.getParameter("cmbResultByPage") != null && !request.getParameter("cmbResultByPage").isEmpty()) {
            concordancer.setResultsByPage(Integer.parseInt(request.getParameter("cmbResultByPage")));
        }
        //current page
        if (request.getParameter("currentPage") != null && !request.getParameter("currentPage").isEmpty()){
            concordancer.setCurrentPage(Integer.parseInt(request.getParameter("currentPage")));
        }
        if (!request.getParameter("search").equals(request.getParameter("old_search"))){
            concordancer.setCurrentPage(1);
        }
        if (concordancer.getCurrentPage() <= 1){
            concordancer.setInterval(0);
            concordancer.setCurrentPage(1);
        } else {
            concordancer.setInterval((concordancer.getCurrentPage() * concordancer.getResultsByPage()) - concordancer.getResultsByPage());
        }
        //Metadata
        String[] metadata = request.getParameterValues("metadata[]");
        if (metadata != null) {
            List<TextTagValues> textTagValueList = new ArrayList<>();
            for (String value : metadata) {
                String[] aux = value.split("\\|", 3);
                TextTagValues ttv = new TextTagValues();
                ttv.setIdTextTag(Integer.parseInt(aux[0]));
                ttv.setId(Integer.parseInt(aux[1]));
                ttv.setValue(aux[2]);
                textTagValueList.add(ttv);
            }
            concordancer.setLstTextTagValues(textTagValueList);
        }
        //Concordancer Align Facade
        ConcordancerAlignDAO.search(concordancer);
        //Error
        concordancer.setError("");
        if ((concordancer.getTotalPages() > 0) && 
                (concordancer.getTotalPages() < concordancer.getCurrentPage())) {
            concordancer.setError("Não existem mais páginas, clique no botão anterior");
        } else if (concordancer.getEntries().size() <= 0){
            concordancer.setError("Nenhum resultado encontrado");
        }
        //Chronometer end
        Chronometer.stop();
        concordancer.setTime(Chronometer.elapsedTime());
        //Set the result
        request.setAttribute("concordancer", concordancer);
        request.setAttribute("start", ((concordancer.getCurrentPage() - 1) * concordancer.getResultsByPage()) + 1);
    }
}
