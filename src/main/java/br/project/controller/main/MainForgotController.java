/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.main;

import br.library.util.Logger;
import br.library.util.template.TemplateMain;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * MainController
 *
 * <pre>
 * pages:
 * main/forgot.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 03/09/2014
 *
 */
@WebServlet(
        name = "MainForgotController",
        description = "Permite que o usuário (sem estar logado) entre em contato com os admin do sistema.",
        urlPatterns = {
            "/forgot.jsp",
            "/forgot"
        }
)
public class MainForgotController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);

        try {
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Page viewer
            template.pageViewer(
                    "Recuperar a senha", //title
                    "forgot", //menu
                    "main/forgot.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/index", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);

        try {

            request.getParameter("email");
            
            String captcha = (String) request.getSession().getAttribute("captcha");
            String code = (String) request.getParameter("captcha-code");
            if (captcha == null || code == null || !captcha.equals(code)){
                throw new Exception("Captcha errado");
            }

            // enviar email
            Logger.controllerEvent(this, request, "Forgot Email Sent");

            template.redirect(
                    this, //servlet
                    "/forgot", //url
                    "Enviado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/forgot", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

}
