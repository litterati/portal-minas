/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.main;

import br.library.util.Logger;
import br.library.util.template.TemplateMain;
import br.project.dao.SupportDAO;
import br.project.entity.Support;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * MainController
 *
 * <pre>
 * pages:
 * main/contact.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 02/09/2014
 *
 */
@WebServlet(
        name = "MainContactController",
        description = "Permite que o usuário (sem estar logado) entre em contato com os admin do sistema.",
        urlPatterns = {
            "/contact.jsp",
            "/contact"
        }
)
public class MainContactController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);

        try {
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Page viewer
            template.pageViewer(
                    "Contato", //title
                    "contact", //menu
                    "main/contact.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/index", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);

        /*String from = request.getParameter("email");
         String subject = "[Portal Min@s] " + request.getParameter("assunto") + " // " + request.getParameter("nome");
         String messageText = request.getParameter("mensagem");

         if (this.sendEmail(from, subject, messageText)) {
         request.setAttribute("alert_success", "Mensagem enviada com sucesso.");
         } else {
         request.setAttribute("nome", request.getParameter("nome"));
         request.setAttribute("email", request.getParameter("email"));
         request.setAttribute("mensagem", request.getParameter("mensagem"));

         request.setAttribute("title", "Contato");
         request.setAttribute("menu", "contato");
         request.setAttribute("page", "main/contact.jsp");

         if (!response.isCommitted()) {
         request.getRequestDispatcher(template).forward(request, response);
         }*/
        try {
            
            String captcha = (String) request.getSession().getAttribute("captcha");
            String code = (String) request.getParameter("captcha-code");
            if (captcha == null || code == null || !captcha.equals(code)){
                throw new Exception("Captcha errado");
            }

            Support support = new Support(
                    request.getParameter("name"),
                    request.getParameter("email"),
                    request.getParameter("subject"),
                    request.getParameter("message"),
                    Support.NOT_READ);

            // insert
            SupportDAO.insert(support);

            Logger.controllerEvent(this, request, "Contact Msg Sent");

            template.redirect(
                    this, //servlet
                    "/contact", //url
                    "Enviado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/contact", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    //http://www.tutorialspoint.com/java/java_sending_email.htm
    private boolean sendEmail(String from, String subject, String messageText) {

        // Recipient's email ID needs to be mentioned.
        String to = "to-email@gmail.com";

        final String username = "user@gmail.com";
        final String password = "password";

        // New properties object
        Properties properties = new Properties();

        // Setup mail server
        properties.put("mail.smtp.auth", "true");//autenticaçao
        properties.put("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.host", "smtp.gmail.com");//host
        properties.put("mail.smtp.port", "587");//port

        // Get the default Session object.
        //Session mailSession = Session.getDefaultInstance(properties);
        // Authenticated Session
        Session mailSession = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(mailSession);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Now set the actual message
            message.setText(messageText);

            // Send message
            Transport.send(message);

            return true;

        } catch (MessagingException mex) {
            mex.printStackTrace();
            return false;
        }
    }

}
