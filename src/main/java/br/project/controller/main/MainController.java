/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.main;

import br.library.util.template.TemplateMain;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * MainController
 *
 * <pre>
 * pages:
 * main/about.jsp
 * main/index.jsp
 * main/login.jsp
 * main/thanks.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 27/05/2014
 *
 */
@WebServlet(
        name = "MainController",
        description = "Controla as páginas padrões, em que o usuário não está logado.",
        urlPatterns = {
            "/about.jsp", "/about",
            "/index.jsp", "/index",
            "/login.jsp", "/login",
            "/thanks.jsp", "/thanks"
        }
)
public class MainController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);
        //Mostra as mensagens de sucesso, alerta e erro
        template.getAlerts();

        String uri = request.getRequestURI();
        if (uri.indexOf("/about") > 0) {
            //Page viewer
            template.pageViewer(
                    "Sobre", //title
                    "about", //menu
                    "main/about.jsp"); //page
        } else if (uri.indexOf("/login") > 0) {
            //Page viewer
            template.pageViewer(
                    "Autenticação", //title
                    "login", //menu
                    "main/login.jsp"); //page
        } else if (uri.indexOf("/thanks") > 0) {
            //Page viewer
            template.pageViewer(
                    "Agradecimentos", //title
                    "thanks", //menu
                    "main/thanks.jsp"); //page
        } else {//index
            //Page viewer
            template.pageViewer(
                    null, //title
                    null, //menu
                    "main/index.jsp"); //page
        }

        try {
            template.forward();
        } catch (URISyntaxException ex) {
            // redirect to index
            response.sendRedirect(this.getServletContext().getContextPath());
        }

    }

}
