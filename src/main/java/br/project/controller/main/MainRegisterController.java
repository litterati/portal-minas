/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.main;

import br.library.util.Logger;
import br.library.util.template.TemplateMain;
import br.project.dao.UserDAO;
import br.project.entity.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * MainRegisterController
 *
 * <pre>
 * pages:
 * main/register.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 02/09/2014
 *
 */
@WebServlet(
        name = "MainRegisterController",
        description = "Permite que o usuário se cadastre no sistema.",
        urlPatterns = {
            "/register.jsp",
            "/register"
        }
)
public class MainRegisterController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);

        try {
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Page viewer
            template.pageViewer(
                    "Registro", //title
                    "register", //menu
                    "main/register.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/index", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateMain template = new TemplateMain(request, response);

        try {

            if (!request.getParameter("general-terms").equals("accept")) {
                throw new Exception("É necessário aceitar os termos e condições gerais de uso");
            }

            //Create new user
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setEmail(request.getParameter("email"));
            user.setLogin(request.getParameter("login"));
            user.setPassword(request.getParameter("password"));
            //permission
            user.setType(User.BASIC);
            
            String captcha = (String) request.getSession().getAttribute("captcha");
            String code = (String) request.getParameter("captcha-code");
            if (captcha == null || code == null || !captcha.equals(code)){
                throw new Exception("Captcha errado");
            }

            //insert
            UserDAO.insert(user);

            Logger.controllerEvent(this, request, "User Registed");

            template.redirect(
                    this, //servlet
                    "/register", //url
                    "Registro efetuado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/register", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

}
