/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.admin;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.UserDAO;
import br.project.entity.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * AdminChangePasswordController
 *
 * <pre>
 * pages:
 * admin/change-password.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 31/07/2014
 *
 */
@WebServlet(
        name = "AdminChangePasswordController",
        description = "Portal Min@s",
        urlPatterns = {
            "/admin/settings/change-password"
        }
)
public class AdminChangePasswordController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            
            // Session user
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("userLoged");
            request.setAttribute("user", user);

            //Page viewer
            template.pageViewer(
                    null, //"menu/admin.jsp", //page_menu
                    null, //"breadcrumb/admin.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "login", //page_menu_option
                    "settings", //page_menu_option_sub
                    "password", //page_menu_tools
                    null, //page_menu_tools_sub
                    "Alterar senha", //page_title
                    null, //page_subtitle
                    "admin/change-password.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this,//servlet
                    "/admin/settings", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Update user
            User user = UserDAO.authenticate(
                    request.getParameter("login"), 
                    request.getParameter("old-password"));
            
            if (user == null){
                throw new Exception("Login or password wrong");
            }
            
            if (!request.getParameter("new-password").equals(request.getParameter("new-password2"))){
                throw new Exception("Different passwords");
            }
            
            user.setPassword(request.getParameter("new-password"));

            //update
            UserDAO userDAO = new UserDAO();
            userDAO.updatePassword(user);
            
            Logger.controllerEvent(this, request, "Password update");

            template.redirect(
                    this,//servlet
                    "/admin/settings", //path
                    "Configurações alteradas com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this,//servlet
                    "/admin/settings/change-password", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
