/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.admin;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.entity.Corpus;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AdminHomeController
 *
 * <pre>
 * pages:
 * admin/tools.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 20/12/2014
 *
 */
@WebServlet(
        name = "AdminToolsController",
        description = "Portal Min@s",
        urlPatterns = {
            "/admin/tools"
        }
)
public class AdminToolsController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            request.setAttribute("corpus", corpus);

            //Page viewer
            template.pageViewer(
                    null, //"menu/admin.jsp", //page_menu
                    null, //"breadcrumb/admin.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "tools", //page_menu_tools
                    null, //page_menu_tools_sub
                    "Ferramentas", //page_title
                    "admin/tools.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
