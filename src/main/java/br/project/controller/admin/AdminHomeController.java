/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.admin;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.entity.Corpus;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * AdminHomeController
 *
 * <pre>
 * pages:
 * admin/home.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 31/07/2014
 *
 */
@WebServlet(
        name = "AdminHomeController",
        description = "Portal Min@s",
        urlPatterns = {
            "/admin/home"
        }
)
public class AdminHomeController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //List of corpus to home page
            request.setAttribute("corpora", CorpusDAO.selectAll(Corpus.WORK));

            //Page viewer
            template.pageViewer(
                    null, //"menu/admin.jsp", //page_menu
                    null, //"breadcrumb/admin.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "home", //page_menu_option
                    null, //page_menu_option_sub
                    null, //page_menu_tools
                    null, //page_menu_tools_sub
                    null, //page_title
                    null, //page_subtitle
                    "admin/home.jsp"); //page

            template.forward();

        } catch (DAOException | IOException | URISyntaxException | ServletException e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/index", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

}
