/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.controller.keywords;

import br.library.multipart.MultipartMap;
import br.library.util.Config;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.dao.TextDAO;
import br.project.dao.WordDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.entity.User;
import br.project.entity.Words;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * KeywordsLdaController
 *
 * <pre>
 * pages:
 * keywords/lda.jsp
 * keywords/result.jsp
 * </pre>
 *
 * @author Régis Zangirolami
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "KeywordsLdaController",
        description = "Portal Min@s",
        urlPatterns = {
            "/keywords/lda",
            "/keywords/result"
        }
)
public class KeywordsLdaController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.KEYWORDS);

            List<Text> listTexts = TextDAO.selectAllByIdCorpus(corpus.getId(), TextDAO.TITLE);

            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("textsList", listTexts);

            //Page viewer
            template.pageViewer(
                    null, //"menu/keywords.jsp", //page_menu
                    "breadcrumb/keywords.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "keywords", //page_menu_tools
                    "keywords", //page_menu_tools_sub
                    "Palavras-chave", //page_title
                    "keywords/lda.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            response.sendRedirect(
                    getServletContext().getContextPath()
                    + "/keywords/lda?id_corpus="
                    + request.getParameter("id_corpus")
                    + "alert_danger=" + URLEncoder.encode(e.getMessage(), "UTF-8"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(corpus.getId(), TextDAO.TITLE);

            // Validar o texto
            Text text = TextDAO.selectById(Integer.parseInt(request.getParameter("texts")));
            if (text == null) {
                //Text invalido
            }

            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("userLoged");
            String userId = String.valueOf(user.getId());

            File dir = new File(Config.getPathProperties().getProperty("path.tmp") + "/" + String.valueOf(userId));
            dir.mkdir();

            Integer ntopics;
            Integer nwords;

            if ("".equals(request.getParameter("nwords"))) {
                nwords = 25;
            } else {
                nwords = Integer.parseInt(request.getParameter("nwords"));
            }

            if ("".equals(request.getParameter("ntopics"))) {
                ntopics = 2;
            } else {
                ntopics = Integer.parseInt(request.getParameter("ntopics"));
            }

            //Create text
            MultipartMap map = new MultipartMap(request);
            File stopWordsFile = map.getFile("stop_words");
            List<String> wordList = this.getWordList(text, stopWordsFile);
            HashMap<String, Integer> vocabFile = this.getVocabFile(wordList);

            this.getDataFile(text, stopWordsFile, wordList, vocabFile, userId);
            this.runLda(ntopics, userId);

            List<List> topicsList = this.getTopics(nwords, vocabFile, userId);

            // Criar arquivo de tópicos
            StringBuilder sb = new StringBuilder();

            Integer i = 1;
            for (List<String> stringList : topicsList) {
                sb.append("Tópico ").append(i.toString()).append("\n");

                for (String string : stringList) {
                    sb.append(string).append("\n");
                }
                sb.append("\n");
            }

            File file = new File(Config.getPathProperties().getProperty("path.tmp") + "topics" + userId + ".txt");
            Writer writer = new BufferedWriter(new FileWriter(file, false));
            writer.write(sb.toString());
            writer.close();

            request.setAttribute("topics", topicsList);
            request.setAttribute("textSelected", text);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("textsList", listTexts);

            //Page viewer
            template.pageViewer(
                    null, //"menu/keywords.jsp", //page_menu
                    "breadcrumb/keywords.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "keywords", //page_menu_tools
                    "view", //page_menu_tools_sub
                    "Palavras-chave", //page_title
                    "keywords/lda.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/keywords/lda", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private List<String> getWordList(Text text, File stopWordsFile) throws IOException, URISyntaxException {
        List<Words> textSplit = WordDAO.selectAllByIdText(text.getId());

        List<String> stopWords = new ArrayList<>();
        String path = null;

        if (stopWordsFile == null) {
            switch (text.getLanguage()) {
                case "PORTUGUESE":
                    path = Config.getPathProperties().getProperty("path.resources") + "pt.txt";
                    break;
                case "ENGLISH":
                    path = Config.getPathProperties().getProperty("path.resources") + "en.txt";
                    break;
                case "SPANISH":
                    path = Config.getPathProperties().getProperty("path.resources") + "es.txt";
                    break;
            }
        } else {
            path = stopWordsFile.getPath();
        }

        if (path != null) {
            File file = new File(path);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String stop = null;
            while ((stop = reader.readLine()) != null) {
                stopWords.add(stop.toUpperCase());
            }
        }

        List<String> wordList = new ArrayList<>();

        for (Words wordText : textSplit) {
            String word = wordText.getWord();

            // Remover caracteres especiais
            String regularExpression = "[0-9\\!¡@#$%&\\*\\(\\)\\+\\=\\?¿\\^~\\[\\]{}«»\\|/,\\.:;'\"“”‘’]";
            word = word.replaceAll(regularExpression, "");
            word = word.toUpperCase();

            // Remover hífen no meio da palavra
            // e adicionar a palavra na lista
            if (word.length() > 0) {
                String[] wordSplitString = word.split("-");
                List<String> wordSplit = Arrays.asList(wordSplitString);

                for (String wordNew : wordSplit) {
                    if (wordNew.length() > 0 & !stopWords.contains(wordNew)) {
                        wordList.add(wordNew);
                    }
                }
            }
        }

        return wordList;
    }

    private HashMap<String, Integer> getVocabFile(List<String> wordList) {
        HashMap<String, Integer> map = new HashMap<>();

        for (String key : wordList) {
            if (wordList.contains(key)) {
                Integer value = map.get(key);
                if (value == null) {
                    value = 0;
                }
                value++;
                map.put(key, value);
            }
        }

        return map;
    }

    private String setWordlistFormat(HashMap<String, Integer> vocabFile) {
        Integer count = vocabFile.size();

        // Sort by values and get list of keys                
        HashMap<String, Integer> indices = new HashMap<>();
        indices = this.sortByValues(vocabFile);

        Integer i = 0;
        String words = count.toString();
        for (String word : indices.keySet()) {
            Integer wordCount = vocabFile.get(word);
            String wordNew = i.toString() + ":" + wordCount.toString();
            words = words + " " + wordNew;
            i++;
        }

        return words;
    }

    private void getDataFile(Text text, File stopWordsFile, List<String> wordList, HashMap<String, Integer> vocabFile, String userId) throws IOException, URISyntaxException {
        String words = this.setWordlistFormat(vocabFile);

        // Criar arquivo data
        Writer writer = null;

        try {
            File file = new File(Config.getPathProperties().getProperty("path.tmp") + "/" + String.valueOf(userId) + "/" + "data.txt");
            writer = new BufferedWriter(new FileWriter(file, false));
            writer.write(words);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void runLda(Integer ntopics, String userId) throws IOException, URISyntaxException {
        String comando = Config.getPathProperties().getProperty("path.lda.tool") + "lda est 1.0 " + ntopics.toString() + " " + Config.getPathProperties().getProperty("path.lda.tool") + "settings.txt " + Config.getPathProperties().getProperty("path.tmp") + "/" + String.valueOf(userId) + "/" + "data.txt random " + Config.getPathProperties().getProperty("path.tmp") + "/" + String.valueOf(userId) + "/";

        try {
            Process process = Runtime.getRuntime().exec(comando);
            System.out.println(comando);

            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            System.out.print("Output of running is: ");
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

            is.close();
            isr.close();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<List> getTopics(Integer nwords, HashMap<String, Integer> vocabFile, String userId) throws IOException, URISyntaxException {
        // Abrir beta file
        File file = new File(Config.getPathProperties().getProperty("path.tmp") + "/" + String.valueOf(userId) + "/" + "final.beta");
        BufferedReader reader = new BufferedReader(new FileReader(file));

        String topic = null;
        List<List> topics = new ArrayList<>();

        while ((topic = reader.readLine()) != null) {
            HashMap<Integer, Float> indices = new HashMap<>();

            Integer i = 0;
            for (String string : topic.split(" ")) {
                if (string != null & !"".equals(string)) {
                    float number = Float.parseFloat(string);
                    indices.put(i, number);
                    i++;
                }
            }

            // Sort by values and get list of keys                
            indices = this.sortByValues(indices);
            List<Integer> indicesList = new ArrayList<>();

            for (Integer key : indices.keySet()) {
                indicesList.add(key);
            }

            List<String> words = new ArrayList<>();

            for (int n = 0; n < nwords; n++) {
                List<String> list = new ArrayList(vocabFile.keySet());
                String word = list.get(indicesList.get(n));
                words.add(word);

                System.out.println(word);
                System.out.println("\n");
            }

            Collections.sort(words);

            topics.add(words);

            System.out.println("\n");
        }

        reader.close();

        return topics;
    }

    private HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
