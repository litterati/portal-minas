package br.project.controller.word;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.WordDAO;
import br.project.entity.Corpus;
import br.project.entity.Words;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * UserUpdateController
 *
 * <pre>
 * pages:
 * word/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 20/08/2014
 *
 */
@WebServlet(
        name = "WordUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/word/update",
            "/word/edit"
        }
)
public class WordUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Screen to update the word
            Words word = WordDAO.selectById(Integer.parseInt(request.getParameter("id_word")));

            request.setAttribute("word", word);

            //Page viewer
            template.pageViewer(
                    null, //"menu/word.jsp", //page_menu
                    "breadcrumb/word.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "word", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Alterar palavra", //page_title
                    "word/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Update word
            Words word = new Words();
            word.setId(Integer.parseInt(request.getParameter("id")));
            word.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            word.setIdText(Integer.parseInt(request.getParameter("id_text")));
            word.setIdSentence(Integer.parseInt(request.getParameter("id_sentence")));
            word.setPosition(Integer.parseInt(request.getParameter("position")));
            word.setWord(request.getParameter("word"));
            word.setLemma(request.getParameter("lemma"));
            word.setPos(request.getParameter("pos"));
            word.setNorm(request.getParameter("norm"));
            word.setTranskription(request.getParameter("transkription"));
            word.setDepHead(request.getParameter("dep_head"));
            word.setDepFunction(request.getParameter("dep_function"));
            word.setContraction(request.getParameter("contraction"));

            //update
            word.setId(Integer.parseInt(request.getParameter("id")));
            WordDAO wordDAO = new WordDAO();
            wordDAO.update(word);

            Logger.controllerEvent(this, request, "Word Updated");

            //Success
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Palavra atualizada com sucesso!", //success
                    null, //warning
                    null //error
            );
        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
