/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.fields_type;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FieldsTypeSelectController
 *
 * <pre>
 * pages:
 * fields-type/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/07/2014
 *
 */
@WebServlet(
        name = "FieldsTypeSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/fields-type/select",
            "/fields-type/list"
        }
)
public class FieldsTypeSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Select all fields type
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            request.setAttribute("fields_type_list", fields_type_dao.selectAll());

            //Page viewer
            template.pageViewer(
                    "menu/fields-type.jsp", //page_menu
                    "breadcrumb/fields-type.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "fields-type", //page_menu_option_sub
                    "select", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Fields Type", //page_title
                    null, //page_subtitle
                    "fields-type/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
