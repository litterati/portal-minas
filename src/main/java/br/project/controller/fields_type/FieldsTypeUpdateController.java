/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.fields_type;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.entity.FieldsType;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FieldsTypeUpdateController
 *
 * <pre>
 * pages:
 * fields-type/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/07/2014
 *
 */
@WebServlet(
        name = "FieldsTypeUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/fields-type/update",
            "/fields-type/edit"
        }
)
public class FieldsTypeUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //select
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            FieldsType fields_type = fields_type_dao.selectById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("fields_type", fields_type);

            //Page viewer
            template.pageViewer(
                    "menu/fields-type.jsp", //page_menu
                    "breadcrumb/fields-type.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "fields-type", //page_menu_option_sub
                    "update", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Alterar Fields Type", //page_title
                    fields_type.getValue(), //page_subtitle
                    "fields-type/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/fields-type/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new Fields Type
            FieldsType fields_type = new FieldsType();
            fields_type.setId(Integer.parseInt(request.getParameter("id")));
            fields_type.setValue(request.getParameter("value"));

            //update
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            fields_type_dao.update(fields_type);

            Logger.controllerEvent(this, request, "Fields Type Updated");

            // Success
            template.redirect(
                    this, //servlet
                    "/fields-type/select", //path
                    "Field Type alterado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/fields-type/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
