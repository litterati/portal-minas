/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.fields_type.value;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.dao.FieldsTypeValuesDAO;
import br.project.entity.FieldsTypeValues;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FieldsTypeValueInsertController
 *
 * <pre>
 * pages:
 * fields-type/value/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/07/2014
 *
 */
@WebServlet(
        name = "FieldsTypeValueInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/fields-type/value/insert",
            "/fields-type/value/create"
        }
)
public class FieldsTypeValueInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            if (request.getParameter("field_type_id") != null) {
                //set the id_type id
                FieldsTypeValues fields_type_value = new FieldsTypeValues();
                fields_type_value.setIdType(Integer.parseInt(request.getParameter("field_type_id")));
                request.setAttribute("fields_type_value", fields_type_value);
            } else {
                //error
            }

            //Select all fields type
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            request.setAttribute("fields_type_list", fields_type_dao.selectAll());

            //Page viewer
            template.pageViewer(
                    "menu/fields-type-value.jsp", //page_menu
                    "breadcrumb/fields-type-value.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "fields-type-value", //page_menu_option_sub
                    "insert", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Fields Type Value", //page_title
                    null, //page_subtitle
                    "fields-type/value/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/fields-type/value/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new Fields Type
            FieldsTypeValues fields_type_value = new FieldsTypeValues();
            fields_type_value.setIdType(Integer.parseInt(request.getParameter("id_type")));
            fields_type_value.setValue(request.getParameter("value"));

            //insert
            FieldsTypeValuesDAO fields_type_values_dao = new FieldsTypeValuesDAO();
            fields_type_values_dao.insert(fields_type_value);

            Logger.controllerEvent(this, request, "Fields Type Value Inserted");

            // Success
            template.redirect(
                    this, //servlet
                    "/fields-type/value/select", //path
                    "Field Type Value inserido com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/fields-type/value/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
