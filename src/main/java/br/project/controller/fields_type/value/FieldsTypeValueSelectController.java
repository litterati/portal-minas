/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.fields_type.value;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.dao.FieldsTypeValuesDAO;
import br.project.entity.FieldsType;
import br.project.entity.FieldsTypeValues;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FieldsTypeValueSelectController
 *
 * <pre>
 * pages:
 * fields-type/value/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 30/07/2014
 *
 */
@WebServlet(
        name = "FieldsTypeValueSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/fields-type/value/select",
            "/fields-type/value/list"
        }
)
public class FieldsTypeValueSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            FieldsTypeValuesDAO fields_type_value_dao = new FieldsTypeValuesDAO();
            if (request.getParameter("field_type_id") != null) {
                //Select some fields type values
                FieldsTypeValues fieldsTypeValues = new FieldsTypeValues();
                fieldsTypeValues.setIdType(Integer.parseInt(request.getParameter("field_type_id")));
                request.setAttribute("fields_type_value_list", fields_type_value_dao.selectAllByType(fieldsTypeValues));
                
                //Select FieldsType
                FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
                FieldsType fields_type = fields_type_dao.selectById(Integer.parseInt(request.getParameter("field_type_id")));
                request.setAttribute("fields_type", fields_type);
            } else {
                //Select all fields type
                request.setAttribute("fields_type_value_list", fields_type_value_dao.selectAll());
            }

            //Page viewer
            template.pageViewer(
                    "menu/fields-type-value.jsp", //page_menu
                    "breadcrumb/fields-type-value.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "fields-type-value", //page_menu_option_sub
                    "select", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Fields Type Value", //page_title
                    null, //page_subtitle
                    "fields-type/value/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
