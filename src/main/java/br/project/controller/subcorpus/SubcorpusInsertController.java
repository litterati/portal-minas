/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.subcorpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.SubcorpusDAO;
import br.project.dao.SubcorpusTextDAO;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Subcorpus;
import br.project.entity.SubcorpusText;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SubcorpusInsertController
 *
 * <pre>
 * pages:
 * subcorpus/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 16/01/2015
 *
 */
@WebServlet(
        name = "SubcorpusInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/subcorpus/insert",
            "/subcorpus/create"
        }
)
public class SubcorpusInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            
            List texts_list = TextDAO.selectAllByIdCorpus(corpus.getId(), TextDAO.TITLE);

            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("texts_list", texts_list);

            //Page viewer
            template.pageViewer(
                    "menu/subcorpus.jsp", //page_menu
                    "breadcrumb/subcorpus.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "subcorpus", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Subcorpus", //page_title
                    "subcorpus/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/subcorpus/select", //url
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new Subcorpus
            Subcorpus subcorpus = new Subcorpus();
            subcorpus.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            subcorpus.setName(request.getParameter("name"));
            subcorpus.setDescription(request.getParameter("description"));
            
            //insert subcorpus
            SubcorpusDAO.insert(subcorpus);
            
            String[] subcorpus_texts_id = request.getParameterValues("subcorpus_text[]");
            for (String text_id : subcorpus_texts_id) {
                SubcorpusText subcorpusText = new SubcorpusText();
                subcorpusText.setIdSubcorpus(subcorpus.getId());
                subcorpusText.setIdText(Integer.parseInt(text_id));
                //insert subcorpus text
                SubcorpusTextDAO.insert(subcorpusText);
            }

            Logger.controllerEvent(this, request, "Subcorpus Inserted");

            // Success
            template.redirect(
                    this, //servlet
                    "/subcorpus/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Subcorpus inserido com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/subcorpus/select", //url
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
