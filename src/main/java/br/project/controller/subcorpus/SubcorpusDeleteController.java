/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.subcorpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.SubcorpusDAO;
import br.project.dao.SubcorpusTextDAO;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Subcorpus;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * SubcorpusDeleteController
 *
 * <pre>
 * pages:
 * subcorpus/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 16/01/2015
 *
 */
@WebServlet(
        name = "SubcorpusDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/subcorpus/delete",
            "/subcorpus/remove"
        }
)
public class SubcorpusDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //select
            Subcorpus subcorpus = SubcorpusDAO.selectById(Integer.parseInt(request.getParameter("id_subcorpus")));
            List texts_list = TextDAO.selectAllByIdCorpus(corpus.getId(), TextDAO.TITLE);
            List subcorpus_texts_list = SubcorpusTextDAO.selectAllByIdSubcorpus(subcorpus.getId(), SubcorpusTextDAO.ID_TEXT);
            
            request.setAttribute("subcorpus", subcorpus);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("texts_list", texts_list);
            request.setAttribute("subcorpus_texts_list", subcorpus_texts_list);

            //Page viewer
            template.pageViewer(
                    "menu/subcorpus.jsp", //page_menu
                    "breadcrumb/subcorpus.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "subcorpus", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover Subcorpus", //page_title
                    "subcorpus/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/subcorpus/select", //url
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new subcorpus
            Subcorpus subcorpus = new Subcorpus();
            subcorpus.setId(Integer.parseInt(request.getParameter("id_subcorpus")));

            //delete all subcorpus texts
            SubcorpusTextDAO.deleteAllByIdSubcorpus(subcorpus.getId());
            //delete subcorpus
            SubcorpusDAO.delete(subcorpus);

            Logger.controllerEvent(this, request, "Subcorpus Deleted");
            
            // Ajax response
            if (request.getParameter("ajax") != null){
                JSONObject obj = new JSONObject();
                obj.put("alert_success","Subcorpus removido com sucesso");
                obj.put("id_subcorpus", subcorpus.getId());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            // Success
            template.redirect(
                    this, //servlet
                    "/subcorpus/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Subcorpus removido com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception ex) {
            // Error
            Logger.controllerError(this, request, ex);
            // Ajax response
            if (request.getParameter("ajax") != null){
                JSONObject obj = new JSONObject();
                obj.put("alert_danger",ex.getMessage());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }
            template.redirect(
                    this, //servlet
                    "/subcorpus/select", //url
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }
    }

}
