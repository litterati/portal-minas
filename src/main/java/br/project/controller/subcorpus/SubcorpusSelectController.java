/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.subcorpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.dao.SubcorpusDAO;
import br.project.entity.Corpus;
import br.project.entity.Subcorpus;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SubcorpusSelectController
 *
 * <pre>
 * pages:
 * subcorpus/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 22/10/2014
 *
 */
@WebServlet(
        name = "SubcorpusSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/subcorpus/select",
            "/subcorpus/list"
        }
)
public class SubcorpusSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.SUBCORPUS);

            String order = request.getParameter("order");
            if (order != null && order.equals(SubcorpusDAO.DESCRIPTION)) {
                order = SubcorpusDAO.DESCRIPTION;
            } else {
                order = SubcorpusDAO.NAME;
            }
            request.setAttribute("order", order);
            
            List<Subcorpus> list = SubcorpusDAO.selectAllByIdCorpus(corpus.getId(), order);

            //Select all align tags
            request.setAttribute("list_subcorpus", list);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/subcorpus.jsp", //page_menu
                    "breadcrumb/subcorpus.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "subcorpus", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Listar Subcorpus", //page_title
                    "subcorpus/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
