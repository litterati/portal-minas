package br.project.controller.corpus.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusTagsDAO;
import br.project.dao.FieldsTypeDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTags;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusMetadataSelectController
 *
 * <pre>
 pages:
 corpus/metadata/selectAllByIdCorpus-form.jsp
 </pre>
 *
 * @author Thiago Vieira
 * @since 29/07/2014
 *
 */
@WebServlet(
        name = "CorpusMetadataSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/metadata/select",
            "/corpus/metadata/list"
        }
)
public class CorpusMetadataSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            List<CorpusTags> listCorpusMetadata = CorpusTagsDAO.selectAllByIdCorpus(corpus.getId());

            request.setAttribute("listCorpusMetadata", listCorpusMetadata);
            request.setAttribute("lstFieldsType", FieldsTypeDAO.selectAll());
            
            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata-corpus", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Etiquetas do Corpus", //page_title
                    "corpus/metadata/list-form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
}
