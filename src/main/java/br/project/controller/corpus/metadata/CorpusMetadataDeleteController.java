package br.project.controller.corpus.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusTagsDAO;
import br.project.dao.FieldsTypeDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTags;
import br.project.facade.CorpusTagsFacade;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusMetadataDeleteController
 *
 * <pre>
 * pages:
 * corpus/metadata/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 29/07/2014
 *
 */
@WebServlet(
        name = "CorpusMetadataDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/metadata/delete",
            "/corpus/metadata/remove"
        }
)
public class CorpusMetadataDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Metadata
            CorpusTags corpusMetadata = CorpusTagsDAO.selectById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("corpus_metadata", corpusMetadata);
            
            //Select all fields type
            request.setAttribute("fields_type_list", FieldsTypeDAO.selectAll());

            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata-corpus", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover Etiquetas do Corpus", //page_title
                    "corpus/metadata/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {

            CorpusTags corpusMetadata = new CorpusTags();
            corpusMetadata.setId(Integer.parseInt(request.getParameter("id_tags_corpus")));

            //Delete
            if (CorpusTagsFacade.existTagValues(corpusMetadata)) {
                //You can't delete the metadata, because there is value inside it
                //error = "Não é possível remover o metadado \"" 
                //        + corpusMetadata.getTag()
                //        + "\" pois, existem valores nele.";
            } else {
                
                CorpusTagsDAO corpusMetadataDAO = new CorpusTagsDAO();
                corpusMetadataDAO.delete(corpusMetadata);
                
            }
            
            Logger.controllerEvent(this, request, "Corpus Metadata Deleted");
            
            //Success
            template.redirect(
                    this, //servlet
                    "/corpus/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Etiqueta do corpus removida com sucesso", //success
                    null, //warning
                    null //error
            );
            
        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
