package br.project.controller.corpus.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.CorpusTagsDAO;
import br.project.dao.FieldsTypeDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTags;
import br.project.facade.CorpusTagsFacade;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusMetadataInsertController
 *
 * <pre>
 * pages:
 * corpus/metadata/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 29/07/2014
 *
 */
@WebServlet(
        name = "CorpusMetadataInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/metadata/insert",
            "/corpus/metadata/create"
        }
)
public class CorpusMetadataInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = CorpusDAO.selectById(Integer.parseInt(request.getParameter("id_corpus")));
            if (corpus == null) {
                //Corpus inválido
            }
            
            //Select all fields type
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            request.setAttribute("fields_type_list", fields_type_dao.selectAll());

            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata-corpus", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Etiquetas do Corpus", //page_title
                    "corpus/metadata/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {

            //Create new corpusMetadata
            CorpusTags corpusMetadata = new CorpusTags();
            corpusMetadata.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            corpusMetadata.setTag(request.getParameter("metadata_CorpusTags"));
            corpusMetadata.setIsRequired(Boolean.parseBoolean(request.getParameter("is_required_CorpusTags")));
            corpusMetadata.setIdFieldType(Integer.parseInt(request.getParameter("id_field_type_CorpusTags")) == 0 ? 7 : Integer.parseInt(request.getParameter("id_field_type_CorpusTags")));

            //Insert
            CorpusTagsDAO.insert(corpusMetadata);
            CorpusTagsFacade.insertEmptyTagValues(corpusMetadata);
            
            Logger.controllerEvent(this, request, "Corpus Metadata Inserted");
            
            //Success
            if (request.getParameter("wizard") != null) {
                PrintWriter out = response.getWriter();
                out.print(corpusMetadata.getId());
            } else {
                template.redirect(
                    this, //servlet
                    "/corpus/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Etiqueta do corpus inserida com sucesso", //success
                    null, //warning
                    null //error
                );
            }
            
        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
