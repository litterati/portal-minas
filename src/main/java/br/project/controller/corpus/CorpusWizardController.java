package br.project.controller.corpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusWizardController
 *
 * <pre>
 * pages:
 * corpus/corpus-wizard.jsp
 * </pre>
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 *
 */
@WebServlet(
        name = "CorpusWizardController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/wizard"
        }
)
public class CorpusWizardController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Page viewer
            template.pageViewer(
                    "menu/corpus.jsp", //page_menu
                    "breadcrumb/corpus.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "corpus", //page_menu_option_sub
                    "wizard", //page_menu_tools
                    "wizard", //page_menu_tools_sub
                    "Inserir Corpus - Assistente", //page_title
                    null, //page_subtitle
                    "corpus/corpus-wizard.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
