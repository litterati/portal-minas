package br.project.controller.corpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.entity.Corpus;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusSelectController
 *
 * <pre>
 * pages:
 * corpus/corpus-options.jsp
 * corpus/corpus-wizard.jsp
 * corpus/edit-corpus.jsp
 * corpus/list-corpus.jsp
 * corpus/tags-corpus.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "CorpusSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/select",
            "/corpus/list"
        }
)
public class CorpusSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);
        
        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Scree with all corpus
            List<Corpus> listCorpus = CorpusDAO.selectAll(Corpus.ALL);

            request.setAttribute("corpora", listCorpus);

            //Page viewer
            template.pageViewer(
                    "menu/corpus.jsp", //page_menu
                    "breadcrumb/corpus.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "corpus", //page_menu_option_sub
                    "select", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Corpora", //page_title
                    null, //page_subtitle
                    "corpus/list-corpus.jsp"); //page

            template.forward();

        } catch (DAOException | IOException | URISyntaxException | ServletException e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
