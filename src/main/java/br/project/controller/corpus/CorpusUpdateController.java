package br.project.controller.corpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.CorpusTagValuesDAO;
import br.project.dao.CorpusToolsDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTagValues;
import br.project.entity.CorpusTools;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusUpdateController
 *
 * <pre>
 * pages:
 * corpus/corpus-options.jsp
 * corpus/corpus-wizard.jsp
 * corpus/edit-corpus.jsp
 * corpus/list-corpus.jsp
 * corpus/tags-corpus.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "CorpusUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/update",
            "/corpus/edit"
        }
)
public class CorpusUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Screen to update the corpus
            Corpus corpus = CorpusDAO.selectByIdWithTagValues(Integer.parseInt(request.getParameter("id_corpus")));
            if (corpus == null){
                corpus = CorpusDAO.selectById(Integer.parseInt(request.getParameter("id_corpus")));
            }
            request.setAttribute("corpus", corpus);
            
            List<CorpusTools> listCorpusTools = CorpusToolsDAO.selectAll(corpus.getId());
            request.setAttribute("tools", listCorpusTools);

            //Page viewer
            template.pageViewer(
                    "menu/corpus.jsp", //page_menu
                    "breadcrumb/corpus.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "corpus", //page_menu_option_sub
                    "update", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Alterar Corpus", //page_title
                    null, //page_subtitle
                    "corpus/edit-corpus.jsp"); //page

            template.forward();

        } catch (NumberFormatException | DAOException | IOException | URISyntaxException | ServletException e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Get corpus
            Corpus corpus = CorpusDAO.selectByIdWithTagValues(Integer.parseInt(request.getParameter("id_corpus")));
            if (corpus == null){
                corpus = CorpusDAO.selectById(Integer.parseInt(request.getParameter("id_corpus")));
            }
            
            String userTerms = request.getParameter("userTerms");
            
            corpus.setName(request.getParameter("name"));
            corpus.setDescription(request.getParameter("description"));
            corpus.setType(Integer.parseInt(request.getParameter("type")));
            corpus.setUserTerms(userTerms);
            //Update corpus
            CorpusDAO.update(corpus);
            
            if (corpus.getCorpusTagValues() != null){
                for (CorpusTagValues corpusMetadataValues : corpus.getCorpusTagValues()) {
                    corpusMetadataValues.setValue(request.getParameter(corpusMetadataValues.getCorpusTags().getTag()));
                }
                
                //Update Corpus Tags
                CorpusTagValuesDAO.updateByMetadataId(corpus.getCorpusTagValues());
            }

            //Update Corpus Tools
            this.updateCorpusTools(request, corpus);
            
            Logger.controllerEvent(this, request, "Corpus Updated");
            
            template.redirect(
                    this, //servlet
                    "/corpus/select", //path
                    "Corpus alterado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private void updateCorpusTools (HttpServletRequest request, Corpus corpus) throws DAOException {
        // Corpus Tools
        List<CorpusTools> oldCorpusTools = CorpusToolsDAO.selectAll(corpus.getId());
        List<CorpusTools> newCorpusTools = new ArrayList<>();
        
        String[] tools = request.getParameterValues("tools[]");
        if (tools != null) {
            for (String tool : tools) {
                CorpusTools ct = new CorpusTools();
                ct.setTool(tool);
                ct.setCorpus(corpus);
                ct.setIdCorpus(corpus.getId());
                
                newCorpusTools.add(ct);
                
                // if not exist in old list -> insert in DB
                if (!this.corpusToolsListContains(oldCorpusTools, ct)){
                    // Insert
                    CorpusToolsDAO.insert(ct);
                }
            }
            corpus.setCorpusTools(newCorpusTools);
        }
        
        // delete all in the old list
        for (CorpusTools ct : oldCorpusTools) {
            CorpusToolsDAO.delete(ct);
        }
    }
    
    private boolean corpusToolsListContains (List<CorpusTools> listCorpusTools, CorpusTools corpusTools){
        for (CorpusTools ct : listCorpusTools) {
            if (ct.getTool().equals(corpusTools.getTool())){
                // if exist in the old list -> remove the reference, not in DB
                listCorpusTools.remove(ct);
                return true;
            }
        }
        return false;
    }

}
