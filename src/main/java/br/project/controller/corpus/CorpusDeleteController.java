package br.project.controller.corpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.CorpusToolsDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTools;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusDeleteController
 *
 * <pre>
 * pages:
 * corpus/corpus-options.jsp
 * corpus/corpus-wizard.jsp
 * corpus/edit-corpus.jsp
 * corpus/list-corpus.jsp
 * corpus/tags-corpus.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "CorpusDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/delete",
            "/corpus/remove"
        }
)
public class CorpusDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Screen to update the corpus
            Corpus corpus = CorpusDAO.selectByIdWithTagValues(Integer.parseInt(request.getParameter("id_corpus")));
            if (corpus == null){
                corpus = CorpusDAO.selectById(Integer.parseInt(request.getParameter("id_corpus")));
            }
            request.setAttribute("corpus", corpus);
            
            List<CorpusTools> listCorpusTools = CorpusToolsDAO.selectAll(corpus.getId());
            request.setAttribute("tools", listCorpusTools);
            
            //Page viewer
            template.pageViewer(
                    "menu/corpus.jsp", //page_menu
                    "breadcrumb/corpus.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "corpus", //page_menu_option_sub
                    "delete", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover Corpus", //page_title
                    null, //page_subtitle
                    "corpus/edit-corpus.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Delete corpus
            Corpus corpus = new Corpus();
            corpus.setId(Integer.parseInt(request.getParameter("id_corpus")));

            CorpusToolsDAO.deleteAllByIdCorpus(corpus.getId());
            CorpusDAO.delete(corpus);
            
            Logger.controllerEvent(this, request, "Corpus Deleted");

            // Success
            template.redirect(
                    this, //servlet
                    "/corpus/select", //path
                    "Corpus removido com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
