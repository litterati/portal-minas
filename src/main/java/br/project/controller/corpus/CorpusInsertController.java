package br.project.controller.corpus;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.CorpusToolsDAO;
import br.project.entity.Corpus;
import br.project.entity.CorpusTools;
import br.project.exception.DAOException;
import br.project.facade.CorpusTagsFacade;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CorpusInsertController
 *
 * <pre>
 * pages:
 * corpus/corpus-options.jsp
 * corpus/corpus-wizard.jsp
 * corpus/edit-corpus.jsp
 * corpus/list-corpus.jsp
 * corpus/tags-corpus.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "CorpusInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/corpus/insert",
            "/corpus/create"
        }
)
public class CorpusInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Screen to create corpus
            Corpus corpus = new Corpus();
            CorpusTagsFacade.fillCorpusTagsWithoutValue(corpus);

            //Page viewer
            template.pageViewer(
                    "menu/corpus.jsp", //page_menu
                    "breadcrumb/corpus.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "corpus", //page_menu_option_sub
                    "insert", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Corpus", //page_title
                    null, //page_subtitle
                    "corpus/edit-corpus.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new corpus
            Corpus corpus = new Corpus();
            corpus.setName(request.getParameter("name"));
            corpus.setDescription(request.getParameter("description"));
            corpus.setType(Integer.parseInt(request.getParameter("type")));

            CorpusDAO.insert(corpus);
            this.insertCorpusTools(request, corpus);
            
            Logger.controllerEvent(this, request, "Corpus Inserted");

            template.redirect(
                    this, //servlet
                    "/corpus/select", //path
                    "Corpus cadastrado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/corpus/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    private void insertCorpusTools (HttpServletRequest request, Corpus corpus) throws DAOException {
        //Corpus Tools
        String[] tools = request.getParameterValues("tools[]");
        if (tools != null) {
            List<CorpusTools> corpusTools = new ArrayList<>();
            for (String tool : tools) {
                CorpusTools ct = new CorpusTools();
                ct.setTool(tool);
                ct.setCorpus(corpus);
                ct.setIdCorpus(corpus.getId());
                corpusTools.add(ct);
                
                // Insert
                CorpusToolsDAO.insert(ct);
            }
            corpus.setCorpusTools(corpusTools);
        }
    }

}
