/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.controller.corpus;

import br.library.util.Config;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.TemplateMain;
import br.project.dao.CorpusDAO;
import br.project.entity.Corpus;
import br.project.exception.DAOException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author marcel
 */
@WebServlet(name = "CorpusUserTermsController", urlPatterns = {"/corpus/userTerms"})
public class CorpusUserTermsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CorpusUserTermsController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CorpusUserTermsController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Corpus corpus;
        try{
        corpus = CorpusDAO.selectById(Integer.parseInt(request.getParameter("id_corpus")));
        request.setAttribute("userTerms", corpus.getUserTerms());
        
        request.getRequestDispatcher(
            Config.getPathProperties().getProperty("path.pages") + "corpus/user-terms.jsp"
        ).forward(request, response);
        
        } catch(NumberFormatException | DAOException | IOException | URISyntaxException | ServletException ex){
            throw new RuntimeException("Erro em CorpusUserTermsController. " + ex.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
