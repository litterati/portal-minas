package br.project.controller.filter;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * LoggedCheckerFilter.java
 *
 * @author Michelle, Thiago Vieira
 */
@WebFilter(
        urlPatterns = {"/*"},
        description = "Session Checker Filter"
)
public class LoggedCheckerFilter implements Filter {

    private FilterConfig config = null;

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
        config.getServletContext().log("Initializing SessionCheckerFilter");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        req.setCharacterEncoding("UTF-8");
        res.setCharacterEncoding("UTF-8");
        String uri = request.getRequestURI();

        // Check if the user's session attribute contains an attribute
        // named userLoged. If the attribute not exists redirect 
        // user to the login page.
        if (uri.indexOf("/css") > 0) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/images") > 0) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/js") > 0) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/plugin") > 0) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/font") > 0) {
            chain.doFilter(request, response);
        } else if (uri.endsWith(request.getContextPath() + "/index.jsp") || // Index.jsp
                uri.endsWith(request.getContextPath() + "/index") || 
                uri.endsWith(request.getContextPath() + "/about.jsp") || // About.jsp
                uri.endsWith(request.getContextPath() + "/about") ||
                uri.endsWith(request.getContextPath() + "/thanks.jsp") || // Thanks.jsp
                uri.endsWith(request.getContextPath() + "/thanks") || 
                uri.endsWith(request.getContextPath() + "/contact.jsp") || // Contact.jsp
                uri.endsWith(request.getContextPath() + "/contact") || 
                uri.endsWith(request.getContextPath() + "/login.jsp") || // Login.jsp
                uri.endsWith(request.getContextPath() + "/login") ||
                uri.endsWith(request.getContextPath() + "/account/login") ||
                uri.endsWith(request.getContextPath() + "/forgot.jsp") || // Forgot.jsp
                uri.endsWith(request.getContextPath() + "/forgot") ||
                uri.endsWith(request.getContextPath() + "/register.jsp") || // Register.jsp
                uri.endsWith(request.getContextPath() + "/register") ||
                uri.contains(request.getContextPath() + "/captcha/") || //captcha
                uri.endsWith(request.getContextPath() + "/user/exist")) { 
            // Nada, a página será exibida normalmente
        } else if (request.getSession().getAttribute("userLoged") == null) {
            response.sendRedirect(request.getContextPath() + "/index.jsp");
        }

        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {
        config.getServletContext().log("Destroying SessionCheckerFilter");
    }
}
