/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.controller.annotation;

import br.library.util.Config;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.entity.Corpus;
import java.io.FileReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author regis
 */

@WebServlet(
        name = "AnnotationEditConnectionController",
        description = "Portal Min@s",
        urlPatterns = {
            "/annotation/annotation-edit-connection"
        }
)

public class AnnotationEditConnectionController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            /*
            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                //Text invalido
                throw new Exception("Texto Inválido");
            }
            */
            
            
            // read json file
            String path = path = Config.getPathProperties().getProperty("path.resources") + "text.json";
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(path));
            JSONObject jsonObject = (JSONObject) obj;
            
            JSONArray text_words = (JSONArray)jsonObject.get("words");
            JSONArray annotation = (JSONArray)jsonObject.get("annotation");
            JSONArray connection = (JSONArray)jsonObject.get("connection");
            JSONArray typesConnection = (JSONArray)jsonObject.get("typesConnection");
            
            /*
            ArrayList<String> categories = new ArrayList<>();
            categories.add("Conexão A");
            categories.add("Conexão B");
            categories.add("Conexão C");
            categories.add("Conexão D");
            categories.add("Conexão E");
            */

            //List<Words> text_words = null;

            request.setAttribute("text_words", text_words);
            request.setAttribute("annotation", annotation);
            request.setAttribute("connection", connection);
            request.setAttribute("category", typesConnection);
            //request.setAttribute("text", text);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "annotation", //page_menu_tools
                    "simple", //page_menu_tools_sub
                    "Editor de relações", //page_title
                    "annotation/annotation-edit-connection.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/annotation/annotation-edit-connection", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
}