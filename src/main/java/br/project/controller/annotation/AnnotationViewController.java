/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.controller.annotation;

import br.library.util.Config;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.entity.Corpus;
import java.io.FileReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author regis
 */

@WebServlet(
        name = "AnnotationViewController",
        description = "Portal Min@s",
        urlPatterns = {
            "/annotation/annotation-view"
        }
)

public class AnnotationViewController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            /*
            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                //Text invalido
                throw new Exception("Texto Inválido");
            }
            */
            
            
            // read json file
            String path = Config.getPathProperties().getProperty("path.resources") + "text.json";
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(path));
            JSONObject jsonObject = (JSONObject) obj;
            
            JSONArray text_words = (JSONArray)jsonObject.get("words");
            JSONArray annotation = (JSONArray)jsonObject.get("annotation");
            JSONArray connection = (JSONArray)jsonObject.get("connection");
            JSONArray typesAnnotation = (JSONArray)jsonObject.get("typesAnnotation");
            JSONArray typesConnection = (JSONArray)jsonObject.get("typesConnection");

            //List<Words> text_words = null;

            request.setAttribute("text_words", text_words);
            request.setAttribute("annotation", annotation);
            request.setAttribute("connection", connection);
            request.setAttribute("typeAnnotation", typesAnnotation);
            request.setAttribute("typeConnection", typesConnection);
            //request.setAttribute("text", text);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "annotation", //page_menu_tools
                    "simple", //page_menu_tools_sub
                    "Anotador", //page_title
                    "annotation/annotation-view.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/annotation/annotation-view", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            
            /*
            List<Text> listTexts = TextDAO.selectAllByIdCorpus(corpus.getId(), TextDAO.TITLE);

            // Validar o texto
            Text text = TextDAO.selectById(Integer.parseInt(request.getParameter("texts")));
            if (text == null) {
                //Text invalido
            }
            */

            // read json file
            String path = Config.getPathProperties().getProperty("path.resources") + "text.json";
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(path));
            JSONObject jsonObject = (JSONObject) obj;
            
            JSONArray text_words = (JSONArray)jsonObject.get("words");
            JSONArray annotation = (JSONArray)jsonObject.get("annotation");
            JSONArray connection = (JSONArray)jsonObject.get("connection");
            JSONArray typesAnnotation = (JSONArray)jsonObject.get("typesAnnotation");
            JSONArray typesConnection = (JSONArray)jsonObject.get("typesConnection");
            
            // new element received by post
            JSONObject jsonObj = new JSONObject();
            
            jsonObj.put("start", request.getParameter("start"));
            jsonObj.put("end", request.getParameter("end"));
            jsonObj.put("type", request.getParameter("type"));
            
            String idAnnotation = request.getParameter("id");
            
            if ("element".equals(request.getParameter("edit_type"))) {
                jsonObj.put("id", annotation.size()+1);
                annotation.add(jsonObj);
            } 
            if ("connection".equals(request.getParameter("edit_type"))) {
                jsonObj.put("id", connection.size()+1);
                connection.add(jsonObj);
            }
            if ("editAnnotation".equals(request.getParameter("edit_type"))) {
                for (Object annotation1 : annotation) {
                    JSONObject objAnnotation = (JSONObject)annotation1;
                    System.out.println(idAnnotation+objAnnotation.get("id").toString());
                    
                    if (idAnnotation.equals(objAnnotation.get("id").toString())) {
                        System.out.println("ok");
                        annotation.remove(annotation1);
                        
                        jsonObj.put("start", objAnnotation.get("start"));
                        jsonObj.put("end", objAnnotation.get("end"));
                        jsonObj.put("id", request.getParameter("id"));
                        
                        break;
                    }
                }
                
                if (!request.getParameter("start").equals("0")) {
                    System.out.println(jsonObj.toJSONString());
                    annotation.add(jsonObj);
                }
            }

            //List<Words> text_words = null;

            request.setAttribute("text_words", text_words);
            request.setAttribute("annotation", annotation);
            request.setAttribute("connection", connection);
            request.setAttribute("typeAnnotation", typesAnnotation);
            request.setAttribute("typeConnection", typesConnection);
            //request.setAttribute("text", text);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/align.jsp", //page_menu
                    "breadcrumb/align.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "annotation", //page_menu_tools
                    "simple", //page_menu_tools_sub
                    "Anotador", //page_title
                    "annotation/annotation-view.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/annotation/annotation-view", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
}