/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.concordancer;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextTagsDAO;
import br.project.entity.Concordancer;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.entity.TextTags;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/* 
 * ConcordancerAdvancedController
 *
 * <pre>
 * pages:
 * concordancer/advanced.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 20/11/2014
 *
 */
@WebServlet(
        name = "ConcordancerAdvancedController",
        description = "Portal Min@s",
        urlPatterns = {
            "/concordancer/advanced"
        }
)
public class ConcordancerAdvancedController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);
        
        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            Text text = new Text();
            text.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));

            Concordancer concordancer = new Concordancer();
            
            List<TextTags> textTagsList = TextTagsDAO.selectDistinctWithTagValuesByIdCorpus(corpus.getId());
            
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("concordancer", concordancer);
            request.setAttribute("textTagsList", textTagsList);

            //Page viewer
            template.pageViewer(
                    "menu/concordancer.jsp", //page_menu
                    "breadcrumb/concordancer.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "concordancer", //page_menu_tools
                    "advanced", //page_menu_tools_sub
                    "Concordanciador", //page_title
                    "concordancer/advanced.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e); //log
            template.redirect(
                    this, //servlet
                    "/concordancer/search", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
}
