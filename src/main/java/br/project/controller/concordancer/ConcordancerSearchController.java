/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.concordancer;

import br.library.util.Chronometer;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.ConcordancerDAO;
import br.project.dao.TextTagsDAO;
import br.library.util.template.Tool;
import br.project.dao.SearchHistoryDAO;
import br.project.entity.Concordancer;
import br.project.entity.Corpus;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.entity.User;
import br.project.exception.DAOException;
import br.project.exception.InvalidCorpusException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/* 
 * ConcordancerSearchController
 *
 * <pre>
 * pages:
 * concordancer/search.jsp
 * </pre>
 *
 * @author Michelle, Thiago Vieira
 * @revised Marcel Akira Serikawa email:marcel.serikawa@gmail.com
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "ConcordancerSearchController",
        description = "Portal Min@s",
        urlPatterns = {
            "/concordancer/search"
        }
)
public class ConcordancerSearchController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.CONCORDANCER);

            //if is a search
            if (request.getParameter("search") != null) {
                this.concordancer(request);
            }
            //List<TextTags> textTagsList = TextTagsDAO.selectByIdCorpus(corpus.getId());
            List<TextTags> textTagsList = TextTagsDAO.selectDistinctWithTagValuesByIdCorpus(corpus.getId());

            request.setAttribute("textTagsList", textTagsList);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("searchHistory", searchHistory(request));

            //Page viewer
            template.pageViewer(
                    "menu/concordancer.jsp", //page_menu
                    "breadcrumb/concordancer.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "concordancer", //page_menu_tools
                    "simple", //page_menu_tools_sub
                    "Concordanciador", //page_title
                    "concordancer/search.jsp"); //page

            template.forward();

        } catch (DAOException | InvalidCorpusException | IOException | URISyntaxException | ServletException e) {
            // Error
            Logger.controllerError(this, request, e); //log
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private void concordancer(HttpServletRequest request) {
        //Chronometer start
        Chronometer.start();
        //Concordancer
        Concordancer concordancer = new Concordancer();
        concordancer.setTerm(request.getParameter("search"));
        concordancer.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
        //tolerance
        if (request.getParameter("tolerance") != null && !request.getParameter("tolerance").isEmpty()) {
            concordancer.setTolerance(Integer.parseInt(request.getParameter("tolerance")));
        }
        //type, search target
        if (request.getParameter("tokenType") != null && !request.getParameter("tokenType").isEmpty()) {
            if (request.getParameter("tokenType").equals(Concordancer.TARGET_METATEXTO)){
                concordancer.setSearchTarget(Concordancer.TARGET_METATEXTO);
            }
        }
        //result by pages
        if (request.getParameter("cmbResultByPage") != null && !request.getParameter("cmbResultByPage").isEmpty()) {
            concordancer.setResultsByPage(Integer.parseInt(request.getParameter("cmbResultByPage")));
        }
        //search target
        if (request.getParameter("cmbSearchTarget") != null && !request.getParameter("cmbSearchTarget").isEmpty()) {
            concordancer.setSearchTarget(request.getParameter("cmbSearchTarget"));
        }
        //left tokens
        if (request.getParameter("leftTokens") != null && !request.getParameter("leftTokens").isEmpty()) {
            concordancer.setLeftTokensNumber(Integer.parseInt(request.getParameter("leftTokens")));
        }
        //right tokens
        if (request.getParameter("rightTokens") != null && !request.getParameter("rightTokens").isEmpty()) {
            concordancer.setRightTokensNumber(Integer.parseInt(request.getParameter("rightTokens")));
        }
        //current page
        if (request.getParameter("currentPage") != null && !request.getParameter("currentPage").isEmpty()){
            concordancer.setCurrentPage(Integer.parseInt(request.getParameter("currentPage")));
        }
        if (concordancer.getCurrentPage() <= 1){
            concordancer.setInterval(0);
            concordancer.setCurrentPage(1);
        } else {
            concordancer.setInterval((concordancer.getCurrentPage() * concordancer.getResultsByPage()) - concordancer.getResultsByPage());
        }
        //Metadata
        String[] metadata = request.getParameterValues("metadata[]");
        if (metadata != null) {
            List<TextTagValues> textTagValueList = new ArrayList<>();
            for (String value : metadata) {
                String[] aux = value.split("\\|", 3);
                TextTagValues ttv = new TextTagValues();
                ttv.setIdTextTag(Integer.parseInt(aux[0]));
                ttv.setId(Integer.parseInt(aux[1]));
                ttv.setValue(aux[2]);
                textTagValueList.add(ttv);
            }
            concordancer.setLstTextTagValues(textTagValueList);
        }
        //Concordancer
        ConcordancerDAO.search(concordancer);
        //Error
        concordancer.setError("");
        if ((concordancer.getTotalPages() > 0) && 
                (concordancer.getTotalPages() < concordancer.getCurrentPage())) {
            concordancer.setError("Não existem mais páginas, clique no botão anterior");
        } else if (concordancer.getEntries().size() <= 0){
            concordancer.setError("Nenhum resultado encontrado");
        }
        //Chronometer end
        Chronometer.stop();
        concordancer.setTime(Chronometer.elapsedTime());
        //Set the result
        request.setAttribute("concordancer", concordancer);
        request.setAttribute("start", ((concordancer.getCurrentPage() - 1) * concordancer.getResultsByPage()) + 1);
        
        //Atualizando buscas
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userLoged");
        SearchHistoryDAO searchHistoryDAO = new SearchHistoryDAO();
        searchHistoryDAO.updateSearch(user.getId(), request.getParameter("search"));
    }

    private ArrayList<String> searchHistory(HttpServletRequest request){
        HttpSession session = request.getSession();
        ArrayList<String> searchHistory;
        User user = (User) session.getAttribute("userLoged");
        
        SearchHistoryDAO searchHistoryDAO = new SearchHistoryDAO();
        searchHistory = searchHistoryDAO.lastSearches(user.getId());
        
        return searchHistory;
    }
}