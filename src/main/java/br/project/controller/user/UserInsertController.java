package br.project.controller.user;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.PermissionDAO;
import br.project.dao.UserDAO;
import br.project.entity.Corpus;
import br.project.entity.Permission;
import br.project.entity.User;
import br.project.exception.DAOException;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * UserInsertController
 *
 * <pre>
 * pages:
 * user/edit.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "UserInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/user/insert",
            "/user/create"
        }
)
public class UserInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Screen to create user
            request.setAttribute("corpora", CorpusDAO.selectAll(Corpus.ALL));

            //Page viewer
            template.pageViewer(
                    "menu/user.jsp", //page_menu
                    "breadcrumb/user.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "user", //page_menu_option_sub
                    "insert", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir usuário", //page_title
                    null, //page_subtitle
                    "user/edit.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/user/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create new user
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLogin(request.getParameter("login"));
            user.setPassword(request.getParameter("password"));
            user.setEmail(request.getParameter("email"));
            user.setType(Integer.parseInt(request.getParameter("type")));

            //insert
            UserDAO userDAO = new UserDAO();
            userDAO.insert(user);
            this.insertCorpusPermission(request, user);

            Logger.controllerEvent(this, request, "User Inserted");

            // Success
            template.redirect(
                    this, //servlet
                    "/user/select", //path
                    "Usuário inserido com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/user/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private void insertCorpusPermission(HttpServletRequest request, User user) throws DAOException {
        // User permissions
        String[] permissions = request.getParameterValues("permission[]");
        if (permissions != null) {
            ArrayList<Permission> listPermission = new ArrayList<>();
            for (String id_corpus : permissions) {
                Permission p = new Permission();
                p.setLevel(1);
                p.setIdCorpus(Integer.parseInt(id_corpus));
                p.setIdUser(user.getId());
                p.setUser(user);
                listPermission.add(p);

                // Insert
                PermissionDAO.insert(p);
            }
            user.setPermissions(listPermission);
        }
    }

}
