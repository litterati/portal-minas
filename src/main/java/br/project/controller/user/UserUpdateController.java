package br.project.controller.user;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.PermissionDAO;
import br.project.dao.UserDAO;
import br.project.entity.Corpus;
import br.project.entity.Permission;
import br.project.entity.User;
import br.project.exception.DAOException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * UserUpdateController
 *
 * <pre>
 * pages:
 * user/edit.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "UserUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/user/update",
            "/user/edit"
        }
)
public class UserUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Screen to update the user
            User user = new User();
            user.setId(Integer.parseInt(request.getParameter("id")));

            UserDAO userDAO = new UserDAO();
            userDAO.selectById(user);

            request.setAttribute("user", user);
            request.setAttribute("corpora", CorpusDAO.selectAll(Corpus.ALL));

            List<Permission> listPermission = PermissionDAO.selectAll(user.getId());
            request.setAttribute("permission", listPermission);

            //Page viewer
            template.pageViewer(
                    "menu/user.jsp", //page_menu
                    "breadcrumb/user.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "user", //page_menu_option_sub
                    "update", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Alterar usuário", //page_title
                    null, //page_subtitle
                    "user/edit.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/user/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Update user
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLogin(request.getParameter("login"));
            user.setEmail(request.getParameter("email"));
            user.setPassword(request.getParameter("password"));
            user.setType(Integer.parseInt(request.getParameter("type")));

            //update
            user.setId(Integer.parseInt(request.getParameter("id")));
            UserDAO userDAO = new UserDAO();
            userDAO.update(user);
            this.updateCorpusTools(request, user);

            Logger.controllerEvent(this, request, "User Updated");

            // Success
            template.redirect(
                    this, //servlet
                    "/user/select", //path
                    "Usuário alterado com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/user/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private void updateCorpusTools(HttpServletRequest request, User user) throws DAOException {
        // User permissions
        List<Permission> oldPermissions = PermissionDAO.selectAll(user.getId());
        List<Permission> newPermissions = new ArrayList<>();

        String[] permissions = request.getParameterValues("permission[]");
        if (permissions != null) {
            for (String id_corpus : permissions) {
                Permission p = new Permission();
                p.setLevel(1);
                p.setIdCorpus(Integer.parseInt(id_corpus));
                p.setIdUser(user.getId());
                p.setUser(user);

                newPermissions.add(p);

                // if not exist in old list -> insert in DB
                if (!this.permissionListContains(oldPermissions, p)) {
                    // Insert
                    PermissionDAO.insert(p);
                }
            }
            user.setPermissions(newPermissions);
        }

        // delete all in the old list
        for (Permission p : oldPermissions) {
            PermissionDAO.delete(p);
        }
    }

    private boolean permissionListContains(List<Permission> listPermission, Permission permission) {
        for (Permission p : listPermission) {
            if (p.getIdCorpus() == permission.getIdCorpus()) {
                // if exist in the old list -> remove the reference, not in DB
                listPermission.remove(p);
                return true;
            }
        }
        return false;
    }

}
