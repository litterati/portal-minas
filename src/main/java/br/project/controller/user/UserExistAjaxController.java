/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.user;

import br.project.dao.UserDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * UserExistAjaxController
 *
 * @author Thiago Vieira
 * @since 02/09/2014
 *
 */
@WebServlet(
        name = "UserExistAjaxController",
        description = "Permite que o usuário se cadastre no sistema.",
        urlPatterns = {
            "/user/exist"
        }
)
public class UserExistAjaxController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject obj = new JSONObject();
        
        if (request.getParameter("login") != null) {
            obj.put("login", UserDAO.existLogin(request.getParameter("login")));
        } else if (request.getParameter("email") != null) {
            obj.put("email", UserDAO.existEmail(request.getParameter("email")));
        }

        //Ajax response
        response.setContentType("application/json");
        response.getWriter().write(obj.toString());
    }

}
