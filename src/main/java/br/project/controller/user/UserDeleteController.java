package br.project.controller.user;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.CorpusDAO;
import br.project.dao.PermissionDAO;
import br.project.dao.UserDAO;
import br.project.entity.Corpus;
import br.project.entity.Permission;
import br.project.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * UserDeleteController
 *
 * <pre>
 * pages:
 * user/edit.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "UserDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/user/delete",
            "/user/remove"
        }
)
public class UserDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Screen to update the user
            User user = new User();
            user.setId(Integer.parseInt(request.getParameter("id")));

            UserDAO userDAO = new UserDAO();
            userDAO.selectById(user);

            request.setAttribute("user", user);
            request.setAttribute("corpora", CorpusDAO.selectAll(Corpus.ALL));

            List<Permission> listPermission = PermissionDAO.selectAll(user.getId());
            request.setAttribute("permission", listPermission);

            //Page viewer
            template.pageViewer(
                    "menu/user.jsp", //page_menu
                    "breadcrumb/user.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "user", //page_menu_option_sub
                    "delete", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover usuário", //page_title
                    null, //page_subtitle
                    "user/edit.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/user/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Delete user

            User user = new User();
            user.setId(Integer.parseInt(request.getParameter("id")));
            UserDAO userDAO = new UserDAO();
            userDAO.delete(user);

            Logger.controllerEvent(this, request, "User Deleted");

            // Success
            template.redirect(
                    this, //servlet
                    "/user/select", //path
                    "Usuário removido com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/user/select", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
