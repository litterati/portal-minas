package br.project.controller.user;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.UserDAO;
import br.project.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * UserSelectController
 *
 * <pre>
 * pages:
 * user/list.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 28/07/2014
 *
 */
@WebServlet(
        name = "UserSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/user/select",
            "/user/list"
        }
)
public class UserSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            //Scree with all users
            String order = request.getParameter("order");
            if ( order != null && order.equals(UserDAO.NAME) ){
                order = UserDAO.NAME;
            } else if ( order != null && order.equals(UserDAO.TYPE) ){
                order = UserDAO.TYPE;
            } else {
                order = UserDAO.LOGIN;
            }
            request.setAttribute("order", order);
        
            List<User> listUsers = UserDAO.selectAll(order);
            request.setAttribute("users", listUsers);

            //Page viewer
            template.pageViewer(
                    "menu/user.jsp", //page_menu
                    "breadcrumb/user.jsp", //page_breadcrumb
                    null, //page_menu_id_corpus
                    null, //page_menu_name_corpus
                    "manage", //page_menu_option
                    "user", //page_menu_option_sub
                    "select", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de usuários", //page_title
                    null, //page_subtitle
                    "user/list.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //url
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
