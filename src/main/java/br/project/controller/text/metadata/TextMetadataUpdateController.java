/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.dao.TextTagsDAO;
import br.project.entity.Corpus;
import br.project.entity.TextTags;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextMetadataUpdateController
 *
 * <pre>
 * pages:
 * texts/metadata/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextMetadataUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/metadata/update",
            "/text/metadata/edit"
        }
)
public class TextMetadataUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Metadata
            TextTagsDAO textMetadataDAO = new TextTagsDAO();
            TextTags corpusMetadata = textMetadataDAO.selectById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("text_metadata", corpusMetadata);

            //Select all fields type
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            request.setAttribute("fields_type_list", fields_type_dao.selectAll());

            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata-text", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Alterar Etiqueta do Texto", //page_title
                    "text/metadata/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {

            TextTags textMetadata = new TextTags();
            textMetadata.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            textMetadata.setTag(request.getParameter("metadata_TextTags"));
            textMetadata.setIsRequired(Boolean.parseBoolean(request.getParameter("is_required_TextTags")));
            textMetadata.setIdFieldType(Integer.parseInt(request.getParameter("id_field_type_TextTags")) == 0 ? 7 : Integer.parseInt(request.getParameter("id_field_type_TextTags")));

            TextTagsDAO textMetadataDAO = new TextTagsDAO();
            textMetadata.setId(Integer.parseInt(request.getParameter("id_tag")));
            textMetadataDAO.update(textMetadata);

            Logger.controllerEvent(this, request, "Text Metadata Updated");

            //Success
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Etiqueta do texto alterada com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }

    }

}
