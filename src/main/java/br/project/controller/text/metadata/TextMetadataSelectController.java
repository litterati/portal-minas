/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.dao.MetaWordTagsDAO;
import br.project.dao.TextTagsDAO;
import br.project.dao.WordTagsDAO;
import br.project.entity.Corpus;
import br.project.entity.MetaWordTags;
import br.project.entity.TextTags;
import br.project.entity.WordTags;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextMetadataSelectController
 *
 * <pre>
 * pages:
 * texts/metadata/select-form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextMetadataSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/metadata/select",
            "/text/metadata/list"
        }
)
public class TextMetadataSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();

            TextTags textTag;
            textTag = new TextTags();
            textTag.setIdCorpus(corpus.getId());
            MetaWordTags textMetaWordTag;
            textMetaWordTag = new MetaWordTags();
            textMetaWordTag.setIdCorpus(corpus.getId());
            WordTags textWordTag;
            textWordTag = new WordTags();
            textWordTag.setIdCorpus(corpus.getId());

            List<TextTags> listTextTags = TextTagsDAO.select(textTag);
            List<MetaWordTags> listTextMetaWordTags = MetaWordTagsDAO.selectByIdCorpus(corpus.getId());
            List<WordTags> listTextStyleTags = WordTagsDAO.selectStyleWordTags(corpus.getId());

            if (listTextTags.size() < 1) {
                listTextTags.add(textTag);
            }
            if (listTextMetaWordTags.size() < 1) {
                listTextMetaWordTags.add(textMetaWordTag);
            }
            if (listTextStyleTags.size() < 1) {
                listTextStyleTags.add(textWordTag);
            }

            //request.setAttribute("error", error);
            request.setAttribute("listTextTags", listTextTags);
            request.setAttribute("listTextMetaWordTags", listTextMetaWordTags);
            request.setAttribute("listTextStyleTags", listTextStyleTags);
            request.setAttribute("lstFieldsType", FieldsTypeDAO.selectAll());

            if (request.getParameter("wizard") != null && !request.getParameter("wizard").isEmpty()) {
                request.setAttribute("wizard", "true");
            }

            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata-text", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Etiquetas dos Textos", //page_title
                    "text/metadata/list-form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
}
