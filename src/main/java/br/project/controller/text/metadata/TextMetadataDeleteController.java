/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.dao.MetaWordTagsDAO;
import br.project.dao.TextTagsDAO;
import br.project.dao.WordTagsDAO;
import br.project.entity.Corpus;
import br.project.entity.MetaWordTags;
import br.project.entity.TextTags;
import br.project.entity.WordTags;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * TextMetadataDeleteController
 *
 * <pre>
 * pages:
 * texts/metadata/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextMetadataDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/metadata/delete",
            "/text/metadata/remove"
        }
)
public class TextMetadataDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Metadata
            TextTagsDAO textMetadataDAO = new TextTagsDAO();
            TextTags corpusMetadata = textMetadataDAO.selectById(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("text_metadata", corpusMetadata);

            //Select all fields type
            FieldsTypeDAO fields_type_dao = new FieldsTypeDAO();
            request.setAttribute("fields_type_list", fields_type_dao.selectAll());

            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata-text", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover Etiqueta do Texto", //page_title
                    "text/metadata/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {

            String type = request.getParameter("type");
            TextTags textTags;
            MetaWordTags metaWordTags;
            WordTags wordTag;

            switch (type) {
                case "metaWordTag":
                    metaWordTags = new MetaWordTags();
                    metaWordTags.setId(Integer.parseInt(request.getParameter("id_meta_word_tag")));
                    MetaWordTagsDAO.deleteById(metaWordTags);
                    break;
                case "textTag":
                    textTags = new TextTags();
                    textTags.setId(Integer.parseInt(request.getParameter("id_text_tag")));
                    TextTagsDAO.deleteById(textTags);
                    break;
                case "style":
                    wordTag = new WordTags();
                    metaWordTags = new MetaWordTags();
                    wordTag.setId(Integer.parseInt(request.getParameter("id_style_tag")));
                    metaWordTags.setTag(request.getParameter("tag"));
                    WordTagsDAO.deleteById(wordTag);
                    MetaWordTagsDAO.deleteByTag(metaWordTags);
                    break;
            }

            //Delete
//            if (TextTagsFacade.existMetadataValue(textMetadata)) {
//                //You can't deleteById the metadata, because there is value inside it
//                //error = "Não é possível remover o metadado \"" + textMetadata.getTag() + "\" pois, existem textos com valores nele.";
//            } else {
//            }
            Logger.controllerEvent(this, request, "Text Metadata Deleted");

            // Ajax response
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                obj.put("alert_success", "Etiqueta removida com sucesso!");
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            //Success
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Etiqueta do texto removida com sucesso", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                obj.put("alert_success", "Erro na remoção da etiqueta");
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }
            
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }
}
