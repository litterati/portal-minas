/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text.metadata;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.FieldsTypeDAO;
import br.project.dao.MetaWordTagsDAO;
import br.project.dao.TextTagsDAO;
import br.project.dao.WordTagsDAO;
import br.project.entity.Corpus;
import br.project.entity.MetaWordTags;
import br.project.entity.TextTags;
import br.project.entity.WordTags;
import br.project.exception.DAOException;
import br.project.exception.TextTagsFacadeException;
import br.project.facade.TextTagsFacade;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * TextMetadataInsertController
 *
 * <pre>
 * pages:
 * texts/metadata/form.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextMetadataInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/metadata/insert",
            "/text/metadata/create"
        }
)
public class TextMetadataInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            //Select all fields type
            request.setAttribute("fields_type_list", FieldsTypeDAO.selectAll());

            if (request.getParameter("wizard") != null && !request.getParameter("wizard").isEmpty()) {
                request.setAttribute("wizard", "true");
            }

            //Page viewer
            template.pageViewer(
                    "menu/metadata.jsp", //page_menu
                    "breadcrumb/metadata.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "metadata", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Etiquetas dos Textos", //page_title
                    "text/metadata/form.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);
        String error = "";
        TextTags textTag;
        MetaWordTags metaWordTag;

        try {

            String opt = request.getParameter("type");
            switch (opt) {
                case "textTags":
                    textTag = new TextTags();
                    textTag.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
                    textTag.setTag(request.getParameter("tag"));
                    textTag.setIsRequired(Boolean.parseBoolean(request.getParameter("is_required")));
                    textTag.setIdFieldType(Integer.parseInt(request.getParameter("id_field_type")) == 0 ? 7 : Integer.parseInt(request.getParameter("id_field_type")));
                    if (!existTextTag(textTag)) {
                        insertTextTag(textTag);
                    } else {
                        error = "Etiqueta de cabeçalho já existente!";
                    }
                    break;
                case "metaWordTags":
                    metaWordTag = new MetaWordTags();
                    metaWordTag.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
                    metaWordTag.setTag(request.getParameter("tag"));
                    metaWordTag.setXces(request.getParameter("xces"));
                    if (!existMetaWordTag(metaWordTag)) {
                        insertMetaWordTag(metaWordTag);
                    } else {
                        error = "Etiqueta de Seção de Texto já existente!";
                    }
                    break;
                case "style":
                    metaWordTag = new MetaWordTags();
                    metaWordTag.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
                    metaWordTag.setTag(request.getParameter("tag"));
                    metaWordTag.setXces("xces.style");

                    WordTags wordTag = new WordTags();
                    wordTag.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
                    wordTag.setTag(request.getParameter("tag"));
                    wordTag.setXces("xces.style");

                    if (!existWordTag(wordTag)) {
                        insertWordTag(wordTag);
                        insertMetaWordTag(metaWordTag);
                    } else {
                        error = "Etiqueta de formatação já existente!";
                    }
                    break;
            }
            if (error.isEmpty()) {
                Logger.controllerEvent(this, request, "Text Metadata Inserted");
            }

            // Ajax response
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                if (error.isEmpty()) {
                    obj.put("alert_success", "Etiqueta cadastrada com sucesso!");

                } else {
                    obj.put("alert_danger", error);
                }
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            //Success
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    (error.isEmpty() ? "Etiqueta do texto inserida com sucesso!" : null), //success
                    null, //warning
                    (error.isEmpty() ? null : error) //error
            );

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/metadata/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private static void insertMetaWordTag(MetaWordTags metaWordTag) throws DAOException {
        MetaWordTagsDAO metaWordTagsDAO = new MetaWordTagsDAO();
        try {
            metaWordTagsDAO.insert(metaWordTag);
        } catch (DAOException ex) {
            throw new DAOException(ex);
        }
    }

    private static void insertWordTag(WordTags wordTag) throws DAOException {
        try {
            WordTagsDAO.insert(wordTag);
        } catch (DAOException ex) {
            throw new DAOException(ex);
        }
    }

    private static void insertTextTag(TextTags textTag) throws DAOException, TextTagsFacadeException {
        TextTagsDAO textTagsDAO = new TextTagsDAO();
        try {
            textTagsDAO.insert(textTag);
            if (textTag.isIsRequired()) {
                TextTagsFacade.insertEmptyTagValues(textTag.getIdCorpus(), textTag);
            }
        } catch (DAOException e) {
            throw new DAOException(e);
        }
    }

    private String alertStr(List<String> textErrors) {
        String errors = "";
        for (String error : textErrors) {
            errors = errors.concat(error);
        }
        return errors;
    }

    private static boolean existTextTag(TextTags textTag) throws DAOException {
        TextTagsDAO textTagsDAO = new TextTagsDAO();
        return textTagsDAO.existTag(textTag);
    }

    private static boolean existMetaWordTag(MetaWordTags metaWordTag) throws DAOException {
        MetaWordTagsDAO metaWordTagsDAO = new MetaWordTagsDAO();
        return metaWordTagsDAO.find(metaWordTag) != null;
    }

    private static boolean existWordTag(WordTags wordTag) throws DAOException {
        WordTagsDAO wordTagsDAO = new WordTagsDAO();
        return wordTagsDAO.find(wordTag) != null;
    }
}
