/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextDAO;
import br.project.dao.TextImagesDAO;
import br.project.entity.Corpus;
import br.project.entity.Task;
import br.project.entity.Text;
import br.project.entity.TextImages;
import br.project.entity.User;
import br.project.task.DeleteTextTask;
import br.project.task.TaskManagement;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 * TextDeleteController
 *
 * <pre>
 * pages:
 * text/edit-text.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextDeleteController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/delete",
            "/text/remove"
        }
)
public class TextDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                text = TextDAO.selectById(Integer.parseInt(request.getParameter("id_text")));
                if (text == null) {//Text invalido
                    throw new Exception("Texto não existe");
                }
            }
            TextImages textImages = TextImagesDAO.selectByIdText(text.getId());
            
            //Screen to update text
            text.setCorpus(corpus);
            request.setAttribute("text", text);
            request.setAttribute("text_image", textImages);

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "delete", //page_menu_tools_sub
                    "Remover Texto", //page_title
                    "text/edit-text.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Delete text
            Text text = new Text();
            text.setId(Integer.parseInt(request.getParameter("id")));

            TextDAO.delete(text);
            
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("userLoged");
            
            TaskManagement tm = TaskManagement.getInstance();
            tm.addTask(new DeleteTextTask(text), Task.TYPE_DELETE_TEXT, user);

            Logger.controllerEvent(this, request, "Text Deleted [TextDeleteTask]");

            // Ajax response
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                obj.put("alert_success", "Texto(s) na fila de remoção");
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            //Success
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Texto(s) na fila de remoção", //success
                    null, //warning
                    null //error
            );

        } catch (Exception e) {
            Logger.controllerError(this, request, e);

            // Ajax response
            if (request.getParameter("ajax") != null) {
                JSONObject obj = new JSONObject();
                obj.put("alert_danger", e.getMessage());
                response.setContentType("application/json");
                response.getWriter().write(obj.toString());
                return;
            }

            // Error
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
