/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.MetaWordsDAO;
import br.project.dao.TextDAO;
import br.project.dao.WordDAO;
import br.project.entity.Corpus;
import br.project.entity.MetaWords;
import br.project.entity.Text;
import br.project.entity.Words;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextViewController
 *
 * <pre>
 * pages:
 * text/view-text.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextViewController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/view",
            "/text/show"
        }
)
public class TextViewController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                text = TextDAO.selectById(Integer.parseInt(request.getParameter("id_text")));
                if (text == null) {//Text invalido
                    throw new Exception("Texto não existe");
                }
            }

            //Screen to view the text
            List<Words> text_words = this.pagination(request, text);

            request.setAttribute("text_words", text_words);
            request.setAttribute("text", text);
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("metawords_class_name", MetaWords.class);

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "view", //page_menu_tools_sub
                    "Visualizar texto", //page_title
                    "text/view-text.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private List<Words> pagination(HttpServletRequest request, Text text) {
        // Paginação
        int start = 0;
        int range = 5000;
        int page = 1;

        int highlight_page = highlightWord(request);
        if (request.getParameter("page") != null || highlight_page > 0) {
            try {
                //priority for the specified page
                if (request.getParameter("page") != null){
                    page = Integer.parseInt(request.getParameter("page"));
                } else if (highlight_page > 0){
                    page = highlight_page;
                } 
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }

        int total_tokens = WordDAO.countByIdText(text.getId());
        int size = (int) Math.ceil((double) total_tokens / (double) range);

        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total_tokens", total_tokens);
        request.setAttribute("pagination_range", range);

        //to show metawords into the text
        List<Words> text_words = WordDAO.selectByIdText(text.getId(), start, range);
        List<MetaWords> text_metawords = MetaWordsDAO.selectAllByIdText(text.getId());
        for (int i = 1, j = 0; i < text_words.size() && j < text_metawords.size(); i++){
            if (text_metawords.get(j).getPosition() < text_words.get(i).getPosition() &&
                    text_metawords.get(j).getPosition() > (text_words.get(i).getPosition() - range)){
                text_words.add(i-1, text_metawords.get(j));
                j++;
            }
        }
        request.setAttribute("pagination_total_metatokens", text_metawords.size());
        return text_words;
    }

    private int highlightWord(HttpServletRequest request) {
        if (request.getParameter("idWord") != null) {
            Words w = WordDAO.selectById(Integer.parseInt(request.getParameter("idWord")));
            if (w != null) {
                try {
                    int page = (int) Math.ceil((double) w.getPosition() / (double) 5000);
                    request.setAttribute("highlight", w);
                    return page;
                } catch (NumberFormatException e) {
                    //
                }
            }
        }
        return 0;
    }
}
