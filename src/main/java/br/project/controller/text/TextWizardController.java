/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.controller.text;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.entity.Corpus;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextWizardController
 *
 * <pre>
 * pages:
 * text/wizard.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextWizardController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/wizard"
        }
)
public class TextWizardController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("name_corpus", corpus.getName());
            request.setAttribute("annotation", request.getParameter("annotation"));

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "wizard", //page_menu_tools_sub
                    "Inserir Texto - Assistente", //page_title
                    "text/wizard.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
