/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text.image;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextDAO;
import br.project.dao.TextImagesDAO;
import br.project.entity.Text;
import br.project.entity.TextImages;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * TextImageSelectController
 *
 *
 * @author Thiago Vieira
 * @since 06/11/2014
 *
 */
@WebServlet(
        name = "TextImageSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/image/select",
            "/text/image/list"
        }
)
public class TextImageSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            
            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                //Text invalido
                throw new Exception("Texto Inválido");
            }

            //Carregar imagem do texto
            TextImages textImage = TextImagesDAO.selectByIdText(text.getId());
            if (textImage == null) {
                //TextImages não existe
                throw new Exception("Não há imagens");
            }

            // Ajax response
            JSONObject obj = new JSONObject();
            obj.put("text_image_id", textImage.getId());
            obj.put("text_image_file_name", textImage.getFileName());
            obj.put("text_image_comment", textImage.getComment());
            response.setContentType("application/json");
            response.getWriter().write(obj.toString());
            
        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            JSONObject obj = new JSONObject();
            obj.put("alert_danger", e.getMessage());
            response.setContentType("application/json");
            response.getWriter().write(obj.toString());
        }
    }

}
