/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text;

import br.library.multipart.MultipartMap;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.MetaWordTagsDAO;
import br.project.dao.TextTagsDAO;
import br.project.dao.WordTagsDAO;
import br.project.entity.Corpus;
import br.project.entity.MetaWordTags;
import br.project.entity.Task;
import br.project.entity.Text;
import br.project.entity.TextTagValues;
import br.project.entity.TextTags;
import br.project.entity.User;
import br.project.entity.WordTags;
import br.project.exception.DAOException;
import br.project.exception.InvalidCorpusException;
import br.project.exception.TextFacadeException;
import br.project.exception.TextTagsFacadeException;
import br.project.facade.TextFacade;
import br.project.facade.TextTagsFacade;
import br.project.task.InsertTextTask;
import br.project.task.TaskManagement;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * TextInsertController
 *
 * <pre>
 * pages:
 * text/edit-text.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextInsertController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/insert",
            "/text/create"
        }
)
@MultipartConfig(location = "") // 10MB.
public class TextInsertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            Text text = new Text();
            text.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            text.setTypeAnnotation(request.getParameter("annotation") == null ? "" : request.getParameter("annotation"));
            text.setFile(request.getParameter("file"));
            text.setLanguage(request.getParameter("language") == null ? "" : request.getParameter("language"));

            String type = request.getParameter("type");
            if (type.compareTo("create") == 0) { //Screen to create text sending a file
                //request.setAttribute("error", error);
                request.setAttribute("text", text);
                request.setAttribute("page", "text/edit-text.jsp");
            } else if (type.compareTo("create-manual") == 0) { //Screen to create text by typing
                request.setAttribute("page", "text/edit-text.jsp");
                request.setAttribute("manual", "true");
            }

            if (text.getTypeAnnotation().isEmpty()) {
                TextTagsFacade.fillTextTagsWithoutValue(text);
            }

            text.setCorpus(corpus);
            request.setAttribute("text", text);

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "insert", //page_menu_tools_sub
                    "Inserir Texto", //page_title
                    null); //page

            template.forward();

        } catch (DAOException | InvalidCorpusException | TextTagsFacadeException | IOException | NumberFormatException | URISyntaxException | ServletException e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create text
            MultipartMap map = new MultipartMap(request);

            Text text = new Text();
            text.setIdCorpus(Integer.parseInt(request.getParameter("id_corpus")));
            List<TextTags> lstTextTags = TextTagsDAO.selectByIdCorpus(text.getIdCorpus());
            List<WordTags> lstWordTag = WordTagsDAO.selectAllByIdCorpus(text.getIdCorpus());
            List<MetaWordTags> lstMetaWordTag = MetaWordTagsDAO.selectByIdCorpus(text.getIdCorpus());
            text.setLanguage(map.getParameter("language"));
            text.setTypeAnnotation(map.getParameter("annotation") == null ? "" : map.getParameter("annotation"));
            text.setFile("automatic");
            text.setTextTags(lstTextTags);
            text.setWordTags(lstWordTag);
            text.setMetaWordTags(lstMetaWordTag);

            if (map.getFile("path") != null && (map.getFile("path").getPath().toLowerCase().endsWith(".zip")
                    || map.getFile("path").getPath().toLowerCase().endsWith(".doc") || map.getFile("path").getPath().toLowerCase().endsWith(".docx"))
                    || map.getFile("path").getPath().toLowerCase().endsWith(".pdf") || map.getFile("path").getPath().toLowerCase().endsWith(".txt")) {
                text.setPath(map.getFile("path").getPath());
                text.setTitle(text.getPath().substring(text.getPath().lastIndexOf("/") + 1, text.getPath().lastIndexOf(".")).replaceAll("_\\d+", ""));
            } else {    // Extensão de arquivo inválida
                //Error
                template.redirect(
                        this, //servlet
                        "/text/select", //path
                        request.getParameter("id_corpus"), //id_corpus
                        null, //success
                        null, //warning
                        "Extensão de arquivo inválida. São aceitos apenas arquivos .zip, .doc, .docx, .txt e .pdf" //error
                );

            }

            if (map.getParameter("text") != null && request.getParameter("text").isEmpty()) {
                throw new RuntimeException("É necessário informar o texto.");
            } else {
                text.setTextString(map.getParameter("text"));
            }

            if (text.getTypeAnnotation().isEmpty()) {
                for (TextTagValues textMetadataValues : text.getTextTagsValues()) { //Texts metadata
                    textMetadataValues.setValue(request.getParameter(textMetadataValues.getTextTag().getTag()));
                }
            }

            if (text.getPath().endsWith(".zip")) {
                HttpSession session = request.getSession();
                User user = (User) session.getAttribute("userLoged");

                File[] files = TextFacade.returnFilesFromZip(text);

                List<File> textFiles = returnTextDocFiles(files);
                List<File> mediaFiles = returnMediaFiles(files);

                TaskManagement tm = TaskManagement.getInstance();

                for (File file : textFiles) {
                    if (file.getPath().endsWith(".doc") || file.getPath().endsWith(".docx")
                            || file.getPath().endsWith(".pdf") || file.getPath().endsWith(".doc")) {
                        Text text2 = new Text();
                        text2.setIdCorpus(text.getIdCorpus());
                        text2.setTypeAnnotation(text.getTypeAnnotation());
                        text2.setLanguage(text.getLanguage());
                        text2.setTextTags(text.getTextTags());
                        text2.setWordTags(text.getWordTags());
                        text2.setMetaWordTags(text.getMetaWordTags());
                        text2.setFile(text.getFile());

                        text2.setPath(file.getPath());
                        text2.setTitle(file.getPath().substring(file.getPath().lastIndexOf("/") + 1, file.getPath().lastIndexOf(".")));

                        for (File f : mediaFiles) {
                            if (f.getAbsolutePath().substring(f.getPath().lastIndexOf("/") + 1, f.getPath().lastIndexOf(".")).contains(text2.getTitle())) {
                                if (f.getPath().endsWith(".jpg") || f.getPath().endsWith(".png")
                                        || f.getPath().endsWith(".bmp") || f.getPath().endsWith(".gif")) {
                                    text2.setImagePath(f.getPath());
                                }
                                if (f.getPath().endsWith(".mp3") || f.getPath().endsWith(".wav")) {
                                    text2.setAudioPath(f.getPath());
                                }
                            }
                        }
                        tm.addTask(new InsertTextTask(text2), Task.TYPE_INSERT_TEXT, user);
                        Logger.controllerEvent(this, request, "Text Inserted - " + text2.getTitle() + " - [InsertTextTask]");
                    }
                }
                //Success
                template.redirect(
                        this, //servlet
                        "/text/select", //path
                        request.getParameter("id_corpus"), //id_corpus
                        "Texto(s) na fila de inserção", //success
                        null, //warning
                        null //error
                );
            } else {
                if (map.getFile("text_image") != null) {
                    if (map.getFile("text_image").getPath().toLowerCase().endsWith(".jpg")
                            || map.getFile("text_image").getPath().toLowerCase().endsWith(".png")
                            || map.getFile("text_image").getPath().toLowerCase().endsWith(".bmp")
                            || map.getFile("text_image").getPath().toLowerCase().endsWith(".gif")) {
                        text.setImagePath(map.getFile("text_image").getPath());
                    } else {
                        template.redirect(
                                this, //servlet
                                "/text/select", //path
                                request.getParameter("id_corpus"), //id_corpus
                                null, //success
                                null, //warning
                                "Arquivo de capa: Extensão inválida. São aceitos apenas arquivos .jpg e .png" //error
                        );
                    }
                }

                if (map.getFile("text_audio") != null) {
                    if (map.getFile("text_audio").getPath().toLowerCase().endsWith(".mp3")
                            || map.getFile("text_audio").getPath().toLowerCase().endsWith(".wav")) {
                        text.setAudioPath(map.getFile("text_audio").getPath());
                    } else {
                        template.redirect(
                                this, //servlet
                                "/text/select", //path
                                request.getParameter("id_corpus"), //id_corpus
                                null, //success
                                null, //warning
                                "Arquivo de áudio: Extensão inválida. São aceitos apenas arquivos .mp3 e .wav" //error
                        );
                    }
                }

                TextFacade textFacade = new TextFacade();
                List<String> textWarnings = textFacade.insertText(text);

                Logger.controllerEvent(this, request, "Text Inserted");

                //request.setAttribute("text_metadata_problems", textFacade.getTextMetadataProblems());
                //request.setAttribute("files_not_extracted", textFacade.getFilesNotExtracted());
                //Success
                template.redirect(
                        this, //servlet
                        "/text/select", //path
                        request.getParameter("id_corpus"), //id_corpus
                        "Texto(s) inserido(s) com sucesso!", //success
                        alertStr(textWarnings), //warning
                        "" //error
                );
            }
        } catch (TextFacadeException | IOException | RuntimeException | ServletException | URISyntaxException | DAOException e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private String alertStr(List<String> textErrors) {
        String errors = "";
        for (String error : textErrors) {
            errors = errors.concat(error);
        }
        return errors;
    }

    private List<File> returnTextDocFiles(File[] files) {
        List<File> textDocFiles = new ArrayList<>();
        for (File f : files) {
            if (f.getAbsolutePath().toLowerCase().endsWith(".doc")
                    || f.getAbsolutePath().toLowerCase().endsWith(".docx")
                    || f.getAbsolutePath().toLowerCase().endsWith(".pdf")
                    || f.getAbsolutePath().toLowerCase().endsWith(".txt")) {
                textDocFiles.add(f);
            }
        }
        return textDocFiles;
    }

    private List<File> returnMediaFiles(File[] files) {
        List<File> mediaFiles = new ArrayList<>();
        for (File f : files) {
            if (f.getAbsolutePath().toLowerCase().endsWith(".wav")
                    || f.getAbsolutePath().toLowerCase().endsWith(".mp3")
                    || f.getAbsolutePath().toLowerCase().endsWith(".jpg")
                    || f.getAbsolutePath().toLowerCase().endsWith(".png")
                    || f.getAbsolutePath().toLowerCase().endsWith(".gif")
                    || f.getAbsolutePath().toLowerCase().endsWith(".bmp")) {
                mediaFiles.add(f);
            }
        }
        return mediaFiles;
    }

    private List<File> returnInvalidExtFiles(File[] files) {
        List<File> invalidFiles = new ArrayList<>();
        for (File f : files) {
            if (!f.getAbsolutePath().toLowerCase().matches("([^\\\\s]+(\\\\.(?i)(doc|docx|pdf|txt|wav|mp3|jpg|png|gif|bmp))$)")) {
                invalidFiles.add(f);
            }
        }
        return invalidFiles;
    }
}
