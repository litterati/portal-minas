/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.MetaWordsDAO;
import br.project.dao.SentenceDAO;
import br.project.dao.TextDAO;
import br.project.dao.TextImagesDAO;
import br.project.dao.WordDAO;
import br.project.entity.Corpus;
import br.project.entity.Sentence;
import br.project.entity.Text;
import br.project.entity.TextImages;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextInfoController
 *
 * <pre>
 * pages:
 * text/info.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextInfoController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/info"
        }
)
public class TextInfoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                text = TextDAO.selectById(Integer.parseInt(request.getParameter("id_text")));
                if (text == null) {//Text invalido
                    throw new Exception("Texto não existe");
                }
            }

            //Carregar imagem do texto
            TextImages textImages = TextImagesDAO.selectByIdText(text.getId());
            if (textImages != null){
                request.setAttribute("textImage", textImages);
            }
            
            //Total de tokens
            int total_tokens = WordDAO.countByIdText(text.getId());
            int total_meta_tokens = MetaWordsDAO.countByIdText(text.getId());
            
            //Total de sentenças
            int total_text_sentences = SentenceDAO.countByIdText(text.getId(), Sentence.TYPE_TEXT_SENTENCE);
            int total_meta_sentences = SentenceDAO.countByIdText(text.getId(), Sentence.TYPE_META_SENTENCE);

            request.setAttribute("text", text);
            request.setAttribute("corpus", corpus);
            request.setAttribute("total_tokens", total_tokens);
            request.setAttribute("total_meta_tokens", total_meta_tokens);
            request.setAttribute("total_text_sentences", total_text_sentences);
            request.setAttribute("total_meta_sentences", total_meta_sentences);

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "info", //page_menu_tools_sub
                    "Informações do texto", //page_title
                    "text/info.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

}
