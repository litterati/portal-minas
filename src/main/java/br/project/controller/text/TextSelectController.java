/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text;

import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.dao.TextDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.exception.TextTagsFacadeException;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextSelectController
 *
 * <pre>
 * pages:
 * text/list-texts.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextSelectController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/select",
            "/text/list"
        }
)
public class TextSelectController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Mostra as mensagens de sucesso, alerta e erro
            template.getAlerts();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.TEXT);
            
            List<Text> listTexts = this.pagination(request, corpus);

            request.setAttribute("texts", listTexts);
            request.setAttribute("id_corpus", corpus.getId());

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "select", //page_menu_tools_sub
                    "Lista de Textos", //page_title
                    "text/list-texts.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    private List<Text> pagination(HttpServletRequest request, Corpus corpus) throws TextTagsFacadeException {
        List<Text> texts;

        // Paginação
        int start = 0;
        int range = 30;
        int page = 1;

        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
                start = (range * page) - range;
                if (start < 0) {
                    start = 0;
                    page = 1;
                }
            } catch (NumberFormatException e) {
                // Em caso de erro, não parar a execução, exibir a primeira página
            }
        }

        int total_tokens = TextDAO.countByIdCorpus(corpus.getId());
        int size = (int) Math.ceil((double) total_tokens / (double) range);

        request.setAttribute("pagination_current_page", page);
        request.setAttribute("pagination_size", size);
        request.setAttribute("pagination_total", total_tokens);
        request.setAttribute("pagination_range", range);

        String order = request.getParameter("order");
        if (order != null && order.equals(TextDAO.LANGUAGE)) {
            order = TextDAO.LANGUAGE;
        } else {
            order = TextDAO.TITLE;
        }
        request.setAttribute("order", order);

        texts = TextDAO.selectByIdCorpusWithTagValues(corpus.getId(), range, start, order);
        //for empty tags
        List<Text> texts_aux = TextDAO.selectByIdCorpus(corpus.getId(), range, start, order);
        if (texts_aux.size() > 0){
            for (Text text : texts_aux) {
                if (!texts.contains(text)){//if the texts not contains then add
                    texts.add(text);
                }
            }
        }
        return texts;
    }
}
