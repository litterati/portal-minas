/*
 
 Um controlador (controller) pode enviar comandos para sua visão associada para 
 alterar a apresentação da visão do modelo (por exemplo, percorrendo um documento). 
 Ele também pode enviar comandos para o modelo para atualizar o estado do modelo 
 (por exemplo, editando um documento).
 (Wikipédia, http://pt.wikipedia.org/wiki/MVC)

 */
package br.project.controller.text;

import br.library.multipart.MultipartMap;
import br.library.util.Logger;
import br.library.util.template.TemplateAdmin;
import br.project.dao.TextDAO;
import br.project.dao.TextImagesDAO;
import br.project.entity.Corpus;
import br.project.entity.Text;
import br.project.entity.TextImages;
import br.project.entity.TextTagValues;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TextUpdateController
 *
 * <pre>
 * pages:
 * text/edit-text.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 11/08/2014
 *
 */
@WebServlet(
        name = "TextUpdateController",
        description = "Portal Min@s",
        urlPatterns = {
            "/text/update",
            "/text/edit"
        }
)
public class TextUpdateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();

            // Validar o texto
            Text text = TextDAO.selectByIdWithTextTagValues(Integer.parseInt(request.getParameter("id_text")));
            if (text == null) {
                text = TextDAO.selectById(Integer.parseInt(request.getParameter("id_text")));
                if (text == null) {//Text invalido
                    throw new Exception("Texto não existe");
                }
            }
            TextImages textImages = TextImagesDAO.selectByIdText(text.getId());

            //Screen to update text
            text.setCorpus(corpus);
            request.setAttribute("text", text);
            request.setAttribute("text_image", textImages);

            //Page viewer
            template.pageViewer(
                    "menu/text.jsp", //page_menu
                    "breadcrumb/text.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "text", //page_menu_tools
                    "update", //page_menu_tools_sub
                    "Alterar o Texto", //page_title
                    "text/edit-text.jsp"); //page

            template.forward();

        } catch (Exception e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Create text
            MultipartMap map = new MultipartMap(request);
            //Update text
            Text text = new Text();
            text.setId(Integer.parseInt(map.getParameter("id")));
            text.setLanguage(map.getParameter("language"));
            text.setTitle(map.getParameter("title"));

            TextImages textImages = TextImagesDAO.selectByIdText(text.getId());
            if (map.getFile("text_image") != null) {
                textImages.setFileName(map.getFile("text_image").getPath());
            }
            textImages.setComment(map.getParameter("image_comment"));
            TextImagesDAO.update(textImages);

            for (TextTagValues textMetadataValues : text.getTextTagsValues()) {
                textMetadataValues.setValue(map.getParameter(textMetadataValues.getTextTag().getTag()));
                textMetadataValues.setIdText(text.getId());
            }

            //TextDAO textDAO = new TextDAO();
            //TextFacade.update(text, textDAO);
            Logger.controllerEvent(this, request, "Text Updated");

            //Success
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    "Texto atualizado com sucesso!", //success
                    null, //warning
                    null //error
            );

        } catch (IOException | NumberFormatException e) {
            // Error
            Logger.controllerError(this, request, e);
            template.redirect(
                    this, //servlet
                    "/text/select", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    e.getMessage() //error
            );
            throw new RuntimeException("Erro em TextUpdateController.doPost(). " + e.getMessage());

        }
    }

}
