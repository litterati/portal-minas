/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.controller.ngrama;

import br.library.util.template.TemplateAdmin;
import br.project.entity.Corpus;
import br.project.entity.Ngram;
import br.project.entity.NgramResults;
import br.project.exception.DAOException;
import br.project.exception.InvalidCorpusException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author marcel
 */
@WebServlet(name = "ngramsearch", urlPatterns = {"/ngramsearch"})
public class NgramSearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ngramsearch</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ngramsearch at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            
        TemplateAdmin template = new TemplateAdmin(request, response);
        
         Corpus corpus = template.isCorpusValid();
        //Verifica se o usuário tem permissão de usar o corpus/ferramenta
        template.hasUserPermission();
        //Page viewer
            
        HttpSession session = request.getSession(true);
        if(session.getAttribute("ngram")!=null){
            List<NgramResults> allResults = (List<NgramResults>)session.getAttribute("ngram");
            List<NgramResults> newResults = new ArrayList<>();    
            for(NgramResults ngramResult : allResults){
                NgramResults results = new NgramResults();
                int count = 0; 
                List<Ngram> ngrams = new ArrayList<>();
                results.setLanguage(ngramResult.getLanguage());
                results.setPage(1);
                results.setStatistic(ngramResult.getStatistic());
                results.setWindow(ngramResult.getWindow());
                for(Ngram ngram : ngramResult.getResults()){
                    String[] terms = ngram.getNgram().split(" ");
                                      
                    if (terms[0].equalsIgnoreCase(request.getParameter("searchterm"))){
                        count++;
                        ngrams.add(ngram);
                    }
                }
                results.setResults(ngrams);
                results.setTotal(count);
                newResults.add(results);
            }
            request.setAttribute("allResults", newResults);
        }
        
        request.setAttribute("id_corpus", corpus.getId());

        template.pageViewer(
                null,
                "breadcrumb/ngrama.jsp", //page_menu
                corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                "corpus", //page_menu_option
                "ngrama", //page_menu_tools
                "ngrama", //page_menu_tools_sub
                "Gerador de Ngrama", //page_title
                "ngrama/result.jsp"); //page

        template.forward();
        }catch(DAOException | InvalidCorpusException | IOException | URISyntaxException | ServletException ex){
            throw new RuntimeException("Erro na busca." + ex.getMessage());
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
