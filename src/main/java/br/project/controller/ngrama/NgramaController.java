/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.controller.ngrama;

import br.library.util.Logger;
import br.library.util.ngrama.NgramaGenerator;
import br.library.util.template.TemplateAdmin;
import br.library.util.template.Tool;
import br.project.entity.Corpus;
import br.project.entity.NgramResults;
import br.project.exception.DAOException;
import br.project.exception.InvalidCorpusException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author marcel
 */
@WebServlet(
        name = "NgramaController",
        description = "Portal Min@s",
        urlPatterns = {
            "/ngrama"
        }
)
public class NgramaController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);

        try {
            //Precisa validar o corpus passado
            Corpus corpus = template.isCorpusValid();
            //Registrar acesso do usuário
            template.recordUserAccess(corpus, Tool.NGRAMA);
            
            HttpSession session = request.getSession(true);
            if((request.getParameter("newsearch"))!=null || (session.getAttribute("ngram")==null)){
            //Page viewer

            template.pageViewer(
                    null, //"menu/ngrama.jsp", //page_menu
                    "breadcrumb/ngrama.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "ngrama", //page_menu_tools
                    "ngrama", //page_menu_tools_sub
                    "Gerador de Ngrama", //page_title
                    "ngrama/ngrama.jsp"); //page
            }
            if(session.getAttribute("ngram")!=null){
                request.setAttribute("allResults", session.getAttribute("ngram"));        
                template.pageViewer(
                    null, //"menu/ngrama.jsp", //page_menu
                    "breadcrumb/ngrama.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "ngrama", //page_menu_tools
                    "ngrama", //page_menu_tools_sub
                    "Gerador de Ngrama", //page_title
                    "ngrama/result.jsp"); //page
            }

            template.forward();

        } catch (DAOException | InvalidCorpusException | IOException | URISyntaxException | ServletException ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/admin/home", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }
    }  
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        TemplateAdmin template = new TemplateAdmin(request, response);
        
        try {
            Corpus corpus = template.isCorpusValid();
            //Verifica se o usuário tem permissão de usar o corpus/ferramenta
            template.hasUserPermission();
            //Page viewer
            
            HttpSession session = request.getSession(true);
            String idUser = session.getId();
            int gramSize = Integer.parseInt(request.getParameter("gramSize"));       
            int frequency = Integer.parseInt(request.getParameter("frequency"));
            String statistic = request.getParameter("statistic");
            int window = Integer.parseInt(request.getParameter("window"));
            Double score = Double.parseDouble(request.getParameter("score"));

            NgramaGenerator ngram = new NgramaGenerator(idUser,corpus.getId(), gramSize, frequency, statistic, score);
            ngram.begin();

            List<NgramResults> allResults = ngram.allResults(window);
            session.setAttribute("ngram", allResults);
            
            request.setAttribute("id_corpus", corpus.getId());
            request.setAttribute("allResults", allResults);
            
            template.pageViewer(
                    null, //"menu/ngrama.jsp", //page_menu
                    "breadcrumb/ngrama.jsp", //page_breadcrumb
                    corpus, //page_menu_id_corpus, page_menu_name_corpus, page_menu_option_sub, page_subtitle
                    "corpus", //page_menu_option
                    "ngrama", //page_menu_tools
                    "ngrama", //page_menu_tools_sub
                    "Gerador de Ngrama", //page_title
                    "ngrama/result.jsp"); //page

            template.forward();

        } catch (DAOException | InvalidCorpusException | IOException | URISyntaxException | ServletException ex) {
            // Error
            Logger.controllerError(this, request, ex);
            template.redirect(
                    this, //servlet
                    "/ngrama", //path
                    request.getParameter("id_corpus"), //id_corpus
                    null, //success
                    null, //warning
                    ex.getMessage() //error
            );
        }
    }
}
