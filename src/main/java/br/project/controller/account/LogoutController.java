package br.project.controller.account;

import br.library.util.Logger;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * LogoutController
 *
 * @author Thiago Vieira
 * @since 31/07/2014
 *
 */
@WebServlet(
        name = "LogoutController",
        description = "Portal Min@s",
        urlPatterns = {
            "/account/logout"
        }
)
public class LogoutController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Logger.controllerEvent(this, request, "Logout");
        
        //Log out the user
        HttpSession session = request.getSession();
        session.invalidate();
        response.sendRedirect(getServletContext().getContextPath() + "/login");

    }
}
