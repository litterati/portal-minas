package br.project.controller.account;

import br.library.util.Logger;
import br.library.util.template.TemplateMain;
import br.project.dao.UserDAO;
import br.project.entity.User;
import br.project.exception.DAOException;
import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * LoginController
 * 
 * <pre>
 * pages:
 * admin/login.jsp
 * </pre>
 *
 * @author Thiago Vieira
 * @since 31/07/2014
 *
 */
@WebServlet(
        name = "LoginController",
        description = "Portal Min@s",
        urlPatterns = {
            "/account/login"
        }
)
public class LoginController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        try { //User is tryng to login in the site
            User user = UserDAO.authenticate(
                    request.getParameter("login"),
                    request.getParameter("password"));

            if (user == null) {
                //Login or password wrong
                Logger.controllerEvent(this, request, "Login or password wrong");
                session.invalidate();
                response.sendRedirect(getServletContext().getContextPath() + "/login?alert_danger=" + URLEncoder.encode("Login ou senha incorreta.", "UTF-8"));
            } else {
                //Login and password ok
                session.setAttribute("userLoged", user);
                Logger.controllerEvent(this, request, "Login");
                response.sendRedirect(getServletContext().getContextPath() + "/admin/home");
            }
        } catch (DAOException | IOException e) {
            //error
            Logger.controllerError(this, request, e);
            response.sendRedirect(getServletContext().getContextPath() + "/login?alert_danger=" + URLEncoder.encode("Erro durante o login. Entre em contato comm o administrador do sistema.", "UTF-8"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String type = request.getParameter("type");
        
        TemplateMain template = new TemplateMain(request, response);
        //Mostra as mensagens de sucesso, alerta e erro
        template.getAlerts();

        if (type.compareTo("logout") == 0) { //Log out the user
            Logger.controllerEvent(this, request, "Logout");
            session.invalidate();
            response.sendRedirect(getServletContext().getContextPath() + "/login");
        } else { //User is tryng to access the home page
            if (session.getAttribute("userLoged") != null) {
                //Redirect to admin/home
                response.sendRedirect(getServletContext().getContextPath() + "/admin/home");
            } else {
                session.invalidate();
                response.sendRedirect(getServletContext().getContextPath() + "/login?alert_info=" + URLEncoder.encode("É necessário realizar login par continuar", "UTF-8"));
            }
        }
    }
}
