package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextTagValues implements Comparable<TextTagValues>, Serializable {

    private int id;
    private int idText;
    private int idTextTag;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdText() {
        return idText;
    }

    public void setIdText(int idText) {
        this.idText = idText;
    }

    public int getIdTextTag() {
        return idTextTag;
    }

    public void setIdTextTag(int idTextTag) {
        this.idTextTag = idTextTag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Text text;
    private TextTags textTag;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public TextTags getTextTag() {
        return textTag;
    }

    public void setTextTag(TextTags textTag) {
        this.textTag = textTag;
    }

    @Override
    public int compareTo(TextTagValues o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TextTagValues)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((TextTagValues) obj).getId();
    }

}
