/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Access.java
 *
 * @author Thiago Vieira
 * @since 13/01/2015
 */
public class Access implements Comparable<Access>, Serializable {

    private int idUser;
    private int idCorpus;
    private Date dateAccess;
    private String tool;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    public Date getDateAccess() {
        return dateAccess;
    }

    public void setDateAccess(Date dateAccess) {
        this.dateAccess = dateAccess;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private User user;
    private Corpus corpus;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    @Override
    public int compareTo(Access o) {
        return (this.getIdUser() < o.getIdUser()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Access)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return (this.getIdUser() == ((Access) obj).getIdUser())
                && (this.getIdCorpus() == ((Access) obj).getIdCorpus())
                && (this.getDateAccess().equals(((Access) obj).getDateAccess()));
    }

}
