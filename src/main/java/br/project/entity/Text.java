package br.project.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class Text implements Comparable<Text>, Serializable {

    private int id;
    private int idCorpus;
    private String language;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;
    private List<TextImages> textImages;
    private List<Words> words;
    private List<Sentence> sentences;
    private List<TextAlign> textAlign;
    private List<WordTags> wordTags;
    private List<MetaWordTags> metaWordTags;
    private List<String> textErrors;
    private List<String> textWarnings;
    private String imagePath;
    private String audioPath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    
    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public List<TextImages> getTextImages() {
        return textImages;
    }

    public void setTextImages(List<TextImages> textImages) {
        this.textImages = textImages;
    }

    public List<Words> getWords() {
        return words;
    }

    public void setWords(List<Words> words) {
        this.words = words;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public List<TextAlign> getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(List<TextAlign> textAlign) {
        this.textAlign = textAlign;
    }

    public List<WordTags> getWordTags() {
        return wordTags;
    }

    public void setWordTags(List<WordTags> wordTags) {
        this.wordTags = wordTags;
    }

    public List<MetaWordTags> getMetaWordTags() {
        return metaWordTags;
    }

    public void setMetaWordTags(List<MetaWordTags> metaWordTags) {
        this.metaWordTags = metaWordTags;
    }

    public List<WordTags> getMetaWordTagsList() {
        List<WordTags> list = new ArrayList<>(metaWordTags.size());
        for (MetaWordTags meta_word_tag : metaWordTags) {
            list.add((WordTags) meta_word_tag);
        }
        return list;
    }

    public void setMetaWordTagsList(List<WordTags> metaWordTags) {
        List<MetaWordTags> list = new ArrayList<>(metaWordTags.size());
        for (WordTags word_tag : metaWordTags) {
            list.add((MetaWordTags) word_tag);
        }
        this.metaWordTags = list;
    }

    public List<String> getTextErrors() {
        return textErrors;
    }

    public void setTextErrors(List<String> textErrors) {
        this.textErrors = textErrors;
    }

    public List<String> getTextWarnings() {
        return textWarnings;
    }

    public void setTextWarnings(List<String> textWarnings) {
        this.textWarnings = textWarnings;
    }

    // -------------------------------------------------------------------------
    // Auxiliar
    // -------------------------------------------------------------------------
    private String path;

    /* We can have the annotation describe to the project or XCES */
    private String typeAnnotation;

    /* List of metadata */
    private List<TextTags> textTags = new ArrayList<>();
    /* List of metadata values */
    private List<TextTagValues> textTagsValues = new ArrayList<>();

    /* The text can have two file kind: manual or automatic
     * Manual file are that one where the user digit the text by the web interface
     * Automatic file are that one the user send by upload
     */
    private String file;

    /* The full text */
    private StringBuilder textString = new StringBuilder();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFile_name() {
        return this.path.split("/")[this.path.split("/").length - 1];
    }

    public String getTypeAnnotation() {
        return typeAnnotation;
    }

    public void setTypeAnnotation(String typeAnnotation) {
        this.typeAnnotation = typeAnnotation;
    }

    public List<TextTagValues> getTextTagsValues() {
        return textTagsValues;
    }

    public void setTextTagsValues(List<TextTagValues> textTagsValues) {
        this.textTagsValues = textTagsValues;
    }

    public void addTextTagsValues(TextTagValues textTagsValues) {
        if (this.textTagsValues == null) {
            this.textTagsValues = new ArrayList<>();
        }
        this.textTagsValues.add(textTagsValues);
    }

    public List<TextTags> getTextTags() {
        return textTags;
    }

    public void setTextTags(List<TextTags> textTags) {
        this.textTags = textTags;
    }

    public void addTextTags(TextTags textTags) {
        if (this.textTags == null) {
            this.textTags = new ArrayList<>();
        }
        this.textTags.add(textTags);
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public StringBuilder getTextString() {
        return textString;
    }

    public void addTextString(String textString) {
        this.textString.append(textString);
    }

    public void setTextString(String textString) {
        this.textString.setLength(0);
        this.textString.append(textString);
    }

    @Override
    public int compareTo(Text o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Text)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Text) obj).getId();
    }

}
