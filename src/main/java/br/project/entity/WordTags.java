package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class WordTags implements Comparable<WordTags>, Serializable {

    private int id;
    private int idCorpus;
    private String tag;
    private String xces;

    public WordTags(int id, int idCorpus, String tag, String xces) {
        this.id = id;
        this.idCorpus = idCorpus;
        this.tag = tag;
        this.xces = xces;
    }

    public WordTags(String tag) {
        this.tag = tag;
    }

    public WordTags() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getXces() {
        return xces;
    }

    public void setXces(String xces) {
        this.xces = xces;
    }

    @Override
    public int compareTo(WordTags o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WordTags)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((WordTags) obj).getId();
    }

}
