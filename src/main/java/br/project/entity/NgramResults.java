/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.entity;

import java.util.List;

/**
 *
 * @author marcel
 */
public class NgramResults {

    private int total;
    private String statistic;
    private int page;
    private int window;
    private String language;
    private List<Ngram> results;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total/window;
    }

    public String getStatistic() {
        return statistic;
    }

    public void setStatistic(String statistic) {
        this.statistic = statistic;
    }
    
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getWindow() {
        return window;
    }

    public void setWindow(int window) {
        this.window = window;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<Ngram> getResults() {
        return results;
    }

    public void setResults(List<Ngram> results) {
        this.results = results;
    }
    
}
