/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author marcel
 */
public class MetaWordTags extends WordTags implements Serializable {
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MetaWordTags)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((MetaWordTags) obj).getId();
    }

}
