package br.project.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class TextTags implements Comparable<TextTags>, Serializable {

    private int id = 0;
    private int idCorpus;
    private String tag;
    private boolean isRequired;
    private int idFieldType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isIsRequired() {
        return isRequired;
    }

    public void setIsRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public int getIdFieldType() {
        return idFieldType;
    }

    public void setIdFieldType(int idFieldType) {
        this.idFieldType = idFieldType;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;
    private FieldsType fieldType;
    private List<TextTagValues> textTagsValues;

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public FieldsType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldsType fieldType) {
        this.fieldType = fieldType;
    }

    public List<TextTagValues> getTextTagsValues() {
        return textTagsValues;
    }

    public void setTextTagsValues(List<TextTagValues> textTagsValues) {
        this.textTagsValues = textTagsValues;
    }
    
    public void addTextTagsValues(TextTagValues textTagsValues) {
        if (this.textTagsValues == null){
            this.textTagsValues = new ArrayList<>();
        }
        this.textTagsValues.add(textTagsValues);
    }
    
    @Override
    public int compareTo(TextTags o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TextTags)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((TextTags) obj).getId();
    }

}
