package br.project.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class Sentence implements Comparable<Sentence>, Serializable {
    
    public static final int TYPE_TEXT_SENTENCE = 1;
    public static final int TYPE_META_SENTENCE = 2;

    private int id;
    private int idText;
    private int position;
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdText() {
        return idText;
    }

    public void setIdText(int id_text) {
        this.idText = id_text;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Text text;
    private List<SentenceAlign> sentenceAlign;
    private List<Words> words; // or metaWords

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public List<SentenceAlign> getSentenceAlign() {
        return sentenceAlign;
    }

    public void setSentenceAlign(List<SentenceAlign> sentenceAlign) {
        this.sentenceAlign = sentenceAlign;
    }

    public List<Words> getWords() {
        return words;
    }

    public void setWords(List<Words> words) {
        this.words = words;
    }

    @Override
    public int compareTo(Sentence o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Sentence)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Sentence) obj).getId();
    }

}
