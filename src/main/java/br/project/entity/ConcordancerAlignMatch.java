/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * ConcordancerAlignMatch
 *
 * @author Thiago Vieira
 * @since 10/11/2014
 */
public class ConcordancerAlignMatch {

    private Words source;
    private Map<Words, Integer> targets;
    private int total = 0;

    public Words getSource() {
        return source;
    }

    public void setSource(Words source) {
        this.source = source;
    }

    //this method was replaced to order the Map by value
    //public Map<Words, Integer> getTargets() {
    //    return targets;
    //}

    public Map<Integer, Words>  getTargets() {
        Map<Integer, Words> treeMap = new TreeMap<>(
                new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return (o1 < o2) ? 1 : -1;
                    }
                });
        //add
        for (Map.Entry<Words, Integer> entry : this.targets.entrySet()) {
            treeMap.put(entry.getValue(), entry.getKey()); 
        }
        return treeMap;
    }

    public void setTargets(Map<Words, Integer> targets) {
        this.targets = targets;
    }

    public void addTarget(Words target) {
        if (this.targets == null) {
            this.targets = new HashMap<>();
        }
        this.targets.put(target, 1);
        this.total++;
    }

    public void upTargetCounter(Words target) {
        if (this.targets != null) {
            this.targets.put(target, this.targets.get(target) + 1);
            this.total++;
        }
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int updateTotal() {
        int aux = 0;
        for (Map.Entry<Words, Integer> entry : this.targets.entrySet()) {
            aux += entry.getValue();
        }
        this.total = aux;
        return this.total;
    }

    public static boolean similar(Words w1, Words w2) {
        if (w1 != null && w2 != null) {
            //same superficial form
            if (w1.getWord() != null && w2.getWord() != null
                    && w1.getWord().toLowerCase().equals(w2.getWord().toLowerCase())) {
                //same PoS
                if ((w1.getPos() != null && w2.getPos() != null
                        && w1.getPos().equals(w2.getPos()))
                        || (w1.getPos() == null && w2.getPos() == null)) {
                    //same language
                    if (w1.getText() != null && w2.getText() != null
                            && w1.getText().getLanguage().equals(w2.getText().getLanguage())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Words getSimilarTarget(Words target) {
        for (Map.Entry<Words, Integer> entry : targets.entrySet()) {
            Words words = entry.getKey();
            if (similar(target, words)) {
                return words;
            }
        }
        return null;
    }

}
