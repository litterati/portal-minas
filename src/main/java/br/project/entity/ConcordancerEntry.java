/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * ConcordancerEntry
 *
 * @author Thiago Vieira
 * @since 10/11/2014
 */
public class ConcordancerEntry {

    private List<Words> words;
    private Words term;
    private int idMainTerm;

    public List<Words> getWords() {
        return words;
    }

    public void setWords(List<Words> words) {
        this.words = words;
    }

    public void addWords(Words word) {
        if (this.words == null) {
            this.words = new ArrayList<>();
        }
        this.words.add(word);
    }

    public Words getTerm() {
        return term;
    }

    public void setTerm(Words term) {
        this.term = term;
    }

    public int getIdMainTerm() {
        return idMainTerm;
    }

    public void setIdMainTerm(int idMainTerm) {
        this.idMainTerm = idMainTerm;
    }

}
