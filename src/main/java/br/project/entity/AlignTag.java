/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author Thiago Vieira
 * @since 22/10;/2014
 */
public class AlignTag implements Comparable<AlignTag>, Serializable {

    private int id;
    private String value;
    private int idCorpus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    @Override
    public int compareTo(AlignTag o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AlignTag)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((AlignTag) obj).getId();
    }

}
