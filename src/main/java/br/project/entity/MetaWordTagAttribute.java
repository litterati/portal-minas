/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * MetaWordTagAttributes.java
 *
 * @author Thiago Vieira
 * @since 11/07/2014
 */
public class MetaWordTagAttribute extends WordTagAttribute implements Serializable {

    /**
     * @see WordTagAttribute.getWordTag()
     */
    public WordTags getMetaWordTags() {
        return super.getWordTag();
    }

    /**
     * @see WordTagAttribute.setWordTag(WordTags)
     */
    public void setMetaWordTags(WordTags metaWordTag) {
        super.setWordTag(metaWordTag);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MetaWordTagAttribute)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((MetaWordTagAttribute) obj).getId();
    }

}
