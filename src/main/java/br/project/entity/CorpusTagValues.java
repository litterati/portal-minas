package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class CorpusTagValues implements Comparable<CorpusTagValues>, Serializable {

    private int id;
    private int idCorpus;
    private int idCorpusTag;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public int getIdCorpusTag() {
        return idCorpusTag;
    }

    public void setIdCorpusTag(int idCorpusTag) {
        this.idCorpusTag = idCorpusTag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;
    private CorpusTags corpusTags;

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public CorpusTags getCorpusTags() {
        return corpusTags;
    }

    public void setCorpusTags(CorpusTags corpusTags) {
        this.corpusTags = corpusTags;
    }
    
    @Override
    public int compareTo(CorpusTagValues o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CorpusTagValues)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((CorpusTagValues) obj).getId();
    }
    
}
