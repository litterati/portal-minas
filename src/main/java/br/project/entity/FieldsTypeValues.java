package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class FieldsTypeValues implements Comparable<FieldsTypeValues>, Serializable {

    private int id;
    private int idType;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private FieldsType fieldsType;

    public FieldsType getFieldsType() {
        return fieldsType;
    }

    public void setFieldsType(FieldsType fieldsType) {
        this.fieldsType = fieldsType;
    }
    
    @Override
    public int compareTo(FieldsTypeValues o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FieldsTypeValues)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((FieldsTypeValues) obj).getId();
    }

}
