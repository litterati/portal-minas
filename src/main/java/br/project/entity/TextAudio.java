/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * TextAudio.java
 *
 * @author Thiago Vieira
 * @since 11/07/2014
 */
public class TextAudio implements Comparable<TextAudio>, Serializable {

    private int id;
    private int idText;
    private String fileName;
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdText() {
        return idText;
    }

    public void setIdText(int id_text) {
        this.idText = id_text;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String file_name) {
        this.fileName = file_name;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Text text;

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    @Override
    public int compareTo(TextAudio o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TextAudio)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((TextAudio) obj).getId();
    }

}
