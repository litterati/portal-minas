/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * WordTagAttribute.java
 *
 * @author Thiago Vieira
 * @since 11/07/2014
 */
public class WordTagAttribute implements Comparable<WordTagAttribute>, Serializable {

    private int id;
    private int idWordTagLabel;
    private String attribute;
    private String value;

    public WordTagAttribute() {
    }
    
    public WordTagAttribute(String attribute, String value) {
        this.attribute = attribute;
        this.value = value;
    }
    
    public WordTagAttribute(int id, int idWordTagLabel, String attribute, String value) {
        this.id = id;
        this.idWordTagLabel = idWordTagLabel;
        this.attribute = attribute;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdWordTagLabel() {
        return idWordTagLabel;
    }

    public void setIdWordTagLabel(int idWordTagLabel) {
        this.idWordTagLabel = idWordTagLabel;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private WordTags wordTag;

    public WordTags getWordTag() {
        return wordTag;
    }

    public void setWordTag(WordTags wordTag) {
        this.wordTag = wordTag;
    }
    
    @Override
    public int compareTo(WordTagAttribute o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WordTagAttribute)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((WordTagAttribute) obj).getId();
    }

}
