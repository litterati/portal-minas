/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;
import java.util.List;

/**
 * MetaWords.java
 *
 * @author Thiago Vieira
 * @since 11/07/2014
 */
public class MetaWords extends Words implements Serializable {

    /**
     * @see Words.getMetaWordTags()
     */
    public List<WordTags> getMetaWordTags() {
        return super.getWordTags();
    }

    /**
     * @see Words.setWordTags(List<WordTags>)
     */
    public void setMetaWordTags(List<WordTags> metaWordTags) {
        super.setWordTags(metaWordTags);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MetaWords)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((MetaWords) obj).getId();
    }

}
