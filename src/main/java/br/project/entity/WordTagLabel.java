package br.project.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author matheus_silva, Thiago Vieira
 * @since 2014
 */
public class WordTagLabel implements Comparable<WordTagLabel>, Serializable {

    private int id;
    private int idWordOpen;
    private int idWordClose;
    private int idWordTag;
    private int idText;

    public WordTagLabel() {
        this.idWordOpen = -1;
        this.idWordClose = -1;
    }

    public WordTagLabel(WordTags wordTag) {
        this.wordTag = wordTag;
    }

    public WordTagLabel(int idWordOpen, int idWordClose, int idText) {
        this.idWordOpen = idWordOpen;
        this.idWordClose = idWordClose;
        this.idText = idText;
        this.attributes = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdWordTag() {
        return idWordTag;
    }

    public void setIdWordTag(int idWordTag) {
        this.idWordTag = idWordTag;
    }

    public int getIdWordOpen() {
        return idWordOpen;
    }

    public void setIdWordOpen(int idWordOpen) {
        this.idWordOpen = idWordOpen;
    }

    public int getIdWordClose() {
        return idWordClose;
    }

    public void setIdWordClose(int idWordClose) {
        this.idWordClose = idWordClose;
    }

    public int getIdText() {
        return idText;
    }

    public void setIdText(int idText) {
        this.idText = idText;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private List<WordTagAttribute> attributes;
    private WordTags wordTag;

    public List<WordTagAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<WordTagAttribute> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(WordTagAttribute attribute) {
        this.attributes.add(attribute);
    }

    public WordTags getWordTag() {
        return wordTag;
    }

    public void setWordTag(WordTags wordTag) {
        this.wordTag = wordTag;
    }

    @Override
    public int compareTo(WordTagLabel o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WordTagLabel)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((WordTagLabel) obj).getId();
    }

}
