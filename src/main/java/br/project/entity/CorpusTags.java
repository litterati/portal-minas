package br.project.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class CorpusTags implements Comparable<CorpusTags>, Serializable {

    private int id;
    private int idCorpus;
    private String tag;
    private boolean isRequired;
    private int idFieldType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isIsRequired() {
        return isRequired;
    }

    public void setIsRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public int getIdFieldType() {
        return idFieldType;
    }

    public void setIdFieldType(int idFieldType) {
        this.idFieldType = idFieldType;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;
    private FieldsType fieldsType;
    private List<CorpusTagValues> corpusTagValues;

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public FieldsType getFieldsType() {
        return fieldsType;
    }

    public void setFieldsType(FieldsType fieldsType) {
        this.fieldsType = fieldsType;
    }

    public List<CorpusTagValues> getCorpusTagValues() {
        return corpusTagValues;
    }

    public void setCorpusTagValues(List<CorpusTagValues> corpusTagValues) {
        this.corpusTagValues = corpusTagValues;
    }
    
    public void addCorpusTagValues(CorpusTagValues corpusTagValues) {
        if (this.corpusTagValues == null){
            this.corpusTagValues = new ArrayList<>();
        }
        this.corpusTagValues.add(corpusTagValues);
    }
    
    @Override
    public int compareTo(CorpusTags o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CorpusTags)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((CorpusTags) obj).getId();
    }

}
