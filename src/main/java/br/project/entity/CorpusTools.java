/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * CorpusTools.java
 *
 * @author Thiago Vieira
 * @since 11/07/2014
 */
public class CorpusTools implements Comparable<CorpusTools>, Serializable {

    private int id;
    private int idCorpus;
    private String tool;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }
    
    @Override
    public int compareTo(CorpusTools o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CorpusTools)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((CorpusTools) obj).getId();
    }

}
