/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 *
 * WordsAlign.java
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 */
public class WordsAlign implements Comparable<WordsAlign>, Serializable {

    private int idSource;
    private int idTarget;
    private String type;
    private int idAlignTag;

    public int getIdSource() {
        return idSource;
    }

    public void setIdSource(int idSource) {
        this.idSource = idSource;
    }

    public int getIdTarget() {
        return idTarget;
    }

    public void setIdTarget(int idTarget) {
        this.idTarget = idTarget;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIdAlignTag() {
        return idAlignTag;
    }

    public void setIdAlignTag(int idAlignTag) {
        this.idAlignTag = idAlignTag;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Words source;
    private Words target;

    public Words getSource() {
        return source;
    }

    public void setSource(Words source) {
        this.source = source;
    }

    public Words getTarget() {
        return target;
    }

    public void setTarget(Words target) {
        this.target = target;
    }

    @Override
    public int compareTo(WordsAlign o) {
        return (this.getIdSource() < o.getIdSource()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WordsAlign)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return (this.getIdSource() == ((WordsAlign) obj).getIdSource())
                && (this.getIdTarget() == ((WordsAlign) obj).getIdTarget());
    }

}
