/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.project.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marcel
 */
public class MetaWordTagLabel extends WordTagLabel implements Serializable {

    /**
     * @see WordTagLabel.getIdWordTag()
     */
    public int getIdMetaWordTag() {
        return super.getIdWordTag();
    }

    /**
     * @see WordTagLabel.setIdWordTag(int)
     */
    public void setIdMetaWordTag(int idMetaWordTag) {
        super.setIdWordTag(idMetaWordTag);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MetaWordTagLabel)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((MetaWordTagLabel) obj).getId();
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private List<MetaWordTagAttribute> attributes;
    private MetaWordTags wordTag;

    public List<MetaWordTagAttribute> getMetaAttributes() {
        return attributes;
    }
    
    public void setMetaAttributes(List<MetaWordTagAttribute> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(MetaWordTagAttribute attribute) {
        this.attributes.add(attribute);
    }

}
