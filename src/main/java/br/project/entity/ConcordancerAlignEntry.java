/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * ConcordancerAlignEntry
 *
 * @author Thiago Vieira
 * @since 10/11/2014
 */
public class ConcordancerAlignEntry {

    private List<Words> sourceList;
    private List<Words> targetList;
    private String alignGroup;
    private Words sourceTerm;
    private Words targetTerm;

    public List<Words> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<Words> sourceList) {
        this.sourceList = sourceList;
    }

    public void addSourceList(Words source) {
        if (this.sourceList == null) {
            this.sourceList = new ArrayList<>();
        }
        this.sourceList.add(source);
    }

    public List<Words> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<Words> targetList) {
        this.targetList = targetList;
    }

    public void addTargetList(Words target) {
        if (this.targetList == null) {
            this.targetList = new ArrayList<>();
        }
        this.targetList.add(target);
    }

    public String getAlignGroup() {
        return alignGroup;
    }

    public void setAlignGroup(String alignGroup) {
        this.alignGroup = alignGroup;
    }

    public Words getSourceTerm() {
        return sourceTerm;
    }

    public void setSourceTerm(Words sourceTerm) {
        this.sourceTerm = sourceTerm;
    }

    public Words getTargetTerm() {
        return targetTerm;
    }

    public void setTargetTerm(Words targetTerm) {
        this.targetTerm = targetTerm;
    }

}
