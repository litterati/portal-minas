package br.project.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class FieldsType implements Comparable<FieldsType>, Serializable {

    private int id;
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private List<FieldsTypeValues> fieldsTypeValues;
    private List<CorpusTags> corpusTags;

    public List<FieldsTypeValues> getFieldsTypeValues() {
        return fieldsTypeValues;
    }

    public void setFieldsTypeValues(List<FieldsTypeValues> fieldsTypeValues) {
        this.fieldsTypeValues = fieldsTypeValues;
    }

    public List<CorpusTags> getCorpusTags() {
        return corpusTags;
    }

    public void setCorpusTags(List<CorpusTags> corpusTags) {
        this.corpusTags = corpusTags;
    }
    
    @Override
    public int compareTo(FieldsType o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FieldsType)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((FieldsType) obj).getId();
    }

}
