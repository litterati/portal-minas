/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import br.project.task.TaskInterface;
import java.io.Serializable;
import java.util.Date;

/**
 * Task
 *
 * @author Thiago Vieira
 * @since 28/09/2014
 */
public class Task implements Comparable<Task>, Serializable {

    private int id;
    private int idUser;
    private int type;// InsertTextTask, DeleteTextTask, AlignTask...
    private int progress;// 0 to 100
    private int status;// waiting, running, completed_success, completed_error
    private String response;// response from the method (success_msg, error_msg)
    private Date dateInserted;// inserted in the queue
    private Date dateStarted;//when the job was started
    private Date dateFinished;//when the job finished
    private TaskInterface javaObject;//the task (TaskInterface)

    public static final int STATUS_WAITING = 1;
    public static final int STATUS_RUNNING = 2;
    public static final int STATUS_COMPLETED_SUCCESS = 3;
    public static final int STATUS_COMPLETED_ERROR = 4;
    public static final int STATUS_ABORTED = 5;

    public static final int TYPE_INSERT_TEXT = 1;
    public static final int TYPE_DELETE_TEXT = 2;
    public static final int TYPE_ALIGNER = 3;
    public static final int TYPE_TAGGER = 4;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Date dateStarted) {
        this.dateStarted = dateStarted;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public TaskInterface getJavaObject() {
        return javaObject;
    }

    public void setJavaObject(TaskInterface javaObject) {
        this.javaObject = javaObject;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int compareTo(Task o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Task)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Task) obj).getId();
    }

}
