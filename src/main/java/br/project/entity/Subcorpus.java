/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * Subcorpus.java
 *
 * @author Thiago Vieira
 * @since 16/01/2015
 */
public class Subcorpus implements Comparable<Subcorpus>, Serializable {

    private int id;
    private int idCorpus;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;
    
    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    @Override
    public int compareTo(Subcorpus o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Subcorpus)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return (this.getId() == ((Subcorpus) obj).getId());
    }

}
