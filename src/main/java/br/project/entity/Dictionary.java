/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.project.entity;

import java.io.Serializable;

/**
 *
 * @author marcel
 */
public class Dictionary implements Comparable<Dictionary>, Serializable {
    private int id;
    private String word;
    private String lemma;
    private String pos;
    private String flex;
    private int lang;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getFlex() {
        return flex;
    }

    public void setFlex(String flex) {
        this.flex = flex;
    }

    public int getLang() {
        return lang;
    }

    public void setLang(int lang) {
        this.lang = lang;
    }
    
    @Override
    public int compareTo(Dictionary o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Dictionary)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Dictionary) obj).getId();
    }
    
}
