/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * SubcorpusText.java
 *
 * @author Thiago Vieira
 * @since 16/01/2015
 */
public class SubcorpusText implements Comparable<SubcorpusText>, Serializable {

    private int idSubcorpus;
    private int idText;

    public int getIdSubcorpus() {
        return idSubcorpus;
    }

    public void setIdSubcorpus(int idSubcorpus) {
        this.idSubcorpus = idSubcorpus;
    }

    public int getIdText() {
        return idText;
    }

    public void setIdText(int idText) {
        this.idText = idText;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Subcorpus subcorpus;
    private Text text;

    public Subcorpus getSubcorpus() {
        return subcorpus;
    }

    public void setSubcorpus(Subcorpus subcorpus) {
        this.subcorpus = subcorpus;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    @Override
    public int compareTo(SubcorpusText o) {
        return (this.getIdSubcorpus() < o.getIdSubcorpus()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SubcorpusText)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return (this.getIdSubcorpus() == ((SubcorpusText) obj).getIdSubcorpus())
                && (this.getIdText() == ((SubcorpusText) obj).getIdText());
    }

}
