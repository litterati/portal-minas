package br.project.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class Words implements Comparable<Words>, Serializable {

    private int id;
    private int idCorpus;
    private int idText;
    private int idSentence;
    private int position;
    private String word;
    private String lemma;
    private String pos;
    private String norm;
    private String transkription;
    private String DepHead;
    private String DepFunction;
    private String contraction;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public int getIdText() {
        return idText;
    }

    public void setIdText(int idText) {
        this.idText = idText;
    }

    public int getIdSentence() {
        return idSentence;
    }

    public void setIdSentence(int idSentence) {
        this.idSentence = idSentence;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

    public String getTranskription() {
        return transkription;
    }

    public void setTranskription(String transkription) {
        this.transkription = transkription;
    }

    public String getDepHead() {
        return DepHead;
    }

    public void setDepHead(String DepHead) {
        this.DepHead = DepHead;
    }

    public String getDepFunction() {
        return DepFunction;
    }

    public void setDepFunction(String DepFunction) {
        this.DepFunction = DepFunction;
    }

    public String getContraction() {
        return contraction;
    }

    public void setContraction(String contraction) {
        this.contraction = contraction;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Corpus corpus;
    private Text text;
    private Sentence sentence;
    private List<WordTags> wordTags;
    private List<WordsAlign> wordAlign;

    public Corpus getCorpus() {
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public Sentence getSentence() {
        return sentence;
    }

    public void setSentence(Sentence sentence) {
        this.sentence = sentence;
    }

    public List<WordTags> getWordTags() {
        return wordTags;
    }

    public void setWordTags(List<WordTags> wordTags) {
        this.wordTags = wordTags;
    }

    public List<WordsAlign> getWordAlign() {
        return wordAlign;
    }

    public void setWordAlign(List<WordsAlign> wordAlign) {
        this.wordAlign = wordAlign;
    }

    @Override
    public int compareTo(Words o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Words)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Words) obj).getId();
    }

}
