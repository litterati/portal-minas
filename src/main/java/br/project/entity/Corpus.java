package br.project.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class Corpus implements Comparable<Corpus>, Serializable {

    private int id;
    private String name;
    private String description;
    private String userTerms;
    private int type;

    public static final int ALL = 0;
    public static final int WORK = 1;
    public static final int REFERENCE = 2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserTerms() {
        return userTerms;
    }

    public void setUserTerms(String userTerms) {
        this.userTerms = userTerms;
    }
    
    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private List<Text> texts;
    private List<Words> words;
    private List<Permission> permissions;
    private List<CorpusTagValues> corpusTagValues;
    private List<CorpusTags> corpusTags;
    private List<TextTags> textTags;
    private List<CorpusTools> corpusTools;

    public List<Text> getTexts() {
        return texts;
    }

    public void setTexts(List<Text> texts) {
        this.texts = texts;
    }

    public List<Words> getWords() {
        return words;
    }

    public void setWords(List<Words> words) {
        this.words = words;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public List<CorpusTagValues> getCorpusTagValues() {
        return corpusTagValues;
    }

    public void setCorpusTagValues(List<CorpusTagValues> corpusTagValues) {
        this.corpusTagValues = corpusTagValues;
    }

    public void addCorpusTagValues(CorpusTagValues corpusTagValues) {
        if (this.corpusTagValues == null) {
            this.corpusTagValues = new ArrayList<>();
        }
        this.corpusTagValues.add(corpusTagValues);
    }

    public List<CorpusTags> getCorpusTags() {
        return corpusTags;
    }

    public void setCorpusTags(List<CorpusTags> corpusTags) {
        this.corpusTags = corpusTags;
    }

    public void addCorpusTags(CorpusTags corpusTags) {
        if (this.corpusTags == null) {
            this.corpusTags = new ArrayList<>();
        }
        this.corpusTags.add(corpusTags);
    }

    public List<TextTags> getTextTags() {
        return textTags;
    }

    public void setTextTags(List<TextTags> textTags) {
        this.textTags = textTags;
    }

    public List<CorpusTools> getCorpusTools() {
        return corpusTools;
    }

    public void setCorpusTools(List<CorpusTools> corpusTools) {
        this.corpusTools = corpusTools;
    }

    // -------------------------------------------------------------------------
    // Auxiliar
    // -------------------------------------------------------------------------
    private String corpusPath;

    public String getCorpusPath() {
        return corpusPath;
    }

    public void setCorpusPath(String corpusPath) {
        this.corpusPath = corpusPath;
    }
    
    @Override
    public int compareTo(Corpus o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Corpus)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Corpus) obj).getId();
    }

}
