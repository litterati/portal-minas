package br.project.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Concordancer
 *
 * @author Michelle, Thiago Vieira
 * @since 2013
 */
public class Concordancer {

    public static final String TARGET_TEXTO = "texto";
    public static final String TARGET_METATEXTO = "metatexto";

    //Page config
    private int resultsByPage = 50;
    private int leftTokensNumber = 7;
    private int rightTokensNumber = 7;
    private int currentPage = 1;
    private int interval = 0;
    private int tolerance = 0;
    private String term = "";
    //Filters
    private List<TextTags> lstTextTags = new ArrayList<>();
    private List<TextTagValues> lstTextTagValues = new ArrayList<>();
    private String searchTarget = TARGET_TEXTO;
    private int idCorpus = 0;
    //Resultados
    private List<ConcordancerEntry> entries = new ArrayList<>();
    private String error = "";
    private int totalPages = 0;
    private int totalConcordances = 0;
    private long time = 0;

    public int getResultsByPage() {
        return resultsByPage;
    }

    public void setResultsByPage(int resultsByPage) {
        this.resultsByPage = resultsByPage;
    }

    public int getLeftTokensNumber() {
        return leftTokensNumber;
    }

    public void setLeftTokensNumber(int leftTokensNumber) {
        this.leftTokensNumber = leftTokensNumber;
    }

    public int getRightTokensNumber() {
        return rightTokensNumber;
    }

    public void setRightTokensNumber(int rightTokensNumber) {
        this.rightTokensNumber = rightTokensNumber;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getTolerance() {
        return tolerance;
    }

    public void setTolerance(int tolerance) {
        this.tolerance = tolerance;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public List<TextTags> getLstTextTags() {
        return lstTextTags;
    }

    public void setLstTextTags(List<TextTags> lstTextTags) {
        this.lstTextTags = lstTextTags;
    }

    public List<TextTagValues> getLstTextTagValues() {
        return lstTextTagValues;
    }

    public void setLstTextTagValues(List<TextTagValues> lstTextTagValues) {
        this.lstTextTagValues = lstTextTagValues;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public int getIdCorpus() {
        return idCorpus;
    }

    public void setIdCorpus(int idCorpus) {
        this.idCorpus = idCorpus;
    }

    public List<ConcordancerEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<ConcordancerEntry> entries) {
        this.entries = entries;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalConcordances() {
        return totalConcordances;
    }

    public void setTotalConcordances(int totalConcordances) {
        this.totalConcordances = totalConcordances;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
