/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import java.io.Serializable;

/**
 * Support.java
 *
 * @author Thiago Vieira
 * @since 28/08/2014
 */
public class Support implements Comparable<Support>, Serializable {

    private int id;
    private String name;
    private String email;
    private String subject;
    private String message;
    private int status;

    public static int NOT_READ = 1;
    public static int READ = 2;
    public static int HIDDEN = 3;

    public Support(int id, String name, String email, String subject, String message, int status) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.subject = subject;
        this.message = message;
        this.status = status;
    }

    public Support(String name, String email, String subject, String message, int status) {
        this(0, name, email, subject, message, status);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int compareTo(Support o) {
        return (this.getId() < o.getId()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Support)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return this.getId() == ((Support) obj).getId();
    }

}
