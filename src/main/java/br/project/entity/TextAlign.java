/*

 Uma entity é um objeto leve de domínio persistente utilizado para representar 
 uma tabela da base de dados, sendo que cada instância da entity corresponde a 
 uma linha da tabela. A entity é baseada em um simples POJO (Plain Old Java 
 Object), ou seja, uma classe Java comum.
 (http://www.universidadejava.com.br/docs/entity)

 */
package br.project.entity;

import br.project.dao.TextDAO;
import java.io.Serializable;

/**
 * TextAlign.java
 *
 * @author Thiago Vieira
 * @since 30/06/2014
 */
public class TextAlign implements Comparable<TextAlign>, Serializable {

    private int idSource;
    private int idTarget;
    private String type;

    public int getIdSource() {
        return idSource;
    }

    public void setIdSource(int idSource) {
        this.idSource = idSource;
    }

    public int getIdTarget() {
        return idTarget;
    }

    public void setIdTarget(int idTarget) {
        this.idTarget = idTarget;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    // -------------------------------------------------------------------------
    // Objetos
    // -------------------------------------------------------------------------
    private Text source;
    private Text target;

    public Text getSource() {
        if (source == null) {
            source = TextDAO.selectById(idSource);
        }
        return source;
    }

    public void setSource(Text source) {
        this.source = source;
    }

    public Text getTarget() {
        if (target == null) {
            target = TextDAO.selectById(idTarget);
        }
        return target;
    }

    public void setTarget(Text target) {
        this.target = target;
    }

    @Override
    public int compareTo(TextAlign o) {
        return (this.getIdSource() < o.getIdSource()) ? -1 : ((this.equals(o)) ? 0 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TextAlign)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return (this.getIdSource() == ((TextAlign) obj).getIdSource())
                && (this.getIdTarget() == ((TextAlign) obj).getIdTarget());
    }

}
