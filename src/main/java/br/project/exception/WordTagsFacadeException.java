/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */
package br.project.exception;

/**
 * WordTagsFacadeException
 *
 * @author Matheus Silva
 * @since 19/08/2014
 */
public class WordTagsFacadeException extends Exception {
    
    public WordTagsFacadeException(Exception e) {
        super("[WordTagsFacadeException]" + e.getMessage(), e.getCause());
    }
    
    private WordTagsFacadeException(String string) {
        super("[WordTagsFacadeException]" + string);
    }
    
    public static WordTagsFacadeException textFormatError(String text){
        return new WordTagsFacadeException ("That text is not right formated: " + text);
    }
    
}
