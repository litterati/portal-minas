/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */
package br.project.exception;

/**
 * TokenizerException
 *
 * @author Thiago Vieira
 * @since 15/07/2014
 */
public class TokenizerException extends Exception {
    
    public TokenizerException(Exception e) {
        super("[TokenizerException]" + e.getMessage(), e.getCause());
    }

    private TokenizerException(String string) {
        super("[TokenizerException]" + string);
    }
    
    public static TokenizerException languageNotAcceptable(String language){
        return new TokenizerException ("That is not an acceptable language: " + language);
    }

}
