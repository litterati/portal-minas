/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */
package br.project.exception;

/**
 *
 * DAOException
 *
 * @author Thiago Vieira
 * @since 21/07/2014
 */
public class DAOException extends Exception {

    public DAOException(Exception e) {
        super("[DAOException]" + e.getMessage(), e.getCause());
    }

    private DAOException(String string) {
        super("[DAOException]" + string);
    }
    
    //public static DAOException textFormatError(String text){
    //    return new DAOException ("That text is not right formated: " + text);
    //}

}
