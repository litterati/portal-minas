/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */
package br.project.exception;

/**
 * SentencesAndWordsServiceException
 *
 * @author Thiago Vieira
 * @since 21/07/2014
 */
public class SentencesAndWordsServiceException extends Exception {

    public SentencesAndWordsServiceException(Exception e) {
        super("[SentencesAndWordsServiceException]" + e.getMessage(), e.getCause());
    }

    private SentencesAndWordsServiceException(String string) {
        super("[SentencesAndWordsServiceException]" + string);
    }

    public static SentencesAndWordsServiceException deleteSentencesAndWords(String text_name) {
        return new SentencesAndWordsServiceException("Error to delete the sentences and th words from the text: " + text_name);
    }
    
    public static SentencesAndWordsServiceException wordListEmpty () {
        return new SentencesAndWordsServiceException("Problemas durante tokenização");
    }

}
