/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */
package br.project.exception;

/**
 * TextFacadeException
 *
 * @author Thiago Vieira
 * @since 15/07/2014
 */
public class TextFacadeException extends Exception {

    public TextFacadeException(Exception e) {
        super("[TextFacadeException]" + e.getMessage(), e.getCause());
    }
    
    private TextFacadeException(String string) {
        super("[TextFacadeException]" + string);
    }
    
    public static TextFacadeException metatextInsertError(Exception e){
        return new TextFacadeException (e.getMessage());
    }
    
    public static TextFacadeException headInsertError(Exception e){
        return new TextFacadeException (e.getMessage());
    }
    
    public static TextFacadeException textInsertError(Exception e){
        return new TextFacadeException (e.getMessage());
    }
    
    public static TextFacadeException tokenizerError(Exception e){
        return new TextFacadeException (e.getMessage());
    }
    
    public static TextFacadeException wordTagInsertError(Exception e){
        return new TextFacadeException (e.getMessage());
    }
    
    public static TextFacadeException deleteSentencesAndWords(String text_name) {
        return new TextFacadeException("Error to delete the sentences and th words from the text: " + text_name);
    }
    
    public static TextFacadeException wordListEmpty () {
        return new TextFacadeException("Problemas durante tokenização");
    }

}
