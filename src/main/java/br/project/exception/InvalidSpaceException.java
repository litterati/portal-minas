
package br.project.exception;

/**
 * InvalidSpaceException
 *
 * @author Marcel, Thiago Vieira
 * @since 2014
 */
public class InvalidSpaceException extends Exception {

    public InvalidSpaceException() {
        super("Remova os espaços após os \':\'.");
    }

}
