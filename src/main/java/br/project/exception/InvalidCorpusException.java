/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */

package br.project.exception;

/**
 *
 * InvalidCorpusException
 *
 * @author Thiago Vieira
 * @since 09/09/2014
 */
public class InvalidCorpusException extends Exception {
    
    public InvalidCorpusException() {
        super("Corpus não é válido");
    }
    
}
