/*
 * O tratamento de exceção, na ciência da computação, é o mecanismo responsável 
 * pelo tratamento da ocorrência de condições que alteram o fluxo normal da 
 * execução de programas de computadores. Para condições consideradas parte do 
 * fluxo normal de execução, ver os conceitos de sinal e evento.
 * (Wikipédia: http://pt.wikipedia.org/wiki/Tratamento_de_exce%C3%A7%C3%A3o)
 */
package br.project.exception;

/**
 * TextTagsFacadeException
 *
 * @author Thiago Vieira
 * @since 21/07/2014
 */
public class TextTagsFacadeException extends Exception {
    
    public TextTagsFacadeException(Exception e) {
        super("[TextTagsFacadeException]" + e.getMessage(), e.getCause());
    }
    
    private TextTagsFacadeException(String string) {
        super("[TextTagsFacadeException]" + string);
    }
    
    public static TextTagsFacadeException textFormatError(String text){
        return new TextTagsFacadeException ("That text is not right formated: " + text);
    }
    
}
