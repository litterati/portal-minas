<%-- 
    Document   : wizard
    Created on : 07/06/2013, 15:55:20
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.TextWizardController
    Variables  : 
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="control-group" id="fileType">
    <h3>Passo 1 de 4: Informe o tipo de arquivo a ser inserido</h3>
    <label class="radio">
        <input type="radio" name="optionsRadios1" id="optManualFile">
        <label for="optFileType">Arquivo de texto ser� inserido manualmente</label>
    </label>
    <label class="radio">
        <input type="radio" name="optionsRadios1" id="optFile" checked>
        <label for="optZipType">Arquivo .txt, .doc, .docx, .pdf</label><!-- ou compactado (.zip)</label> -->
    </label>
    <button class="btn btn-primary" onclick="show_hide('existAnotation', 'fileType')">Avan�ar</button>
</div>
<div class="control-group" style="display: none" id="existAnotation">
    <h3>Passo 2 de 4: O texto est� anotado?</h3>
    <label class="radio">
        <input type="radio" name="optionsRadios2" id="optAnotationY" checked>
        <label for="optAnotationY">Sim</label>
    </label>
    <label class="radio">
        <input type="radio" name="optionsRadios2" id="optAnotationN">
        <label for="optAnotationN">N�o</label>
    </label>
    <a href="#" class="btn btn-primary" onclick="text_without_annotation('typeAnotation', 'existAnotation', '<c:out value='${id_corpus}'/>')">Avan�ar</a>
    <button class="btn btn-default" onclick="show_hide('fileType', 'existAnotation')">Voltar</button>
</div>
<div class="control-group" style="display: none" id="typeAnotation">
    <h3>Passo 3 de 4: Qual o tipo de anota��o?</h3>
    <label class="radio">
        <input type="radio" name="optionsRadios3" id="optXces">
        <label for="optXces">XCES</label>
    </label>
    <label class="radio">
        <input type="radio" name="optionsRadios3" id="optOthers" checked>
        <label for="optOthers">Outra</label>
    </label>
    <button class="btn btn-primary" onclick="text_with_annotation('<c:out value='${id_corpus}'/>')">Avan�ar</button>
    <button class="btn btn-default" onclick="show_hide('existAnotation', 'typeAnotation')">Voltar</button>
</div>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function show_hide(id_to_show, id_to_hide) {
        document.getElementById(id_to_show).style.display = 'block';
        document.getElementById(id_to_hide).style.display = 'none';
    }

    function text_without_annotation(id_to_show, id_to_hide, id_corpus) {
        if (document.getElementById('optAnotationN').checked) {

            var type = "create";
            var file = "automatic";

            if (document.getElementById('optManualFile').checked) {
                type = "create-manual";
                file = "manual";
            }

            window.location = "<%=request.getContextPath()%>/text/insert?annotation=&id_corpus=" + id_corpus + "&type=" + type + "&file=" + file;
        }
        else {
            show_hide(id_to_show, id_to_hide);
        }
    }

    function text_with_annotation(id_corpus) {

        var type = "create";
        var file = "automatic";

        if (document.getElementById('optManualFile').checked) {
            type = "create-manual";
            file = "manual";
        }

        if (document.getElementById('optAnotationY').checked &&
                document.getElementById('optOthers').checked) {
            //The text or zip file have annotaion convention used by this project
            window.location = "<%=request.getContextPath()%>/text/insert?annotation=other&id_corpus=" + id_corpus + "&type=" + type + "&file=" + file;
        }
        if (document.getElementById('optAnotationY').checked &&
                document.getElementById('optXces').checked) {
            //The text or zip file have XCES annotaion

        }
    }
</script>