<%-- 
    Document   : view-text
    Created on : 30/06/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.TextViewController
    Variables  : text_words (List<Words>)
                 text (Text)
                 id_corpus (int)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total_tokens (int)
                 pagination_total_metatokens (int)
                 pagination_range (int)
                 highlight (Words)
                 metawords_class_name (String)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    s {
        text-decoration:none;
        padding: 3px 0px;
        line-height: 25px;
    }
    s:hover, s:hover.emphasize, s:hover.metaword {
        background-color: rgb(255, 255, 0);
        color: #333;
    }
    w {
        padding: 3px;
        margin: 3px;
    }
    w:hover, w:hover.emphasize, w:hover.metaword {
        background-color: rgb(135, 206, 235);
    }
    s.emphasize {
        background-color: rgb(255, 255, 153);
    }
    w.emphasize {
        background-color: rgb(207, 235, 247);
    }
    s.metaword {
        background-color: #999;
        color: #fff;
    }
</style>

<div>
    <!-- |${metawords_class_name}|Text -->
    <h3>${text.title} <small>${text.language}</small></h3>

    <div idText="${text.id}" idCorpus="${id_corpus}" style="position: relative;">
        
        <c:if test="${pagination_current_page > 1}">
            <a title="Anterior" style="float: left;" href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${id_corpus}&page=${pagination_current_page - 1}">&laquo;</a>
        </c:if>
        
        <c:forEach var="w" items="${text_words}" varStatus="status">
            <c:if test="${text_words[status.index].position == 0}">
                <p><s idSentence="${text_words[status.index].idSentence}">
            </c:if>
            <c:choose>
                <c:when test="${w.word eq '_NEWLINE_'}">
                    <!--<br idWord="${w.id}" position="${w.position}">-->
                    </s></p><p><s idSentence="${text_words[status.index].idSentence}">
                </c:when>
                <c:when test="${not empty w.contraction}">
                    <c:if test="${not empty text_words[status.index + 1].contraction && w.contraction eq text_words[status.index + 1].contraction}">
                        <w idWord="${w.id}" position="${w.position}+${text_words[status.index + 1].position}" lemma="${w.lemma}+${text_words[status.index + 1].lemma}" pos="${w.pos}+${text_words[status.index + 1].pos}" norm="${w.norm}+${text_words[status.index + 1].norm}" transkription="${w.transkription}+${text_words[status.index + 1].transkription}" depHead="${w.depHead}+${text_words[status.index + 1].depHead}" depFunction="${w.depFunction}+${text_words[status.index + 1].depFunction}" contraction="${w.word}+${text_words[status.index + 1].word}" >${w.contraction} </w>
                    </c:if>
                </c:when>
                <c:otherwise>
                    <w idWord="${w.id}" position="${w.position}" lemma="${w.lemma}" pos="${w.pos}" norm="${w.norm}" transkription="${w.transkription}" depHead="${w.depHead}" depFunction="${w.depFunction}" contraction="${w.contraction}" <c:if test="${w['class'] eq metawords_class_name}"> class="metaword"</c:if> >${w.word} </w>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${(text_words[status.index].idSentence != text_words[status.index + 1].idSentence) && (text_words[status.index + 1].idSentence > 0)}">
                    </s><s idSentence="${text_words[status.index + 1].idSentence}">
                </c:when>
                <c:when test="${!(text_words[status.index + 1].idSentence > 0)}">
                    </s></p>
                </c:when>
            </c:choose>
        </c:forEach>
        
        <c:if test="${pagination_current_page < pagination_size}">
            <a title="Pr�ximo" style="position: absolute; bottom: 0px; right: 0px;" href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${id_corpus}&page=${pagination_current_page + 1}">&raquo;</a>
        </c:if>
        
    </div>
    <!-- /Text -->
    <!-- Pagination -->
    <div class="text-center">
        <ul class="pagination">
            <li title="Primeira"><a href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${id_corpus}&page=1">&larr;</a></li>
            <c:forEach var="page_i" begin="1" end="${pagination_size}">
                <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${id_corpus}&page=${page_i}">${page_i}</a></li>
            </c:forEach>
            <li title="�ltima"><a href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${id_corpus}&page=${pagination_size}">&rarr;</a></li>
        </ul>
        <div class="clearfix"></div>
        <span class="label label-warning">Exibindo ${fn:length(text_words)} de um total de ${pagination_total_tokens + pagination_total_metatokens} tokens (incluso meta tokens).</span>
    </div>
    <!-- /Pagination -->
</div>
    
<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    //To view
    var listClicked = {};
    $("w").hover(function(){
        //var content = "idWord : " + $(this).attr("idWord");
        var content = "Posi��o : " + $(this).attr("position");
        if ($(this).attr("lemma") !== "" && $(this).attr("lemma") !== "+"){
            content += "<br>Lemma : " + $(this).attr("lemma");
        }
        if ($(this).attr("pos") !== "" && $(this).attr("pos") !== "+"){
            content += "<br>PoS : " + $(this).attr("pos");
        }
        if ($(this).attr("norm") !== "" && $(this).attr("norm") !== "+"){
            content += "<br>Normaliza��o : " + $(this).attr("norm");
        }
        if ($(this).attr("transkription") !== "" && $(this).attr("transkription") !== "+"){
            content += "<br>Transcri��o : " + $(this).attr("transkription");
        }
        if ($(this).attr("depHead") !== "" && $(this).attr("depHead") !== "+"){
            content += "<br>depHead : " + $(this).attr("depHead");
        }
        if ($(this).attr("depFunction") !== "" && $(this).attr("depFunction") !== "+"){
            content += "<br>depFunction : " + $(this).attr("depFunction");
        }
        if ($(this).attr("contraction") !== ""){
            content += "<br>Contra��o : " + $(this).attr("contraction");
        }
        if ($(this).attr("class") === "metaword"){
            content += "<br>(Metatexto)";
        }
        //content += "<br><a href=\"<%=request.getContextPath()%>/word/update?id_word=" + $(this).attr("idWord") + "&id_corpus=${id_corpus}\" ><span class=\"glyphicon glyphicon-pencil\"></span> Alterar</a>";
        
        $(this).popover({
            title: $(this).text(),
            content: content,
            html: true,
            placement: "top",
            trigger: "manual"
        }).popover('show');
    }, function(){
        if (listClicked[$(this).attr("idWord")] !== $(this).attr("idWord")){
            $(this).popover('hide');
        }
    });
    
    $("w").click(function(){
        if (listClicked[$(this).attr("idWord")] === $(this).attr("idWord")){
            listClicked[$(this).attr("idWord")] = null;
        } else {
            listClicked[$(this).attr("idWord")] = $(this).attr("idWord");
        }
    });
    //Metaword
    $("w[class='metaword']").parent().addClass("metaword");
    
    <c:if test="${highlight != null}">
    $.fn.scrollView = function () {
        return this.each(function () {
            $('html, body').animate({
                scrollTop: $(this).offset().top - 250
            }, 1000);
        });
    };
    //sentence
    $("s[idsentence='${highlight.idSentence}']")
            .addClass("emphasize");
    //word
    $("w[idword='${highlight.id}']")
            .addClass("emphasize")
            .scrollView();
    </c:if>
</script>