<%-- 
    Document   : info
    Created on : 09/11/2013
    Author     : Thiago Vieira
    Controller : br.project.controller.TextInfoController
    Variables  : text (Text)
                 textImage (TextImages)
                 corpus (Corpus)
                 total_tokens (int)
                 total_meta_tokens (int)
                 total_text_sentences (int)
                 total_meta_sentences (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div>
    <!-- Text -->
    <h3>${text.title} <small>${text.language}</small></h3>

    <!-- Metadata -->
    <div class="row">
        <div class="col-md-8">
            <table class="table table-hover table-striped table-responsive">
                <tr>
                    <th>Etiqueta</th>
                    <th>Valor</th>
                    <th title="Requerido">*</th>
                </tr>
                <c:forEach var="textTag" items="${text.textTags}">
                    <tr>
                        <td>${textTag.tag}</td>
                        <td>${textTag.textTagsValues[0].value}</td>
                        <td><c:if test="${textTag.isRequired eq true}">*</c:if></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td title="Contados automaticamente">N�mero de tokens</td>
                    <td colspan="2">${total_tokens}</td>
                </tr>
                <tr>
                    <td title="Contados automaticamente">N�mero de meta tokens</td>
                    <td colspan="2">${total_meta_tokens}</td>
                </tr>
                <tr>
                    <td title="Contados automaticamente">N�mero total de tokens</td>
                    <td colspan="2">${total_tokens + total_meta_tokens}</td>
                </tr>
                <tr>
                    <td title="Contados automaticamente">N�mero de senten�as no texto</td>
                    <td colspan="2">${total_text_sentences}</td>
                </tr>
                <tr>
                    <td title="Contados automaticamente">N�mero de senten�as no metatexto</td>
                    <td colspan="2">${total_meta_sentences}</td>
                </tr>
                <tr>
                    <td title="Contados automaticamente">N�mero total de senten�as</td>
                    <td colspan="2">${total_text_sentences + total_meta_sentences}</td>
                </tr>
                <tr>
                    <td title="Informado no upload">Idioma</td>
                    <td colspan="2">${text.language}</td>
                </tr>
                <tr>
                    <td title="Informado no upload">Nome do arquivo</td>
                    <td colspan="2">${text.title}</td>
                </tr>
                <tr>
                    <td title="Informado no upload">Corpus pertencente</td>
                    <td colspan="2">${corpus.name}</td>
                </tr>
            </table>
            <span class="label label-warning">Total: <span id="total">${fn:length(text.textTags)+4}</span></span>
        </div>
        <div class="col-md-4">
            <c:if test="${textImage != null && textImage.fileName != '' }">
                <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/highslide/highslide.css" />
                <a href="${textImage.fileName}" class="highslide" onclick="return hs.expand(this)">
                    <img src="${textImage.fileName}" alt="" title="" class="img-thumbnail" width="100%" />
                </a>
                <div class="highslide-caption" style="display: block;">${textImage.comment}</div>
            </c:if>
        </div>
    </div>
    <!-- /Metadata -->
</div>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/highslide/highslide-full.min.js"></script>
<script type="text/javascript">
            //For highslide
            hs.graphicsDir = '<%=request.getContextPath()%>/highslide/graphics/';
            hs.align = 'center';
            hs.transitions = ['expand', 'crossfade'];
            hs.wrapperClassName = 'dark borderless floating-caption';
            hs.fadeInOut = true;
            hs.dimmingOpacity = .75;
</script>