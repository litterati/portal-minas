<%-- 
    Document   : list-texts
    Created on : 29/05/2013, 17:15:31
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.text.TextsSelectController
    Variables  : texts (List<Text>)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total (int)
                 pagination_range (int)
                 order (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="4"></th>
            <th>Texto <a href="<%=request.getContextPath()%>/text/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=title" title="Ordernar por t�tulo"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Idioma <a href="<%=request.getContextPath()%>/text/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=language" title="Ordernar por idioma"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>${texts[0].textTags[0].tag}</th>
            <th>${texts[0].textTags[1].tag}</th>
            <th>${texts[0].textTags[2].tag}</th>
        </tr>
    </thead>
    <c:if test="${texts[0].id > 0}">
        <tbody>
            <c:forEach var="text" items="${texts}">
                <tr id="row${text.id}">
                    <td width="20">
                        <!-- Visualizar -->
                        <a href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${text.idCorpus}" title="Visualizar">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Info -->
                        <a href="<%=request.getContextPath()%>/text/info?id_text=${text.id}&id_corpus=${text.idCorpus}" title="Informa��es">
                            <span class="glyphicon glyphicon-info-sign"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Alterar -->
                        <a href="<%=request.getContextPath()%>/text/update?id_text=${text.id}&id_corpus=${text.idCorpus}" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Remover -->
                        <a href="#" data-toggle="modal" data-target="#modalDelete${text.id}" title="Remover">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="modalDelete${text.id}" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title">Confirma��o</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Tem certeza que deseja remover o texto '${text.title}' [${text.id}]?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${text.id},'${text.path}',${text.idCorpus},'${text.language}');">Confirmar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </td>
                    <td>${text.title}</td>
                    <td>${text.language}</td>
                    <td><c:choose><c:when test="${texts[0].textTags[0].id == text.textTags[0].id}">${text.textTags[0].textTagsValues[0].value}</c:when><c:otherwise>--</c:otherwise></c:choose></td>
                    <td><c:choose><c:when test="${texts[0].textTags[1].id == text.textTags[1].id}">${text.textTags[1].textTagsValues[0].value}</c:when><c:otherwise>--</c:otherwise></c:choose></td>
                    <td><c:choose><c:when test="${texts[0].textTags[2].id == text.textTags[2].id}">${text.textTags[2].textTagsValues[0].value}</c:when><c:otherwise>--</c:otherwise></c:choose></td>
                </tr>
            </c:forEach>
        </tbody>
    </c:if>
</table>
            
<c:choose>
    <c:when test="${texts[0].id > 0 && pagination_total > 30}">
        <span class="label label-warning">Exibindo <span id="total">${fn:length(texts)}</span> de um total de ${pagination_total} textos.</span>
        
        <!-- Pagination -->
        <div class="text-center">
            <ul class="pagination">
                <li title="Primeira"><a href="<%=request.getContextPath()%>/text/select?id_corpus=${id_corpus}&page=1&order=${order}">&larr;</a></li>
                <c:forEach var="page_i" begin="1" end="${pagination_size}">
                    <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/text/select?id_corpus=${id_corpus}&page=${page_i}&order=${order}">${page_i}</a></li>
                </c:forEach>
                <li title="�ltima"><a href="<%=request.getContextPath()%>/text/select?id_corpus=${id_corpus}&page=${pagination_size}&order=${order}">&rarr;</a></li>
            </ul>
        </div>
        <!-- /Pagination -->
        
    </c:when>
    <c:when test="${texts[0].id > 0}">
        <span class="label label-warning">Total: <span id="total">${fn:length(texts)}</span></span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� arquivos</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id_texto, path, id_corpus, language) {
        $.post("<%=request.getContextPath()%>/text/delete", {id: id_texto, path: path, id_corpus: id_corpus, language: language, ajax: true})
                .done(function(data) {
                    // data is JSON
                    if (data.alert_success){
                        alert( data.alert_success );
                        
                        $( "#row" + id_texto ).hide();
                        $( "#total" ).text( $( "#total" ).html()-1 );//counter
                    } else if (data.alert_danger){
                        alert( data.alert_danger );
                        
                        $( "#row" + id_texto ).addClass("danger");
                        setTimeout(function() {
                            $( "#row" + id_texto ).removeClass("danger"); // change it back after ...
                        }, 1500);
                    }
                })
                .fail(function() {
                    $( "#row" + id_texto ).addClass("danger");
                });
    }
</script>