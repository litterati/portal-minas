<%-- 
    Document   : list-form
    Created on : 11/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.text.metadata.TextMetadataSelectController
    Variables  : error (String)
                 param.name_corpus (String)
                 wizard (boolean)
                 listCorpusMetadata ()
                 lstFieldsType ()
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="tags-text">

    <!-- ERROR, WHEN IT EXIST -->
    <c:if test="${not empty error}">    
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            ${error}
        </div>
    </c:if>
    <!-- END ERROR, WHEN IT EXIST -->
    
    <div class="alert alert-success hidden" id="alerts_success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    
    <div class="alert alert-danger hidden" id="alerts_danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <!-- END ERROR, WHEN IT EXIST -->
    <table id="rowForm" class="table table-hover table-striped table-responsive hidden">
        <thead>
            <tr>
                <th colspan="2"></th>
                <!--<th>Id</th>-->
                <th title="Categoria da etiqueta. As etiquetas de 'Cabe�alho' ir�o delimitar informa��es de metadado do texto; Etiquetas 'Se��es do texto' demarcam informa��es de metatexto  ">Categoria</th>
                <th>Etiqueta</th>
                <th id="fieldTypeLabel">Tipo</th>
                <th id="requiredFieldLabel">Obrigat�rio</th>
                <th id="paginacaoLabel" class="hidden" title="teste">Pagina��o?</th>
                <th id="xcesLabel" class="hidden">XCES</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2"></td>
                <td>
                    <select name="cmbTagType" id="cmbTagType" class="form-control "  onchange="updateAddDiv()">
                        <option value= "textTags" selected>Cabe�alho</option>
                        <option value= "metaWordTags">Se��es do texto</option>
                        <option value= "style">Formata��o</option>
                    </select>
                </td>                
                <td class="hidden">
                    <input type='hidden' id='id_tag' class='form-control' value = '0' />
                    <input type='hidden' id='type' value = 'insert' />
                </td>
                <td valign='middle'>
                    <input type='text' id='tag' class='form-control' size="45px"/>
                </td>
                <td id="fieldTypeField">
                    <select name='cmbFieldType' id='cmbFieldType' class='form-control'>
                        <option value= '0' selected>Escolha um tipo</option>
                        <c:forEach var="fieldType" items="${lstFieldsType}">
                            <option value= '<c:out value='${fieldType.id}'/>'><c:out value='${fieldType.value}'/></option>
                        </c:forEach>
                    </select>
                </td>
                <td id="requiredField">
                    <label class='radio'>
                        <input type='radio' name='optionsRadios2' id='optRequiredF' checked />
                        <label for='optRequiredF'>N�o</label>
                    </label>
                    <label class='radio'>
                        <input type='radio' name='optionsRadios2' id='optRequiredT' />
                        <label for='optRequiredT'>Sim</label>
                    </label>
                </td>
                <td id="paginacaoField" class="hidden">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="paginacao" value="paginacaoType">
                        </label>
                    </div>
                </td>                
                <td id="xcesField" class="hidden">
                    <input type='text' id='xces_word_tag' class='form-control'/>
                </td>
                <td>
                    <button class='btn btnSave'>Salvar</button>&nbsp;
                    <button class= 'btn btnCancel'>Cancelar</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="panel-group" id="accordion" style="margin-top: 5px;">

    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id='linkMetadado'>Cabe�alho (Metadados)</a>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <!-- METADATA LIST -->
            <table class="table table-hover table-striped table-responsive" id="tblDataText">
                <thead>
                    <tr>
                        <th colspan="2"></th>
                        <!--<th>Id</th>-->
                        <th>Etiqueta</th>
                        <th>Tipo</th>
                        <th>Obrigat�rio</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${listTextTags[0].id > 0}">
                        <c:forEach var="textTag" items="${listTextTags}">
                            <tr id="textTagRow${textTag.id}">
                                <td width="20">
                                    <!-- Alterar -->
                                    <a href="<%=request.getContextPath()%>/text/metadata/update?id=${textTag.id}&id_corpus=${param.id_corpus}" title="Alterar">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                                <td width="20">
                                    <!-- Remover -->
                                    <a href="#" data-toggle="modal" data-target="#modalDeleteTextTags${textTag.id}" title="Remover">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    <!-- modal -->
                                    <div class="modal fade" id="modalDeleteTextTags${textTag.id}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                                    <h4 class="modal-title">Confirma��o</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Tem certeza que deseja remover a etiqueta '${textTag.tag}'?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteTextTagModalAction(${textTag.id});">Confirmar</button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                </td>
                                <!--<td>${textTag.id}</td>-->
                                <td>${textTag.tag}</td>
                                <td>${textTag.fieldType.value}</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${textTag.isRequired == 'true'}">Sim</c:when>
                                        <c:otherwise>N�o</c:otherwise>
                                    </c:choose>
                                </td>
                                <th></th>
                            </tr>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <!-- END METADATA LIST -->

            <c:choose>
                <c:when test="${listTextTags[0].id > 0}">
                    <span class="label label-warning">Total: <span id="totalTextTags">${fn:length(listTextTags)}</span></span>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-warning" role="alert">N�o h� etiquetas de cabe�alho cadastradas para o corpus</div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id='linkMetatexto'>Se��es do texto (Metatexto)</a>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <table class="table table-hover table-striped table-responsive" id="tblTextMetaWordTags">
                <thead>
                    <tr>
                        <th colspan="2"></th>
                        <!--<th>Id</th>-->
                        <th>Etiqueta</th>
                        <th>XCES</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${listTextMetaWordTags[0].id > 0}">
                        <c:forEach var="textMetaWordTag" items="${listTextMetaWordTags}">
                            <tr id="metaWordTagRow${textMetaWordTag.id}">
                                <td width="20">
                                    <!-- Alterar -->
                                    <a href="<%=request.getContextPath()%>/text/metadata/update?id=${textMetaWordTag.id}&id_corpus=${param.id_corpus}" title="Alterar">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                                <td width="20">
                                    <!-- Remover -->
                                    <a href="#" data-toggle="modal" data-target="#modalDeleteTextMetaWordTags${textMetaWordTag.id}" title="Remover">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    <!-- modal -->
                                    <div class="modal fade" id="modalDeleteTextMetaWordTags${textMetaWordTag.id}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                                    <h4 class="modal-title">Confirma��o</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Tem certeza que deseja remover a etiqueta '${textMetaWordTag.tag}'?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteMetaWordTagModalAction(${textMetaWordTag.id});">Confirmar</button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                </td>
                                <!--<td>${textMetaWordTag.id}</td>-->
                                <td>${textMetaWordTag.tag}</td>
                                <td>${textMetaWordTag.xces}</td>
                                <th></th>
                            </tr>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>

            <c:choose>
                <c:when test="${listTextMetaWordTags[0].id > 0}">
                    <span class="label label-warning" id="totalMetaWordTag">Total: ${fn:length(listTextMetaWordTags)}</span>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-warning" role="alert">N�o h� etiquetas de se��es de texto cadastradas para o corpus</div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" id='linkStyle'>Formata��o</a>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <table class="table table-hover table-striped table-responsive" id="tblTextStyleTags">
                <thead>
                    <tr>
                        <th colspan="2"></th>
                        <!--<th>Id</th>-->
                        <th>Etiqueta</th>
                        <th>XCES</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${listTextStyleTags[0].id > 0}">
                        <c:forEach var="textStyleTag" items="${listTextStyleTags}">
                            <tr id="styleTagRow${textStyleTag.id}">
                                <td width="20">
                                    <!-- Alterar -->
                                    <a href="<%=request.getContextPath()%>/text/metadata/update?id=${textStyleTag.id}&id_corpus=${param.id_corpus}" title="Alterar">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                                <td width="20">
                                    <!-- Remover -->
                                    <a href="#" data-toggle="modal" data-target="#modalDeleteTextStyleTags${textStyleTag.id}" title="Remover">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    <!-- modal -->
                                    <div class="modal fade" id="modalDeleteTextStyleTags${textStyleTag.id}" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                                <h4 class="modal-title">Confirma��o</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Tem certeza que deseja remover a etiqueta '${textStyleTag.tag}'?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteStyleTagModalAction(${textStyleTag.id});">Confirmar</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                </td>
                                <!--<td>${textStyleTag.id}</td>-->
                                <td>${textStyleTag.tag}</td>
                                <td>${textStyleTag.xces}</td>
                                <th></th>
                            </tr>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>

            <c:choose>
                <c:when test="${listTextStyleTags[0].id > 0}">
                    <span class="label label-warning" id="totalStyleTag">Total: ${fn:length(listTextStyleTags)}</span>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-warning" role="alert">N�o h� etiquetas de formata��o de estilo cadastradas para o corpus</div>
                </c:otherwise>
            </c:choose>        
        </div>
    </div>

    <input type="hidden" id="name_corpus" value="${param.name_corpus}" />
    <input type="hidden" id="wizard_text" value="${wizard}" />

</div>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteTextTagModalAction(id_text_tag) {
    var tag_type = 'textTag';
    $.post("<%=request.getContextPath()%>/text/metadata/delete", {id_text_tag : id_text_tag, type : tag_type, ajax: true})
        .done(function(data) {
            // data is JSON
            if (data.alert_success){
                alert(data.alert_success);
                $( "#textTagRow" + id_text_tag ).hide();
                $( "#totalWordTag" ).text( $( "#totalWordTag" ).html()-1 );//counter
            } else if (data.alert_danger){
                alert( data.alert_danger );

                $( "#textTagRow" + id_text_tag ).addClass("danger");
                setTimeout(function() {
                    $( "#textTagRow" + id_text_tag ).removeClass("danger"); // change it back after ...
                }, 1500);
            }
        })
        .fail(function() {
            $( "#textTagRow" + id_text_tag ).addClass("danger");
        });
    }

    function deleteMetaWordTagModalAction(id_meta_word_tag) {
    var tag_type = 'metaWordTag';
    $.post("<%=request.getContextPath()%>/text/metadata/delete", {id_meta_word_tag : id_meta_word_tag, type : tag_type, ajax: true})
        .done(function(data) {
            // data is JSON
            if (data.alert_success){
                alert(data.alert_success);
                $( "#metaWordTagRow" + id_meta_word_tag ).hide();
                $( "#totalMetaWordTag" ).text( $( "#totalMetaWordTag" ).html()-1 );//counter
            } else if (data.alert_danger){
                alert( data.alert_danger );

                $( "#metaWordTagRow" + id_meta_word_tag ).addClass("danger");
                setTimeout(function() {
                    $( "#metaWordTagRow" + id_meta_word_tag ).removeClass("danger"); // change it back after ...
                }, 1500);
            }
        })
        .fail(function() {
            $( "#metaWordTagRow" + id_meta_word_tag ).addClass("danger");
        });
    }

    function deleteStyleTagModalAction(id_style_tag) {
    var tag_type = 'style';
    $.post("<%=request.getContextPath()%>/text/metadata/delete", {id_style_tag : id_style_tag, type : tag_type, ajax: true})
        .done(function(data) {
            // data is JSON
            if (data.alert_success){
                alert(data.alert_success);
                $( "#styleTagRow" + id_style_tag ).hide();
                $( "#totalStyleTag" ).text( $( "#totalStyleTag" ).html()-1 );//counter
            } else if (data.alert_danger){
                alert( data.alert_danger );
                $( "#wordTagRow" + id_style_tag ).addClass("danger");
                setTimeout(function() {
                    $( "#wordTagRow" + id_style_tag ).removeClass("danger"); // change it back after ...
                }, 1500);
            }
        })
        .fail(function() {
            $( "#wordTagRow" + id_style_tag ).addClass("danger");
        });
    }

    $("#btnAddText").bind("click", function() {
        collapseAccordion();
        $("#rowForm").removeClass("hidden");
        $('#metadata_TextTags').focus();
        $(".btnSave").bind("click", Save);
        $(".btnCancel").bind("click", Cancel);
        $("#btnAddCorpus").attr("disabled","disabled");
    });

    function updateAddDiv() {
        var type = $("#cmbTagType").val();
        if (type === "textTags") {
            $("#fieldTypeLabel").removeClass("hidden");
            $("#requiredFieldLabel").removeClass("hidden");
            $("#xcesLabel").addClass("hidden");
            $("#paginacaoLabel").addClass("hidden");

            $("#fieldTypeField").removeClass("hidden");
            $("#requiredField").removeClass("hidden");
            $("#xcesField").addClass("hidden");
            $("#paginacaoField").addClass("hidden");
        }
        if (type === "metaWordTags") {
            $("#fieldTypeLabel").addClass("hidden");
            $("#requiredFieldLabel").addClass("hidden");
            $("#xcesLabel").removeClass("hidden");
            $("#paginacaoLabel").removeClass("hidden");

            $("#fieldTypeField").addClass("hidden");
            $("#requiredField").addClass("hidden");
            $("#xcesField").removeClass("hidden");
            $("#paginacaoField").removeClass("hidden");
        }
        if (type === "style") {
            $("#fieldTypeLabel").addClass("hidden");
            $("#requiredFieldLabel").addClass("hidden");
            $("#xcesLabel").removeClass("hidden");
            $("#paginacaoLabel").addClass("hidden");

            $("#fieldTypeField").addClass("hidden");
            $("#requiredField").addClass("hidden");
            $("#xcesField").removeClass("hidden");
            $("#paginacaoField").addClass("hidden");
        }
    };

    function collapseAccordion() {
        $('#collapseOne').collapse({'toggle': false});
        $('#collapseTwo').collapse({'toggle': false});
        $('#collapseThree').collapse({'toggle': false});

        $('#collapseOne').collapse('hide');
        $('#collapseTwo').collapse('hide');
        $('#collapseThree').collapse('hide');
    };

    function Save() {
        var id_corpus = ${param.id_corpus};
        var type = $("#cmbTagType").val();
        var xces = $("#xces_word_tag").val();
        var tag = $("#tag").val();
        var is_required = $('#optRequiredT').is(':checked');
        var id_field_type = $("#cmbFieldType").val();

        if ($.trim(tag) === "") {
            alert("Informe a etiqueta");
            $("#tag").focus();
            return;
        }

        $.post("<%=request.getContextPath()%>/text/metadata/insert",
                {id_corpus: id_corpus, tag: tag, is_required: is_required, id_field_type: id_field_type, type: type,xces : xces, wizard: "wizard", ajax: true})

                .done(function(data) {
                    if (data.alert_success){
                        // data is JSON
                        //alert(data.alert_success);
                        $("#rowForm").addClass("hidden");                    
                        $("#alerts_success").removeClass("hidden");
                        $("#alerts_success").text(data.alert_success);
                    }
                    else
                    if (data.alert_danger){
                        //alert(data.alert_danger);
                        $("#rowForm").addClass("hidden");                    
                        $("#alerts_danger").removeClass("hidden");
                        $("#alerts_danger").text(data.alert_danger);
                    }
                })
                .fail(function() {
                    $("#rowForm").addClass("hidden");                    
                    $("#alerts_danger").removeClass("hidden");
                    $("#alerts_danger").text(data.alert_danger);
                });
    }

    function Cancel() {
    $("#rowForm").addClass("hidden");

    $(".btnSave").unbind("click", Save);
    $(".btnCancel").unbind("click", Cancel);
    $("#btnAddCorpus").removeAttr("disabled", "disabled");
}
</script>