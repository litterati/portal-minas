<%-- 
    Document   : pages.text.metadata.form
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.TextMetadataInsertController
                 br.project.controller.TextMetadataUpdateController
                 br.project.controller.TextMetadataDeleteController
    Variables  : text_metadata (TextTags)
                 fields_type_list (List<FieldsType>)
                 page_menu_tools_sub (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/text/metadata/${page_menu_tools_sub}">
    <input type="hidden" name="id_corpus" value="${param.id_corpus}" />
    <c:if test="${not empty text_metadata.id}">
        <div class="form-group">
            <label for="id_tag">Id</label> 
            <input type="text" name="id_tag" value="${text_metadata.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group has-feedback">
        <label for="metadata_TextTags">Etiqueta</label> 
        <input type="text" name="metadata_TextTags" id="metadata_TextTags" value="${text_metadata.tag}" class="form-control" />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label class="control-label" for="id_field_type_TextTags">Tipo</label>
        <select class="form-control" name="id_field_type_TextTags" id="id_field_type_TextTags">
            <c:forEach var="ft" items="${fields_type_list}">
                <option <c:if test="${text_metadata.idFieldType == ft.id}"> selected </c:if> value="${ft.id}">${ft.value}</option>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <label class="control-label" for="radio">Obrigat�rio</label>
        <label class="radio">
            <input type="radio" name="is_required_TextTags" id="is_required_TextTags" <c:if test="${text_metadata.isRequired != true}"> checked </c:if> value="false" />
            <label for='optRequiredF'>N�o</label>
        </label>
        <label class="radio">
            <input type="radio" name="is_required_TextTags" id="is_required_TextTags" <c:if test="${text_metadata.isRequired == true}"> checked </c:if> value="true" />
            <label for="optRequiredT">Sim</label>
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/text/metadata/select?id_corpus=${param.id_corpus}" class="btn btn-default">Voltar</a>    
    </div>
</form>