<%-- 
    Document   : edit-text
    Created on : 07/08/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.TextsInsertController
                 br.project.controller.TextsUpdateController
                 br.project.controller.TextsDeleteController
    Variables  : text (Text)
                 param.type (String)
                 param.annotation
                 param.file
                 page_menu_tools_sub (String)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/text/${page_menu_tools_sub}" enctype="multipart/form-data">

    <!--TEXT TITLE AND HIDDEN FIELDS-->
    <c:if test="${param.type != 'update'}">
        <input type="hidden" name="type" value="insert" />
        <input type="hidden" name="annotation" value=${param.annotation} />
    </c:if>    
    <c:if test="${param.type == 'update'}">
        <input type="hidden" name="type" value="update" />
    </c:if>
    <input type="hidden" name="file" value=${param.file} />
    <input type="hidden" name="id_corpus" value="${text.idCorpus}" />
    <input type="hidden" name="name_corpus" value="${text.corpus.name}" />
    <input type="hidden" name="id" value="<c:out value="${text.id}"/>" />
    <!--END TEXT TITLE AND HIDDEN FIELDS-->

    <!--TEXT ERRORS, WHEN IT EXIST -->
    <c:if test="${not empty error}">
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            ${error}
        </div>
    </c:if>
    <!--END TEXT ERROR-->

    <!-- ID, CORPUS NAME AND LANGUAGE FIELDS-->
    <!--<c:if test="${text.id != 0}">
        <div class="form-group">
            <label for="id">Id</label>    
            <input type="text" name="id" class="form-control" value="<c:out value="${text.id}"/>" readonly="true" />
        </div>
    </c:if>-->
    <div class="form-group">
        <label for="name_corpus">Corpus</label>    
        <input type="text" name="name_corpus" class="form-control" value="${text.corpus.name}" readonly="true" />
        <input type="hidden" name="id_corpus" class="form-control" value="${text.idCorpus}" readonly="true" />
    </div>
    <div class="form-group">
        <label for="language">Linguagem</label>    
        <select name="language" id="language" class="form-control">
            <c:if test="${empty text.language}"><option value="Selecione" selected>Selecione a linguagem do(s) texto(s)</option></c:if>
            <option value="PORTUGUESE" <c:if test="${text.language=='PORTUGUESE'}">selected</c:if>>PORTUGUESE</option>
            <option value="ENGLISH" <c:if test="${text.language=='ENGLISH'}">selected</c:if>>ENGLISH</option>
            <option value="FRENCH" <c:if test="${text.language=='FRENCH'}">selected</c:if>>FRENCH</option>
            <option value="SPANISH" <c:if test="${text.language=='SPANISH'}">selected</c:if>>SPANISH</option>
            <option value="ITALIAN" <c:if test="${text.language=='ITALIAN'}">selected</c:if>>ITALIAN</option>
        </select>
    </div>
    <c:if test="${text.id != 0}">
        <div class="form-group">
            <label for="title">T�tulo</label>
            <input type="text" name="title" class="form-control" value="${text.title}" />
        </div>
        <div class="form-group">
            <label for="text_image">Imagem de Capa</label><br/>
            <c:if test="${text_image.fileName != null && text_image.fileName != '' }">
                <a href="${text_image.fileName}" target="_blank">
                    <img src="${text_image.fileName}" class="img-thumbnail" width="10%" />
                </a>
            </c:if>
            <input type="file" name="text_image" placeholder="Caminho da imagem" class="form-control" />
            <p class="help-block">(Arquivos .jpg, .png ou .bmp)</p>
        </div>
        <div class="form-group">
            <label for="image_comment">Coment�rios da Capa</label>
            <input type="text" name="image_comment" class="form-control" value="${text_image.comment}" />
        </div>
    </c:if>
    <!-- END ID, CORPUS NAME  AND LANGUAGE FIELDS-->

    <!--TEXT METADATA FIELDS -->
    <c:forEach var="textTag" items="${text.textTags}">
        <c:set var="required" value="" />
        <c:set var="field_required" value="" />
        <c:if test="${textTag.isRequired eq true}"><c:set var="required" value="required" /><c:set var="field_required" value="*" /></c:if>
        <div class="form-group">
            <label for="${textTag.tag}">${textTag.tag}</label>    
            <c:choose>
                <%-- Data Type (id==1) --%>
                <c:when test="${textTag.idFieldType eq 1}">
                    <input type="text" name="${textTag.tag}" class="form-control date" maxlength="10" 
                           value="${textTag.textTagsValues[0].value}" ${required} /><span class="text-error">${field_required}</span>
                </c:when>
                <%-- Integer Type (id==2) --%>
                <c:when test="${textTag.idFieldType eq 2}">
                    <input type="text" name="${textTag.tag}" class="form-control numbersOnly" maxlength="50" 
                           value="${textTag.textTagsValues[0].value}" ${required} /><span class="text-error">${field_required}</span>
                </c:when>
                <%-- Nacionality Type (id==4) --%>
                <c:when test="${textTag.idFieldType eq 4}">
                    <input type="text" name="${textTag.tag}" class="form-control" maxlength="200" 
                           value="${textTag.textTagsValues[0].value}" ${required} /><span class="text-error">${field_required}</span>
                </c:when>
                <%-- Text Type (id==7) --%>
                <c:when test="${textTag.idFieldType eq 7}">
                    <textarea name="${textTag.tag}" class="form-control" ${required} >${textTag.textTagsValues[0].value}</textarea><span class="text-error">${field_required}</span>
                    </c:when>
                    <%-- Type with a collection of options --%>
                    <c:otherwise>
                    <select name="${textTag.tag}" class="form-control">
                        <c:forEach var="textTagsValue" items="${textTag.textTagsValues}">
                            <option value="${textTagsValue.value}" 
                                    <c:if test="${fn:containsIgnoreCase(textTag.textTagsValues[0].value, textTagsValue.value)==true}">selected</c:if>>
                                ${textTagsValue.value}
                            </option>
                        </c:forEach>
                    </select>
                </c:otherwise>
            </c:choose>
            <c:if test="${textMetadata.isRequired eq true}"><span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span></c:if>
        </div>
    </c:forEach>
    <!--END TEXT METADATA FIELDS-->
    
    <!-- INPUT UPLOAD -->
    <div class="form-group">
        <label for="path">Caminho</label>
        <input type="file" name="path" placeholder="Caminho texto" class="form-control"  /> <br />
        <p class="help-block">(Arquivos .txt, .doc, .docx, .pdf ou .zip)</p>
    </div> 
    <!-- END INPUT UPLOAD -->
    
     <!-- COVER UPLOAD -->
    <div class="form-group">
        <label for="text_image">Imagem de Capa</label>
        <input type="file" name="text_image" placeholder="Caminho da imagem" class="form-control" /> <br />
        <p class="help-block">(Arquivos .jpg, .png ou .bmp)</p>
    </div> 
    <!-- END COVER UPLOAD -->
    
     <!-- AUDIO UPLOAD -->
    <div class="form-group">
        <label for="text_image">Arquivo de �udio</label>
        <input type="file" name="text_image" placeholder="Caminho da �udio" class="form-control" /> <br />
        <p class="help-block">(Arquivos .mp3 ou .wav)</p>
    </div> 
    <!-- END AUDIO UPLOAD -->
    
    <c:if test="${not empty manual}">
        <!-- TEXT -->
        <div class="form-group has-feedback">
            <label for="text">Texto: </label> 
            <textarea name="text" id="text_edit" rows="10" class="form-control" rows="3">${text.textString}</textarea>
            <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
        </div>
        <!-- END TEXT -->
    </c:if>
    
    <!-- BUTTONS -->
    <div class="form-group">
        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalProgress">Salvar</button>
        <a href="<%=request.getContextPath()%>/text/select?id_corpus=${text.idCorpus}" class="btn btn-default">Voltar</a>
    </div>
    <!-- END BUTTONS -->
    
    
</form>
    
<!-- modal -->
<div class="modal fade" id="modalProgress" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Aguarde, importando texto...</h4>
            </div>
            <div class="modal-body">
                <img src="<%=request.getContextPath()%>/images/progress.gif" class="img-responsive center-block" alt="Responsive image">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    /*$('#form').submit(function() {
         Validar o form
        if ($("#text_edit").val() == '') {
            return fase; // return false to cancel form action
        }
        else {
            return true;
        }
    });*/

    $(function() {

        $(".date").mask("99/99/9999"); //Mask for date

        $('.numbersOnly').keyup(function() {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });

    });

</script>