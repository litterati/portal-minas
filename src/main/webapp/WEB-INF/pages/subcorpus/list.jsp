<%-- 
    Document   : list
    Created on : 16/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.SubcorpusSelectController
    Variables  : list_subcorpus (List<Subcorpus>)
                 id_corpus (int)
                 order (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- SUBCORPUS LIST -->
<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="2"></th>
            <th>Nome <a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=name" title="Ordernar por nome"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Descri��o <a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=description" title="Ordernar por descri��o"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
        </tr>
    </thead>
    <tbody>
        <c:if test="${not empty list_subcorpus}">
            <c:forEach var="subcorpus" items="${list_subcorpus}">
                <tr id="row${subcorpus.id}">
                    <td width="20">
                        <!-- Alterar -->
                        <a href="<%=request.getContextPath()%>/subcorpus/update?id_subcorpus=${subcorpus.id}&id_corpus=${id_corpus}" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Remover -->
                        <a href="#" data-toggle="modal" data-target="#modalDelete${subcorpus.id}" title="Remover">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="modalDelete${subcorpus.id}" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title">Confirma��o</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Tem certeza que deseja remover o subcorpus '${subcorpus.name}'?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${subcorpus.id}, ${subcorpus.idCorpus});">Confirmar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </td>
                    <td>${subcorpus.name}</td>
                    <td>${subcorpus.description}</td>
                </tr>
            </c:forEach>
        </c:if>
    </tbody>
</table>
<!-- END SUBCORPUS LIST -->

<c:choose>
    <c:when test="${not empty list_subcorpus && pagination_total > 50}">
        <span class="label label-warning">Exibindo <span id="total">${fn:length(list_subcorpus)}</span> de um total de ${pagination_total} subcorpus.</span>
        
        <!-- Pagination -->
        <div class="text-center">
            <ul class="pagination">
                <li title="Primeira"><a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${id_corpus}&page=1&order=${order}">&larr;</a></li>
                <c:forEach var="page_i" begin="1" end="${pagination_size}">
                    <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${id_corpus}&page=${page_i}&order=${order}">${page_i}</a></li>
                </c:forEach>
                <li title="�ltima"><a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${id_corpus}&page=${pagination_size}&order=${order}">&rarr;</a></li>
            </ul>
        </div>
        <!-- /Pagination -->
        
    </c:when>
    <c:when test="${not empty list_subcorpus}">
        <span class="label label-warning">Total: <span id="total">${fn:length(list_subcorpus)}</span></span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� subcorpus cadastrados</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id_subcorpus, id_corpus) {
        $.post("<%=request.getContextPath()%>/subcorpus/delete", {id_subcorpus: id_subcorpus, id_corpus: id_corpus, ajax: true})
                .done(function(data) {
                    // data is JSON
                    if (data.alert_success){
                        alert( data.alert_success );
                        
                        $( "#row" + data.id_subcorpus ).hide();
                        $( "#total" ).text( $( "#total" ).html()-1 );//counter
                    } else if (data.alert_danger){
                        alert( data.alert_danger );
                        
                        $( "#row" + data.id_subcorpus ).addClass("danger");
                        setTimeout(function() {
                            $( "#row" + data.id_subcorpus ).removeClass("danger"); // change it back after ...
                        }, 1500);
                    }
                })
                .fail(function() {
                    $( "#row" + data.id_subcorpus ).addClass("danger");
                });
    }
</script>