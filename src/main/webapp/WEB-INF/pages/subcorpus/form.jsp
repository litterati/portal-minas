<%-- 
    Document   : pages.subcorpus.tag.form
    Created on : 16/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.SubcorpusInsertController
                 br.project.controller.SubcorpusUpdateController
                 br.project.controller.SubcorpusDeleteController
    Variables  : subcorpus (Subcorpus)
                 texts_list (List<Text>)
                 subcorpus_texts_list (List<SubcorpusText>)
                 page_menu_tools_sub (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="<%=request.getContextPath()%>/multiselect/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<form id="form" method="post" action="<%=request.getContextPath()%>/subcorpus/${page_menu_tools_sub}">
    <input type="hidden" name="id_corpus" value="${id_corpus}" />
    <c:if test="${not empty subcorpus.id}">
        <div class="form-group hidden">
            <label for="id_subcorpus">Id</label> 
            <input type="text" name="id_subcorpus" value="${subcorpus.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group has-feedback">
        <label for="name">Nome</label> 
        <input type="text" name="name" id="name" value="${subcorpus.name}" class="form-control" maxlength="100" />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <label for="description">Descri��o</label> 
        <textarea name="description" id="description" class="form-control" rows="3" maxlength="400">${subcorpus.description}</textarea>
    </div>
    <div class="form-group">
        <label for="subcorpus_text">Textos do subcorpus</label>
        <select name="subcorpus_text[]" id="subcorpus_text" class="form-control" multiple>
            <c:if test="${not empty texts_list}">
                <c:forEach items="${texts_list}" var="text">
                    <option value="${text.id}">${text.title} / ${text.language}</option>
                </c:forEach>
            </c:if>
        </select>
        <p class="help-block">Clique nos textos para selecion�-los.</p>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/subcorpus/select" class="btn btn-default">Voltar</a>    
    </div>
</form>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/multiselect/jquery.multi-select.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#name').focus();
    });
    //texts that are in subcorpus
    var texts = [<c:if test="${not empty subcorpus_texts_list}"><c:forEach items="${subcorpus_texts_list}" var="text">${text.idText},</c:forEach></c:if>];
    texts.forEach(function(entry) {
        $('#subcorpus_text option[value=' + entry + ']').attr('selected', true) ;
    });
    $('#subcorpus_text').multiSelect();
</script>