<%-- 
    Document   : pages.task.list
    Created on : 28/09/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.task.TaskSelectController
    Variables  : tasks_list (List<Task>)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total (int)
                 pagination_range (int)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th></th>
            <!--<th>Id</th>-->
            <th>Tipo</th>
            <th>Progresso</th>
            <th>Estado</th>
            <th>Inserido</th>
            <th>Iniciado</th>
            <th>Finalizado</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="task" items="${tasks_list}">
            <tr id="row${task.id}">
                <td width="20">
                    <c:if test="${fn:length(task.response) > 0}">
                        <!-- Response -->
                        <a href="#" data-toggle="modal" data-target="#modalResponse${task.id}" title="Resposta da tarefa">
                            <span class="glyphicon glyphicon-flag"></span>
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="modalResponse${task.id}" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title">
                                            Resposta da tarefa 
                                            <small>
                                                <c:choose>
                                                    <c:when test="${task.type == 1}">INSERT_TEXT</c:when>
                                                    <c:when test="${task.type == 2}">DELETE_TEXT</c:when>
                                                    <c:when test="${task.type == 3}">ALIGNER</c:when>
                                                    <c:when test="${task.type == 4}">TAGGER</c:when>
                                                    <c:otherwise>${task.type}</c:otherwise>
                                                </c:choose>
                                            </small>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        ${fn:replace(task.response, newLineChar,'<br/>')}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </c:if>
                </td>
                <!--<td>${task.id}</td>-->
                <td>
                    <c:choose>
                        <c:when test="${task.type == 1}">INSERT_TEXT</c:when>
                        <c:when test="${task.type == 2}">DELETE_TEXT</c:when>
                        <c:when test="${task.type == 3}">ALIGNER</c:when>
                        <c:when test="${task.type == 4}">TAGGER</c:when>
                        <c:otherwise>${task.type}</c:otherwise>
                    </c:choose>
                </td>
                <td>${task.progress}</td>
                <td>
                    <c:choose>
                        <c:when test="${task.status == 1}">WAITING</c:when>
                        <c:when test="${task.status == 2}">RUNNING</c:when>
                        <c:when test="${task.status == 3}">COMPLETED_SUCCESS</c:when>
                        <c:when test="${task.status == 4}">COMPLETED_ERROR</c:when>
                        <c:when test="${task.status == 5}">ABORTED</c:when>
                        <c:otherwise>${task.status}</c:otherwise>
                    </c:choose>
                </td>
                <td>${task.dateInserted}</td>
                <td>${task.dateStarted}</td>
                <td>${task.dateFinished}</td>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty tasks_list && pagination_total > 10}">
        <span class="label label-warning">Exibindo <span id="total">${fn:length(tasks_list)}</span> de um total de ${pagination_total} tarefas.</span>
        
        <!-- Pagination -->
        <div class="text-center">
            <ul class="pagination">
                <li title="Primeira"><a href="<%=request.getContextPath()%>/task/select?page=1">&larr;</a></li>
                <c:forEach var="page_i" begin="1" end="${pagination_size}">
                    <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/task/select?page=${page_i}">${page_i}</a></li>
                </c:forEach>
                <li title="�ltima"><a href="<%=request.getContextPath()%>/task/select?page=${pagination_size}">&rarr;</a></li>
            </ul>
        </div>
        <!-- /Pagination -->
    </c:when>
    <c:when test="${not empty tasks_list}">
        <span class="label label-warning">Total: ${fn:length(tasks_list)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� tarefas</div>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
    //Fun��o para recarregar a p�gina (milliseconds)
    function timedRefresh() {
        setTimeout("location.reload(true);",30000);
    }
    timedRefresh();
</script>