<%-- 
    Document   : pages.task.dashboard
    Created on : 28/09/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.task.TaskDashboardController
    Variables  : queueSize (int)
                 numberOfProcessRunning (int)
                 stateName (String)
                 dateStarted (Date)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="panel panel-default">
    <div class="panel-body">
        Estado do gerenciador de tarefas: <strong>${stateName}</strong>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        Gerenciador come�ou em: <strong>${dateStarted}</strong>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        Tamanho da Fila: <strong>${queueSize}</strong>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        Tarefas em execu��o: <strong>${numberOfProcessRunning}</strong>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <a href="<%=request.getContextPath()%>/task/dashboard/start" class="btn btn-success" title="Iniciar gerenciador de tarefas">
            Iniciar
            <span class="glyphicon glyphicon-play" aria-hidden="true"></span>
        </a>
        <a href="<%=request.getContextPath()%>/task/dashboard/stop" class="btn btn-danger" title="Parar gerenciador de tarefas">
            Parar
            <span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
        </a>
        <p class="help-block">Ao parar o gerenciador, a tarefa atual ser� abortada e n�o ser� executada novamente.</p>
    </div>
</div>

<script type="text/javascript">
    //Fun��o para recarregar a p�gina (milliseconds)
    function timedRefresh() {
        setTimeout("location.reload(true);",30000);
    }
    timedRefresh();
</script>