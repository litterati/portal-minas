<%-- 
    Document   : form
    Created on : 20/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.WordUpdateController
    Variables  : word (Word)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/word/update">   
    <div class="form-group">
        <label for="id">Id</label> 
        <input type="text" name="id" value="${word.id}" readonly class="form-control"/>
    </div>
    <div class="form-group">
        <label for="id_corpus">Id do Corpus</label> 
        <input type="text" name="id_corpus" value="${word.idCorpus}" readonly class="form-control"/>
    </div>
    <div class="form-group">
        <label for="id_text">Id do Texto</label> 
        <input type="text" name="id_text" value="${word.idText}" readonly class="form-control"/>
    </div>
    <div class="form-group">
        <label for="id_sentence">Id da Senten�a</label> 
        <input type="text" name="id_sentence" value="${word.idSentence}" readonly class="form-control"/>
    </div>
    <div class="form-group">
        <label for="position">Posi��o</label> 
        <input type="text" name="position" value="${word.position}" readonly class="form-control"/>
    </div>
    <div class="form-group has-feedback">
        <label for="word">Palavra</label> 
        <input type="text" name="word" id="word" value="${word.word}" class="form-control" maxlength="100" required />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <label for="lemma">Lema</label> 
        <input type="text" name="lemma" id="lemma" value="${word.lemma}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <label for="pos">PoS</label> 
        <input type="text" name="pos" id="pos" value="${word.lemma}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <label for="norm">Normaliza��o</label> 
        <input type="text" name="norm" id="norm" value="${word.norm}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <label for="transkription">Transcri��o</label> 
        <input type="text" name="transkription" id="transkription" value="${word.transkription}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <label for="dep_head">dep_head</label> 
        <input type="text" name="dep_head" id="dep_head" value="${word.depHead}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <label for="dep_function">dep_function</label> 
        <input type="text" name="dep_function" id="dep_function" value="${word.depFunction}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <label for="contraction">Contra��o</label> 
        <input type="text" name="contraction" id="contraction" value="${word.contraction}" class="form-control" maxlength="100" />
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/text/select?id_corpus=${page_menu_id_corpus}" class="btn btn-default">Voltar</a>   
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#word').focus();
    });
</script>