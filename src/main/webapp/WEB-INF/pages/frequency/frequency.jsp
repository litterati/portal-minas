<%-- 
    Document   : frequency
    Created on : 17/07/2013, 17:51:17
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.FrequencyController
    Variables  : frequencies (Map<String, String>)
                 page_menu_id_corpus (String)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total_tokens (int)
                 pagination_range (int)
                 order (String)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th>Palavra <a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=word" title="Ordernar por token"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Frequ�ncia <a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=frequency" title="Ordernar por frequ�ncia"><span class="glyphicon glyphicon-sort-by-order-alt"></span></a></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="frequency" items="${frequencies}">
            <tr>
                <td>${frequency.key}</td>
                <td>${frequency.value}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>
                
<c:choose>
    <c:when test="${not empty frequencies}">
        <!-- Pagination -->
        <div class="text-center">
            <ul class="pagination">
                <li title="Primeira"><a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}&page=1&order=${order}">&larr;</a></li>
                <c:forEach var="page_i" begin="1" end="${pagination_size}">
                    <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}&page=${page_i}&order=${order}">${page_i}</a></li>
                </c:forEach>
                <li title="�ltima"><a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}&page=${pagination_size}&order=${order}">&rarr;</a></li>
            </ul>
            <div class="clearfix"></div>
            <span class="label label-warning">Exibindo ${fn:length(frequencies)} de um total de ${pagination_total_tokens} tokens distintos.</span>
        </div>
        <!-- /Pagination -->
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� tokens</div>
    </c:otherwise>
</c:choose>