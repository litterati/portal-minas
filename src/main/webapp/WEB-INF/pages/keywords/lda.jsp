<%-- 
    Document   : lda
    Created on : 10/09/2014
    Author     : R�gis Zangirolami
    Controller : 
    Variables  : keywords
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<p>A extra��o de palavras-chave � feita com base no m�todo Latent Dirichlet Allocation (LDA).<br>
    Preencha os campos abaixo para definir os par�metros da extra��o.</p>

<form id="form" method="post" action="<%=request.getContextPath()%>/keywords/lda" enctype="multipart/form-data">
    <input type="hidden" name="id_corpus" value="${id_corpus}" />
    <div class="panel-body">
        <div class="control-group">
            <label for="topics">Quantidade de t�picos</label>
            <div class="controls">
                <input type="text" name="ntopics" class="form-control" value="${keywords.ntopics}" placeholder="2" >
            </div>
        </div>
        <br>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="words">Quantidade de palavras por t�pico</label>    
                    <div class="controls">
                        <input type="text" name="nwords" class="form-control" value="${keywords.nwords}" placeholder="25" >
                    </div>
                </div>
            </div>
            <br>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="selected">Texto selecionado</label>    
                    <div class="controls">
                        <select name="texts" id="list" class="form-control">
                            <c:forEach var="text" items="${textsList}">
                                <option value="${text.id}">${text.title}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
                    <br>
        <div class="form-group">
            <label for="stop_words">Lista de stop words customizada</label>
            <input type="file" name="stop_words" placeholder="Caminho do arquivo" class="form-control" />
            <p class="help-block">(Se nenhum arquivo for enviado, ser� utilizada uma lista de stop words padr�o de acordo com o idioma do texto selecionado.)</p>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalProgress">Extrair palavras-chave</button>
        
    </div>
</form>
                    <br>
<c:if test="${topics != null}">
<form role="form">
    <div>
        <h3>${textSelected.title} <small>${textSelected.language}</small>
            <a class="btn btn-link" href="<%=request.getContextPath()%>/keywords/download" title="Baixar em formato texto">
            <span class="glyphicon glyphicon-download-alt"></span>
            Download
        </a>
        </h3>
        

        <c:set var="count" value="1" scope="page" />
        
        <c:forEach var="words" items="${topics}">
            <h3><small>T�pico ${count}</small></h3>
            
            <c:forEach var="word" items="${words}">
                ${word}
                <br>
            </c:forEach>                
                
            <c:set var="count" value="${count + 1}" scope="page"/>
        </c:forEach>
    </div>
</form>
</c:if>
        
<!-- modal -->
<div class="modal fade" id="modalProgress" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Extraindo palavras-chave...</h4>
            </div>
            <div class="modal-body">
                <img src="../images/progress.gif" class="img-responsive center-block" alt="Responsive image">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>