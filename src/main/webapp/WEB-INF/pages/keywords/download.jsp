<%-- 
    Document   : result
    Created on : 22/09/2014
    Author     : R�gis Zangirolami
    Controller : 
    Variables  : keywords
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form role="form">
    <div>
        <h3>${text.title} <small>${text.language}</small></h3>

        <c:set var="count" value="1" scope="page" />
        
        <c:forEach var="words" items="${topics}">
            <h3><small>T�pico ${count}</small></h3>
            
            <c:forEach var="word" items="${words}">
                ${word}
                <br>
            </c:forEach>                
                
            <c:set var="count" value="${count + 1}" scope="page"/>
        </c:forEach>
    </div>
</form>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>