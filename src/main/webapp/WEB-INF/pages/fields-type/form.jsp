<%-- 
    Document   : pages.fields-type.form
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : 
    Variables  : fields_type (FieldsType)
                 page_menu_tools_sub (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/fields-type/${page_menu_tools_sub}">
    <c:if test="${not empty fields_type.id}">
        <div class="form-group">
            <label for="id">Id</label> 
            <input type="text" name="id" value="${fields_type.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group has-feedback">
        <label for="value">Value</label> 
        <input type="text" name="value" id="value" value="${fields_type.value}" class="form-control" maxlength="400" />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/fields-type/select" class="btn btn-default">Voltar</a>    
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#name').focus();
    });
</script>