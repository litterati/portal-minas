<%-- 
    Document   : pages.fields-type.value.form
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : 
    Variables  : fields_type_value (FieldsTypeValues)
                 fields_type_list (List<FieldsType>)
                 page_menu_tools_sub (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/fields-type/value/${page_menu_tools_sub}">
    <c:if test="${not empty fields_type_value.id}">
        <div class="form-group">
            <label for="id">Id</label> 
            <input type="text" name="id" value="${fields_type_value.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group has-feedback">
        <label class="control-label" for="id_type">Tipo</label> 
        <select class="form-control" name="id_type" id="id_type">
            <c:forEach var="ft" items="${fields_type_list}">
                <option <c:if test="${fields_type_value.idType == ft.id}"> selected </c:if> value="${ft.id}">${ft.value}</option>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="value">Value</label> 
        <input type="text" name="value" id="value" value="${fields_type_value.value}" class="form-control" maxlength="400" />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/fields-type/value/select" class="btn btn-default">Voltar</a>    
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#name').focus();
    });
</script>