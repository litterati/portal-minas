<%-- 
    Document   : pages.fields-type.value.list
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : 
    Variables  : fields_type_value_list (List<FieldsTypeValue>)
                 fields_type (FieldsType) <optional>
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="2"></th>
            <!--<th>Id</th>-->
            <th>Fields Type</th>
            <th>Value</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="ftv" items="${fields_type_value_list}">
            <tr id="row${ftv.id}">
                <td width="20">
                    <!-- Alterar -->
                    <a href="<%=request.getContextPath()%>/fields-type/value/update?id=${ftv.id}" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </td>
                <td width="20">
                    <!-- Remover -->
                    <a href="#" data-toggle="modal" data-target="#modalDelete${ftv.id}" title="Remover">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                    <!-- modal -->
                    <div class="modal fade" id="modalDelete${ftv.id}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                    <h4 class="modal-title">Confirma��o</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Tem certeza que deseja remover o Field Type Value '${ftv.value}'?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${ftv.id});">Confirmar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </td>
                <!--<td>${ftv.id}</td>-->
                <td>
                    <!-- Fields Type -->
                    <a href="<%=request.getContextPath()%>/fields-type/update?id=${ftv.idType}">
                        <span class="glyphicon glyphicon-arrow-up" title="Editar"></span>
                        <c:choose>
                            <c:when test="${not empty fields_type}">${fields_type.value}</c:when>
                            <c:otherwise>${ftv.idType}</c:otherwise>
                        </c:choose>
                    </a>
                </td>
                <td>${ftv.value}</td>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty fields_type_value_list}">
        <span class="label label-warning">Total: ${fn:length(fields_type_value_list)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� fields type values</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id) {
        $.post("<%=request.getContextPath()%>/fields-type/value/delete", {id: id, ajax: true})
                .done(function(data) {
                    $("#row" + id).hide();
                })
                .fail(function() {
                    $("#row" + id).addClass("danger");
                });
    }
</script>