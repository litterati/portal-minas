<%-- 
    Document   : pages.fields-type.list
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : 
    Variables  : fields_type_list (List<FieldsType>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="2"></th>
            <!--<th>Id</th>-->
            <th>Value</th>
            <th>Fields Type Values</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="ft" items="${fields_type_list}">
            <tr id="row${ft.id}">
                <td width="20">
                    <!-- Alterar -->
                    <a href="<%=request.getContextPath()%>/fields-type/update?id=${ft.id}" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </td>
                <td width="20">
                    <!-- Remover -->
                    <a href="#" data-toggle="modal" data-target="#modalDelete${ft.id}" title="Remover">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                    <!-- modal -->
                    <div class="modal fade" id="modalDelete${ft.id}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                    <h4 class="modal-title">Confirma��o</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Tem certeza que deseja remover o Field Type '${ft.value}'?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${ft.id});">Confirmar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </td>
                <!--<td>${ft.id}</td>-->
                <td>${ft.value}</td>
                <td>
                    <!-- Fields Type Values -->
                    <a href="<%=request.getContextPath()%>/fields-type/value/select?field_type_id=${ft.id}">
                        <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                        Values
                    </a>
                </td>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty fields_type_list}">
        <span class="label label-warning">Total: ${fn:length(fields_type_list)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� fields type</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id) {
        $.post("<%=request.getContextPath()%>/fields-type/delete", {id: id, ajax: true})
                .done(function(data) {
                    $("#row" + id).hide();
                })
                .fail(function() {
                    $("#row" + id).addClass("danger");
                });
    }
</script>