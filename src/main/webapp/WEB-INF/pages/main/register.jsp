<%-- 
    Document   : register
    Created on : 02/09/2014, 15:31:09
    Author     : Thiago Vieira
    Controller : br.project.controller.main.MainRegisterController
    Variables  : captcha
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="page-header">Registro</h1>

<form role="form" method="post" action="<%=request.getContextPath()%>/register">
    <div class="form-group has-feedback">
        <label for="name" class="control-label">Nome</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Seu nome" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="email" class="control-label">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Seu email" onblur="emailValidate(this);" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="login" class="control-label">Login</label>
        <input type="text" class="form-control" id="login" name="login" placeholder="Seu nome" onblur="loginValidate(this);" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="password" class="control-label">Senha</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Sua senha" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="captcha-code" class="control-label">Captcha</label><br/>
        <img id="captcha" class="img-thumbnail" src="<%=request.getContextPath()%>/captcha/">
        <a href="#" onclick="refreshCaptcha();" class="btn btn-link">
            <span class="glyphicon glyphicon-refresh"></span>
        </a>
        <input type="text" class="form-control" id="captcha-code" name="captcha-code" placeholder="Voc� � humano?" required style="margin-top: 5px;" autocomplete="off">
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="general-terms" id="general-terms" value="accept" required> Li e aceito os termos e condi��es gerais de uso
        </label>
    </div>
    <button type="submit" class="btn btn-primary">Registrar-se</button>
</form>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function emailValidate(obj) {
        if (obj.value === ""){
            hasNone(obj);
        } else {
            $.get("<%=request.getContextPath()%>/user/exist", {email: obj.value, ajax: true})
                .done(function(data) {
                    if (data.email === false){
                        hasSuccess(obj);
                    } else {
                        hasError(obj);
                    }
                })
                .fail(function() {
                    hasWarning(obj);
                });
        }
    }
    function loginValidate(obj) {
        if (obj.value === ""){
            hasNone(obj);
        } else {
            $.get("<%=request.getContextPath()%>/user/exist", {login: obj.value, ajax: true})
                .done(function(data) {
                    if (data.login === false){
                        hasSuccess(obj);
                    } else {
                        hasError(obj)
                    }
                })
                .fail(function() {
                    hasWarning(obj);
                });
        }
    }
    function hasSuccess(obj){
        $(obj).parent()
            .removeClass("has-error has-warning")
            .addClass("has-success")
            .children("span")
            .removeClass("glyphicon-asterisk glyphicon-warning-sign glyphicon-remove")
            .addClass("glyphicon-ok");
    }
    function hasError(obj){
        $(obj).parent()
            .removeClass("has-success has-warning")
            .addClass("has-error")
            .children("span")
            .removeClass("glyphicon-asterisk glyphicon-warning-sign glyphicon-ok")
            .addClass("glyphicon-remove");
    }
    function hasWarning(obj){
        $(obj).parent()
            .removeClass("has-success has-error")
            .addClass("has-warning")
            .children("span")
            .removeClass("glyphicon-asterisk glyphicon-ok glyphicon-remove")
            .addClass("glyphicon-warning-sign");
    }
    function hasNone(obj){
        $(obj).parent()
            .removeClass("has-success has-error has-warning")
            .children("span")
            .removeClass("glyphicon-ok glyphicon-remove glyphicon-warning-sign")
            .addClass("glyphicon-asterisk");
    }
    function refreshCaptcha(){
        $("#captcha").attr("src", "<%=request.getContextPath()%>/captcha/" + Math.floor(Math.random()*1000));
    }
</script>