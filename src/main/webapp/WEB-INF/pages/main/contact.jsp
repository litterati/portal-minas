<%-- 
    Document   : contact
    Created on : 26/05/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.main.MainContactController
    Variables  : name (String) // when error
                 email (String) // when error
                 subject (String) // when error
                 message (String) // when error
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="page-header">Contato</h1>
<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vestibulum mattis turpis congue interdum. Quisque sit amet luctus nisi. Mauris hendrerit placerat euismod. Suspendisse at orci id tellus consequat adipiscing. Nunc a tincidunt justo. Etiam porta lacinia nunc sit amet dignissim. Cras tristique eros lectus. Donec sit amet magna dui. Pellentesque tempor lacus a erat iaculis molestie.</p>-->

<form role="form" method="post" action="<%=request.getContextPath()%>/contact">
    <div class="form-group has-feedback">
        <label for="name">Nome</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Seu nome" value="${name}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Seu email" value="${email}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="subject">Assunto</label>
        <input type="text" class="form-control" id="subject" name="subject" placeholder="Assunto" value="${subject}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="message">Mensagem</label>
        <textarea class="form-control" rows="3" id="message" name="message" placeholder="Mensagem" required>${message}</textarea>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="captcha-code" class="control-label">Captcha</label><br/>
        <img id="captcha" class="img-thumbnail" src="<%=request.getContextPath()%>/captcha/">
        <a href="#" onclick="refreshCaptcha();" class="btn btn-link">
            <span class="glyphicon glyphicon-refresh"></span>
        </a>
        <input type="text" class="form-control" id="captcha-code" name="captcha-code" placeholder="Voc� � humano?" required style="margin-top: 5px;" autocomplete="off">
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
</form>
<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function refreshCaptcha(){
        $("#captcha").attr("src", "<%=request.getContextPath()%>/captcha/" + Math.floor(Math.random()*1000));
    }
</script>