<%-- 
    Document   : thanks
    Created on : 30/06/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.main.MainController
--%>

<h1 class="page-header">Agradecimentos</h1>
<p>Agrade�o a todos que colaboraram com esse projeto.</p>

<h2>Coordenadores</h2>

<a href="#">e-labore</a>
<ul class="list-unstyled">
    <li>Tha�s Crist�faro Alves da Silva</li>
</ul>

<a href="#">LETRA - Laborat�rio experimental de tradu��o</a>
<ul class="list-unstyled">
    <li>Adriana Silvina Pagano</li>
    <li>F�bio Alves</li>
    <li>C�lia Maria Magalh�es</li>
    <li>Giacomo Figueiredo (UFOP)</li>
</ul>

<a href="#">LAVAL � Laborat�rio de Estudos em varia��o lingu�stica</a>
<ul class="list-unstyled">
    <li>J�nia Martins Ramos</li>
    <li>Al�xia Duchouny</li>
</ul>

<a href="http://www.nilc.icmc.usp.br">NILC - N�cleo Interinstitucional de Lingu�stica Computacional</a>
<ul class="list-unstyled">
    <li>Arnaldo Candido Junior</li>
    <li>Helena de Medeiros Caseli</li>
    <li>Leandro Henrique Mendon�a de Oliveira</li>
    <li>Sandra Maria Alu�sio</li>
    <li>Thiago Lima Vieira</li>
</ul>

<h2>Desenvolvedores</h2>
<ul class="list-unstyled">
    <li>Marcel Serikawa</li>
    <li>Matheus Antonio Ribeiro Silva</li>
    <li>Michelle Mendon�a</li>
    <li>R�gis Zangirolami</li>
</ul>

<h2>Recursos & Ferramentas</h2>

<ul class="list-unstyled">
    <li><a href="http://tomcat.apache.org/">Apache Tomcat</a></li>
    <li><a href="http://bitbucket.org/">Bitbucket</a></li>
    <li><a href="http://fonts.googleapis.com/css?family=Bree+Serif">Bree Serif Font</a></li>
    <li><a href="http://getbootstrap.com/">Bootstrap</a></li>
    <li><a href="http://git-scm.com/">Git</a></li>
    <li><a href="http://www.inkscape.org/">Inkscape</a></li>
    <li><a href="http://www.java.com/">Java Server Pages</a></li>
    <li><a href="http://jquery.com/">jQuery</a></li>
    <li><a href="http://netbeans.org/">Netbeans</a></li>
    <li><a href="http://www.postgresql.org/">PostgreSQL</a></li>
    <li><a href="http://www.ubuntu.com/">Ubuntu</a></li>
</ul>

<ul class="list-unstyled">
    <li><a href="http://code.google.com/p/giza-pp/">GIZA++</a></li>
    <li><a href="http://opennlp.apache.org/">OpenNLP</a></li>
    <li><a href="http://www.lalic.dc.ufscar.br/portal/">PorTAl</a></li>
</ul>

<p>Obrigado!</p>
