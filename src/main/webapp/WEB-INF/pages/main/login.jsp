<%-- 
    Document   : login
    Created on : 26/05/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.main.MainController
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1 class="page-header">Autentica��o</h1>

<form method="post" action="<%=request.getContextPath()%>/account/login" class="form-signin">

    <div class="form-group has-feedback">
        <label for="login_username">Login</label>
        <input type="text" name="login" id="login" class="form-control" placeholder="usu�rio" required />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>

    <div class="form-group has-feedback">
        <label for="login_password">Senha</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="senha" required /> 
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>

    <div class="form-group">
        <button class="btn btn-primary" type="submit">Entrar</button>
        <a href="<%=request.getContextPath()%>/forgot" class="btn btn-link" title="Recuperar senha" >Esqueceu sua senha?</a>
    </div>
</form>
