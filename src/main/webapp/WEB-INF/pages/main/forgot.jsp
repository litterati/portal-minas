<%-- 
    Document   : forgot
    Created on : 03/09/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.main.MainContactController
    Variables  : 
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="page-header">Esqueceu a sua senha?</h1>

<form role="form" method="post" action="<%=request.getContextPath()%>/forgot">
    <div class="form-group has-feedback">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Seu email" value="${email}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="captcha-code" class="control-label">Captcha</label><br/>
        <img id="captcha" class="img-thumbnail" src="<%=request.getContextPath()%>/captcha/">
        <a href="#" onclick="refreshCaptcha();" class="btn btn-link">
            <span class="glyphicon glyphicon-refresh"></span>
        </a>
        <input type="text" class="form-control" id="captcha-code" name="captcha-code" placeholder="Voc� � humano?" required style="margin-top: 5px;" autocomplete="off">
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <button type="submit" class="btn btn-primary">Nova senha</button>
</form>
<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function refreshCaptcha(){
        $("#captcha").attr("src", "<%=request.getContextPath()%>/captcha/" + Math.floor(Math.random()*1000));
    }
</script>