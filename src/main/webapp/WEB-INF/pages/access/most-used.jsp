<%-- 
    Document   : most-used
    Created on : 13/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.AccessMostUsedController
    Variables  : mostUsedTool (Map<String, Integer>)
                 mostUsedCorpus (Map<String, Integer>)
                 mostActiveUser (Map<String, Integer>)
                 start (String)
                 end (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link rel="stylesheet" href="<%=request.getContextPath()%>/jquery-ui/jquery-ui.min.css">

<form role="form" method="get" class="form-inline">
    <div class="form-group">
        <label for="start">Data de �nicio</label>
        <input type="text" id="start" name="start" class="form-control" value="${start}" placeholder="yyyy-mm-dd">
    </div>
    <div class="form-group">
        <label for="end"> e fim</label>
        <input type="text" id="end" name="end" class="form-control" value="${end}" placeholder="yyyy-mm-dd">
    </div>
    <button type="submit" class="btn btn-primary">Listar</button>
</form>
<br/>
<c:choose>
    <c:when test="${empty mostUsedTool && empty mostUsedCorpus && empty mostActiveUser}">
        <div class="alert alert-warning" role="alert">N�o h� acessos neste intervalo</div>
    </c:when>
    <c:otherwise>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
            google.load("visualization", "1", {packages: ["corechart"]});
            google.setOnLoadCallback(drawMostUsedToolChart);
            google.setOnLoadCallback(drawMostUsedCorpusChart);
            google.setOnLoadCallback(drawMostActiveUserChart);
            function drawMostUsedToolChart() {

                var data = google.visualization.arrayToDataTable([
                    ['Ferramenta', '# de acessos'],
                    <c:forEach var="mut" items="${mostUsedTool}">
                        ['${mut.key}', ${mut.value}],
                    </c:forEach>
                ]);

                var options = {
                    title: 'Ferramenta mais usada'
                };

                var chart = new google.visualization.PieChart(document.getElementById('mostUsedTool'));

                chart.draw(data, options);
            }
            function drawMostUsedCorpusChart() {

                var data = google.visualization.arrayToDataTable([
                    ['Corpus', '# de acessos'],
                    <c:forEach var="muc" items="${mostUsedCorpus}">
                        ['${muc.key}', ${muc.value}],
                    </c:forEach>
                ]);

                var options = {
                    title: 'Corpus mais usado'
                };

                var chart = new google.visualization.PieChart(document.getElementById('mostUsedCorpus'));

                chart.draw(data, options);
            }
            function drawMostActiveUserChart() {

                var data = google.visualization.arrayToDataTable([
                    ['Usu�rio', '# de acessos'],
                    <c:forEach var="mau" items="${mostActiveUser}">
                        ['${mau.key}', ${mau.value}],
                    </c:forEach>
                ]);

                var options = {
                    title: 'Usu�rio mais ativo'
                };

                var chart = new google.visualization.PieChart(document.getElementById('mostActiveUser'));

                chart.draw(data, options);
            }
        </script>
        
        <h3>Ferramenta mais usada</h3>
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th>Ferramenta</th>
                    <th># vezes</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="mut" items="${mostUsedTool}">
                    <tr>
                        <td>${mut.key}</td>
                        <td>${mut.value}</td>
                    </tr>
                </c:forEach>    
            </tbody>
        </table>
        <div id="mostUsedTool" style="width: 100%; height: 400px;"></div>

        <h3>Corpus mais usado</h3>
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th>Corpus</th>
                    <th># vezes</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="muc" items="${mostUsedCorpus}">
                    <tr>
                        <td>${muc.key}</td>
                        <td>${muc.value}</td>
                    </tr>
                </c:forEach>    
            </tbody>
        </table>
        <div id="mostUsedCorpus" style="width: 100%; height: 400px;"></div>

        <h3>Usu�rio mais ativo</h3>
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th>Usu�rio</th>
                    <th># vezes</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="mau" items="${mostActiveUser}">
                    <tr>
                        <td>${mau.key}</td>
                        <td>${mau.value}</td>
                    </tr>
                </c:forEach>    
            </tbody>
        </table>
        <div id="mostActiveUser" style="width: 100%; height: 400px;"></div>
    </c:otherwise>
</c:choose>


<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
    $("#start").datepicker({dateFormat: 'yy-mm-dd'});
    $("#end").datepicker({dateFormat: 'yy-mm-dd'});
</script>