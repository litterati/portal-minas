<%-- 
    Document   : history
    Created on : 24/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.AccessSelectController
    Variables  : accessList (List<Access>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th>Corpus <a href="<%=request.getContextPath()%>/access/history?order=corpus" title="Ordernar por corpus"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Usu�rio <a href="<%=request.getContextPath()%>/access/history?order=user" title="Ordernar por usu�rio"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Ferramenta <a href="<%=request.getContextPath()%>/access/history?order=tool" title="Ordernar por ferramenta"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Data <a href="<%=request.getContextPath()%>/access/history?order=date" title="Ordernar por data"><span class="glyphicon glyphicon-sort-by-order"></span></a></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="access" items="${accessList}">
            <tr>
                <td>${access.corpus.name}</td>
                <td>${access.user.name} (${access.user.login})</td>
                <td>${access.tool}</td>
                <td>${access.dateAccess}</td>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty accessList}">
        <span class="label label-warning">Total: ${fn:length(accessList)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� acessos</div>
    </c:otherwise>
</c:choose>

