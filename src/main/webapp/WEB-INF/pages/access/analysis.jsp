<%-- 
    Document   : analysis
    Created on : 27/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.AccessAnalysisController
    Variables  : accessMap (Map<String, Integer>) <date|corpus_name, count>
                 corpusNameList (List<String>)
                 dateList (List<String>)
                 start (String)
                 end (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link rel="stylesheet" href="<%=request.getContextPath()%>/jquery-ui/jquery-ui.min.css">

<form role="form" method="get" class="form-inline">
    <div class="form-group">
        <label for="start">Data de �nicio</label>
        <input type="text" id="start" name="start" class="form-control" value="${start}" placeholder="yyyy-mm-dd">
    </div>
    <div class="form-group">
        <label for="end"> e fim</label>
        <input type="text" id="end" name="end" class="form-control" value="${end}" placeholder="yyyy-mm-dd">
    </div>
    <button type="submit" class="btn btn-primary">Listar</button>
</form>
<br/>
<c:choose>
    <c:when test="${empty accessMap}">
        <div class="alert alert-warning" role="alert">N�o h� acessos neste intervalo</div>
    </c:when>
    <c:otherwise>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
            google.load("visualization", "1.1", {packages: ["bar"]});
            google.setOnLoadCallback(drawAccessMapChart);
            function drawAccessMapChart() {

                var data = google.visualization.arrayToDataTable([
                    ['tempo decorrido'<c:forEach var="corpusName" items="${corpusNameList}">, '${corpusName}'</c:forEach>],
                    <c:forEach var="date" items="${dateList}">
                        ['${date}'<c:forEach var="corpusName" items="${corpusNameList}"><c:set var="key" value="${date}|${corpusName}"/>, <c:choose><c:when test="${empty accessMap.get(key)}">0</c:when><c:otherwise>${accessMap.get(key)}</c:otherwise></c:choose></c:forEach>],
                    </c:forEach>
                ]);

                var options = {
                    chart: {
                        title: 'Uso dos corpora',
                        subtitle: 'de ${start} at� ${end}'
                    },
                    axes: {
                        y: {
                            0: {label: 'n� de acessos'} // Left y-axis.
                        }
                    }
                };

                var chart = new google.charts.Bar(document.getElementById('curve_chart'));

                chart.draw(data, options);
            }
        </script>
        
        <h3>Uso dos corpora</h3>
        <table class="table table-hover table-striped table-responsive" title="n� de acessos">
            <thead>
                <tr>
                    <th>Tempo/Nome do Corpus</th>
                    <c:forEach var="corpusName" items="${corpusNameList}"><th title="${corpusName}">${corpusName}</th></c:forEach>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="date" items="${dateList}">
                <tr>
                    <td>${date}</td>
                    <c:forEach var="corpusName" items="${corpusNameList}"><c:set var="key" value="${date}|${corpusName}"/><td><c:choose><c:when test="${empty accessMap.get(key)}">0</c:when><c:otherwise>${accessMap.get(key)}</c:otherwise></c:choose></td></c:forEach>
                </tr>
            </c:forEach>    
            </tbody>
        </table>
        <div id="curve_chart" style="width: 100%; height: 400px;"></div>

    </c:otherwise>
</c:choose>


<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
    $("#start").datepicker({dateFormat: 'yy-mm-dd'});
    $("#end").datepicker({dateFormat: 'yy-mm-dd'});
    //to shorten the corpus title
    $(function() {
        $('table th').each(function() {
            if ($(this).text().length > 20) {
                $(this).text($(this).text().substring(0,17) + "...");
            }
        });
    });
</script>