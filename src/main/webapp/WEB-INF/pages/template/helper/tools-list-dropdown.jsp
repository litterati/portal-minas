<%-- 
    Document   : tools
    Created on : 22/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.* 
    Variables  : page_menu_tools (String)
                 page_menu_id_corpus (int)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ferramentas <b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li <c:if test="${page_menu_tools eq 'align' || page_menu_tools eq 'align-tag' || page_menu_tools eq 'align-concordancer'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}">Alinhador</a></li>
        <li <c:if test="${page_menu_tools eq 'concordancer'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${page_menu_id_corpus}">Concord�nciador</a></li>
        <li <c:if test="${page_menu_tools eq 'info'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/info?id_corpus=${page_menu_id_corpus}">Estat�sticas</a></li>
        <li <c:if test="${page_menu_tools eq 'metadata-text' || page_menu_tools eq 'metadata-corpus'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/text/metadata/select?id_corpus=${page_menu_id_corpus}">Etiquetas</a></li>
        <!--<li <c:if test="${page_menu_tools eq 'metadata-corpus'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/corpus/metadata/select?id_corpus=${page_menu_id_corpus}">Etiquetas do Corpus</a></li>-->
        <!--<li <c:if test="${page_menu_tools eq 'metadata-text'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/text/metadata/select?id_corpus=${page_menu_id_corpus}">Etiquetas dos Textos</a></li>-->
        <li <c:if test="${page_menu_tools eq 'frequency'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}">Frequ�ncias</a></li>
        <li <c:if test="${page_menu_tools eq 'ngrama'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/ngrama?id_corpus=${page_menu_id_corpus}">N-gramas</a></li>
        <li <c:if test="${page_menu_tools eq 'keywords'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/keywords/lda?id_corpus=${page_menu_id_corpus}">Palavras-chave</a></li>
        <li <c:if test="${page_menu_tools eq 'subcorpus'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${page_menu_id_corpus}">Subcorpus</a></li>
        <li <c:if test="${page_menu_tools eq 'text'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/text/select?id_corpus=${page_menu_id_corpus}">Textos do Corpus</a></li>
        <li class="divider"></li>
        <li><a  href="<%=request.getContextPath()%>/admin/tools?id_corpus=${page_menu_id_corpus}">Todas</a></li>
    </ul>
</li>
