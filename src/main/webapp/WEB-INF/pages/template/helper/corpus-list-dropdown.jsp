<%-- 
    Document   : tools
    Created on : 22/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.* 
    Variables  : page_menu_corpus (List<Corpus>)
                 page_menu_id_corpus (int)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Corpus <b class="caret"></b></a>
    <!-- List of avalible corpora -->
    <ul class="dropdown-menu">
        <c:forEach var="corpus" items="${page_menu_corpus}">
            <li <c:if test="${page_menu_id_corpus eq corpus.id}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${corpus.id}">${corpus.name}</a></li>
        </c:forEach>
        <li class="divider"></li>
        <li><a  href="<%=request.getContextPath()%>/admin/home">Todos</a></li>
    </ul>
    <!-- /List of avalible corpora -->
</li>
