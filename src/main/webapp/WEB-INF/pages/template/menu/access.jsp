<%-- 
    Document   : template.menu.access
    Created on : 13/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.AccessSelectController
                 br.project.controller.AccessGraphController
    Variables  : page_menu_tools (String)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- NAV -->
<ul class="nav nav-pills navbar-right">
    <!-- Select -->
    <li <c:if test="${page_menu_tools == 'analysis'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/access/analysis">
            <span class="glyphicon glyphicon-stats" title="An�lise"></span>
            An�lise
        </a>
    </li>
    <!-- /Select -->

    <!-- Most Used -->
    <li <c:if test="${page_menu_tools == 'most-used'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/access/most-used">
            <span class="glyphicon glyphicon-stats" title="Mais usado"></span>
            Mais usado
        </a>
    </li>
    <!-- /Most Used -->
    
    <!-- History -->
    <li <c:if test="${page_menu_tools == 'history'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/access/history">
            <span class="glyphicon glyphicon-calendar" title="Hist�rico"></span>
            Hist�rico
        </a>
    </li>
    <!-- /History -->
    
</ul>
<!-- /NAV -->
