<%-- 
    Document   : template.menu.user
    Created on : 28/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.UsersSelectController
                 br.project.controller.UsersInsertController
                 br.project.controller.UsersUpdateController
                 br.project.controller.UsersDeleteController
    Variables  : page_menu_tools (String)
                 user (User) // to update
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- NAV -->
<ul class="nav nav-pills navbar-right">
    <!-- Select -->
    <li <c:if test="${page_menu_tools == 'select'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/user/select">
            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
            Listar
        </a>
    </li>
    <!-- /Select -->

    <!-- Insert -->
    <li <c:if test="${page_menu_tools == 'insert'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/user/insert">
            <span class="glyphicon glyphicon-plus" title="Inserir"></span>
            Inserir
        </a>
    </li>
    <!-- /Insert -->
    
    <!-- Alterar -->
    <c:if test="${not empty user.id}">
        <li <c:if test="${page_menu_tools == 'update'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/user/update?id=${user.id}">
                <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                Alterar
            </a>
        </li>
    </c:if>
    <!-- /Alterar -->
</ul>
<!-- /NAV -->
