<%-- 
    Document   : template.menu.info
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.info.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Info -->
    <li <c:if test="${page_menu_tools_sub == 'info'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/info?id_corpus=${page_menu_id_corpus}">
            <span class="glyphicon glyphicon-info-sign" title="Informações"></span>
            Informações
        </a>
    </li>
    <li <c:if test="${page_menu_tools_sub == 'description'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/info/description?id_corpus=${page_menu_id_corpus}">
            <span class="glyphicon glyphicon-bullhorn" title="Descrição"></span>
            Descrição
        </a>
    </li>
    <!-- /Info -->
</ul>
