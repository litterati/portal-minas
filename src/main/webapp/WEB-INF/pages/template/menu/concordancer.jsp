<%-- 
    Document   : template.menu.concordancer
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.concordancer.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Concordancer -->
    <c:if test="${page_menu_tools == 'concordancer'}">
        <li <c:if test="${page_menu_tools_sub == 'simple'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${page_menu_id_corpus}">
                <span class="glyphicon glyphicon-search" title="Simples"></span>
                Simples
            </a>
        </li>
        <li <c:if test="${page_menu_tools_sub == 'advanced'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/concordancer/advanced?id_corpus=${page_menu_id_corpus}">
                <span class="glyphicon glyphicon-zoom-in" title="Avan�ado"></span>
                Avan�ado
            </a>
        </li>
    </c:if>
    <!-- /Concordancer -->
</ul>
