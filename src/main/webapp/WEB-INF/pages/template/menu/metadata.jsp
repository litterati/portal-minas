<%-- 
    Document   : template.menu.info
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.metadata.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Text Metadata -->
    <c:if test="${page_menu_tools == 'metadata-text'}">
        <!-- Text Metadada Select -->
        <li <c:if test="${page_menu_tools_sub == 'select'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/text/metadata/select?id_corpus=${page_menu_id_corpus}" >
                <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                Listar
            </a>
        </li>
        <c:choose>
            <c:when test="${page_menu_tools_sub == 'select'}">
                <!-- Text Metadada Insert -->
                <li>
                    <a id="btnAddText" href="#">
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
            </c:when>
            <c:when test="${page_menu_tools_sub == 'insert'}">
                <!-- Text Metadada Insert -->
                <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/text/metadata/insert?id_corpus=${page_menu_id_corpus}" >
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
            </c:when>
            <c:when test="${page_menu_tools_sub == 'update' || page_menu_tools_sub == 'delete'}">
                <!-- Text Metadada Insert -->
                <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/text/metadata/insert?id_corpus=${page_menu_id_corpus}" >
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
                <!-- Text Metadada Update -->
                <li <c:if test="${page_menu_tools_sub == 'update'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/text/metadata/update?id_corpus=${page_menu_id_corpus}&id=${text_metadata.id}">
                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                        Alterar
                    </a>
                </li>
                <!-- Text Metadada Remove -->
                <li <c:if test="${page_menu_tools_sub == 'delete'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/text/metadata/delete?id_corpus=${page_menu_id_corpus}&id=${text_metadata.id}">
                        <span class="glyphicon glyphicon-trash" title="Remover"></span>
                        Remover
                    </a>
                </li>
            </c:when>
        </c:choose>
    </c:if>
    <!-- /Text Metadata -->
    <!-- Corpus Metadata -->
    <c:if test="${page_menu_tools == 'metadata-corpus'}">
        <!-- Corpus Metadada Select -->
        <li <c:if test="${page_menu_tools_sub == 'select'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/corpus/metadata/select?id_corpus=${page_menu_id_corpus}" >
                <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                Listar
            </a>
        </li>
        <c:choose>
            <c:when test="${page_menu_tools_sub == 'select'}">
                <!-- Text Metadada Insert -->
                <li>
                    <a id="btnAddCorpus" href="#">
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
            </c:when>
            <c:when test="${page_menu_tools_sub == 'insert'}">
                <!-- Text Metadada Insert -->
                <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/corpus/metadata/insert?id_corpus=${page_menu_id_corpus}" >
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
            </c:when>
            <c:when test="${page_menu_tools_sub == 'update' || page_menu_tools_sub == 'delete'}">
                <!-- Corpus Metadada Insert -->
                <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/corpus/metadata/insert?id_corpus=${page_menu_id_corpus}" >
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
                <!-- Corpus Metadada Update -->
                <li <c:if test="${page_menu_tools_sub == 'update'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/corpus/metadata/update?id_corpus=${page_menu_id_corpus}&id=${corpus_metadata.id}">
                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                        Alterar
                    </a>
                </li>
                <!-- Corpus Metadada Remove -->
                <li <c:if test="${page_menu_tools_sub == 'delete'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/corpus/metadata/delete?id_corpus=${page_menu_id_corpus}&id=${corpus_metadata.id}">
                        <span class="glyphicon glyphicon-trash" title="Remover"></span>
                        Remover
                    </a>
                </li>
            </c:when>
        </c:choose>
    </c:if>
    <!-- /Corpus Metadata -->
</ul>
