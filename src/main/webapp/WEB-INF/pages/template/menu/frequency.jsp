<%-- 
    Document   : template.menu.frequency
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.frequency.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Frequency -->
    <li <c:if test="${page_menu_tools_sub == 'frequency'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/frequency?id_corpus=${page_menu_id_corpus}">
            <span class="glyphicon glyphicon-list" title="Frequências"></span>
            Frequências
        </a>
    </li>
    <!-- /Frequency -->
</ul>
