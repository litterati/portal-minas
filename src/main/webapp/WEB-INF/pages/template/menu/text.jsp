<%-- 
    Document   : template.menu.text
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.text.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Texts -->
    <!-- Text List -->
    <li <c:if test="${page_menu_tools_sub == 'select'}">  class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/text/select?id_corpus=${page_menu_id_corpus}" >
            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
            Listar
        </a>
    </li>
    <c:choose>
        <c:when test="${page_menu_tools_sub == 'select'}">
            <!-- Text Insert -->
            <li>
                <a href="<%=request.getContextPath()%>/text/wizard?id_corpus=${page_menu_id_corpus}" >
                    <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                    Inserir
                </a>
            </li>
        </c:when>
        <c:when test="${page_menu_tools_sub == 'view' || page_menu_tools_sub == 'info' || page_menu_tools_sub == 'update' || page_menu_tools_sub == 'delete'}">
            <!-- Text View -->
            <li <c:if test="${page_menu_tools_sub == 'view'}">  class="active" </c:if> >
                <a href="<%=request.getContextPath()%>/text/view?id_corpus=${page_menu_id_corpus}&id_text=${text.id}">
                    <span class="glyphicon glyphicon-eye-open" title="Visualizar"></span>
                    Visualizar
                </a>
            </li>
            <!-- Text Info -->
            <li <c:if test="${page_menu_tools_sub == 'info'}">  class="active" </c:if> >
                <a href="<%=request.getContextPath()%>/text/info?id_corpus=${page_menu_id_corpus}&id_text=${text.id}">
                    <span class="glyphicon glyphicon-info-sign" title="Informações"></span>
                    Informações
                </a>
            </li>
            <!-- Text Insert -->
            <li>
                <a href="<%=request.getContextPath()%>/text/wizard?id_corpus=${page_menu_id_corpus}" >
                    <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                    Inserir
                </a>
            </li>
            <!-- Text Update -->
            <li <c:if test="${page_menu_tools_sub == 'update'}">  class="active" </c:if> >
                <a href="<%=request.getContextPath()%>/text/update?id_corpus=${page_menu_id_corpus}&id_text=${text.id}">
                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                    Alterar
                </a>
            </li>
            <!-- Text Remove -->
            <li <c:if test="${page_menu_tools_sub == 'delete'}">  class="active" </c:if> >
                <a href="<%=request.getContextPath()%>/text/delete?id_corpus=${page_menu_id_corpus}&id_text=${text.id}">
                    <span class="glyphicon glyphicon-trash" title="Remover"></span>
                    Remover
                </a>
            </li>
        </c:when>
        <c:when test="${page_menu_tools_sub == 'wizard' || page_menu_tools_sub == 'insert'}">
            <!-- Text Insert -->
            <li class="active">
                <a href="<%=request.getContextPath()%>/text/wizard?id_corpus=${page_menu_id_corpus}">
                    <span class="glyphicon glyphicon-plus-sign" title="Assistente"></span>
                    Assistente
                </a>
            </li>
        </c:when>
    </c:choose>
    <!-- /Texts -->
</ul>
