<%-- 
    Document   : template.menu.ngrama
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.ngrama.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Ngram -->
    <li <c:if test="${page_menu_tools_sub == 'ngrama'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/ngrama?id_corpus=${page_menu_id_corpus}">
            <span class="glyphicon glyphicon-list" title="N-gramas"></span>
            N-gramas
        </a>
    </li>
    <!-- /Ngram -->
</ul>
