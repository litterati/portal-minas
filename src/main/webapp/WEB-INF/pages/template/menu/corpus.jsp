<%-- 
    Document   : template.menu.corpus
    Created on : 28/07/2014, 18:30:26
    Author     : Thiago Vieira
    Controller : br.project.controller.CorpusSelectController
                 br.project.controller.CorpusInsertController
                 br.project.controller.CorpusUpdateController
                 br.project.controller.CorpusDeleteController
    Variables  : page_menu_tools (String)
                 corpus (Corpus) // to update
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- NAV -->
<ul class="nav nav-pills navbar-right">
    <!-- Select -->
    <li <c:if test="${page_menu_tools == 'select'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/corpus/select">
            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
            Listar
        </a>
    </li>
    <!-- /Select -->
    
    <!-- Insert -->
    <li <c:if test="${page_menu_tools == 'insert'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/corpus/insert">
            <span class="glyphicon glyphicon-plus" title="Inserir"></span>
            Inserir
        </a>
    </li>
    <!-- /Insert -->
    
    <!-- Wizard -->
    <!--<li <c:if test="${page_menu_tools == 'wizard'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/corpus/wizard">
            <span class="glyphicon glyphicon-plus-sign" title="Inserir"></span>
            Inserir - Assistente
        </a>
    </li>-->
    <!-- /Wizard -->
    
    <!-- Update -->
    <c:if test="${not empty corpus.id}">
        <li <c:if test="${page_menu_tools == 'update'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/corpus/update?id_corpus=${corpus.id}">
                <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                Alterar
            </a>
        </li>
    </c:if>
    <!-- /Update -->
</ul>
<!-- /NAV -->
