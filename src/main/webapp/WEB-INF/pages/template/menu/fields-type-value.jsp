<%-- 
    Document   : template.menu.fields-type-value
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.fields-type.value.*
    Variables  : page_menu_tools (String)
                 fields_type_value.id (FieldsTypeValue)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- NAV -->
<ul class="nav nav-pills navbar-right">
    <!-- Select -->
    <li <c:if test="${page_menu_tools == 'select'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/fields-type/value/select">
            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
            Listar
        </a>
    </li>
    <!-- /Select -->

    <!-- Insert -->
    <li <c:if test="${page_menu_tools == 'insert'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/fields-type/value/insert">
            <span class="glyphicon glyphicon-plus" title="Inserir"></span>
            Inserir
        </a>
    </li>
    <!-- /Insert -->

    <!-- Alterar -->
    <c:if test="${not empty fields_type_value.id}">
        <li <c:if test="${page_menu_tools == 'update'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/fields-type/value/update?id=${fields_type_value.id}">
                <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                Alterar
            </a>
        </li>
    </c:if>
    <!-- /Alterar -->
</ul>
<!-- /NAV -->
