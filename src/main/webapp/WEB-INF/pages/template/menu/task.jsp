<%-- 
    Document   : template.menu.task
    Created on : 18/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.TaskSelectController
                 br.project.controller.TaskDashboardController
    Variables  : page_menu_tools (String)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- NAV -->
<ul class="nav nav-pills navbar-right">
    <!-- Select -->
    <li <c:if test="${page_menu_tools == 'select'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/task/select">
            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
            Listar
        </a>
    </li>
    <!-- /Select -->
    
    <!-- Dashboard -->
    <li <c:if test="${page_menu_tools == 'dashboard'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/task/dashboard">
            <span class="glyphicon glyphicon-dashboard" title="Painel de Controle"></span>
            Painel de Controle
        </a>
    </li>
    <!-- /Dashboard -->
    
</ul>
<!-- /NAV -->
