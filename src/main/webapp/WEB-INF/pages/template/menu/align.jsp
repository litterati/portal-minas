<%-- 
    Document   : template.menu.align
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.align.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Align -->
    <c:if test="${page_menu_tools == 'align'}">
        <!-- Align List -->
        <li <c:if test="${page_menu_tools_sub == 'select'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}" >
                <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                Listar
            </a>
        </li>
        <!-- Align Tag -->
        <!--<li <c:if test="${page_menu_tools eq 'align-tag'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/align/tag/select?id_corpus=${page_menu_id_corpus}" >
                <span class="glyphicon glyphicon-tag" title="Gerenciamento de etiquetas de alinhamento"></span>
                Etiquetas
            </a>
        </li>-->
        <c:choose>
            <c:when test="${page_menu_tools_sub == 'select'}">
                <!-- Align Insert -->
                <li>
                    <a href="#" id="btnAddAlign">
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
            </c:when>
            <c:when test="${page_menu_tools_sub == 'insert'}">
                <!-- Align Insert -->
                <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
                    <a  href="<%=request.getContextPath()%>/align/insert?id_corpus=${page_menu_id_corpus}" >
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
            </c:when>
            <c:when test="${page_menu_tools_sub == 'view' || page_menu_tools_sub eq 'view_sentence' || page_menu_tools_sub eq 'view_word' || page_menu_tools_sub == 'update' || page_menu_tools_sub == 'delete'}">
                <!-- Align View -->
                <li <c:if test="${page_menu_tools_sub eq 'view' || page_menu_tools_sub eq 'view_sentence' || page_menu_tools_sub eq 'view_word'}">  class="active" </c:if> >
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                            <span class="glyphicon glyphicon-eye-open" title="Visualizar"></span>
                            Visualizar <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li <c:if test="${page_menu_tools_sub eq 'view'}">  class="active" </c:if> >
                            <a href="<%=request.getContextPath()%>/align/view?id_corpus=${page_menu_id_corpus}&id_source=${text_align.source.id}&id_target=${text_align.target.id}" >
                                <!-- <span class="glyphicon glyphicon-eye-open" title="Visualizar"></span> -->
                                Visualiza��o padr�o
                            </a>
                        </li>
                        <li <c:if test="${page_menu_tools_sub eq 'view_sentence'}">class="active"</c:if> >
                            <a href="<%=request.getContextPath()%>/align/sentence/view?id_corpus=${id_corpus}&id_source=${text_align.source.id}&id_target=${text_align.target.id}&page=${pagination_current_page}">
                                <!-- <span class="glyphicon glyphicon-pencil" title="Alterar"></span> -->
                                Editar Alinhamento de Senten�as
                            </a>
                        </li>
                        <li <c:if test="${page_menu_tools_sub eq 'view_word'}">class="active"</c:if> >
                            <a href="<%=request.getContextPath()%>/align/word/view?id_corpus=${id_corpus}&id_source=${text_align.source.id}&id_target=${text_align.target.id}&page=${pagination_current_page}">
                                <!-- <span class="glyphicon glyphicon-pencil" title="Alterar"></span> -->
                                Editar Alinhamento de Palavras
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- Align Insert -->
                <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
                    <a  href="<%=request.getContextPath()%>/align/insert?id_corpus=${page_menu_id_corpus}" >
                        <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                        Inserir
                    </a>
                </li>
                <!-- Align Update -->
                <li <c:if test="${page_menu_tools_sub == 'update'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/align/update?id_corpus=${page_menu_id_corpus}&id_source=${text_align.source.id}&id_target=${text_align.target.id}" >
                        <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                        Alterar
                    </a>
                </li>
                <!-- Align Remove -->
                <li <c:if test="${page_menu_tools_sub == 'delete'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/align/delete?id_corpus=${page_menu_id_corpus}&id_source=${text_align.source.id}&id_target=${text_align.target.id}" >
                        <span class="glyphicon glyphicon-trash" title="Remover"></span>
                        Remover
                    </a>
                </li>
            </c:when>
        </c:choose>
    </c:if>
    <!-- /Align -->
    <!-- Align Tag -->
    <c:if test="${page_menu_tools == 'align-tag'}">
        <!-- Align Tag List -->
        <li <c:if test="${page_menu_tools_sub eq 'select' || page_menu_tools_sub eq 'align-tag-sentence' || page_menu_tools_sub eq 'align-tag-word'}">  class="active" </c:if> >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                    <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                    Listar <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li <c:if test="${page_menu_tools_sub eq 'select'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/align/tag/select?id_corpus=${page_menu_id_corpus}" >
                        Etiquetas
                    </a>
                </li>
                <li <c:if test="${page_menu_tools_sub eq 'align-tag-sentence'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/align/tag/sentence?id_corpus=${page_menu_id_corpus}" >
                        Senten�as
                    </a>
                </li>
                <li <c:if test="${page_menu_tools_sub eq 'align-tag-word'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/align/tag/word?id_corpus=${page_menu_id_corpus}" >
                        Palavras
                    </a>
                </li>
            </ul>
        </li>
        <!-- Align List -->
        <!--<li <c:if test="${page_menu_tools == 'align'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}" >
                <span class="glyphicon glyphicon-align-left" title="Listar"></span>
                Alinhamentos
            </a>
        </li>-->
        <!-- Align Tag Insert -->
        <li <c:if test="${page_menu_tools_sub eq 'insert'}">class="active"</c:if> >
            <a href="<%=request.getContextPath()%>/align/tag/insert?id_corpus=${page_menu_id_corpus}">
                <span class="glyphicon glyphicon-plus" title="Inserir"></span>
                Inserir
            </a>
        </li>
        <c:if test="${page_menu_tools_sub eq 'update' || page_menu_tools_sub eq 'delete'}">
            <!-- Align Tag Update -->
            <li <c:if test="${page_menu_tools_sub eq 'update'}">class="active"</c:if> >
                <a href="<%=request.getContextPath()%>/align/tag/update?id=${align_tag.id}&id_corpus=${id_corpus}">
                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                    Alterar
                </a>
            </li>
            <!-- Align Tag Remove -->
            <li <c:if test="${page_menu_tools_sub eq 'delete'}">class="active"</c:if> >
                <a href="<%=request.getContextPath()%>/align/tag/delete?id=${align_tag.id}&id_corpus=${id_corpus}">
                    <span class="glyphicon glyphicon-trash" title="Remover"></span>
                    Remover
                </a>
            </li>
        </c:if>
    </c:if>
    <!-- /Align Tag -->
    <!-- Align Concordancer -->
    <c:if test="${page_menu_tools == 'align-concordancer'}">
        <li <c:if test="${page_menu_tools_sub == 'simple'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/align/concordancer/search?id_corpus=${page_menu_id_corpus}">
                <span class="glyphicon glyphicon-search" title="Simples"></span>
                Simples
            </a>
        </li>
        <li <c:if test="${page_menu_tools_sub == 'advanced'}"> class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/align/concordancer/advanced?id_corpus=${page_menu_id_corpus}">
                <span class="glyphicon glyphicon-zoom-in" title="Avan�ado"></span>
                Avan�ado
            </a>
        </li>
    </c:if>
    <!-- /Align Concordancer -->
</ul>
