<%-- 
    Document   : template.menu.subcorpus
    Created on : 16/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.subcorpus.*
    Variables  : subcorpus (Subcorpus)
                 page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Subcorpus -->
    <!-- Subcorpus List -->
    <li <c:if test="${page_menu_tools_sub == 'select'}">  class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/subcorpus/select?id_corpus=${page_menu_id_corpus}" >
            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
            Listar
        </a>
    </li>
    <!-- Subcorpus Insert -->
    <li <c:if test="${page_menu_tools_sub == 'insert'}">  class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/subcorpus/insert?id_corpus=${page_menu_id_corpus}" >
            <span class="glyphicon glyphicon-plus" title="Inserir"></span>
            Inserir
        </a>
    </li>
    <c:if test="${page_menu_tools_sub == 'update' || page_menu_tools_sub == 'delete'}">
        <!-- Subcorpus Update -->
        <li <c:if test="${page_menu_tools_sub == 'update'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/subcorpus/update?id_corpus=${page_menu_id_corpus}&id_subcorpus=${subcorpus.id}">
                <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                Alterar
            </a>
        </li>
        <!-- Subcorpus Remove -->
        <li <c:if test="${page_menu_tools_sub == 'delete'}">  class="active" </c:if> >
            <a href="<%=request.getContextPath()%>/subcorpus/delete?id_corpus=${page_menu_id_corpus}&id_subcorpus=${subcorpus.id}">
                <span class="glyphicon glyphicon-trash" title="Remover"></span>
                Remover
            </a>
        </li>
    </c:if>
    <!-- /Subcorpus -->
</ul>
