<%-- 
    Document   : template.menu.annotation
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.annotation.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Annotation -->
    <li <c:if test="${page_menu_tools_sub == 'view'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/annotation?id_corpus=${page_menu_id_corpus}">
            <span class="glyphicon glyphicon-info-sign" title="Anota��es"></span>
            Anotador
        </a>
    </li>
    <!-- /Annotation -->
</ul>
