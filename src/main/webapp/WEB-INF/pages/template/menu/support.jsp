<%-- 
    Document   : template.menu.support
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.support.*
    Variables  : page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ul class="nav nav-pills navbar-right">
    <!-- Support -->
    <li <c:if test="${page_menu_tools_sub == 'info'}"> class="active" </c:if> >
        <a href="<%=request.getContextPath()%>/support">
            <span class="glyphicon glyphicon-info-sign" title="Mensagens de Suporte"></span>
            Mensagens
        </a>
    </li>
    <!-- /Support -->
</ul>
