<%-- 
    Document   : template-default
    Created on : 26/05/2014
    Author     : Thiago Vieira
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            Portal Min@s
            <c:if test="${not empty page_title}"> // ${page_title}</c:if>
        </title>

        <link href="<%=request.getContextPath()%>/images/favicon-16.png" rel="icon" type="image/png" sizes="16x16">
        <link href="<%=request.getContextPath()%>/images/favicon-32.png" rel="icon" type="image/png" sizes="32x32">
        <link href="<%=request.getContextPath()%>/images/favicon-48.png" rel="icon" type="image/png" sizes="48x48">
        <link href="<%=request.getContextPath()%>/images/favicon-64.png" rel="icon" type="image/png" sizes="64x64">
        <link href="<%=request.getContextPath()%>/images/favicon-128.png" rel="icon" type="image/png" sizes="128x128">

        <link href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/bootstrap/css/docs.min.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/template-default.css" rel="stylesheet">
               
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<%=request.getContextPath()%>/bootstrap/js/html5shiv.js"></script>
          <script src="<%=request.getContextPath()%>/bootstrap/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="navbar navbar-default navbar-fixed-top visible-xs" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<%=request.getContextPath()%>">
                        <img src="<%=request.getContextPath()%>/images/logo_portal_171x50.png" class="img-logo" />
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li <c:if test="${page_menu == 'about'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/about">Sobre</a></li> -->
                        <li <c:if test="${page_menu == 'thanks'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/thanks">Agradecimentos</a></li>
                        <li <c:if test="${page_menu == 'register'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/register">Registrar-se</a></li>
                        <li <c:if test="${page_menu == 'contact'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/contact">Contato</a></li>
                        <li <c:if test="${page_menu == 'login'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/login">Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 col-md-4 sidebar">
                        <div class="col-sm-11 col-sm-offset-1 col-md-11 col-md-offset-1">
                            <a href="<%=request.getContextPath()%>">
                            <img src="<%=request.getContextPath()%>/images/logo_portal_500x146.png" class="img-responsive img-logo" />
                        </a>
                        <div class="clearfix"></div>
                        <ul class="nav nav-sidebar">
                            <!-- <li <c:if test="${page_menu == 'about'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/about">Sobre</a></li>-->
                            <li <c:if test="${page_menu == 'thanks'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/thanks">Agradecimentos</a></li>
                            <li <c:if test="${page_menu == 'register'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/register">Registrar-se</a></li>
                            <li <c:if test="${page_menu == 'contact'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/contact">Contato</a></li>
                            <li <c:if test="${page_menu == 'login'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/login">Login</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-8 col-sm-offset-4 col-md-8 col-md-offset-4 main">

                        <c:if test="${not empty alert_success}">
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                ${alert_success}
                            </div>
                        </c:if>
                        <c:if test="${not empty alert_info}">
                            <div class="alert alert-info" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                ${alert_info}
                            </div>
                        </c:if>
                        <c:if test="${not empty alert_warning}">
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                ${alert_warning}
                            </div>
                        </c:if>
                        <c:if test="${not empty alert_danger}">
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                ${alert_danger}
                            </div>
                        </c:if>

                        <c:import url="../${page}" />

                    </div>
                </div>
            </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
