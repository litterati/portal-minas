<%-- 
    Document   : template.breadcrumb.ngrama
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.ngrama.*
    Variables  : page_menu_name_corpus (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Ngram -->
        <li>N-gramas</li>
        <!-- /Ngram -->
    </ol>
</div>
