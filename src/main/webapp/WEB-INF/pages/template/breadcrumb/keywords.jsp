<%-- 
    Document   : template.breadcrumb.keywords
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.keywords.*
    Variables  : page_menu_name_corpus (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Keywords -->
        <li>Palavras-chave</li>
        <!-- /Keywords -->
    </ol>
</div>
