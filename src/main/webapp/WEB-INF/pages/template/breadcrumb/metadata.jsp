<%-- 
    Document   : template.breadcrumb.metadata
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.metadata.*
    Variables  : page_menu_name_corpus (String)
                 page_menu_tools (String)
                 page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
            <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Metadata -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Etiquetas <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li <c:if test="${page_menu_tools == 'metadata-text'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/text/metadata/select?id_corpus=${page_menu_id_corpus}">Dos Textos do ${page_menu_name_corpus}</a></li>
                <li <c:if test="${page_menu_tools == 'metadata-corpus'}">class="active"</c:if>><a href="<%=request.getContextPath()%>/corpus/metadata/select?id_corpus=${page_menu_id_corpus}">Do Corpus ${page_menu_name_corpus}</a></li>
            </ul>
        </li>
        <!-- /Metadata -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'insert'}">Inserir</c:when>
                <c:when test="${page_menu_tools_sub eq 'update'}">Alterar</c:when>
                <c:when test="${page_menu_tools_sub eq 'delete'}">Remover</c:when>
            </c:choose>
        </li>
    </ol>
</div>
