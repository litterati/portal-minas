<%-- 
    Document   : template.breadcrumb.info
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.info.*
    Variables  : page_menu_name_corpus (String)
                 page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Info -->
        <li>Estatísticas</li>
        <!-- /Info -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'info'}">Informações</c:when>
                <c:when test="${page_menu_tools_sub eq 'description'}">Descrição</c:when>
            </c:choose>
        </li>
    </ol>
</div>
