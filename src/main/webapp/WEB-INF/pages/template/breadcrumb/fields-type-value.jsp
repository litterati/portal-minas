<%-- 
    Document   : template.breadcrumb.fields-type-value
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.fields-type.value.*
    Variables  : page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <li>Gerenciar</li>
        <!-- Fields Type Value -->
        <li>Fields Type</li>
        <li>Values</li>
        <!-- /Fields Type Value -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'insert'}">Inserir</c:when>
                <c:when test="${page_menu_tools_sub eq 'update'}">Alterar</c:when>
                <c:when test="${page_menu_tools_sub eq 'delete'}">Remover</c:when>
            </c:choose>
        </li>
    </ol>
</div>
