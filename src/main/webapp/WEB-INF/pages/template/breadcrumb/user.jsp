<%-- 
    Document   : template.breadcrumb.user
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.user.*
    Variables  : page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <li>Gerenciar</li>
        <!-- User -->
        <li>Usu�rios</li>
        <!-- /User -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'insert'}">Inserir</c:when>
                <c:when test="${page_menu_tools_sub eq 'update'}">Alterar</c:when>
                <c:when test="${page_menu_tools_sub eq 'delete'}">Remover</c:when>
            </c:choose>
        </li>
    </ol>
</div>
