<%-- 
    Document   : template.breadcrumb.admin
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.admin.*
    Variables  : page_menu_option (String)
                 page_menu_tools (String)
                 page_menu_option_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <li>Principal</li>
        <!-- Admin -->
        <li>
            <c:choose>
                <c:when test="${page_menu_option eq 'home'}">Corpora</c:when>
                <c:when test="${page_menu_option eq 'download'}">Downloads</c:when>
                <c:when test="${page_menu_option eq 'login'}">PreferÍncias</c:when>
                <c:when test="${page_menu_option eq 'tools'}">Ferramentas</c:when>
            </c:choose>
        </li>
        <c:if test="${page_menu_option eq 'login' && page_menu_tools eq 'password'}"><li>Alterar Senha</li></c:if>
        <c:if test="${page_menu_option eq 'login' && page_menu_option_sub eq 'user-support'}"><li>Suporte</li></c:if>
        <!-- /Admin -->
    </ol>
</div>
