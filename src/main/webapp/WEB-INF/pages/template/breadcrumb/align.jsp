<%-- 
    Document   : template.breadcrumb.align
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.align.*
    Variables  : page_menu_name_corpus (String)
                 page_menu_tools (String)
                 page_menu_id_corpus (int)
                 page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Align -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}">
                <c:choose>
                    <c:when test="${page_menu_tools eq 'align'}">Alinhamentos</c:when>
                    <c:when test="${page_menu_tools eq 'align-tag'}">Etiquetas de Alinhamento</c:when>
                    <c:when test="${page_menu_tools eq 'align-concordancer'}">Concordanciador de Alinhamento</c:when>
                </c:choose>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <!-- Align Texts -->
                <li <c:if test="${page_menu_tools eq 'align'}">  class="active" </c:if> >
                    <a  href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}">Alinhamentos</a>
                </li>
                <!-- Align Tag -->
                <li <c:if test="${page_menu_tools eq 'align-tag'}">  class="active" </c:if> >
                    <a href="<%=request.getContextPath()%>/align/tag/select?id_corpus=${page_menu_id_corpus}">Etiquetas de Alinhamento</a>
                </li>
                <!-- Align Concordancer -->
                <li <c:if test="${page_menu_tools eq 'align-concordancer'}">  class="active" </c:if> >
                    <a  href="<%=request.getContextPath()%>/align/concordancer/search?id_corpus=${page_menu_id_corpus}">Concordanciador de Alinhamento</a>
                </li>
            </ul>
        </li>
        <!-- /Align -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'insert'}">Inserir</c:when>
                <c:when test="${page_menu_tools_sub eq 'update'}">Alterar</c:when>
                <c:when test="${page_menu_tools_sub eq 'view'}">Visualizar</c:when>
                <c:when test="${page_menu_tools_sub eq 'delete'}">Remover</c:when>
                <c:when test="${page_menu_tools_sub eq 'simple'}">Pesquisa simples</c:when>
                <c:when test="${page_menu_tools_sub eq 'advanced'}">Pesquisa avan�ada</c:when>
            </c:choose>
        </li>
    </ol>
</div>
