<%-- 
    Document   : template.breadcrumb.concordancer
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.concordancer.*
    Variables  : page_menu_name_corpus (String)
                 page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Concordancer -->
        <li>Concord�nciador</li>
        <!-- /Concordancer -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'simple'}">Pesquisa simples</c:when>
                <c:when test="${page_menu_tools_sub eq 'advanced'}">Pesquisa avan�ada</c:when>
            </c:choose>
        </li>
    </ol>
</div>
