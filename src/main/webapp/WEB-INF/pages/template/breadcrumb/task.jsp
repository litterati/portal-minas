<%-- 
    Document   : template.breadcrumb.task
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.task.*
    Variables  : page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <!-- Task -->
        <li>Tarefas</li>
        <!-- /Task -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'dashboard'}">Painel de Controle</c:when>
            </c:choose>
        </li>
    </ol>
</div>
