<%-- 
    Document   : template.breadcrumb.support
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.support.*
    Variables  : page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <li>Gerenciar</li>
        <!-- User -->
        <li>Mensagens de Suporte</li>
        <!-- /User -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'view'}">Visualizar</c:when>
            </c:choose>
        </li>
    </ol>
</div>
