<%-- 
    Document   : template.breadcrumb.access
    Created on : 13/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.access.*
    Variables  : page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <li>Gerenciar</li>
        <!-- User -->
        <li>Informações de acesso</li>
        <!-- /User -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'most-used'}">Mais usado</c:when>
                <c:when test="${page_menu_tools_sub eq 'history'}">Histórico</c:when>
            </c:choose>
        </li>
    </ol>
</div>
