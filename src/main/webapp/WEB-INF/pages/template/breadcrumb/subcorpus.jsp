<%-- 
    Document   : template.breadcrumb.subcorpus
    Created on : 16/01/2015
    Author     : Thiago Vieira
    Controller : br.project.controller.subcorpus.*
    Variables  : page_menu_name_corpus (String)
                 page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Subcorpus -->
        <li>Subcorpus</li>
        <!-- /Subcorpus -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'insert'}">Inserir</c:when>
                <c:when test="${page_menu_tools_sub eq 'update'}">Alterar</c:when>
                <c:when test="${page_menu_tools_sub eq 'delete'}">Remover</c:when>
                <c:when test="${page_menu_tools_sub eq 'info'}">Informações</c:when>
            </c:choose>
        </li>
    </ol>
</div>
