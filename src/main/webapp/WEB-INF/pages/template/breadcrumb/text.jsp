<%-- 
    Document   : template.breadcrumb.text
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.text.*
    Variables  : page_menu_name_corpus (String)
                 page_menu_tools_sub (String)
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
    <ol class="breadcrumb">
        <c:import url="helper/corpus-list-dropdown.jsp" />
        <li>${page_menu_name_corpus}</a></li>
        <c:import url="helper/tools-list-dropdown.jsp" />
        <!-- Texts -->
        <li>Textos</li>
        <!-- /Texts -->
        <li>
            <c:choose>
                <c:when test="${page_menu_tools_sub eq 'select'}">Listar</c:when>
                <c:when test="${page_menu_tools_sub eq 'insert'}">Inserir</c:when>
                <c:when test="${page_menu_tools_sub eq 'update'}">Alterar</c:when>
                <c:when test="${page_menu_tools_sub eq 'view'}">Visualizar</c:when>
                <c:when test="${page_menu_tools_sub eq 'delete'}">Remover</c:when>
                <c:when test="${page_menu_tools_sub eq 'info'}">Informações</c:when>
                <c:when test="${page_menu_tools_sub eq 'wizard'}">Assistente</c:when>
            </c:choose>
        </li>
    </ol>
</div>
