<%-- 
    Document   : template
    Created on : 30/05/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.*
    Variables  : page_menu (String)
                 page_menu_id_corpus (String)
                 page_menu_name_corpus (String)
                 page_menu_option (String)
                 page_menu_option_sub (String)
                 page_menu_tools (String)
                 page_menu_tools_sub (String)
                 page_title (String)
                 page_subtitle (String)
                 page_menu_corpus (String)
                 page (String)
                 user_permission_level (int)
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            Portal Min@s
            <c:if test="${not empty page_title}"> // ${page_title}</c:if>
            </title>

        <link href="<%=request.getContextPath()%>/images/favicon-16.png" rel="icon" type="image/png" sizes="16x16">
        <link href="<%=request.getContextPath()%>/images/favicon-32.png" rel="icon" type="image/png" sizes="32x32">
        <link href="<%=request.getContextPath()%>/images/favicon-48.png" rel="icon" type="image/png" sizes="48x48">
        <link href="<%=request.getContextPath()%>/images/favicon-64.png" rel="icon" type="image/png" sizes="64x64">
        <link href="<%=request.getContextPath()%>/images/favicon-128.png" rel="icon" type="image/png" sizes="128x128">

        <link href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/bootstrap/css/docs.min.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/template-admin.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<%=request.getContextPath()%>/bootstrap/js/html5shiv.js"></script>
          <script src="<%=request.getContextPath()%>/bootstrap/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<%=request.getContextPath()%>/admin/home">
                        <img src="<%=request.getContextPath()%>/images/logo_portal_171x50.png" class="img-logo" />
                    </a>
                </div>
                <div class="navbar-collapse collapse">

                    <ul class="nav navbar-nav">
                        <li  <c:if test="${page_menu_option eq 'home'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/admin/home">Início</a></li>
                        <li  <c:if test="${page_menu_option eq 'download'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/admin/download">Downloads</a></li>

                        <c:if test="${not empty page_menu_corpus}">
                            <li class='dropdown <c:if test="${page_menu_option eq 'corpus'}"> active </c:if> '>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Corpus <b class="caret"></b></a>
                                <!-- List of avalible corpora -->
                                <ul class="dropdown-menu">
                                    <c:forEach var="corpus" items="${page_menu_corpus}">
                                        <c:choose>
                                            <c:when test="${page_menu_option eq 'corpus'}">
                                                <c:set var="corpus_page_menu_option_sub" value="corpus-${corpus.id}" />
                                                <li <c:if test="${page_menu_option_sub eq corpus_page_menu_option_sub}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${corpus.id}">${corpus.name}</a></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li><a href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${corpus.id}">${corpus.name}</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </ul>
                                <!-- /List of avalible corpora -->
                            </li>
                        </c:if>

                        <c:if test="${user_permission_level >= 2}">
                            <li class='dropdown <c:if test="${page_menu_option eq 'manage'}"> active </c:if> '>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gerenciar <b class="caret"></b></a>  
                                <ul class="dropdown-menu">
                                    <li <c:if test="${page_menu_option_sub eq 'corpus'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/corpus/select">Corpus</a></li>
                                    <li <c:if test="${page_menu_option_sub eq 'access'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/access/analysis">Informações de acesso</a></li>
                                    <li <c:if test="${page_menu_option_sub eq 'support'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/support/select">Mensagens de suporte</a></li>
                                    <c:if test="${user_permission_level == 3}">
                                        <li <c:if test="${page_menu_option_sub eq 'fields-type'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/fields-type/select">Fields Type</a></li>
                                        <li <c:if test="${page_menu_option_sub eq 'user'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/user/select">Usuários</a></li>
                                    </c:if>
                                </ul>
                            </li>
                        </c:if>
                            
                        <li  <c:if test="${page_menu_option eq 'task'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/task/select">Tarefas</a></li>
                            
                    </ul>
                                
                                

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown  <c:if test="${page_menu_option eq 'login'}"> active </c:if> ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bem vindo(a), ${sessionScope.userLoged.login} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li <c:if test="${page_menu_option_sub eq 'settings'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/admin/settings"><span class="glyphicon glyphicon-cog"></span> Preferências</a></li>
                                <li <c:if test="${page_menu_option_sub eq 'user-support'}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/admin/support"><span class="glyphicon glyphicon-envelope"></span> Suporte</a></li>
                                <li class="divider"></li>
                                <li><a href="<%=request.getContextPath()%>/account/logout"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container">

            <c:if test="${not empty page_breadcrumb}">
                <!-- BREADCRUMB -->
                <c:import url="${page_breadcrumb}" />
                <!-- /BREADCRUMB -->
            </c:if>
                
            <c:if test="${not empty page_menu}">
                <!-- MENU -->
                <c:import url="${page_menu}" />
                <!-- /MENU -->
            </c:if>

            <c:if test="${not empty page_title}">
                <!-- TITLE -->
                <h1 class="page-header">
                    ${page_title} 
                    <c:if test="${not empty page_subtitle}"><small>${page_subtitle}</small></c:if>
                </h1>
                <!-- /TITLE -->
            </c:if>

            <!-- ALERT -->
            <c:if test="${not empty alert_success}">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    ${alert_success}
                </div>
            </c:if>
            <c:if test="${not empty alert_info}">
                <div class="alert alert-info" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    ${alert_info}
                </div>
            </c:if>
            <c:if test="${not empty alert_warning}">
                <div class="alert alert-warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    ${alert_warning}
                </div>
            </c:if>
            <c:if test="${not empty alert_danger}">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    ${alert_danger}
                </div>
            </c:if>
            <!-- /ALERT -->

            <c:import url="../${page}" />

        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<%=request.getContextPath()%>/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
