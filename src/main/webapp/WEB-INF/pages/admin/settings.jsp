<%-- 
    Document   : settings
    Created on : 15/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AdminSettingsController
    Variables  : user (User)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form role="form" method="post" action="<%=request.getContextPath()%>/admin/settings">
    <div class="form-group has-feedback">
        <label for="name">Nome</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Seu nome" value="${user.name}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Seu email" value="${user.email}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" placeholder="Seu login" value="${user.login}" required readonly>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="password">Senha</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Sua senha" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
        <p class="help-block">A senha � necess�ria para alterar os dados, mas ele n�o � alterada. 
            <a href="<%=request.getContextPath()%>/admin/settings/change-password">Para mudar a senha clique aqui.</a>
        </p>
    </div>
    <button type="submit" class="btn btn-primary">Salvar</button>
</form>

