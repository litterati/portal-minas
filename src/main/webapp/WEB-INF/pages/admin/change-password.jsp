<%-- 
    Document   : change-password
    Created on : 04/09/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AdminChangePasswordController
    Variables  : user (User)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form role="form" method="post" action="<%=request.getContextPath()%>/admin/settings/change-password">
    <div class="form-group hidden">
        <label for="login">Login</label> 
        <input type="text" name="login" value="${user.login}" readonly class="form-control"/>
    </div>
    <div class="form-group has-feedback">
        <label for="old-password">Senha Antiga</label>
        <input type="password" class="form-control" id="old-password" name="old-password" placeholder="Senha antiga" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="new-password">Nova Senha</label>
        <input type="password" class="form-control" id="new-password" name="new-password" placeholder="Nova senha" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="new-password2">Repetir a Nova Senha</label>
        <input type="password" class="form-control" id="new-password2" name="new-password2" placeholder="Repetir a nova senha" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <button type="submit" class="btn btn-primary">Salvar</button>
</form>
