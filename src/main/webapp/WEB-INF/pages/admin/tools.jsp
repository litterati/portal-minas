<%-- 
    Document   : tools
    Created on : 20/12/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AdminToolsController 
    Variables  : corpus (Corpus)
--%>

<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/align.png" class="img-responsive">
            <div class="caption">
                <h3>Alinhador</h3>
                <p>Essa ferramenta permite alinhar automaticamente pares de textos, editar de forma visual esse alinhamento, rotular os alinhamentos de palavras e senten�as com etiquetas, e fazer pesquisas sobre os pares de textos.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/align/select?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a> 
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/concordancer.png" class="img-responsive">
            <div class="caption">
                <h3>Concord�nciador</h3>
                <p>Com essa ferramenta � possivel fazer pesquisa sobre o textos inseridos no corpus e aplicar filtros para refinar a busca.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/info.png" class="img-responsive">
            <div class="caption">
                <h3>Estat�sticas</h3>
                <p>Exibe a quantidade de tokens e metatokens de todos os textos do corpus.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/info?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a> 
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/metadata-corpus.png" class="img-responsive">
            <div class="caption">
                <h3>Etiquetas do Corpus</h3>
                <p>Gerenciamento das etiquetas que rotulam os textos.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/corpus/metadata/select?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/metadata-text.png" class="img-responsive">
            <div class="caption">
                <h3>Etiquetas dos Textos</h3>
                <p>Gerenciamento das etiquetas que rotulam o corpus.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/text/metadata/select?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/frequency.png" class="img-responsive">
            <div class="caption">
                <h3>Frequ�ncias</h3>
                <p>Listagem de todos os tokens e o n�mero de suas ocorr�ncias em todo o corpus.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/frequency?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/ngrama.png" class="img-responsive">
            <div class="caption">
                <h3>N-gramas</h3>
                <p>Extra��o autom�tica estat�stica de bigramas e trigramas do corpus.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/ngrama?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/keywords.png" class="img-responsive">
            <div class="caption">
                <h3>Palavras-chave</h3>
                <p>Extra��o autom�tica das principais palavras de um texto.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/keywords/lda?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<%=request.getContextPath()%>/images/tools/text.png" class="img-responsive">
            <div class="caption">
                <h3>Textos do Corpus '${corpus.name}'</h3>
                <p>Gerenciamento do texto do corpus: inser��o, altera��o, remo��o, visualiza��o e informa��es.</p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/text/select?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </div>
    </div>
</div>
