<%-- 
    Document   : home
    Created on : 14/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AdminHomeController 
    Variables  : corpora (List<Corpus>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${corpora[0].id > 0}">
    <div class="row">
        <c:forEach var="corpus" items="${corpora}" varStatus="status">
            <c:if test="${(status.index % 3) == 0}">
                </div><div class="row">
            </c:if>
            <div class="col-4 col-sm-4 col-lg-4">
                <h2>${corpus.name}</h2>
                <p>
                    ${fn:substring(fn:replace(corpus.description, '<.*?>', ''), 0, 200)}
                    <c:if test="${fn:length(corpus.description) > 200}">
                        <a href="<%=request.getContextPath()%>/info/description?id_corpus=${corpus.id}">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </c:if>
                </p>
                <p>
                    <a class="btn btn-default" href="<%=request.getContextPath()%>/concordancer/search?id_corpus=${corpus.id}" role="button">Acessar &raquo;</a>
                </p>
            </div>
        </c:forEach>
    </div>
</c:if>
