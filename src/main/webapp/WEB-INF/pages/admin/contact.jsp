<%-- 
    Document   : contact
    Created on : 15/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AdminSuportController
    Variables  : name (String) // when error
                 email (String) // when error
                 subject (String) // when error
                 message (String) // when error
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<form role="form" method="post" action="<%=request.getContextPath()%>/admin/support">
    <div class="form-group has-feedback">
        <label for="name">Nome</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Seu nome" value="${name}" readonly>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Seu email" value="${email}" readonly>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>

    <div class="form-group has-feedback">
        <label for="subject">Assunto</label>
        <input type="text" class="form-control" id="subject" name="subject" placeholder="Assunto" value="${subject}" required>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="message">Mensagem</label>
        <textarea class="form-control" rows="3" id="message" name="message" placeholder="Mensagem" required>${message}</textarea>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
</form>
