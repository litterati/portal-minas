<%-- 
    Document   : download
    Created on : 06/10/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AdminDownloadController
--%>

<p>Aqui s�o listados os recursos e ferramentas utilizadas no Portal Min@s que podem ser baixados e utilizados pelos usu�rios de acordo com as licen�as de uso de cada uma deles.</p>

<h2>Alinhamentos</h2>
<p>O alinhamento de senten�as processa um par de textos paralelos para inserir indica��es de qual senten�a fonte � a tradu��o de qual senten�a alvo. J� o processa um par de textos paralelos para inserir indica��es de qual palavra (ou palavras) fonte � a tradu��o de qual palavra (ou palavras) alvo. Para alinhar automaticamente as palavras de dois textos paralelos, esses textos j� devem estar alinhados sentencialmente. <a href="http://www.lalic.dc.ufscar.br/portal">(PorTAl)</a>.</p>

<ul class="list-unstyled">
    <li>Senten�as: <a href="http://www.lalic.dc.ufscar.br/portal">TCAlign</a></li>
    <li>Palavras: <a href="http://www.lalic.dc.ufscar.br/portal">GIZA++</a> (modelos treinados em Portugu�s-Ingl�s, Ingl�s-Portugu�s, Portugu�s-Espanhol e Espanhol-Portugu�s)</li>
</ul>

<h2>Apache openNLP</h2>
<p>A biblioteca openNLP � um conjunto de ferramentas de aprendizado de m�quina para o processamento de l�ngua natural. Ele suporta as tarefas mais comuns de PLN como tokeniza��o, segmenta��o de senten�as e tagger part-of-speech utilizados no Portal Min@s al�m de outros <a href="https://opennlp.apache.org">(Apache openNLP)</a>.</p>

<ul class="list-unstyled">
    <li>Ingl�s: <a href="http://opennlp.sourceforge.net/">en-token.bin</a> (XX entradas)</li>
    <li>Espanhol: <a href="http://opennlp.sourceforge.net/">es-token.bin</a> (XX entradas)</li>
    <li>Portugu�s: <a href="http://opennlp.sourceforge.net/">pt-token.bin</a> (XX entradas)</li>
</ul>
<ul class="list-unstyled">
    <li>Ingl�s: <a href="http://opennlp.sourceforge.net/">en-sent.bin</a> (XX entradas)</li>
    <li>Espanhol: <a href="http://opennlp.sourceforge.net/">es-sent.bin</a> (XX entradas)</li>
    <li>Portugu�s: <a href="http://opennlp.sourceforge.net/">pt-sent.bin</a> (XX entradas)</li>
</ul>
<ul class="list-unstyled">
    <li>Ingl�s: <a href="http://opennlp.sourceforge.net/">en-tagger.bin</a> (XX entradas)</li>
    <li>Espanhol: <a href="http://opennlp.sourceforge.net/">es-tagger.bin</a> (XX entradas)</li>
    <li>Portugu�s: <a href="http://opennlp.sourceforge.net/">pt-tagger.bin</a> (XX entradas)</li>
</ul>


<h2>Lista de Lemas</h2>
<p>Lema � a forma can�nica de uma palavra ou um conjunto de palavras. Ele possui not�vel import�ncia em linguagens com grande quantidade de inflex�es. Por exemplo "corri", "corremos", "correu" e "correndo" s�o formas do mesmo lexema, sendo "correr" o lema <a href="http://en.wikipedia.org/wiki/Lemma_(morphology)">(Wikip�dia)</a>.</p>

<ul class="list-unstyled">
    <li>Ingl�s: <a href="#">en.txt</a> (XX entradas)</li>
    <li>Espanhol: <a href="#">es.txt</a> (XX entradas)</li>
    <li>Portugu�s: <a href="#">pt.txt</a> (XX entradas)</li>
</ul>

<h2>Stoplist</h2>
<p>Stoplist � uma lista de palavras comuns, como "um", "o", e "e" que, por qualquer raz�o especial, devem ser ignoradas ou contornadas por uma determinada opera��o de processamento <a href="http://en.wiktionary.org/wiki/stop_list">(Wiktionary)</a>.</p>

<ul class="list-unstyled">
    <li>Ingl�s: <a href="<%=request.getContextPath()%>/admin/download">en.txt</a> (XX entradas)</li>
    <li>Espanhol: <a href="<%=request.getContextPath()%>/admin/download">es.txt</a> (XX entradas)</li>
    <li>Portugu�s: <a href="<%=request.getContextPath()%>/admin/download">pt.txt</a> (XX entradas)</li>
</ul>

