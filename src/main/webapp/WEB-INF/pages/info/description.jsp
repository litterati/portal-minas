<%-- 
    Document   : description
    Created on : 29/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.info.DescriptionController
    Variables  : corpus (Corpus)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div>${corpus.description}</div>
