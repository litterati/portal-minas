<%-- 
    Document   : info
    Created on : 11/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.InfoController
    Variables  : texts (List<Text>)
                 size (int[][2])
                 total_size (int[2])
                 languages (HashMap<String, Integer>)
                 total_languages (int)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th></th>
            <th>Texto <a href="<%=request.getContextPath()%>/info?id_corpus=${page_menu_id_corpus}&order=title" title="Ordernar por t�tulo"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Idioma <a href="<%=request.getContextPath()%>/info?id_corpus=${page_menu_id_corpus}&order=language" title="Ordernar por idioma"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th># de tokens <a href="<%=request.getContextPath()%>/info?id_corpus=${page_menu_id_corpus}&order=tokens" title="Ordernar por n�mero de palavras" class="hidden"><span class="glyphicon glyphicon-sort-by-order"></span></a></th>
            <th># de meta tokens</th>
            <th># total de tokens</th>
        </tr>
    </thead>
    <c:if test="${texts[0].id > 0}">
        <tbody>
            <c:forEach var="text" items="${texts}" varStatus="status">
                <tr>
                    <td width="20">
                        <!-- Visualizar -->
                        <a href="<%=request.getContextPath()%>/text/view?id_text=${text.id}&id_corpus=${text.idCorpus}" title="Visualizar">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                    <td>${text.title}</td>
                    <td>${text.language}</td>
                    <td>${size[status.index][0]}</td>
                    <td>${size[status.index][1]}</td>
                    <td>${size[status.index][0] + size[status.index][1]}</td>
                </tr>
            </c:forEach>
            <tr>
                <td></td>
                <td>Total: ${fn:length(texts)}</td>
                <td>--</td>
                <td>${total_size[0]}</td>
                <td>${total_size[1]}</td>
                <td>${total_size[0] + total_size[1]}</td>
            </tr>
        </tbody>
    </c:if>
</table>
            
<c:if test="${!(texts[0].id > 0)}">
        <div class="alert alert-warning" role="alert">N�o h� arquivos</div>
</c:if>
        
<c:if test="${not empty languages}">
    <table class="table table-hover table-striped table-responsive">
        <thead>
            <tr>
                <th>Idioma</th>
                <th>Quantidade</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="lang" items="${languages}">
                <tr>
                    <td>${lang.key}</td>
                    <td>${lang.value}</td>
                </tr>
            </c:forEach>
            <tr>
                <td>Total: ${fn:length(languages)}</td>
                <td>${total_languages}</td>
            </tr>
        </tbody>
    </table>
</c:if>