<%-- 
    Document   : list-corpus
    Created on : 29/05/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.CorpusSelectController
    Variables  : page_menu_tools (String)
                 corpora (List<Corpus>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="3"></th>
            <th>Nome</th>
            <th>Textos</th>
            <th>Etiquetas</th>
        </tr>
    </thead>
    <c:if test="${corpora[0].id > 0}">
        <tbody>
            <c:forEach var="corpus" items="${corpora}">
                <tr id="row${corpus.id}">
                    <td width="20">
                        <!-- Alterar -->
                        <a href="<%=request.getContextPath()%>/corpus/update?id_corpus=${corpus.id}" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Info -->
                        <a href="<%=request.getContextPath()%>/info?id_corpus=${corpus.id}" title="Informa��es">
                            <span class="glyphicon glyphicon-info-sign"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Remover -->
                        <a href="#" data-toggle="modal" data-target="#modalDelete${corpus.id}" title="Remover">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="modalDelete${corpus.id}" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title">Confirma��o</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Tem certeza que deseja remover o corpus ${corpus.name}?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${corpus.id}, '${corpus.name}');">Confirmar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </td>
                    <td>${corpus.name}</td>
                    <td>
                        <a href="<%=request.getContextPath()%>/text/select?id_corpus=${corpus.id}" data-toggle="modal">
                            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                            Lista de textos
                        </a>
                    </td>
                    <td>
                        <a href="<%=request.getContextPath()%>/corpus/metadata/select?id_corpus=${corpus.id}">
                            <span class="glyphicon glyphicon-th-list" title="Listar"></span>
                            Etiquetas
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </c:if>
</table>

<c:choose>
    <c:when test="${corpora[0].id > 0}">
        <span class="label label-warning">Total: ${fn:length(corpora)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� corpus</div>
    </c:otherwise>
</c:choose>
        
<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id, name) {
        $.post("<%=request.getContextPath()%>/corpus/delete", {id_corpus: id, name: name, ajax: true})
                .done(function(data) {
                    $( "#row" + id ).hide();
                })
                .fail(function() {
                    $( "#row" + id ).addClass("danger");
                });
    }
</script>