<%-- 
    Document   : pages.corpus.metadata.form
    Created on : 30/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.CorpusMetadataInsertController
                 br.project.controller.CorpusMetadataUpdateController
                 br.project.controller.CorpusMetadataDeleteController
    Variables  : corpus_metadata (CorpusTags)
                 fields_type_list (List<FieldsType>)
                 page_menu_tools_sub (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/corpus/metadata/${page_menu_tools_sub}">
    <input type="hidden" name="id_corpus" value="${param.id_corpus}" />
    <c:if test="${not empty corpus_metadata.id}">
        <div class="form-group">
            <label for="id_tags_corpus">Id</label> 
            <input type="text" name="id_tags_corpus" value="${corpus_metadata.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group has-feedback">
        <label for="metadata_CorpusTags">Etiqueta</label> 
        <input type="text" name="metadata_CorpusTags" id="metadata_CorpusTags" value="${corpus_metadata.tag}" class="form-control" />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label class="control-label" for="id_field_type_CorpusTags">Tipo</label>
        <select class="form-control" name="id_field_type_CorpusTags" id="id_field_type_CorpusTags">
            <c:forEach var="ft" items="${fields_type_list}">
                <option <c:if test="${corpus_metadata.idFieldType == ft.id}"> selected </c:if> value="${ft.id}">${ft.value}</option>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <label class="control-label" for="radio">Obrigat�rio</label>
        <label class="radio">
            <input type="radio" name="is_required_CorpusTags" id="is_required_CorpusTags" <c:if test="${corpus_metadata.isRequired != true}"> checked </c:if> value="false" />
            <label for='optRequiredF'>N�o</label>
        </label>
        <label class="radio">
            <input type="radio" name="is_required_CorpusTags" id="is_required_CorpusTags" <c:if test="${corpus_metadata.isRequired == true}"> checked </c:if> value="true" />
            <label for="optRequiredT">Sim</label>
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/corpus/metadata/select?id_corpus=${param.id_corpus}" class="btn btn-default">Voltar</a>    
    </div>
</form>