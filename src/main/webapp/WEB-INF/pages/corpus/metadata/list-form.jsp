<%-- 
    Document   : list-form
    Created on : 12/08/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.CorpusMetadataSelectController
    Variables  : error (String)
                 param.name_corpus (String)
                 wizard (boolean)
                 listCorpusMetadata ()
                 lstFieldsType ()
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="tags-corpus">

    <!-- ERROR, WHEN IT EXIST -->
    <c:if test="${not empty error}">
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            ${error}
        </div>
    </c:if>
    <!-- END ERROR, WHEN IT EXIST -->

    <!-- METADATA LIST -->
    <table class="table table-hover table-striped table-responsive" id="tblDataCorpus">
        <thead>
            <tr>
                <th colspan="2"></th>
                <!--<th>Id</th>-->
                <th>Etiqueta</th>
                <th>Tipo</th>
                <th>Obrigat�rio</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:if test="${listCorpusMetadata[0].id > 0}">
                <tr id="rowForm" class="hidden">
                    <td colspan="2"></td>
                    <!--<td>
                        <input type='hidden' id='id_tags_corpus' class='form-control' value = '0' />
                        <input type='hidden' id='type' value = 'insert' />
                    </td>-->
                    <td valign='middle'>
                        <input type='text' id='metadata_CorpusTags' class='form-control'/>
                    </td>
                    <td>
                        <select name='cmbFieldType' id='cmbFieldType' class='form-control'>
                            <option value= '0' selected>Escolha um tipo</option>
                            <c:forEach var="fieldType" items="${lstFieldsType}">
                                <option value= '<c:out value='${fieldType.id}'/>'><c:out value='${fieldType.value}'/></option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <label class='radio'>
                            <input type='radio' name='optionsRadios2' id='optRequiredF' checked />
                            <label for='optRequiredF'>N�o</label>
                        </label>
                        <label class='radio'>
                            <input type='radio' name='optionsRadios2' id='optRequiredT' />
                            <label for='optRequiredT'>Sim</label>
                        </label>
                    </td>
                    <td>
                        <button class='btn btnSave'>Salvar</button>&nbsp;
                        <button class= 'btn btnCancel'>Cancelar</button>
                    </td>
                </tr>
                <c:forEach var="corpusMetadata" items="${listCorpusMetadata}">
                    <tr id="row${corpusMetadata.id}">
                        <td width="20">
                            <!-- Alterar -->
                            <a href="<%=request.getContextPath()%>/corpus/metadata/update?id=${corpusMetadata.id}&id_corpus=${param.id_corpus}" title="Alterar">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>
                        <td width="20">
                            <!-- Remover -->
                            <a href="#" data-toggle="modal" data-target="#modalDelete${corpusMetadata.id}" title="Remover">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                            <!-- modal -->
                            <div class="modal fade" id="modalDelete${corpusMetadata.id}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                            <h4 class="modal-title">Confirma��o</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Tem certeza que deseja remover a etiqueta '${corpusMetadata.tag}'?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${corpusMetadata.id});">Confirmar</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </td>
                        <!--<td>${corpusMetadata.id}</td>-->
                        <td>${corpusMetadata.tag}</td>
                        <td>${corpusMetadata.fieldsType.value}</td>
                        <td>
                            <c:choose>
                                <c:when test="${corpusMetadata.isRequired == 'true'}">Sim</c:when>
                                <c:otherwise>N�o</c:otherwise>
                            </c:choose>
                        </td>
                        <td></td>
                    </tr>
                </c:forEach>
            </c:if>
        </tbody>
    </table>
    <!-- END METADATA LIST -->

    <c:choose>
        <c:when test="${listCorpusMetadata[0].id > 0}">
            <span class="label label-warning">Total: ${fn:length(listCorpusMetadata)}</span>
        </c:when>
        <c:otherwise>
            <div class="alert alert-warning" role="alert">N�o h� etiquetas do corpus</div>
        </c:otherwise>
    </c:choose>

    <input type="hidden" id="name_corpus" value="${param.name_corpus}" />
    <input type="hidden" id="wizard_corpus" value="${wizard}" />

    <div id="dataConfirmModalCorpus" class="modal hide fade" role="dialog" aria-labelledby="dataConfirmLabelCorpus" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
            <h3 id="dataConfirmLabelCorpus"> Confirma��o</h3>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-danger" id="dataConfirmOKCorpus">OK</button>
        </div>
    </div>
</div>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id) {
        $.post("<%=request.getContextPath()%>/corpus/metadata/delete", {id_tags_corpus: id, ajax: true})
                .done(function(data) {
                    $( "#row" + id ).hide();
                })
                .fail(function() {
                    $( "#row" + id ).addClass("danger");
                });
    }

    $("#btnAddCorpus").bind("click", function() {
        $("#rowForm").removeClass("hidden");
        
        $('#metadata_CorpusTags').focus();
        $(".btnSave").bind("click", Save);
        $(".btnCancel").bind("click", Cancel);
        $("#btnAddCorpus").attr("disabled", "disabled");
    });

    function Save() {
        var id_corpus = ${param.id_corpus};
        var metadata = $("#metadata_CorpusTags").val();
        var is_required = $('#optRequiredT').is(':checked');
        var id_field_type = $("#cmbFieldType").val();

        if ($.trim(metadata) === "") {
            alert("Informe o nome da etiqueta");
            $("#metadata_CorpusTags").focus();
            return;
        }

        $.post("<%=request.getContextPath()%>/corpus/metadata/insert",
                {id_corpus: id_corpus, metadata_CorpusTags: metadata, is_required_CorpusTags: is_required, id_field_type_CorpusTags: id_field_type, wizard: "wizard", ajax: true})
                .done(function(data) {
                    $( "#rowForm").addClass("success");
                    //$("#metadata_CorpusTags").val('');
                    //$("#cmbFieldType").val('');
                })
                .fail(function() {
                    $( "#rowForm" ).addClass("danger");
                });
    }

    function Cancel() {
        $("#rowForm").addClass("hidden");
        
        $(".btnSave").unbind("click", Save);
        $(".btnCancel").unbind("click", Cancel);
        $("#btnAddCorpus").removeAttr("disabled", "disabled");
    }
</script>
