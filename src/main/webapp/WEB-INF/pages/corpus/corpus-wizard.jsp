<%-- 
    Document   : corpus-wizard
    Created on : 06/09/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.CorpusInsertController
    Variables  : corpus (Corpus)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="<%=request.getContextPath()%>/plugin/smartWizard/smart_wizard.css" rel="stylesheet">

<div id="wizard" class="swMain">
    <ul class="anchor">
        <li>
            <a href="#step-1" id="1" class="selected">
                <span class="stepNumber">1</span>
                <span class="stepDesc">Passo 1<br /><small>Novo Corpus</small></span>
            </a>
        </li>
        <li>
            <a href="#step-2" id="2" class="disabled">
                <span class="stepNumber">2</span>
                <span class="stepDesc">Passo 2<br /><small>Etiquetas Corpus</small></span>
            </a>
        </li>
        <li>
            <a href="#step-3" id="3" class="disabled">
                <span class="stepNumber">3</span>
                <span class="stepDesc">Passo 3<br /><small>Etiquetas Textos</small></span>        
            </a>
        </li>
        <li>
            <a href="#step-4" id="4" class="disabled">
                <span class="stepNumber">4</span>
                <span class="stepDesc">Passo 4<br /><small>Inserir Etiquetas</small></span>                 
            </a>
        </li>
        <li>
            <a href="#step-5" class="disabled">
                <span class="stepNumber">5</span>
                <span class="stepDesc">Passo 5<br /><small>Envio dos textos</small></span>         
            </a>
        </li>
    </ul>
    <div class="stepContainer">
        <div id="step-1">
            <h2 class="StepTitle">Passo 1 Adicione Nome do Corpus</h2>
            <!-- step-1 content --> <br>
            <div id="newCorpus">
                <form id="wizardInsertCorpus">
                    <div class="form-group">
                        <label class="control-label" for="id_corpus">Id</label> 
                        <input type="text" name="id_corpus" id="id_corpus" value="${corpus.id}" readonly class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="name">Nome</label> 
                        <input type="text" name="name" id="name" value="${corpus.name}" class="form-control" required maxlength="200" />
                    </div>
                </form>
            </div>
        </div>
        <div id="step-2">
            <h2 class="StepTitle">Passo 2 Configura��o de etiquetas do corpus</h2>
            <!-- step-2 content --><br>
            <div id="metadataCorpus"></div>
        </div>                     
        <div id="step-3">
            <h2 class="StepTitle">Passo 3 Configura��o de etiquetas dos textos</h2>  
            <!-- step-3 content --><br>
            <div id="metadataText"></div>
        </div>
        <div id="step-4">
            <h2 class="StepTitle">Passo 4 Inser��o de etiquetas do corpus</h2>  
            <!-- step-4 content -->
            <div id="editCorpus"></div>
        </div>
        <div id="step-5">
            <h2 class="StepTitle">Passo 5 Envio dos textos</h2>  
            <!-- step-5 content --><br>
            Clique no bot�o finalizar para submiss�o do(s) texto(s).
        </div>
    </div>
</div>

<script src="<%=request.getContextPath()%>/js/jquery-1.4.2.min.js"></script>
<script src="<%=request.getContextPath()%>/plugin/smartWizard/jquery.smartWizard-2.0.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        // Smart Wizard        
        $('#wizard').smartWizard({
            // Properties
            labelNext: 'Pr�ximo', // label for Next button
            labelPrevious: 'Anterior', // label for Previous button
            labelFinish: 'Finalizar', // label for Finish button

            // Events
            onShowStep: showAStepCallback,
            onLeaveStep: leaveAStepCallback,
            onFinish: onFinishCallback
        });


        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel'); // Get the current step number

            return validateSteps(step_num); // return false to stay on step and true to continue navigation
        }

        function showAStepCallback(obj) {

            var stepNumber = obj.attr('rel');

            $(".buttonPrevious").hide();

            if (stepNumber == 1) {
                if ($("#id_corpus").val() == null) {
                    showScreenCorpus(); //Show screen to insert corpus
                }
            }

            if (stepNumber == 2) {
                if ($("#id_corpus").val() == null || $("#id_corpus").val() == '') {
                    saveCorpus($("#name").val()); //Save corpus
                }

                showScreenCorpusMetadata(); //Screen to create corpus metadata
            }

            if (stepNumber == 3) {

                showScreenTextMetadata(); //Screen to create text metadata
            }

            if (stepNumber == 4) {
                showScreenUpdateCorpus(); //Screen to update corpus metadata
            }

            if (stepNumber == 5) {
                insertCorpusMetadata(); //Save corpus metadata
            }

        }

        function onFinishCallback() {
            var corpus = $("#name").val();
            var id_corpus = $("#id_corpus").val();

            //Screen to send texts
            window.location.replace("<%=request.getContextPath()%>/text/wizard?id_corpus=" + id_corpus);

        }

        // Your Step validation logic
        function validateSteps(stepnumber) {
            var isStepValid = true;
            var showMessage = "";


            if (stepnumber == 1) {
                if ($("#name").val().length == 0) {
                    isStepValid = false;
                    showMessage = "Informe o nome do corpus para prosseguir.";
                }
            }
            if (stepnumber == 2) {

                if (!existCorpusMetadata()) { //$('#tblDataCorpus tr').length < 2){
                    isStepValid = false;
                    showMessage = "Adicione etiquetas ao corpus para prosseguir.";
                }
            }
            if (stepnumber == 3) {

                if (!existTextMetadata()) {//$('#tblDataText tr').length < 2){
                    isStepValid = false;
                    showMessage = "Adicione etiquetas dos textos do corpus para prosseguir.";
                }
            }

            if (!isStepValid) {

                $('#wizard').smartWizard('setError', {stepnum: stepnumber, iserror: true});
                $('#wizard').smartWizard('showMessage', showMessage);
                return false;

            } else {
                $('#wizard').smartWizard('setError', {stepnum: stepnumber, iserror: false});
                $("div.msgBox").css("display", "none");
                return true;
            }

            return isStepValid;

        }

        //Screen to insert corpus
        function showScreenCorpus() {

            jQuery.ajax({
                type: "GET",
                url: "<%=request.getContextPath()%>/corpus/insert?wizard=true",
                success: function(data) {
                    $("#newCorpus").html(data);
                },
                async: false
            });
        }

        //Screen to save corpus
        function saveCorpus() {

            jQuery.ajax({
                type: "POST",
                url: "<%=request.getContextPath()%>/corpus/insert?type=insert&wizard=true"
                        + '&name=' + $("#name").val(),
                success: function(data) {
                    $("#id_corpus").val(data.toString());
                },
                async: false
            });

            $("#name").prop('disabled', true);
        }

        //Screen to list and manage text metadata
        function showScreenTextMetadata() {
            jQuery.ajax({
                type: "GET",
                url: "<%=request.getContextPath()%>/text/metadata/select?wizard=true"
                        + '&id_corpus=' + $("#id_corpus").val(),
                success: function(data) {
                    $("#metadataText").html(data);
                },
                async: false
            });

        }

        //Screen to list and manage corpus metadata
        function showScreenCorpusMetadata() {

            jQuery.ajax({
                type: "GET",
                url: "<%=request.getContextPath()%>/corpus/metadata/select?wizard=true"
                        + '&id_corpus=' + $("#id_corpus").val(),
                success: function(data) {
                    $("#metadataCorpus").html(data);
                },
                async: false
            });
        }

        //Screen to insert corpus metadata (update corpus)
        function showScreenUpdateCorpus() {

            jQuery.ajax({
                type: "GET",
                url: "<%=request.getContextPath()%>/corpus/update?type=update&wizard=true"
                        + '&id_corpus=' + $("#id_corpus").val(),
                success: function(data) {
                    $("#editCorpus").html(data);
                },
                async: false
            });
        }

        //Insert corpus metadata (update corpus)
        function insertCorpusMetadata() {

            var formParam = $("form#editCorpusForm").serialize();
            var param = formParam.substr(formParam.indexOf('&name='), formParam.length) + '&id_corpus=' + $("#id_corpus").val() + '&type=update';

            jQuery.ajax({
                type: "POST",
                url: "<%=request.getContextPath()%>/corpus/update?" + param,
                success: function() {

                },
                async: false
            });
        }

        //Return true if exist corpus metadata
        function existCorpusMetadata() {

            var result;

            jQuery.ajax({
                type: "GET",
                url: "<%=request.getContextPath()%>/corpus/metadata/select?type=exist-metadata"
                        + '&id_corpus=' + $("#id_corpus").val(),
                success: function(data) {
                    result = data.toString();
                },
                async: false
            });

            if (result != '') {
                return true;
            }

            return false;

        }

        //Return true if exist text metadata
        function existTextMetadata() {

            var result;

            jQuery.ajax({
                type: "GET",
                url: "<%=request.getContextPath()%>/text/metadata/select?type=exist-metadata"
                        + '&id_corpus=' + $("#id_corpus").val(),
                success: function(data) {
                    result = data.toString();
                },
                async: false
            });

            if (result != '') {
                return true;
            }

            return false;

        }

    });
</script>
