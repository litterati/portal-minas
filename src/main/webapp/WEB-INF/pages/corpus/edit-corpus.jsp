<%-- 
    Document   : update-corpus
    Created on : 29/05/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.CorpusInsertController
    Variables  : corpus (Corpus)
                 error (String)
                 page_menu_tools_sub (String)
                 corpusMetadata ()
                 tools (List<CorpusTools>)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form method="post" id="editCorpusForm" action="<%=request.getContextPath()%>/corpus/${page_menu_tools_sub}">
    <!--TEXT ERRORS, WHEN IT EXIST -->
    <c:if test="${not empty error}">
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            ${error}
        </div>
    </c:if>
    <!--END TEXT ERROR-->

    <!-- END ID AND CORPUS NAME FIELDS-->
    <c:if test="${not empty corpus.id}">
        <div class="form-group">
            <label class="control-label" for="id_corpus">Id</label> 
            <input type="text" name="id_corpus" id="id_corpus" value="${corpus.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group">
        <label class="control-label" for="name">Nome</label> 
        <input type="text" name="name" id="name" value="${corpus.name}" class="form-control" required maxlength="200" />
    </div>
    <div class="form-group">
        <label class="control-label" for="type">Tipo</label> 
        <select class="form-control" name="type" id="type">
            <option <c:if test="${corpus.type == 1}"> selected </c:if> value="1">Normal (de trabalho)</option>
            <option <c:if test="${corpus.type == 2}"> selected </c:if> value="2">Refer�ncia</option>
        </select>
    </div>
    <div class="form-group">
        <label class="control-label" for="description">Descri��o</label> 
        <textarea type="text" name="description" id="description"class="form-control">${corpus.description}</textarea>
    </div>
    <div class="form-group">
        <label class="control-label" for="description">Termos de Uso</label> 
        <textarea type="text" name="userTerms" id="userTerms"class="form-control">${corpus.userTerms}</textarea>
    </div>
    <div class="form-group">
        <label class="control-label" for="tools">Etiquetar</label>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="tools[]" value="pos" <c:forEach var="item" items="${tools}"><c:if test="${item.tool eq 'pos'}"> checked </c:if></c:forEach> > Classe Gramatical (PoS)
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="tools[]" value="lemmatizer" <c:forEach var="item" items="${tools}"><c:if test="${item.tool eq 'lemmatizer'}"> checked </c:if></c:forEach> > Lemas
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="tools[]" value="parser" <c:forEach var="item" items="${tools}"><c:if test="${item.tool eq 'parser'}"> checked </c:if></c:forEach> > Sintaxe (parser)
            </label>
        </div>
        <div class="checkbox disabled" style="color: CadetBlue;">
            <label>
                <input type="checkbox" name="tools[]" value="semantics" disabled> Sem�ntica (campos do discurso)
            </label>
        </div>
        <!--<div class="checkbox disabled" style="color: CadetBlue;">
            <label>
                <input type="checkbox" name="tools[]" value="feelings" disabled> Sentimentos (LIWC)
            </label>
        </div>-->
        
        <label class="control-label" for="tools">Gerar</label>
        <div class="checkbox">
            <label title="Alinhamento autom�tico lexical com GIZA++">
                <input type="checkbox" name="tools[]" value="align" <c:forEach var="item" items="${tools}"><c:if test="${item.tool eq 'align'}"> checked </c:if></c:forEach> > Alinhamento de Senten�as e Palavras
            </label>
        </div>
        <div class="checkbox disabled"  style="color: CadetBlue;">
            <label>
                <input type="checkbox" name="tools[]" value="collocation" disabled> Coloca��es
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="concordances" checked disabled> Concord�ncias
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="statistics" checked disabled> Estat�sticas (n. de tokens, lexemas, ...)
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="frequency" checked disabled> Frequ�ncias
            </label>
        </div>
        <div class="checkbox disabled" style="color: CadetBlue;">
            <label>
                <input type="checkbox" name="tools[]" value="phonetic-normalize" disabled> Normaliza��o Fon�tica
            </label>
        </div>
        <div class="checkbox disabled" style="color: CadetBlue;">
            <label>
                <input type="checkbox" name="tools[]" value="spelling-normalize" disabled> Normaliza��o Gr�fica
            </label>
        </div>
        <div class="checkbox disabled" style="color: CadetBlue;">
            <label>
                <input type="checkbox" name="tools[]" value="key-words" disabled> Palavras-chave
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="tools[]" value="subcorpus" <c:forEach var="item" items="${tools}"><c:if test="${item.tool eq 'subcorpus'}"> checked </c:if></c:forEach> > Subcorpus (filtros bibliogr�ficos)
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="xces" checked disabled> XCES (exportar)
            </label>
        </div>
        
        <label class="control-label" for="tools">Gerenciar</label>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="tools[]" value="covers" <c:forEach var="item" items="${tools}"><c:if test="${item.tool eq 'covers'}"> checked </c:if></c:forEach> > Capas do Texto
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="corpus-metadata" checked disabled> Etiquetas do Corpus
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="text-metadata" checked disabled> Etiquetas do Texto
            </label>
        </div>
        <div class="checkbox disabled">
            <label>
                <input type="checkbox" name="tools[]" value="text-manage" checked disabled> Textos
            </label>
        </div>
    </div>
    <!-- END ID AND CORPUS NAME FIELDS-->

    <!-- CORPUS METADATA FIELDS -->
    <c:forEach var="corpusTag" items="${corpus.corpusTags}">
        <c:set var="required" value="" />
        <c:set var="image_required" value="" />
        <c:if test="${corpusTag.isRequired eq true}"><c:set var="required" value="required" /><c:set var="image_required" value="*" /></c:if>
            <div class="form-group">
                <label class="control-label" for="${corpusTag.tag}">${corpusTag.tag}</label>    
            <c:choose>
                <%-- Data Type (id==1) --%>
                <c:when test="${corpusTag.idFieldType eq 1}">
                    <input type="text" name="${corpusTag.tag}" id="${corpusTag.tag}" class="form-control date" maxlength="10" 
                           value="${corpusTag.corpusTagValues[0].value}" ${required} /><span class="text-error">${image_required}</span>
                </c:when>
                <%-- Integer Type (id==2) --%>
                <c:when test="${corpusTag.idFieldType eq 2}">
                    <input type="text" name="${corpusTag.tag}" id="${corpusTag.tag}" class="form-control numbersOnly" maxlength="10" 
                           value="${corpusTag.corpusTagValues[0].value}" ${required} /><span class="text-error">${image_required}</span>
                </c:when>
                <%-- Nacionality Type (id==4) --%>
                <c:when test="${corpusTag.idFieldType eq 4}">
                    <input type="text" name="${corpusTag.tag}" id="${corpusTag.tag}" class="form-control" maxlength="200" 
                           value="${corpusTag.corpusTagValues[0].value}" ${required} /><span class="text-error">${image_required}</span>
                </c:when> 
                <%-- Text Type (id==7) --%>
                <c:when test="${corpusTag.idFieldType eq 7}">
                    <textarea name="${corpusTag.tag}" id="${corpusTag.tag}" class="form-control" ${required} >${corpusTag.corpusTagValues[0].value}</textarea><span class="text-error">${image_required}</span>
                    </c:when>
                    <%-- Type with a collection of options --%>
                    <c:otherwise>
                    <select name="${corpusTag.tag}" id="${corpusTag.tag}" class="form-control" <c:if test="${corpusTag.isRequired eq true}">required</c:if>>
                            <option value="selected"></option>
                        <c:forEach var="corpusTagValue" items="${corpusTag.corpusTagValues}">
                            <option value="${corpusTagValue.value}" <c:if test="${fn:containsIgnoreCase(corpusTag.corpusTagValues[0].value, corpusTagValue.value)==true}">selected</c:if>>
                                ${corpusTagValue.value}
                            </option>
                        </c:forEach>
                    </select>
                </c:otherwise>
            </c:choose>
        </div>
    </c:forEach>
    <!--END CORPUS METADATA FIELDS-->

    <!-- BUTTONS -->
    <c:if test="${empty wizard}">
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <a href="<%=request.getContextPath()%>/corpus/select" class="btn btn-default">Voltar</a>
        </div>
    </c:if>
    <!-- END BUTTONS -->
</form>

<script src="<%=request.getContextPath()%>/plugin/chosen/chosen.jquery.js"></script>
<script type="text/javascript">

    $(function() {
        $('#name').focus();

        $(".date").mask("99/99/9999"); //Mask for date

        $('.numbersOnly').keyup(function() {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });

        // Apply the autosize plugin to your textarea
        $("textarea").autosize();

        // Pass the debug parameter to see the mirrored element
        //$("#my-textarea").autoresize({debug:true});
        
        //Multtiselect
        $('#tools').chosen();
    });

</script>