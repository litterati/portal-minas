<%-- 
    Document   : list
    Created on : 28/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.support.SupportSelectController
    Variables  : supportList (List<Support>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th></th>
            <th>Nome</th>
            <th>Assunto</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="s" items="${supportList}">
            <tr id="row${s.id}">
                <td width="20">
                    <!-- Icon -->
                    <c:choose>
                        <c:when test="${s.status == 2}">
                            <span class="glyphicon glyphicon-eye-open" title="Lido"></span>
                        </c:when>
                        <c:when test="${s.status == 3}">
                            <span class="glyphicon glyphicon-trash" title="Oculto"></span>
                        </c:when>
                        <c:otherwise>
                            <span class="glyphicon glyphicon-eye-close" title="N�o Lido"></span>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td title="${s.email}">
                    <a href="<%=request.getContextPath()%>/support/view?id=${s.id}" >
                        ${s.name}
                    </a>
                </td>
                <td title="${fn:substring(fn:escapeXml(s.message), 0, 50)}">
                    <a href="<%=request.getContextPath()%>/support/view?id=${s.id}" >
                        ${s.subject}
                    </a>
                </td>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty supportList}">
        <span class="label label-warning">Total: ${fn:length(supportList)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� mensagens</div>
    </c:otherwise>
</c:choose>

