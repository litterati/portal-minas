<%-- 
    Document   : view
    Created on : 28/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.support.SupportViewController
    Variables  : support (Support)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div>
    <!-- Support -->
    <h3>Assunto: ${support.subject}</h3>
    <h4>De: ${support.name} &lt;${support.email}&gt;</h4>
    <div id="${s.id}">
        ${support.message}
    </div>
    <!-- /Support -->
    <br>
</div>
<a href="<%=request.getContextPath()%>/support/select" class="btn btn-default">Voltar</a>