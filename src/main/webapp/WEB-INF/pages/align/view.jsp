<%-- 
    Document   : view
    Created on : 07/08/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AlignShowController
    Variables  : text_aling (TextAling)
                 source (List<Words>)
                 target (List<Words>)
                 id_corpus (int)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total (int)
                 pagination_range (int)
                 align_tags (List<AlignTag>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    s {
        text-decoration:none;
        padding: 3px 0px;
        line-height: 25px;
    }
    s:hover, .selectedSentence {
        background-color: rgb(255, 255, 0);
    }
    w {
        padding: 3px;
        margin: 3px;
    }
    w:hover, .selectedWord {
        background-color: rgb(135, 206, 235);
    }
    w.bonus {
        color: #999;
    }
</style>

<c:import url="../align/helper-view-text.jsp" />

<!-- Pagination -->
<div class="text-center">
    <ul class="pagination">
        <li title="Primeira"><a href="<%=request.getContextPath()%>/align/view?id_source=${text_align.source.id}&id_target=${text_align.target.id}&id_corpus=${id_corpus}&page=1">&larr;</a></li>
        <c:forEach var="page_i" begin="1" end="${pagination_size}">
            <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/align/view?id_source=${text_align.source.id}&id_target=${text_align.target.id}&id_corpus=${id_corpus}&page=${page_i}">${page_i}</a></li>
        </c:forEach>
        <li title="�ltima"><a href="<%=request.getContextPath()%>/align/view?id_source=${text_align.source.id}&id_target=${text_align.target.id}&id_corpus=${id_corpus}&page=${pagination_size}">&rarr;</a></li>
    </ul>
    <div class="clearfix"></div>
    <span class="label label-warning">Exibindo ${fn:length(source)} de um total de ${pagination_total_tokens} tokens fonte.</span>
</div>
<!-- /Pagination -->

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    var align_tags = [];
    <c:forEach var="tag" items="${align_tags}">align_tags[${tag.id}] = "${tag.value}"; </c:forEach>
    $("w").hover(function(){
        //tag element
        tagHighlight(this);
        //highlight
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("w[idword='" + crr[i] + "']").addClass("selectedWord");
            //inverted
            if ($("w[idword='" + crr[i] + "']").size() > 0){
                var crr2 = $("w[idword='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("w[idword='" + crr2[j] + "']").addClass("selectedWord");
                }
            }
        }
    }, function (){
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("w[idword='" + crr[i] + "']").removeClass("selectedWord");
            //inverted
            if ($("w[idword='" + crr[i] + "']").size() > 0){
                var crr2 = $("w[idword='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("w[idword='" + crr2[j] + "']").removeClass("selectedWord");
                }
            }
        }
        //tag parent
        tagHighlight($(this).parent());
    });
    $("s").hover(function(){
        //highlight
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("s[idsentence='" + crr[i] + "']").addClass("selectedSentence");
            //inverted
            if ($("s[idsentence='" + crr[i] + "']").size() > 0){
                var crr2 = $("s[idsentence='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("s[idsentence='" + crr2[j] + "']").addClass("selectedSentence");
                }
            }
        }
    }, function (){
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("s[idsentence='" + crr[i] + "']").removeClass("selectedSentence");
            //inverted
            if ($("s[idsentence='" + crr[i] + "']").size() > 0){
                var crr2 = $("s[idsentence='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("s[idsentence='" + crr2[j] + "']").removeClass("selectedSentence");
                }
            }
        }
        $("#tag-align-notify").addClass("hidden");
    });
    function tagHighlight (element, call){
        var value = undefined;
        if ($(element).attr("idaligntag") && //if idaligntag exist
                $(element).attr("idaligntag") !== undefined && //if it is not empty
                $(element).attr("idaligntag") > 0){ //if it is bigger than 0
            if ($(element).is("w")){//if the element is a word
                value = "Palavra: ";
            } else {
                value = "Senten�a: ";
            }
            value += align_tags[$(element).attr("idaligntag").trim()];
            if ($(element).attr("aligntype") && //if aligntype exist
                $(element).attr("aligntype") !== undefined){ //if it is not empty
                value += " (" + $(element).attr("aligntype") + ")";
            }
            //$(element).attr("title", value);
            if (call !== undefined){
                return value;
            }
        }
        if ($(element).is("w")){//if the element is a word
            var parent_value = tagHighlight($(element).parent(), 1);
            if (parent_value !== undefined 
                    && value !== undefined){
                value = parent_value + "<br/>" + value;
            } else if (parent_value !== undefined ){
                value = parent_value;
            }
        }
        if (value === undefined){
            $("#tag-align-notify")
                    .addClass("hidden");
        } else {
            $("#tag-align-notify")
                    .removeClass("hidden")
                    .children("p")
                    .html(value);
            //$(element).attr("title", value);
        }
    }
</script>