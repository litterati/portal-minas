<%-- 
    Document   : view
    Created on : 13/10/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.align.sentence.SentenceAlignViewController
    Variables  : text_aling (TextAling)
                 source (List<Words>)
                 target (List<Words>)
                 id_corpus (int)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total (int)
                 pagination_range (int)
                 align_tags (List<AlignTag>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    s {
        text-decoration:none;
        padding: 3px 0px;
        line-height: 25px;
    }
    s:hover, .selectedSentence {
        background-color: rgb(255, 255, 0);
    }
    w {
        cursor: pointer;
        padding: 3px;
        margin: 3px;
    }
    w:hover, .selectedWord {
        background-color: rgb(135, 206, 235);
    }
    w.old {
        background-color: rgb(251,172,172); // red
    }
    w.old.modify {
        //background-color: rgb(135, 206, 235); // blue
    }
    w.modify {
        background-color: rgb(184,251,168); // green
    }
    w.bonus {
        color: grey;
    }
</style>

<c:import url="../align/helper-view-text.jsp" />
    
<ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none" >
    <li role="presentation" class="dropdown-header">Edi��o</li>
    <li><a tabindex="-1" href="javascript:void(null)" title="Salvar os alinhamentos marcados">Salvar</a></li>
    <li><a tabindex="-1" href="javascript:void(null)" title="Remover todos os alinhamentos desta palavra">Remover todos</a></li>
    <li><a tabindex="-1" href="javascript:void(null)" title="Desmarcar todas as palavras">Deselecionar</a></li>
    <li role="presentation" class="divider"></li>
    <li role="presentation" class="dropdown-header">Etiquetas</li>
    <c:forEach var="tag" items="${align_tags}">
        <li><a tabindex="-1" href="javascript:void(null)" id_align_tag="${tag.id}" >${tag.value}</a></li>
    </c:forEach>
</ul>

<!-- Pagination -->
<div class="text-center">
    <ul class="pagination">
        <li title="Primeira"><a href="<%=request.getContextPath()%>/align/word/view?id_source=${text_align.source.id}&id_target=${text_align.target.id}&id_corpus=${id_corpus}&page=1">&larr;</a></li>
        <c:forEach var="page_i" begin="1" end="${pagination_size}">
            <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/align/word/view?id_source=${text_align.source.id}&id_target=${text_align.target.id}&id_corpus=${id_corpus}&page=${page_i}">${page_i}</a></li>
        </c:forEach>
        <li title="�ltima"><a href="<%=request.getContextPath()%>/align/word/view?id_source=${text_align.source.id}&id_target=${text_align.target.id}&id_corpus=${id_corpus}&page=${pagination_size}">&rarr;</a></li>
    </ul>
    <div class="clearfix"></div>
    <span class="label label-warning">Exibindo ${fn:length(source)} de um total de ${pagination_total_tokens} tokens fonte.</span>
</div>
<!-- /Pagination -->

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/notify.min.js"></script>
<script type="text/javascript">
    var align_tags = [];
    <c:forEach var="tag" items="${align_tags}">align_tags[${tag.id}] = "${tag.value}"; </c:forEach>
    $("w").hover(function(){
        //tag
        if ($(this).attr("idaligntag") && 
                $(this).attr("idaligntag") !== undefined &&
                $(this).attr("idaligntag") > 0){
            $(this).attr("title", "Palavra: " + align_tags[$(this).attr("idaligntag").trim()]);
        }
        //highlight
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("w[idword='" + crr[i] + "']").addClass("selectedWord");
            //inverted
            if ($("w[idword='" + crr[i] + "']").size() > 0){
                var crr2 = $("w[idword='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("w[idword='" + crr2[j] + "']").addClass("selectedWord");
                }
            }
        }
    }, function (){
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("w[idword='" + crr[i] + "']").removeClass("selectedWord");
            //inverted
            if ($("w[idword='" + crr[i] + "']").size() > 0){
                var crr2 = $("w[idword='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("w[idword='" + crr2[j] + "']").removeClass("selectedWord");
                }
            }
        }
    });
    $("s").hover(function(){
        //highlight
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("s[idsentence='" + crr[i] + "']").addClass("selectedSentence");
            //inverted
            if ($("s[idsentence='" + crr[i] + "']").size() > 0){
                var crr2 = $("s[idsentence='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("s[idsentence='" + crr2[j] + "']").addClass("selectedSentence");
                }
            }
        }
    }, function (){
        var crr = $(this).attr("crr").trim().split(" ");
        for (i = 0; i < crr.length; i++){
            $("s[idsentence='" + crr[i] + "']").removeClass("selectedSentence");
            //inverted
            if ($("s[idsentence='" + crr[i] + "']").size() > 0){
                var crr2 = $("s[idsentence='" + crr[i] + "']").attr("crr").trim().split(" ");
                for (j = 0; j < crr2.length; j++){
                    $("s[idsentence='" + crr2[j] + "']").removeClass("selectedSentence");
                }
            }
        }
    });
    
    /*
     * To edit words align.
     * The sources are set int sources array.
     * @type Array
     */
    var sources = [];
    var targets = [];
    $("#source w").click(function(){
        markObject(sources, this);
    });
    $("#target w").click(function(){
        markObject(targets, this);
    });
    function markObject(myArray, myObj){
        var index = myArray.indexOf(myObj);
        if (index > -1){// if contains
            myArray.splice(index, 1);
            $(myObj).removeClass("modify");
            
            var crr = $(myObj).attr("crr").trim().split(" ");
            for (i = 0; i < crr.length; i++){
                $("w[idword='" + crr[i] + "']").removeClass("old");
            }
        } else {// not contains
            myArray.push(myObj);
            $(myObj).addClass("modify");
            
            var crr = $(myObj).attr("crr").trim().split(" ");
            for (i = 0; i < crr.length; i++){
                $("w[idword='" + crr[i] + "']").addClass("old");
            }
        }
    }
    
    //http://jsfiddle.net/KyleMit/X9tgY/
    (function ($, window) {

        $.fn.contextMenu = function (settings) {

            return this.each(function () {

                // Open context menu
                $(this).on("contextmenu", function (e) {
                    //open menu
                    $(settings.menuSelector)
                        .data("invokedOn", $(e.target))
                        .show()
                        .css({
                            position: "absolute",
                            left: getLeftLocation(e),
                            top: getTopLocation(e)
                        })
                        .off('click')
                        .on('click', function (e) {
                            $(this).hide();

                            var $invokedOn = $(this).data("invokedOn");
                            var $selectedMenu = $(e.target);

                            settings.menuSelected.call(this, $invokedOn, $selectedMenu);
                    });

                    return false;
                });

                //make sure menu closes on any click
                $(document).click(function () {
                    $(settings.menuSelector).hide();
                });
            });

            function getLeftLocation(e) {
                var mouseWidth = e.pageX;
                var pageWidth = $(window).width();
                var menuWidth = $(settings.menuSelector).width();

                // opening menu would pass the side of the page
                if (mouseWidth + menuWidth > pageWidth &&
                    menuWidth < mouseWidth) {
                    return mouseWidth - menuWidth;
                } 
                return mouseWidth;
            }        

            function getTopLocation(e) {
                var mouseHeight = e.pageY;
                var pageHeight = $(window).height();
                var menuHeight = $(settings.menuSelector).height();

                // opening menu would pass the bottom of the page
                if (mouseHeight + menuHeight > pageHeight &&
                    menuHeight < mouseHeight) {
                    return mouseHeight - menuHeight;
                } 
                return mouseHeight;
            }

        };
    })(jQuery, window);

    $.fn.scrollView = function () {
        return this.each(function () {
            $('html, body').animate({
                scrollTop: $(this).offset().top - (window.innerHeight / 2)
            }, 1000);
        });
    };
    
    $("w").contextMenu({
        menuSelector: "#contextMenu",
        menuSelected: function (invokedOn, selectedMenu) {
            if (selectedMenu.text() === "Remover todos"){
                removeAll(sources, "remove_all_source");
                removeAll(targets, "remove_all_target");
                unmarkAll();
            } else if (selectedMenu.text() === "Salvar"){
                //remove old aligns
                removeAll(sources, "remove_all_source");
                removeAll(targets, "remove_all_target");
                //insert new aligns
                saveAll();
                unmarkAll();
            } else if (selectedMenu.text() === "Deselecionar"){
                //invokedOn.scrollView();
                unmarkAll();
            } else if (selectedMenu.attr("id_align_tag")) {
                //fonte
                if (invokedOn.is("#source w")){
                    updateTagAlign(invokedOn.attr("idword"), invokedOn.attr("crr"), selectedMenu.attr("id_align_tag"));
                }
                //alvo
                else {
                    updateTagAlign(invokedOn.attr("crr"), invokedOn.attr("idword"), selectedMenu.attr("id_align_tag"));
                }
            }
        }
    });
    
    function removeAll (list, mode){
        for (var i = 0; i < list.length; i++){
            var id = $(list[i]).attr("idword");
            $.post("<%=request.getContextPath()%>/align/word/view", {id_word: id, mode: mode})
                .done(function(data) {
                    // data is JSON
                    if (data.alert_success){
                        //remove the correspondent
                        $("w[idword='" + data.id_word + "']").attr("crr", "");
                        //remove from every sentece the crr = id
                        var aux = data.old_aligns.trim().split(" ");
                        for (var j = 0; j < aux.length; j++){
                            $("w[idword='" + aux[j] + "']").attr("crr", "");
                        }
                        
                        //invokedOn.notify("Removido idWord:" + id , { style: 'bootstrap', className: 'success', autoHideDelay: 1000 });
                    } else if (data.alert_danger){
                        //invokedOn.notify("Erro: " + data.alert_danger, { style: 'bootstrap', className: 'error', autoHideDelay: 1000 });
                    }
                })
                .fail(function() {
                    //invokedOn.notify("Erro na conex�o", { style: 'bootstrap', className: 'error', autoHideDelay: 1000 });
                });
        }
    }
    
    function saveAll (){
        var type = "t" + sources.length + ":" + targets.length;
        for (var i = 0; i < sources.length; i++){
            var id_word_source = $(sources[i]).attr("idword");
            for (var j = 0; j < targets.length; j++){
                var id_word_target = $(targets[j]).attr("idword");
                $.post("<%=request.getContextPath()%>/align/word/view", {id_word_source: id_word_source, id_word_target: id_word_target, type: type, mode: "save"})
                    .done(function(data) {
                        // data is JSON
                        if (data.alert_success){
                            //add the correspondent
                            $("w[idword='" + data.id_word_source + "']").attr("crr", data.new_align_source);
                            $("w[idword='" + data.id_word_target + "']").attr("crr", data.new_align_target);

                            //invokedOn.notify("Removido idWord:" + id , { style: 'bootstrap', className: 'success', autoHideDelay: 1000 });
                        } else if (data.alert_danger){
                            //invokedOn.notify("Erro: " + data.alert_danger, { style: 'bootstrap', className: 'error', autoHideDelay: 1000 });
                        }
                    })
                    .fail(function() {
                        //invokedOn.notify("Erro na conex�o", { style: 'bootstrap', className: 'error', autoHideDelay: 1000 });
                    });
            }
        }
    }
    
    function unmarkAll (){
        $(".row w").removeClass("modify").removeClass("old");
        sources = [];
        targets = [];
    }
    
    function updateTagAlign (source, target, id_align_tag){
        var idsource = source.trim().split(" ");
        var idtarget = target.trim().split(" ");
        for (var i = 0; i < idsource.length; i++){
            for (var j = 0; j < idtarget.length; j++){
                $.get("<%=request.getContextPath()%>/align/tag/ajax", {id_source: idsource[i], id_target: idtarget[j], id_align_tag: id_align_tag, word: true})
                    .done(function(data) {
                        // data is JSON
                        if (data.alert_success){
                            //update elements
                            $("w[idword='" + data.id_source + "']").attr("idaligntag", data.id_align_tag);
                            $("w[idword='" + data.id_target + "']").attr("idaligntag", data.id_align_tag);

                            //invokedOn.notify("Removido idWord:" + id , { style: 'bootstrap', className: 'success', autoHideDelay: 1000 });
                        } else if (data.alert_danger){
                            //invokedOn.notify("Erro: " + data.alert_danger, { style: 'bootstrap', className: 'error', autoHideDelay: 1000 });
                        }
                    })
                    .fail(function() {
                        //invokedOn.notify("Erro na conex�o", { style: 'bootstrap', className: 'error', autoHideDelay: 1000 });
                    });
            }
        }
    }
</script>