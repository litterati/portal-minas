<%-- 
    Document   : pages.align.word.form
    Created on : 07/10/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.align.word.AlignInsertController
    Variables  : source_words_list (List<Words>)
                 target_words_list (List<Words>)
                 textAlign (TextAlign)
                 wordAlign (WordAlign)
                 id_corpus (String)
                 page_menu_tools_sub (String)
                 tags (List<AlignTag>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/align/word/${page_menu_tools_sub}">
    <div class="form-group hidden">
        <label for="id_source_text">Id Texto Fonte</label> 
        <input type="text" name="id_source_text" value="${textAlign.idSource}" readonly class="form-control"/>
    </div>
    <div class="form-group hidden">
        <label for="id_target_text">Id Texto Alvo</label> 
        <input type="text" name="id_target_text" value="${textAlign.idTarget}" readonly class="form-control"/>
    </div>
    <div class="form-group hidden">
        <label for="id_corpus">Id Corpus</label> 
        <input type="text" name="id_corpus" value="${id_corpus}" readonly class="form-control"/>
    </div>

    <div class="form-group has-feedback">
        <label class="control-label" for="id_source_word">Palavra Fonte</label> 
        <select class="form-control" name="id_source_word" id="id_source_word" >
            <c:forEach var="w" items="${source_words_list}">
                <c:choose>
                    <c:when test="${not empty wordAlign}">
                        <c:if test="${wordAlign.idSource == w.id}">
                            <option selected value="${w.id}">${w.word} // ${w.position}</option>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${w.word != '_NEWLINE_'}">
                            <option value="${w.id}">${w.word} // ${w.position}</option>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label class="control-label" for="id_target_word">Palavra Alvo</label> 
        <select class="form-control" name="id_target_word" id="id_target_word" >
            <c:forEach var="w" items="${target_words_list}">
                <c:choose>
                    <c:when test="${not empty wordAlign}">
                        <c:if test="${wordAlign.idTarget == w.id}">
                            <option selected value="${w.id}">${w.word} // ${w.position}</option>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${w.word != '_NEWLINE_'}">
                            <option value="${w.id}">${w.word} // ${w.position}</option>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>

    <div class="form-group">
        <label for="type">Tipo</label> 
        <input type="text" name="type" id="type" value="${wordAlign.type}" class="form-control" maxlength="10" />
    </div>
    
    <div class="form-group">
        <label for="type">Etiqueta</label> 
        <select class="form-control" name="id_align_tag" id="id_align_tag" >
            <option value="0" <c:if test="${wordAlign.idAlignTag < 1}">selected</c:if>>Sem etiqueta</option>
            <c:forEach var="tag" items="${tags}">
                <option value="${tag.id}" <c:if test="${tag.id == wordAlign.idAlignTag}">selected</c:if>>${tag.value}</option>
            </c:forEach>
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/align/word/select" class="btn btn-default">Voltar</a>    
    </div>
</form>
