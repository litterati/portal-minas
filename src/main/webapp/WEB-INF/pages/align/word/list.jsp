<%-- 
    Document   : pages.align.word.list
    Created on : 07/10/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.align.word.AlignSelectController
    Variables  : word_align (List<WordAlign>)
                 id_corpus (int)
                 id_source_text (int)
                 id_target_text (int)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total (int)
                 pagination_range (int)
                 align_tags (Map<Integer, AlignTag>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- ALIGN LIST -->
<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="2"></th>
            <th>Fonte</th>
            <th>Alvo</th>
            <th>Tipo</th>
            <th>Etiqueta</th>
        </tr>
    </thead>
    
        <tbody>
            <c:if test="${not empty word_align}">
            <c:forEach var="align" items="${word_align}">
                <tr id="row${align.idSource}-${align.idTarget}">
                    <td width="20">
                        <!-- Alterar -->
                        <a href="<%=request.getContextPath()%>/align/word/update?id_source_word=${align.idSource}&id_target_word=${align.idTarget}&id_corpus=${id_corpus}&id_source_text=${align.source.idText}&id_target_text=${align.target.idText}" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Remover -->
                        <a href="#" data-toggle="modal" data-target="#modalDelete${align.idSource}-${align.idTarget}" title="Remover">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="modalDelete${align.idSource}-${align.idTarget}" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title">Confirma��o</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Tem certeza que deseja remover o alinhamento?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${align.idSource}, ${align.idTarget}, ${id_corpus}, ${align.source.idText}, ${align.target.idText});">Confirmar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </td>
                    <td>${align.source.word}</td>
                    <td>${align.target.word}</td>
                    <td>${align.type}</td>
                    <td>
                        <c:if test="${align.idAlignTag != null && align.idAlignTag > 0}">${align_tags[align.idAlignTag].value}</c:if>
                    </td>
                </tr>
            </c:forEach>
                </c:if>
        </tbody>
    
</table>
<!-- END ALIGN LIST -->

<c:choose>
    <c:when test="${not empty word_align && pagination_size > 1}">
        <span class="label label-warning">Exibindo <span id="total">${fn:length(word_align)}</span> de um total de ${pagination_total} alinhamentos.</span>
        
        <!-- Pagination -->
        <div class="text-center">
            <ul class="pagination">
                <li title="Primeira"><a href="<%=request.getContextPath()%>/align/word/select?id_corpus=${id_corpus}&id_source_text=${id_source_text}&id_target_text=${id_target_text}&page=1">&larr;</a></li>
                <c:forEach var="page_i" begin="1" end="${pagination_size}">
                    <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/align/word/select?id_corpus=${id_corpus}&id_source_text=${id_source_text}&id_target_text=${id_target_text}&page=${page_i}">${page_i}</a></li>
                </c:forEach>
                <li title="�ltima"><a href="<%=request.getContextPath()%>/align/word/select?id_corpus=${id_corpus}&id_source_text=${id_source_text}&id_target_text=${id_target_text}&page=${pagination_size}">&rarr;</a></li>
            </ul>
        </div>
        <!-- /Pagination -->
        
    </c:when>
    <c:when test="${not empty word_align}">
        <span class="label label-warning">Total: <span id="total">${fn:length(word_align)}</span></span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� palavras alinhadas</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id_source_word, id_target_word, id_corpus, id_source_text, id_target_text) {
        $.post("<%=request.getContextPath()%>/align/word/delete", {id_source_word: id_source_word, id_target_word: id_target_word, id_corpus: id_corpus, id_source_text: id_source_text, id_target_text: id_target_text, ajax: true})
            .done(function(data) {
                // data is JSON
                if (data.alert_success){
                    $( "#row" + data.id_source_word + "-" + data.id_target_word).hide();
                    $( "#total" ).text( $( "#total" ).html()-1 );//counter
                } else if (data.alert_danger){
                    $( "#row" + data.id_source_word + "-" + data.id_target_word).addClass("danger");
                    setTimeout(function() {
                        $( "#row" + data.id_source_word + "-" + data.id_target_word).removeClass("danger"); // change it back after ...
                    }, 1500);
                }
            })
            .fail(function() {
                //$( "#row" + id_source + "-" + id_target ).addClass("danger");
            });
    }
</script>