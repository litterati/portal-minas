<%-- 
    Document   : list
    Created on : 30/06/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AlignSelectController
    Variables  : text_align (List<TextAlign>)
                 id_corpus (int)
                 pagination_current_page (int)
                 pagination_size (int)
                 pagination_total (int)
                 pagination_range (int)
                 order (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- ALIGN LIST -->
<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="3"></th>
            <th>Fonte <a href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=source" title="Ordernar por fonte"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Alvo <a href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=target" title="Ordernar por alvo"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Tipo <a href="<%=request.getContextPath()%>/align/select?id_corpus=${page_menu_id_corpus}&page=${pagination_current_page}&order=type" title="Ordernar por tipo"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th></th>
        </tr>
    </thead>
    
        <tbody>
            <tr id="rowNew" class="hidden">
                <!-- Insert -->
                <td colspan="3"></td>
                <td>
                    <select class="form-control" id="source" name="source" >
                        <c:if test="${not empty list_texts}">
                            <c:forEach var="text" items="${list_texts}">
                                <option value="${text.id}">${text.title} // ${text.language}</option>
                            </c:forEach>
                        </c:if>
                    </select>
                </td>
                <td>
                    <select class="form-control" id="target" name="target" >
                        <c:if test="${not empty list_texts}">
                            <c:forEach var="text" items="${list_texts}">
                                <option value="${text.id}">${text.title} // ${text.language}</option>
                            </c:forEach>
                        </c:if>
                    </select>
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Paralelo, compar�vel ..." id="type" name="type" maxlength="10">
                </td>
                <th>
                    <button type="submit" class="btn btn-primary" id="btnSaveAlign">Salvar</button>
                    <a href="<%=request.getContextPath()%>/align/select?id_corpus=${id_corpus}" class="btn btn-default">Cancelar</a>
                </th>
                <!-- /Insert -->
            </tr>
            <c:if test="${not empty text_align}">
            <c:forEach var="align" items="${text_align}">
                <tr id="row${align.idSource}-${align.idTarget}">
                    <td width="20">
                        <!-- Ver -->
                        <a href="<%=request.getContextPath()%>/align/show?id_source=${align.idSource}&id_target=${align.idTarget}&id_corpus=${id_corpus}" title="Visualizar">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Alterar -->
                        <a href="<%=request.getContextPath()%>/align/update?id_source=${align.idSource}&id_target=${align.idTarget}&id_corpus=${id_corpus}" title="Alterar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td width="20">
                        <!-- Remover -->
                        <a href="#" data-toggle="modal" data-target="#modalDelete${align.idSource}-${align.idTarget}" title="Remover">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="modalDelete${align.idSource}-${align.idTarget}" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                        <h4 class="modal-title">Confirma��o</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Tem certeza que deseja remover o alinhamento?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${align.idSource}, ${align.idTarget}, ${id_corpus});">Confirmar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </td>
                    <td>${align.source.title}</td>
                    <td>${align.target.title}</td>
                    <td>${align.type}</td>
                    <th></th>
                </tr>
            </c:forEach>
                </c:if>
        </tbody>
    
</table>
<!-- END ALIGN LIST -->

<c:choose>
    <c:when test="${not empty text_align && pagination_total > 50}">
        <span class="label label-warning">Exibindo <span id="total">${fn:length(text_align)}</span> de um total de ${pagination_total} alinhamentos.</span>
        
        <!-- Pagination -->
        <div class="text-center">
            <ul class="pagination">
                <li title="Primeira"><a href="<%=request.getContextPath()%>/align/select?id_corpus=${id_corpus}&page=1&order=${order}">&larr;</a></li>
                <c:forEach var="page_i" begin="1" end="${pagination_size}">
                    <li <c:if test="${page_i == pagination_current_page}"> class="active" </c:if> ><a href="<%=request.getContextPath()%>/align/select?id_corpus=${id_corpus}&page=${page_i}&order=${order}">${page_i}</a></li>
                </c:forEach>
                <li title="�ltima"><a href="<%=request.getContextPath()%>/align/select?id_corpus=${id_corpus}&page=${pagination_size}&order=${order}">&rarr;</a></li>
            </ul>
        </div>
        <!-- /Pagination -->
        
    </c:when>
    <c:when test="${not empty text_align}">
        <span class="label label-warning">Total: <span id="total">${fn:length(text_align)}</span></span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� arquivos alinhados</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id_source, id_target, id_corpus) {
        $.post("<%=request.getContextPath()%>/align/delete", {source: id_source, target: id_target, id_corpus: id_corpus, ajax: true})
                .done(function(data) {
                    // data is JSON
                    if (data.alert_success){
                        alert( data.alert_success );
                        
                        $( "#row" + id_source + "-" + id_target ).hide();
                        $( "#total" ).text( $( "#total" ).html()-1 );//counter
                    } else if (data.alert_danger){
                        alert( data.alert_danger );
                        
                        $( "#row" + id_source + "-" + id_target ).addClass("danger");
                        setTimeout(function() {
                            $( "#row" + id_source + "-" + id_target ).removeClass("danger"); // change it back after ...
                        }, 1500);
                    }
                })
                .fail(function() {
                    $( "#row" + id_source + "-" + id_target ).addClass("danger");
                });
    }
    
    $('#btnAddAlign').bind('click', function (){
        $('#rowNew').removeClass('hidden');
    });
    $('#btnSaveAlign').bind('click', insertAction);
    
    function insertAction() {
        var id_source = $('#source option:selected').val();
        var id_target = $('#target option:selected').val();
        var id_corpus = ${id_corpus};
        var type = $('#type').val();
        
        $.post("<%=request.getContextPath()%>/align/insert", { source: id_source, target: id_target, id_corpus: id_corpus, type: type, ajax: true})
                .done(function(data) {
                    // data is JSON
                    if (data.alert_success){
                        //alert( data.alert_success );
                        
                        $("#rowNew").addClass("success");
                        setTimeout(function() {
                            $("#rowNew").removeClass("success"); // change it back after ...
                        }, 1500);

                        $('table').append(
                                '<tr>\n\
                                 <td><span class="glyphicon glyphicon-eye-open"></span></td>\n\
                                 <td><span class="glyphicon glyphicon-pencil"></span></td>\n\
                                 <td><span class="glyphicon glyphicon-trash"></span></td>\n\
                                 <td>'+data.source_title+'</td>\n\
                                 <td>'+data.target_title+'</td>\n\
                                 <td>'+type+'</td>\n\
                                 <td></td></tr>');
                        $( "#total" ).text( parseInt($("#total").html())+1 );//counter
                        
                    } else if (data.alert_danger){
                        alert( data.alert_danger );
                        
                        $("#rowNew").addClass("danger");
                        setTimeout(function() {
                            $("#rowNew").removeClass("danger"); // change it back after ...
                        }, 1500);
                    }

                    
                })
                .fail(function() {
                    $("#rowNew").addClass("danger");
                });
    }
</script>