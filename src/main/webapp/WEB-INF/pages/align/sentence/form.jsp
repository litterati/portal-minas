<%-- 
    Document   : pages.align.sentence.form
    Created on : 07/10/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.align.sentence.AlignInsertController
    Variables  : source_sentences_list (List<Sentence>)
                 target_sentences_list (List<Sentence>)
                 textAlign (TextAlign)
                 sentenceAlign (SentenceAlign)
                 id_corpus (String)
                 page_menu_tools_sub (String)
                 tags (List<AlignTag>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/align/sentence/${page_menu_tools_sub}">
    <div class="form-group hidden">
        <label for="id_source_text">Id Texto Fonte</label> 
        <input type="text" name="id_source_text" value="${textAlign.idSource}" readonly class="form-control"/>
    </div>
    <div class="form-group hidden">
        <label for="id_target_text">Id Texto Alvo</label> 
        <input type="text" name="id_target_text" value="${textAlign.idTarget}" readonly class="form-control"/>
    </div>
    <div class="form-group hidden">
        <label for="id_corpus">Id Corpus</label> 
        <input type="text" name="id_corpus" value="${id_corpus}" readonly class="form-control"/>
    </div>

    <div class="form-group has-feedback">
        <label class="control-label" for="id_source_sentence">Senten�a Fonte</label> 
        <select class="form-control" name="id_source_sentence" id="id_source_sentence" >
            <c:forEach var="s" items="${source_sentences_list}">
                <c:choose>
                    <c:when test="${not empty sentenceAlign.source.words}">
                        <c:if test="${sentenceAlign.idSource == s.id}">
                            <option selected value="${s.id}"><c:forEach var="w" items="${sentenceAlign.source.words}"><c:if test="${w.word != '_NEWLINE_'}">${w.word} </c:if></c:forEach></option>
                        </c:if>
                    </c:when>
                    <c:when test="${not empty sentenceAlign}">
                        <c:if test="${sentenceAlign.idSource == s.id}">
                            <option selected value="${s.id}">${s.id} // ${s.type} // ${s.position}</option>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <option value="${s.id}">${s.id} // ${s.type} // ${s.position}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
        <label class="control-label" for="id_target_sentence">Senten�a Alvo</label> 
        <select class="form-control" name="id_target_sentence" id="id_target_sentence" >
            <c:forEach var="s" items="${target_sentences_list}">
                <c:choose>
                    <c:when test="${not empty sentenceAlign.target.words}">
                        <c:if test="${sentenceAlign.idTarget == s.id}">
                            <option selected value="${s.id}"><c:forEach var="w" items="${sentenceAlign.target.words}"><c:if test="${w.word != '_NEWLINE_'}">${w.word} </c:if></c:forEach></option>
                        </c:if>
                    </c:when>
                    <c:when test="${not empty sentenceAlign}">
                        <c:if test="${sentenceAlign.idTarget == s.id}">
                            <option selected value="${s.id}">${s.id} // ${s.type} // ${s.position}</option>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <option value="${s.id}">${s.id} // ${s.type} // ${s.position}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>

    <div class="form-group">
        <label for="type">Tipo</label> 
        <input type="text" name="type" id="type" value="${sentenceAlign.type}" class="form-control" maxlength="10" />
    </div>
    
    <div class="form-group">
        <label for="type">Etiqueta</label> 
        <select class="form-control" name="id_align_tag" id="id_align_tag" >
            <option value="0" <c:if test="${sentenceAlign.idAlignTag < 1}">selected</c:if>>Sem etiqueta</option>
            <c:forEach var="tag" items="${tags}">
                <option value="${tag.id}" <c:if test="${tag.id == sentenceAlign.idAlignTag}">selected</c:if>>${tag.value}</option>
            </c:forEach>
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/align/sentence/select" class="btn btn-default">Voltar</a>    
    </div>
</form>
