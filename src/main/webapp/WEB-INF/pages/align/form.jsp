<%-- 
    Document   : edit
    Created on : 10/07/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.AlignInsertController
                 br.project.controller.AlignUpdateController
                 br.project.controller.AlignDeleteController
    Variables  : text_align (TextAlign)
                 list_texts (List<Text>)
                 id_corpus (int)
                 name_corpus (String)
                 page_menu_tools_sub (String)
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<form id="form" method="post" action="<%=request.getContextPath()%>/align/${page_menu_tools_sub}"> 
    
    <input type="hidden" name="id_corpus" value="${id_corpus}" />
    
    <c:choose>
        <c:when test="${not empty text_align}" >
            <div class="form-group">
                <label for="source">Arquivo fonte</label>
                <input type="text" class="form-control" value="${text_align.source.title}" disabled="disabled">
                <input type="hidden" id="source" name="source" value="${text_align.source.id}">
            </div>
            <div class="form-group">
                <label for="target">Arquivo alvo</label>
                <input type="text" class="form-control" value="${text_align.target.title}" disabled="disabled">
                <input type="hidden" id="target" name="target" value="${text_align.target.id}">
            </div>
        </c:when>
        <c:otherwise>
            <div class="form-group">
                <label for="source">Arquivo fonte</label>
                <select class="form-control" id="source" name="source" >
                    <c:if test="${not empty list_texts}">
                        <c:forEach var="text" items="${list_texts}">
                            <option value="${text.id}">${text.title} // ${text.language}</option>
                        </c:forEach>
                    </c:if>
                </select>
                <p class="help-block">Arquivo original</p>
            </div>
            <div class="form-group">
                <label for="target">Arquivo alvo</label>
                <select class="form-control" id="target" name="target" >
                    <c:if test="${not empty list_texts}">
                        <c:forEach var="text" items="${list_texts}">
                            <option value="${text.id}">${text.title} // ${text.language}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="form-group">
        <label for="type">Tipo</label>
        <input type="text" class="form-control" placeholder="Paralelo, comparável ..." id="type" name="type" maxlength="10" value="${text_align.type}">
        <p class="help-block">Máx. 10</p>
    </div>
    <button type="submit" class="btn btn-primary">Salvar</button>
    <a href="<%=request.getContextPath()%>/align/select?id_corpus=${id_corpus}" class="btn btn-default">Cancelar</a>
</form>
