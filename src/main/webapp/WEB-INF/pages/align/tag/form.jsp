<%-- 
    Document   : pages.align.tag.form
    Created on : 22/10/2014
    Author     : Thiago Vieira
    Controller : 
    Variables  : align_tag (AlignTag)
                 page_menu_tools_sub (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/align/tag/${page_menu_tools_sub}">
    <input type="hidden" name="id_corpus" value="${id_corpus}" />
    <c:if test="${not empty align_tag.id}">
        <div class="form-group">
            <label for="id">Id</label> 
            <input type="text" name="id" value="${align_tag.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group has-feedback">
        <label for="value">Value</label> 
        <input type="text" name="value" id="value" value="${align_tag.value}" class="form-control" maxlength="400" />
        <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/align/tag/select" class="btn btn-default">Voltar</a>    
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#value').focus();
    });
</script>