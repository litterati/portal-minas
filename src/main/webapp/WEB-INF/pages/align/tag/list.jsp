<%-- 
    Document   : pages.align.tag.list
    Created on : 22/10/2014
    Author     : Thiago Vieira
    Controller : 
    Variables  : tag_list (List<AlignTag>)
                 tag_map (Map<AlignTag, Integer[]>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="2"></th>
            <!--<th>Id</th>-->
            <th>Value</th>
            <c:if test="${not empty tag_map}">
                <th># Senten�as</th>
                <th># Palavras</th>
            </c:if>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="tag" items="${tag_list}">
            <tr id="row${tag.id}">
                <td width="20">
                    <!-- Alterar -->
                    <a href="<%=request.getContextPath()%>/align/tag/update?id=${tag.id}&id_corpus=${id_corpus}" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </td>
                <td width="20">
                    <!-- Remover -->
                    <a href="#" data-toggle="modal" data-target="#modalDelete${tag.id}" title="Remover">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                    <!-- modal -->
                    <div class="modal fade" id="modalDelete${tag.id}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                    <h4 class="modal-title">Confirma��o</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Tem certeza que deseja remover a etiqueta de alinhamento '${tag.value}'?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${tag.id});">Confirmar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </td>
                <!--<td>${tag.id}</td>-->
                <td>${tag.value}</td>
                <c:if test="${not empty tag_map}">
                    <td>${tag_map[tag][0]}</td>
                    <td>${tag_map[tag][1]}</td>
                </c:if>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty tag_list}">
        <span class="label label-warning">Total: ${fn:length(tag_list)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� etiquetas de alinhamento</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id) {
        $.post("<%=request.getContextPath()%>/align/tag/delete", {id: id, id_corpus: ${id_corpus}, ajax: true})
                .done(function(data) {
                    $("#row" + id).hide();
                })
                .fail(function() {
                    $("#row" + id).addClass("danger");
                });
    }
</script>