<%-- 
    Document   : align.concordancer.search
    Created on : 13/06/2013
    Author     : Thiago Vieira
    Controller : br.project.controller.align.concordancer.ConcordancerAlignSearchController
    Variables  : concordancer (ConcordancerAlign)
                 id_corpus (int)
                 textTagsList (List<TextTags>)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style>
    .no-decoration {
        text-decoration: none;
        color: #333;
    }
</style>

<form role="form" method="get" action="<%=request.getContextPath()%>/align/concordancer/search">
    <!-- INPUT HIDDEN -->
    <input type="hidden" name="id_corpus" id="id_corpus" value="${id_corpus}" >
    <input type="hidden" name="currentPage" id="currentPage" value="${concordancer.currentPage}" >
    <input type="hidden" name="old_search" id="old_search" value="${concordancer.term}" >
    <!-- END INPUT HIDDEN -->

    <!-- SEARCH TERM -->
    <div class="form-group">
        <label class="sr-only" for="search">Palavra ou frase</label>
        <div class="input-group">
            <input type="text" id="search" name="search" class="form-control" value="${concordancer.term}" placeholder="Palavra ou frase" >
            <div class="input-group-btn">
                <button type="button" id="match" class="btn btn-default" data-toggle="modal" data-target="#modalProgress">Combinar</button>
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalProgress">Pesquisar</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right" style="padding:10px" role="menu">
                    <div class="form-group">
                        <label for="tolerance" class="control-label">Toler�ncia</label>
                        <input type="text" class="form-control" id="tolerance" name="tolerance" value="${concordancer.tolerance}" placeholder="0">
                    </div><br/>
                    <div class="form-group">
                        <label for="cmbResultByPage" class="control-label">Resultados por p�gina</label>
                        <input type="text" class="form-control" id="cmbResultByPage" name="cmbResultByPage" value="${concordancer.resultsByPage}" placeholder="50">
                    </div><br/><br/>
                    <a class="btn btn-default btn-block" href="#" onclick="filterShow()">Filtro</a>
                </div>
            </div>
        </div>
    </div>
    
    <c:if test="${fn:length(textTagsList) > 0}">
        <!-- FILTER -->
        <div class="form-group hidden" id="filter">
            <label class="sr-only" for="textTags">Tags do texto</label>
            <div class="input-group">
                <select class="form-control" id="textTags">
                    <option value="--">Selecione o filtro</option>
                    <c:forEach var="tag" items="${textTagsList}">
                        <option value="${tag.id}">${tag.tag}</option>
                    </c:forEach>
                </select>
                <div class="input-group-btn">
                    <button id="cleanFilter" type="button" class="btn btn-default">Limpar</button>
                </div>
            </div>
        </div>
        <c:forEach var="tag" items="${textTagsList}">
            <c:if test="${fn:length(tag.textTagsValues) > 0}">
                <div class="form-group hidden filter-value" id="div-${tag.id}">
                    <label class="sr-only" for="select-${tag.id}">${tag.tag}</label>
                    <select class="form-control filter-value-select" id="select-${tag.id}" idTextTag="${tag.id}" tagTextTag="${tag.tag}">
                        <option value="--" selected="true">${tag.tag}</option>
                        <c:forEach var="value" items="${tag.textTagsValues}">
                            <option value="${value.id}">${value.value}</option>
                        </c:forEach>
                    </select>
                </div>
            </c:if>
        </c:forEach>
        <!-- END FILTER -->
    </c:if>
    <!-- END SEARCH TERM -->
</form>

<c:if test="${not empty concordancer.term}">
    <!-- SEARCH RESULT -->
    <div id="search_result">
        <!-- INFORM THE NUMBER OF RESULTS, THE ANSWER TIME, THE CURRENT PAGE AND TOTAL PAGE NUMBER -->
        <c:if test="${concordancer.totalConcordances != 0 && concordancer.totalPages != 0}">
            <p id="searchStatistic" class="text-muted"> ${concordancer.totalConcordances} resultados, p�gina ${concordancer.currentPage} de ${concordancer.totalPages} (${concordancer.time / 1000} segundos)</p>
        </c:if>
        <!-- END INFORM THE NUMBER OF RESULTS, THE ANSWER TIME, THE CURRENT PAGE AND TOTAL PAGE NUMBER -->

        <!-- PAGE ERROR, WHEN IT EXIST -->
        <c:if test="${not empty concordancer.error}">
            <br/>
            <div class="alert alert-warning">${concordancer.error}</div>
        </c:if>
        <!-- END PAGE ERROR, WHEN IT EXIST -->

        <!-- CONCORDANCES -->
        <c:if test="${fn:length(concordancer.entries) > 0}" >
            <table class="table table-striped table-hover table-responsive" id="search">
                <c:forEach var="entry" items="${concordancer.entries}" varStatus="i">
                    <c:if test="${entry.sourceTerm != null && entry.targetTerm != null}">
                        <tr>
                            <td>
                                <c:forEach var="w" items="${entry.sourceList}" varStatus="status">
                                    <c:choose>
                                        <c:when test="${w.word == '_NEWLINE_'}"></c:when>
                                        <c:when test="${not empty w.contraction}">
                                            <c:if test="${not empty entry.sourceList[status.index + 1].contraction && w.contraction eq entry.sourceList[status.index + 1].contraction}">
                                                <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration" <c:if test="${w == entry.sourceTerm || entry.sourceList[status.index + 1] == entry.sourceTerm}">style="font-weight: bold;"</c:if> contraction="${w.word}+${entry.sourceList[status.index + 1].word}">${w.contraction}</a> 
                                            </c:if>
                                        </c:when>
                                        <c:otherwise> <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration" <c:if test="${w == entry.sourceTerm}">style="font-weight: bold;"</c:if> >${w.word}</a> </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <!-- Link to access the text -->
                                <br/>
                                <a href="<%=request.getContextPath()%>/text/view?id_text=${entry.sourceTerm.idText}&id_corpus=${entry.sourceTerm.idCorpus}" target="_blank"><small>${entry.sourceTerm.text.title}, ${entry.sourceTerm.text.language}</small></a>
                                <small>#${i.index+start}</small>
                            </td>
                            <td>
                                <c:forEach var="w" items="${entry.targetList}" varStatus="status">
                                    <c:choose>
                                        <c:when test="${w.word == '_NEWLINE_'}"></c:when>
                                        <c:when test="${not empty w.contraction}">
                                            <c:if test="${not empty entry.targetList[status.index + 1].contraction && w.contraction eq entry.targetList[status.index + 1].contraction}">
                                                <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration" <c:if test="${w == entry.targetTerm || entry.targetList[status.index + 1] == entry.targetTerm}">style="font-weight: bold;"</c:if> contraction="${w.word}+${entry.targetList[status.index + 1].word}">${w.contraction}</a> 
                                            </c:if>
                                        </c:when>
                                        <c:otherwise> <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration" <c:if test="${w == entry.targetTerm}">style="font-weight: bold;"</c:if> >${w.word}</a> </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <!-- Link to access the text -->
                                <br/>
                                <a href="<%=request.getContextPath()%>/text/view?id_text=${entry.targetTerm.idText}&id_corpus=${entry.targetTerm.idCorpus}" target="_blank"><small>${entry.targetTerm.text.title}, ${entry.targetTerm.text.language}</small></a> 
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </table>
        </c:if>
        <!-- END CONCORDANCES -->
        
        <!-- MATCHES -->
        <c:if test="${fn:length(matches) > 0}" >
            <div class="row">
                <c:forEach var="match" items="${matches}" varStatus="i">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4><strong>${match.source.word}</strong> <small>${match.source.pos} ${match.source.text.language}</small></h4>
                            </div>
                            <table class="table table-striped table-hover table-responsive" id="search">
                                <tr>
                                    <td title="N�mero">#</td>
                                    <td title="Token alinhado">Token</td>
                                    <td title="Part-of-Speech">PoS</td>
                                    <td title="Idioma alvo">Idioma</td>
                                    <td title="Ocorr�ncia">Oco.</td>
                                </tr>
                                <c:forEach var="target" items="${match.targets}" varStatus="status">
                                    <tr>
                                        <td>${status.index+1}</td>
                                        <td>
                                            <a href="<%=request.getContextPath()%>/text/view?id_text=${target.value.idText}&id_corpus=${target.value.idCorpus}&idWord=${target.value.id}&idSentence=${target.value.idSentence}" target="_blank" class="no-decoration" ><strong>${target.value.word}</strong></a>
                                        </td>
                                        <td>${target.value.pos}</td>
                                        <td>${target.value.text.language}</td>
                                        <td>${target.key}</td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td title="Total">${match.total}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <c:if test="${(i.index + 1) % 3 == 0}"></div><div class="row"></c:if>
                </c:forEach>
            </div>
        </c:if>
        <!-- END MATCHES -->

        <!-- Pagination -->
        <c:if test="${concordancer.totalPages > 0 && fn:length(concordancer.entries) > 0}">
            <div class="text-center">
                <ul class="pagination">
                    <li title="Primeira"><a href="#" onclick="formSubmit(1);">&larr;</a></li>
                    <c:forEach var="page_i" begin="1" end="${concordancer.totalPages}">
                        <li <c:if test="${page_i == concordancer.currentPage}"> class="active" </c:if> ><a href="#"  onclick="formSubmit(${page_i});">${page_i}</a></li>
                    </c:forEach>
                    <li title="�ltima"><a href="#"  onclick="formSubmit(${concordancer.totalPages});">&rarr;</a></li>
                </ul>
                <div class="clearfix"></div>
                <span class="label label-warning">Exibindo ${fn:length(concordancer.entries)} de um total de ${concordancer.totalConcordances} tokens.</span>
            </div>
        </c:if>
        <!-- /Pagination -->
    </div>
    <!-- END SEARCH RESULT -->
</c:if>

<c:if test="${empty concordancer.term}">
    <!-- HELP -->
    <div class="bs-callout bs-callout-info" >
        <h4>Como pesquisar!?</h4>
        <p>Voc� pode pesquisar palavras seguidas, por exemplo: <code>caf� da manh�</code></p>
        <p>Tamb�m � possivel pesquisar uma palavra ou outra, por exemplo: <code>caf�+manh�</code></p>
        <p>Pesquisar pelo prefixo, por exemplo: <code>prefix:caf</code><br/>Resultados: caf�, cafeteria, caffeine, caf�-da-manh�, cafund�s...</p>
        <p>Pesquisar pelo sufixo, por exemplo: <code>suffix:f�</code><br/>Resultados: f�, caf�...</p>
        <p>Pesquisar pelo infixo, por exemplo: <code>infix:af</code><br/>Resultados: aferrara, telegrafista, garrafa, caf�, graficamente...</p>
        <p>E os filtros ajudam a limitar sua busca.<br/><img class="img-thumbnail" src="<%=request.getContextPath()%>/images/help/concordancer-filter-text-tag.png"/></p>
        <p>Se voc� precisar de mais ajuda para otimizar sua consulta <a href="<%=request.getContextPath()%>/align/concordancer/advanced?id_corpus=${id_corpus}">clique aqui!</a></p>
    </div>
    <!-- HELP -->
</c:if>
    
<!-- modal -->
<div class="modal fade" id="modalProgress" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Aguarde, pesquisando...</h4>
            </div>
            <div class="modal-body">
                <img src="<%=request.getContextPath()%>/images/progress.gif" class="img-responsive center-block" alt="Responsive image">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function formSubmit(page) {
        $("#currentPage").val(page);
        $("form").submit();
    }
    $(".dropdown-menu").click(function (e) {
        e.stopPropagation();
     });
    function filterShow() {
        $("#filter").removeClass("hidden");
        $(".dropdown-menu").parent().removeClass('open');
    }
    $("#textTags").change(function() {
        var value = $(this).val();
        $('#textTags').find('option').removeAttr('disabled').end();//enable all options
        $(".filter-value").addClass("hidden");//hide all
        if (value !== "--"){
            $("#div-" + value).removeClass("hidden");//show hust one
            $("#textTags option[value='" + $(this).val() + "']").attr('disabled', true);//disable too select again
            $("#select-" + value).focus();
        }
    });
    $("#cleanFilter").click(function() {
        $(".filter-value").addClass("hidden");//hide all
        $(".filter-value-select").find('option').removeAttr('disabled').end();//enable all options
        $('#textTags').find('option').removeAttr('disabled').end();//enable all options
        $(".metadata").remove();//remove all metadata
    });
    $(".filter-value-select").change(function() {
        var value = $(this).val();
        var text = $(this).find('option:selected').text();
        if (value !== "--"){
            $(this).find('option:selected').attr('disabled', true);
            $("form").append("<button type='button' id='metadata-" + value + "' class='btn btn-default btn-xs metadata' title='" + $(this).attr("tagTextTag") + "' onclick='removeFilter(" + value + ")' >"+ text +" <span class='glyphicon glyphicon-remove' aria-hidden='true'></span><input type='hidden' name='metadata[]' value='" + $(this).attr("idTextTag") + "|" + value + "|" + text + "'></button> ");
        }
    });
    function removeFilter(metadata_id) {
        $("#metadata-" + metadata_id).remove();//remove metadata
        $(".filter-value-select").children('option[value=' + metadata_id + ']').removeAttr('disabled');//enable option
    }
    <c:if test="${concordancer.lstTextTagValues != null && fn:length(concordancer.lstTextTagValues) > 0 }">
        //Selected metadata (Filter)
        filterShow();
        <c:forEach var="ttv" items="${concordancer.lstTextTagValues}">
            $("#select-${ttv.idTextTag}")
                    .children("option[value=${ttv.id}]")
                    .attr("selected", true)
                    .trigger("change")
                    .removeAttr("selected");//${ttv.id} ${ttv.idTextTag} ${ttv.value}
        </c:forEach>
    </c:if>
    $("button#match").click(function() {
        var action = "<%=request.getContextPath()%>/align/concordancer/match";
        $("form")
                .attr("action", action)
                .submit();
    });
</script>