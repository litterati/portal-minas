<%-- 
    Document   : helper-view-text
    Created on : 15/10/2014
    Author     : Thiago Vieira
    Controller : 
    JSP        : align/view.jsp
                 align/sentence/view.jsp
                 align/word/view.jsp
    Variables  : text_aling (TextAling)
                 source (List<Words>)
                 target (List<Words>)
                 id_corpus (int)
                 sentenceAlignCrrMap (Map<Integer, String>) Map<IdSentence, crr>
                 sentenceAlignMap (Map<Integer, SentenceAlign>) Map<IdSentence, SentenceAlign>
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="row">
    <div id="source" class="col-md-6">
        <h3>${text_align.source.title} <small>${text_align.source.language}</small></h3>

        <div idtext="${text_align.source.id}" idcorpus="${id_corpus}" >

            <c:forEach var="w" items="${source}" varStatus="status">
                <c:if test="${source[status.index].position == 0}">
                    <p><s idsentence="${source[status.index].idSentence}" crr="${sentenceAlignCrrMap[source[status.index].idSentence]}" idaligntag="${sentenceAlignMap[source[status.index].idSentence].idAlignTag}" aligntype="${sentenceAlignMap[source[status.index].idSentence].type}">
                </c:if>
                <c:choose>
                    <c:when test="${w.word eq '_NEWLINE_'}">
                        <!--<br idword="${w.id}" position="${w.position}">-->
                        </s></p><p><s idsentence="${source[status.index].idSentence}" crr="${sentenceAlignCrrMap[source[status.index].idSentence]}" idaligntag="${sentenceAlignMap[source[status.index].idSentence].idAlignTag}" aligntype="${sentenceAlignMap[source[status.index].idSentence].type}">
                    </c:when>
                    <c:when test="${not empty w.contraction}">
                        <c:if test="${not empty source[status.index + 1].contraction && w.contraction eq source[status.index + 1].contraction}">
                            <w idWord="${w.id}" position="${w.position}+${source[status.index + 1].position}" crr="<c:forEach var="crr" items="${w.wordAlign}">${crr.idTarget} </c:forEach>" contraction="${w.word}+${source[status.index + 1].word}" <c:if test="${status.index >= 5000}">class="bonus"</c:if> idaligntag="${w.wordAlign[0].idAlignTag}" aligntype="${w.wordAlign[0].type}">${w.contraction}</w>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                    <w idword="${w.id}" position="${w.position}" crr="<c:forEach var="crr" items="${w.wordAlign}">${crr.idTarget} </c:forEach>" <c:if test="${status.index >= 5000}">class="bonus"</c:if> idaligntag="${w.wordAlign[0].idAlignTag}" aligntype="${w.wordAlign[0].type}">${w.word}</w>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${(source[status.index].idSentence != source[status.index + 1].idSentence) && (source[status.index + 1].idSentence > 0)}">
                        </s><s idsentence="${source[status.index + 1].idSentence}" crr="${sentenceAlignCrrMap[source[status.index + 1].idSentence]}" idaligntag="${sentenceAlignMap[source[status.index + 1].idSentence].idAlignTag}" aligntype="${sentenceAlignMap[source[status.index + 1].idSentence].type}">
                    </c:when>
                    <c:when test="${!(source[status.index + 1].idSentence > 0)}">
                        </s></p>
                    </c:when>
                </c:choose>
            </c:forEach>

        </div>
    </div>
    <div id="target" class="col-md-6">
        <h3>${text_align.target.title} <small>${text_align.target.language}</small></h3>

        <div idtext="${text_align.target.id}" idcorpus="${id_corpus}" >

            <c:forEach var="w" items="${target}" varStatus="status">
                <c:if test="${target[status.index].position == 0}">
                    <p><s idsentence="${target[status.index].idSentence}" crr="${sentenceAlignCrrMap[target[status.index].idSentence]}" idaligntag="${sentenceAlignMap[target[status.index].idSentence].idAlignTag}" aligntype="${sentenceAlignMap[target[status.index].idSentence].type}">
                </c:if>
                <c:choose>
                    <c:when test="${w.word eq '_NEWLINE_'}">
                        <!--<br idword="${w.id}" position="${w.position}">-->
                        </s></p><p><s idsentence="${target[status.index].idSentence}" crr="${sentenceAlignCrrMap[target[status.index].idSentence]}" idaligntag="${sentenceAlignMap[target[status.index].idSentence].idAlignTag}" aligntype="${sentenceAlignMap[target[status.index].idSentence].type}">
                    </c:when>
                    <c:when test="${not empty w.contraction}">
                        <c:if test="${not empty target[status.index + 1].contraction && w.contraction eq target[status.index + 1].contraction}">
                            <w idWord="${w.id}" position="${w.position}+${target[status.index + 1].position}" crr="<c:forEach var="crr" items="${w.wordAlign}">${crr.idSource} </c:forEach>" contraction="${w.word}+${target[status.index + 1].word}" <c:if test="${status.index >= 5000}">class="bonus"</c:if> idaligntag="${w.wordAlign[0].idAlignTag}" aligntype="${w.wordAlign[0].type}">${w.contraction}</w>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <w idword="${w.id}" position="${w.position}" crr="<c:forEach var="crr" items="${w.wordAlign}">${crr.idSource} </c:forEach>" <c:if test="${status.index >= 5000}">class="bonus"</c:if> idaligntag="${w.wordAlign[0].idAlignTag}" aligntype="${w.wordAlign[0].type}">${w.word}</w>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${(target[status.index].idSentence != target[status.index + 1].idSentence) && (target[status.index + 1].idSentence > 0)}">
                        </s><s idsentence="${target[status.index + 1].idSentence}" crr="${sentenceAlignCrrMap[target[status.index + 1].idSentence]}" idaligntag="${sentenceAlignMap[target[status.index + 1].idSentence].idAlignTag}" aligntype="${sentenceAlignMap[target[status.index + 1].idSentence].type}">
                    </c:when>
                    <c:when test="${!(target[status.index + 1].idSentence > 0)}">
                        </s></p>
                    </c:when>
                </c:choose>
            </c:forEach>

        </div>
    </div>
</div>
<style>
    .bottom-center {
        position: fixed;
        margin: 0px 0px 0px -163.5px;
        z-index: 1031;
        display: inline-block;
        bottom: 20px;
        left: 50%;
    }
    .bottom-right {
        position: fixed;
        margin: 0px;
        z-index: 1031;
        display: inline-block;
        bottom: 20px;
        right: 20px;
    }
</style>
<div id="tag-align-notify" class="col-xs-11 col-sm-3 hidden alert alert-info alert-dismissible fade in bottom-right" role="alert">
    <button type="button" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    <p></p>
</div>
