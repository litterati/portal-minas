<%-- 
    Document   : concordancer.advanced
    Created on : 20/10/2014
    Author     : Thiago Vieira
    Controller : br.project.controller.ConcordancerAdvancedController
    Variables  : id_corpus (int)
                 concordancer (Concordancer)
                 textTagsList (List<TextTags>)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form role="form" method="get" action="<%=request.getContextPath()%>/concordancer/search">
    <!-- INPUT HIDDEN -->
    <input type="hidden" name="id_corpus" id="id_corpus" value="${id_corpus}" >
    <!-- END INPUT HIDDEN -->

    <!-- SEARCH TERM -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Localizar resultados com...</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="allTokens">Todos esses tokens juntos (e)</label>
                <input type="text" name="allTokens" id="allTokens" class="form-control" value="${concordancer.term}" >
                <p class="help-block">c�o de guarda.</p>
            </div>
            <div class="form-group">
                <label for="anyTokens">Qualquer um desses tokens (ou)</label>
                <input type="text" name="anyTokens" id="anyTokens" class="form-control" value="${concordancer.term}" >
                <p class="help-block">c�o, gato ou rato.</p>
            </div>
            <div class="form-group">
                <label for="prefix">Tokens que come�e por (prefixo)</label>
                <input type="text" name="prefix" id="prefix" class="form-control" value="${concordancer.term}" >
                <p class="help-block">Part�cula que se p�e no princ�pio de uma palavra para lhe modificar o sentido. Ex. para-: paralelo, parasita, paradoxo, paradigma.</p>
            </div>
            <div class="form-group">
                <label for="suffix">Tokens que termine por (sufixo)</label>
                <input type="text" name="suffix" id="suffix" class="form-control" value="${concordancer.term}" >
                <p class="help-block">S�laba ou letras que se juntam ao tema ou raiz de um voc�bulo para modificar a sua significa��o. Ex. -ismo: budismo, kantismo, comunismo.</p>
            </div>
            <div class="form-group">
                <label for="infixo">Tokens que contenham (infixo)</label>
                <input type="text" name="infix" id="infix" class="form-control" value="${concordancer.term}" >
                <p class="help-block">Chama-se infixo o afixo que se insere no interior de uma palavra para modificar-lhe o sentido. Ex. -ch-: acho, rachar, deslincha, trechos.</p>
            </div>
            <div class="form-group">
                <label for="lemma">Tokens com o lema</label>
                <input type="text" name="lemma" id="lemma" class="form-control" value="${concordancer.term}" >
                <p class="help-block">Forma gr�fica de uma palavra que � usada como entrada de verbete em dicion�rios ou vocabul�rios (por exemplo, o lema da forma verbal ter� � ter, o lema do adjetivo m� � mau, o lema do substantivo ju�zes � juiz).</p>
            </div>
            <div class="form-group">
                <label for="pos">Tokens com o Part-of-Speech (PoS)</label>
                <select name="pos" id="pos" class="form-control">
                    <option value="--" selected>--</option>
                    <option value="VERB">verbs (all tenses and modes)</option>
                    <option value="NOUN">nouns (common and proper)</option>
                    <option value="PRON">pronouns</option>
                    <option value="ADJ">adjectives</option>
                    <option value="ADV">adverbs</option>
                    <option value="ADP">adpositions (prepositions and postpositions)</option>
                    <option value="CONJ">conjunctions</option>
                    <option value="DET">determiners</option>
                    <option value="NUM">cardinal numbers</option>
                    <option value="PRT">particles or other function words</option>
                    <option value="X">other: foreign words, typos, abbreviations</option>
                    <option value=".">punctuation</option>
                </select>
                <p class="help-block">Classe morfol�gica das palavras e � sua fun��o na frase. Conjunto de etiquetas utilizadas: <a target="_blank" href="https://code.google.com/p/universal-pos-tags/">Universal Part-of-Speech Tagset.</a></p>
            </div>
        </div>
    </div>
    <!-- END SEARCH TERM -->
    
    <!-- METADATA -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Filtrar resultados...</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="tokenType">Por tipo</label>
                <select name="tokenType" id="tokenType" class="form-control">
                    <option value="texto" selected>Texto</option>
                    <option value="metatexto">Metatexto</option>
                </select>
                <p class="help-block">Pesquisar nos tokens do texto ou do metatexto.</p>
            </div>
            <!-- METADATA -->
            <div class="form-group">
                <label for="textTags">Por etiqueta</label>
                <div class="input-group">
                    <select class="form-control" id="textTags">
                        <option value="--">Selecione o filtro</option>
                        <c:forEach var="tag" items="${textTagsList}">
                            <option value="${tag.id}">${tag.tag}</option>
                        </c:forEach>
                    </select>
                    <div class="input-group-btn">
                        <button id="cleanFilter" type="button" class="btn btn-default">Limpar</button>
                    </div>
                </div>
            </div>
            <p class="help-block">Metadados dos textos.</p>
            
            <c:forEach var="tag" items="${textTagsList}">
                <c:if test="${fn:length(tag.textTagsValues) > 0}">
                    <div class="form-group hidden filter-value" id="div-${tag.id}">
                        <label class="sr-only" for="select-${tag.id}">${tag.tag}</label>
                        <select class="form-control filter-value-select" id="select-${tag.id}" idTextTag="${tag.id}" tagTextTag="${tag.tag}">
                            <option value="--" selected="true">${tag.tag}</option>
                            <c:forEach var="value" items="${tag.textTagsValues}">
                                <option value="${value.id}">${value.value}</option>
                            </c:forEach>
                        </select>
                    </div>
                </c:if>
            </c:forEach>

            <div id="metadataFields" class="form-group"></div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" value="uppercaseLowercase" name="uppercaseLowercase">
                    Ignorar letras mai�sculas e min�sculas.
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="specialCharacter" name="specialCharacter">
                    Ignorar acentos e c�-cedilha.
                </label>
            </div>
        </div>
    </div>
    <!-- END METADATA -->

    <!-- PAGE CONFIGURATION -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Exibir resultados...</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="tolerance">Toler�ncia</label>
                <input class="form-control" type="text" name="tolerance" id="tolerance" value="${concordancer.tolerance}">
                <p class="help-block">Quantidade de posi��es de tokens que podem ser pulados na pesquisa.</p>
            </div>
            <div class="form-group">
                <label for="cmbResultByPage">Quantidade por p�gina</label>
                <input class="form-control" type="text" name="cmbResultByPage" id="cmbResultByPage" value="${concordancer.resultsByPage}">
                <p class="help-block">Quantidade de resultados por p�ginas.</p>
            </div>
            <div class="form-group">
                <label for="leftTokens">Quantidade de tokens a esquerda</label>
                <input class="form-control" type="text" name="leftTokens" id="leftTokens" value="${concordancer.leftTokensNumber}">
                <p class="help-block">Tamanho da janela em n�mero de tokens que ser� exibido nos resultados.</p>
            </div>
            <div class="form-group">
                <label for="rightTokens">Quantidade de tokens a direita</label>
                <input class="form-control" type="text" name="rightTokens" id="rightTokens" value="${concordancer.rightTokensNumber}">
                <p class="help-block">Tamanho da janela em n�mero de tokens que ser� exibido nos resultados.</p>
            </div>
        </div>
    </div>
    <!-- END PAGE CONFIGURATION -->         

    <button type="submit" class="btn btn-primary">Pesquisar</button>
</form>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    $("#textTags").change(function() {
        var value = $(this).val();
        $('#textTags').find('option').removeAttr('disabled').end();//enable all options
        $(".filter-value").addClass("hidden");//hide all
        if (value !== "--"){
            $("#div-" + value).removeClass("hidden");//show hust one
            $("#textTags option[value='" + $(this).val() + "']").attr('disabled', true);//disable too select again
            $("#select-" + value).focus();
        }
    });
    $("#cleanFilter").click(function() {
        $(".filter-value").addClass("hidden");//hide all
        $(".filter-value-select").find('option').removeAttr('disabled').end();//enable all options
        $('#textTags').find('option').removeAttr('disabled').end();//enable all options
        $(".metadata").remove();//remove all metadata
    });
    $(".filter-value-select").change(function() {
        var value = $(this).val();
        var text = $(this).find('option:selected').text();
        if (value !== "--"){
            $(this).find('option:selected').attr('disabled', true);
            $("#metadataFields").append("<button type='button' id='metadata-" + value + "' class='btn btn-default btn-xs metadata' title='" + $(this).attr("tagTextTag") + "' onclick='removeFilter(" + value + ")' >"+ text +" <span class='glyphicon glyphicon-remove' aria-hidden='true'></span><input type='hidden' name='metadata[]' value='" + $(this).attr("idTextTag") + "|" + value + "|" + text + "'></button> ");
        }
    });
    function removeFilter(metadata_id) {
        $("#metadata-" + metadata_id).remove();//remove metadata
        $(".filter-value-select").children('option[value=' + metadata_id + ']').removeAttr('disabled');//enable option
    }
    $("form").submit(function() {
        var allTokens = $("#allTokens").val().trim();
        var anyTokens = $("#anyTokens").val().trim();
        var prefix = $("#prefix").val().trim();
        var suffix = $("#suffix").val().trim();
        var infix = $("#infix").val().trim();
        var lemma = $("#lemma").val().trim();
        var pos = $("#pos").find('option:selected').val();
        //remove aux
        $("#allTokens").remove();
        $("#anyTokens").remove();
        $("#prefix").remove();
        $("#suffix").remove();
        $("#infix").remove();
        $("#lemma").remove();
        $("#pos").remove();
        //search
        var pattern = /( +|, ?)/g;
        var search = " ";
        if (allTokens.length > 0){
            search = allTokens.replace(pattern, " ") + " ";
        }
        if (anyTokens.length > 0){
            search += anyTokens.replace(pattern, "+") + " ";
        }
        if (prefix.length > 0){
            search += "prefix:" + prefix.replace(pattern, "+prefix:") + " ";
        }
        if (suffix.length > 0){
            search += "suffix:" + suffix.replace(pattern, "+suffix:") + " ";
        }
        if (infix.length > 0){
            search += "infix:" + infix.replace(pattern, "+infix:") + " ";
        }
        if (lemma.length > 0){
            search += "lemma:" + lemma.replace(pattern, "+lemma:") + " ";
        }
        if (pos !== "--"){
            search += "pos:" + pos + " ";
        }
        //element
        $('<input>').attr({
            type: 'hidden',
            id: 'search',
            name: 'search',
            value: search.trim()
        }).appendTo('form');
    });
</script>