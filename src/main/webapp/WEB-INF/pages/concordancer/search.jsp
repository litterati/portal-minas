<%-- 
    Document   : concordancer.search
    Created on : 13/06/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.concordancer.ConcordancerSearchController
    Variables  : concordancer (Concordancer)
                 id_corpus (int)
                 textTagsList (List<TextTags>)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="<%=request.getContextPath()%>/jquery-ui/jquery-ui.min.css" />
    
<style>
    .no-decoration {
        text-decoration: none;
        color: #333;
    }
</style>

<form role="form" method="get">
    <!-- INPUT HIDDEN -->
    <input type="hidden" name="id_corpus" id="id_corpus" value="${id_corpus}" >
    <input type="hidden" name="currentPage" id="currentPage" value="${concordancer.currentPage}" >
    <!-- END INPUT HIDDEN -->

    <!-- SEARCH TERM -->
    <div class="form-group">
        <label class="sr-only" for="search">Palavra ou frase</label>
        <div class="input-group">
            <input type="text" id="search" name="search" class="form-control" value="${concordancer.term}" placeholder="Palavra ou frase" >
            <div class="input-group-btn">
                <button type="submit" class="btn btn-primary">Pesquisar</button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right" style="padding:10px" role="menu">
                    <div class="form-group">
                        <label for="tolerance" class="control-label">Toler�ncia</label>
                        <input type="text" class="form-control" id="tolerance" name="tolerance" value="${concordancer.tolerance}" placeholder="0">
                    </div><br/>
                    <div class="form-group">
                        <label for="cmbResultByPage" class="control-label">Resultados por p�gina</label>
                        <input type="text" class="form-control" id="cmbResultByPage" name="cmbResultByPage" value="${concordancer.resultsByPage}" placeholder="50">
                    </div><br/>
                    <div class="form-group">
                        <label for="leftTokens" class="control-label">Tokens � esquerda</label>
                        <input type="text" class="form-control" id="leftTokens" name="leftTokens" value="${concordancer.leftTokensNumber}" placeholder="7">
                    </div><br/>
                    <div class="form-group">
                        <label for="rightTokens" class="control-label">Tokens � direita</label>
                        <input type="text" class="form-control" id="rightTokens" name="rightTokens" value="${concordancer.rightTokensNumber}" placeholder="7">
                    </div><br/>
                    <div class="form-group">
                        <label for="tokenType" class="control-label">Tipo</label>
                        <select name="tokenType" id="tokenType" class="form-control">
                            <option value="texto" <c:if test="${concordancer.searchTarget eq 'texto'}"> selected </c:if>>Texto</option>
                            <option value="metatexto" <c:if test="${concordancer.searchTarget eq 'metatexto'}"> selected </c:if>>Metatexto</option>
                        </select>
                    </div>
                    <br/><br/>
                    <a class="btn btn-default btn-block" href="#" onclick="filterShow()">Filtro</a>
                </div>
            </div>
        </div>
    </div>
    
    <c:if test="${fn:length(textTagsList) > 0}">
        <!-- FILTER -->
        <div class="form-group hidden" id="filter">
            <label class="sr-only" for="textTags">Tags do texto</label>
            <div class="input-group">
                <select class="form-control" id="textTags">
                    <option value="--">Selecione o filtro</option>
                    <c:forEach var="tag" items="${textTagsList}">
                        <option value="${tag.id}">${tag.tag}</option>
                    </c:forEach>
                </select>
                <div class="input-group-btn">
                    <button id="cleanFilter" type="button" class="btn btn-default">Limpar</button>
                </div>
            </div>
        </div>
        <c:forEach var="tag" items="${textTagsList}">
            <c:if test="${fn:length(tag.textTagsValues) > 0}">
                <div class="form-group hidden filter-value" id="div-${tag.id}">
                    <label class="sr-only" for="select-${tag.id}">${tag.tag}</label>
                    <select class="form-control filter-value-select" id="select-${tag.id}" idTextTag="${tag.id}" tagTextTag="${tag.tag}">
                        <option value="--" selected="true">${tag.tag}</option>
                        <c:forEach var="value" items="${tag.textTagsValues}">
                            <option value="${value.id}">${value.value}</option>
                        </c:forEach>
                    </select>
                </div>
            </c:if>
        </c:forEach>
        <!-- END FILTER -->
    </c:if>
    <!-- END SEARCH TERM -->
</form>

<c:if test="${not empty concordancer.term}">
    <!-- SEARCH RESULT -->
    <div id="search_result">
        <!-- INFORM THE NUMBER OF RESULTS, THE ANSWER TIME, THE CURRENT PAGE AND TOTAL PAGE NUMBER -->
        <c:if test="${concordancer.totalConcordances != 0 && concordancer.totalPages != 0}">
            <p id="searchStatistic" class="text-muted"> ${concordancer.totalConcordances} resultados, p�gina ${concordancer.currentPage} de ${concordancer.totalPages} (${concordancer.time} milisegundos)</p>
        </c:if>
        <!-- END INFORM THE NUMBER OF RESULTS, THE ANSWER TIME, THE CURRENT PAGE AND TOTAL PAGE NUMBER -->

        <!-- PAGE ERROR, WHEN IT EXIST -->
        <c:if test="${not empty concordancer.error}">
            <br/>
            <div class="alert alert-warning">${concordancer.error}</div>
        </c:if>
        <!-- END PAGE ERROR, WHEN IT EXIST -->

        <!-- CONCORDANCES -->
        <c:if test="${fn:length(concordancer.entries) > 0}" >
            <table class="table table-striped table-hover table-responsive" id="search">
                <c:forEach var="entry" items="${concordancer.entries}" varStatus="i">
                    <c:if test="${entry.term != null}">
                        <tr>
                            <td style="max-width: 50px; overflow: hidden;" title="Texto: ${entry.term.text.title} | Idioma: ${entry.term.text.language}">
                                <!-- Link to access the text -->
                                <a href="<%=request.getContextPath()%>/text/view?id_text=${entry.term.idText}&id_corpus=${entry.term.idCorpus}" target="_blank">${entry.term.text.title}</a> 
                            </td>
                            <td class="text-right">
                                <!-- Left tokens -->
                                <c:forEach var="w" items="${entry.words}" varStatus="status">
                                    <c:choose>
                                        <c:when test="${w == entry.term}">
                                            </td>
                                            <td class="text-center">
                                                <!-- The term used to search -->
                                                <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration"><strong>${w.word}</strong></a>
                                            </td>
                                            <td class="text-left">
                                        </c:when>
                                        <c:when test="${w.word == '_NEWLINE_'}"></c:when>
                                        <c:when test="${not empty w.contraction}">
                                            <c:if test="${not empty entry.words[status.index + 1].contraction && w.contraction eq entry.words[status.index + 1].contraction}">
                                                <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration" contraction="${w.word}+${entry.words[status.index + 1].word}">${w.contraction}</a> 
                                            </c:if>
                                        </c:when>
                                        <c:otherwise> <a href="<%=request.getContextPath()%>/text/view?id_text=${w.idText}&id_corpus=${w.idCorpus}&idWord=${w.id}&idSentence=${w.idSentence}" target="_blank" class="no-decoration">${w.word}</a> </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <!-- Right tokens -->
                            </td>
                            <td style="max-width: 20px; overflow: hidden;" title="resultado #${i.index+start}">${i.index+start}</td>
                        </tr>
                    </c:if>
                </c:forEach>
            </table>
        </c:if>
        <!-- END CONCORDANCES -->

        <!-- Pagination -->
        <c:if test="${concordancer.totalPages > 0}">
            <div class="text-center">
                <ul class="pagination">
                    <li title="Primeira"><a href="#" onclick="formSubmit(1);">&larr;</a></li>
                    <c:forEach var="page_i" begin="1" end="${concordancer.totalPages}">
                        <li <c:if test="${page_i == concordancer.currentPage}"> class="active" </c:if> ><a href="#"  onclick="formSubmit(${page_i});">${page_i}</a></li>
                    </c:forEach>
                    <li title="�ltima"><a href="#"  onclick="formSubmit(${concordancer.totalPages});">&rarr;</a></li>
                </ul>
                <div class="clearfix"></div>
                <span class="label label-warning">Exibindo ${fn:length(concordancer.entries)} de um total de ${concordancer.totalConcordances} tokens.</span>
            </div>
        </c:if>
        <!-- /Pagination -->
    </div>
    <!-- END SEARCH RESULT -->
</c:if>

<c:if test="${empty concordancer.term}">
    <!-- HELP -->
    <div class="bs-callout bs-callout-info" >
        <h4>Como pesquisar!?</h4>
        <p>Voc� pode pesquisar palavras seguidas, por exemplo: <code>caf� da manh�</code></p>
        <p>Tamb�m � possivel pesquisar uma palavra ou outra, por exemplo: <code>caf�+manh�</code></p>
        <p>Pesquisar pelo prefixo, por exemplo: <code>prefix:caf</code><br/>Resultados: caf�, cafeteria, caffeine, caf�-da-manh�, cafund�s...</p>
        <p>Pesquisar pelo sufixo, por exemplo: <code>suffix:f�</code><br/>Resultados: f�, caf�...</p>
        <p>Pesquisar pelo infixo, por exemplo: <code>infix:af</code><br/>Resultados: aferrara, telegrafista, garrafa, caf�, graficamente...</p>
        <p>As configura��es de p�gina ajudam a visualizar melhor o resultado.<br/><img class="img-thumbnail" src="<%=request.getContextPath()%>/images/help/concordancer-filter.png"/></p>
        <p>E os filtros ajudam a limitar sua busca.<br/><img class="img-thumbnail" src="<%=request.getContextPath()%>/images/help/concordancer-filter-text-tag.png"/></p>
        <p>Se voc� precisar de mais ajuda para otimizar sua consulta <a href="<%=request.getContextPath()%>/concordancer/advanced?id_corpus=${id_corpus}">clique aqui!</a></p>
    </div>
    <!-- HELP -->
</c:if>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
    function formSubmit(page) {
        $("#currentPage").val(page);
        $("form").submit();
    }
    $(".dropdown-menu").click(function (e) {
        e.stopPropagation();
     });
    function filterShow() {
        $("#filter").removeClass("hidden");
        $(".dropdown-menu").parent().removeClass('open');
    }
    $("#textTags").change(function() {
        var value = $(this).val();
        $('#textTags').find('option').removeAttr('disabled').end();//enable all options
        $(".filter-value").addClass("hidden");//hide all
        if (value !== "--"){
            $("#div-" + value).removeClass("hidden");//show hust one
            $("#textTags option[value='" + $(this).val() + "']").attr('disabled', true);//disable too select again
            $("#select-" + value).focus();
        }
    });
    $("#cleanFilter").click(function() {
        $(".filter-value").addClass("hidden");//hide all
        $(".filter-value-select").find('option').removeAttr('disabled').end();//enable all options
        $('#textTags').find('option').removeAttr('disabled').end();//enable all options
        $(".metadata").remove();//remove all metadata
    });
    $(".filter-value-select").change(function() {
        var value = $(this).val();
        var text = $(this).find('option:selected').text();
        if (value !== "--"){
            $(this).find('option:selected').attr('disabled', true);
            $("form").append("<button type='button' id='metadata-" + value + "' class='btn btn-default btn-xs metadata' title='" + $(this).attr("tagTextTag") + "' onclick='removeFilter(" + value + ")' >"+ text +" <span class='glyphicon glyphicon-remove' aria-hidden='true'></span><input type='hidden' name='metadata[]' value='" + $(this).attr("idTextTag") + "|" + value + "|" + text + "'></button> ");
        }
    });
    function removeFilter(metadata_id) {
        $("#metadata-" + metadata_id).remove();//remove metadata
        $(".filter-value-select").children('option[value=' + metadata_id + ']').removeAttr('disabled');//enable option
    }
    <c:if test="${concordancer.lstTextTagValues != null && fn:length(concordancer.lstTextTagValues) > 0 }">
        //Selected metadata (Filter)
        filterShow();
        <c:forEach var="ttv" items="${concordancer.lstTextTagValues}">
            $("#select-${ttv.idTextTag}")
                    .children("option[value=${ttv.id}]")
                    .attr("selected", true)
                    .trigger("change")
                    .removeAttr("selected");//${ttv.id} ${ttv.idTextTag} ${ttv.value}
        </c:forEach>
    </c:if>

//SCRIPT PARA AUTO_COMPLETAR AS BUSCAS
    var availableTags = [
        <c:if test="${searchHistory != null}"><c:forEach var="term" items="${searchHistory}">
            "${term}",
        </c:forEach></c:if>
        ];
    
    $("#search").autocomplete({
        source: availableTags
    });

</script>
