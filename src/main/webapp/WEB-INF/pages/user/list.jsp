<%-- 
    Document   : list-user
    Created on : 17/05/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.UsersSelectController
    Variables  : users (List<User>)
                 order (String)
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <th colspan="2"></th>
            <th>Nome <a href="<%=request.getContextPath()%>/user/select?order=name" title="Ordernar por nome"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Login <a href="<%=request.getContextPath()%>/user/select?order=login" title="Ordernar por login"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
            <th>Tipo <a href="<%=request.getContextPath()%>/user/select?order=type" title="Ordernar por tipo"><span class="glyphicon glyphicon-sort-by-attributes"></span></a></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="user" items="${users}">
            <tr id="row${user.id}">
                <td width="20">
                    <!-- Alterar -->
                    <a href="<%=request.getContextPath()%>/user/update?id=${user.id}" title="Alterar">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </td>
                <td width="20">
                    <!-- Remover -->
                    <a href="#" data-toggle="modal" data-target="#modalDelete${user.id}" title="Remover">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                    <!-- modal -->
                    <div class="modal fade" id="modalDelete${user.id}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                    <h4 class="modal-title">Confirma��o</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Tem certeza que deseja remover o usu�rio ${user.name}?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteModalAction(${user.id});">Confirmar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                </td>
                <td>${user.name}</td>
                <td>${user.login}</td>
                <td>
                    <c:choose>
                        <c:when test="${user.type == 1}">Usu�rio (b�sico)</c:when>
                        <c:when test="${user.type == 2}">Coordenador</c:when>
                        <c:when test="${user.type == 3}">Administrador</c:when>
                        <c:otherwise>Inv�lido</c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>    
    </tbody>
</table>

<c:choose>
    <c:when test="${not empty users}">
        <span class="label label-warning">Total: ${fn:length(users)}</span>
    </c:when>
    <c:otherwise>
        <div class="alert alert-warning" role="alert">N�o h� usu�rios</div>
    </c:otherwise>
</c:choose>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteModalAction(id) {
        $.post("<%=request.getContextPath()%>/user/delete", {id: id, ajax: true})
                .done(function(data) {
                    $("#row" + id).hide();
                })
                .fail(function() {
                    $("#row" + id).addClass("danger");
                });
    }
</script>