<%-- 
    Document   : edit-user
    Created on : 10/07/2013
    Author     : Michelle, Thiago Vieira
    Controller : br.project.controller.UsersInsertController
                 br.project.controller.UsersUpdateController
                 br.project.controller.UsersDeleteController
    Variables  : user (User)
                 page_menu_tools_sub (String)
                 corpora (List<Corpus>)
                 permission (List<Permission>)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form id="form" method="post" action="<%=request.getContextPath()%>/user/${page_menu_tools_sub}">
    <c:if test="${not empty user.id}">
        <div class="form-group">
            <label for="id">Id</label> 
            <input type="text" name="id" value="${user.id}" readonly class="form-control"/>
        </div>
    </c:if>
    <div class="form-group">
        <label for="name">Nome</label> 
        <input type="text" name="name" id="name" value="${user.name}" class="form-control" maxlength="400" />
    </div>
    <div class="form-group has-feedback">
        <label for="login">Login</label> 
        <input type="text" name="login" value="${user.login}" <c:if test="${not empty user}">readonly</c:if> class="form-control" maxlength="100" required />
            <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group has-feedback">
            <label for="password"><c:if test="${not empty user.id}">Nova senha</c:if><c:if test="${empty user.id}">Senha</c:if></label> 
            <input type="password" name="password" class="form-control" maxlength="50" required />
            <span class="glyphicon glyphicon-asterisk form-control-feedback" title="Requerido"></span>
    </div>
    <div class="form-group">
        <label for="email">Email</label> 
        <input type="email" name="email" id="email" value="${user.email}" class="form-control" maxlength="400" />
        <span class="help-block">Necess�rio para receber notifica��es geradas pelo sistema.</span>
    </div>
    <div class="form-group">
        <label class="control-label" for="type">Tipo</label> 
        <select class="form-control" name="type" id="type">
            <option <c:if test="${user.type == 1}"> selected </c:if> value="1">Usu�rio (b�sico)</option>
            <option <c:if test="${user.type == 2}"> selected </c:if> value="2">Coordenador</option>
            <option <c:if test="${user.type == 3}"> selected </c:if> value="3">Administrador</option>
        </select>
    </div>
    <div class="form-group">
        <label class="control-label" for="permission">Permiss�o de uso</label>
        <c:forEach var="corpus" items="${corpora}">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="permission[]" value="${corpus.id}" <c:forEach var="item" items="${permission}"><c:if test="${item.idCorpus eq corpus.id}"> checked </c:if></c:forEach> > ${corpus.name}
                </label>
            </div>
        </c:forEach>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <a href="<%=request.getContextPath()%>/user/select" class="btn btn-default">Voltar</a>    
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#name').focus();
    });
</script>