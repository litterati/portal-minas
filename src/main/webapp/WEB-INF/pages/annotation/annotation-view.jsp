<%-- 
    Document   : annotation-view
    Created on : 24/11/2014, 15:37:01
    Author     : regis
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    .annotation {
        text-decoration:none;
        padding: 3px 0px;
        line-height: 25px;
        //background-color: rgb(135, 230, 0);
    }
    .annotation:hover, .selectedSentence {
        background-color: rgb(135, 206, 235);
    }
    .annotationRight:hover, .annotationLeft {
        background-color: rgb(135, 230, 0);
    }
    .word {
        padding: 3px;
        margin: 3px;
        line-height: 300%;
    }
    .word:hover {
        background-color: rgb(135, 206, 235);
    }  
</style>

<td width="20">
    <a href="<%=request.getContextPath()%>/annotation/annotation-edit-element?id_corpus=${id_corpus}" title="Alterar">
        <span class="glyphicon glyphicon-pencil"></span>
    </a>
</td>
<td width="20">
    <a href="<%=request.getContextPath()%>/annotation/annotation-edit-connection?id_corpus=${id_corpus}" title="Alterar">
        <span class="glyphicon glyphicon-pencil"></span>
    </a>
</td>
<br><br>

<div class="row">
    <div id="text" idText="${text.id}" idCorpus="${id_corpus}" class="col-xs-9">
        
        <c:set var="end" value="0" scope="page" />
        
        <c:forEach var="w" items="${text_words}" varStatus="status">
            <c:choose>
                <c:when test="${end==0}">
                    <c:forEach var="a" items="${annotation}" varStatus="status">
                        <c:if test="${a.start == w.id}">
                            <div class="an" style="float:left;">
                                <div id="${a.id}" style="text-align:center;">
                                <span class="glyphicon glyphicon-exclamation-sign"></span>
                                </div>
                            <span class="annotation" idAnnotation="${a.id}" type="${a.type}" connection="${a.connection}">
                            <c:set var="end" value="${a.end}" scope="page" />
                        </c:if>
                    </c:forEach>
                    <c:if test="${end==0}">
                        <div class="wor" style="float:left; text-align:center;">
                            <br>
                            <span class="word" idWord="${w.id}">${w.word} </span>
                        </div>
                    </c:if>
                    <c:if test="${end!=0}">
                        <span class="word" idWord="${w.id}">${w.word} </span>
                    </c:if>
                    <c:if test="${end == w.id}">
                        </span>
                        </div>
                        <c:set var="end" value="0" scope="page" />
                    </c:if>
                </c:when>
                <c:when test="${w.id!=end}">
                    <span class="word" idWord="${w.id}">${w.word} </span> 
                </c:when>
                <c:otherwise>
                    <span class="word" idWord="${w.id}">${w.word} </span></span>
                        </div>
                    <c:set var="end" value="0" scope="page" />
                </c:otherwise>
            </c:choose>
        </c:forEach>        
    </div>
        
    <div id="sidebar" class="col-xs-3">
        
    </div>
</div>
        
<form id="form" method="post" action="<%=request.getContextPath()%>/annotation/annotation-view?id_corpus=${id_corpus}" enctype="multipart/form-data">
    <input type="hidden" name="id_corpus" value="${id_corpus}" />
    <input type="hidden" name="edit_type" value="editAnnotation">
    
    <input type="hidden" name="start" id="start" value="">
    <input type="hidden" name="end" id="end" value="">
    <input type="hidden" name="type" id="type" value="">
    <input type="hidden" name="id" id="id" value="">
</form>
    
<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    //To view
    var listClicked = {};
    var editMode = false;
    $(".annotation").on({ mouseenter: function(){
        if (!editMode) {
            var content = "";
            var connection = "";
            var connectionUp = "";
            var connectionDown = "";
            
            var divId = "";
            var imageHtml = "";
            
            if (listClicked[$(this).attr("idAnnotation")] !== $(this).attr("idAnnotation")) {

                var crr = ${annotation};
        
                for (i=0; i<crr.length; i++) {
                    $("span[idAnnotation='" + crr[i].id + "']").removeClass("selectedSentence");
                    
                    divId = "#"+crr[i].id;
                    var imageHtml = "<span class='glyphicon glyphicon-exclamation-sign'></span>";
            
                    $(divId).html(imageHtml);
                }
            }
            
            $("span[idAnnotation='" + $(this).attr("idAnnotation") + "']").addClass("selectedSentence");

            var annotation = "<h4>Anota��o <small>" + $(this).attr("type") + "</small></h4>";
            annotation +="\"<x idAnnotation=\"" + $(this).attr("idAnnotation") + "\">" + $(this).text() + "</x>\"";
            annotation += "<br>Id: " + $(this).attr("idAnnotation");
            content+=annotation;
            
            divId = "#"+$(this).attr("idAnnotation");
            imageHtml = "<span class='glyphicon glyphicon-ok-circle'></span>";
            
            $(divId).html(imageHtml);

            var crr = ${connection};
            var connectionFound = false;

            for (i=0; i<crr.length; i++) {
                // start
                if (crr[i].start == $(this).attr("idAnnotation")) {
                    connectionFound = true;

                    $("span[idAnnotation='" + crr[i].start + "']").addClass("selectedSentence");
                    $("span[idAnnotation='" + crr[i].end + "']").addClass("selectedSentence");

                    connectionUp = "<br><br><h4>Conex�o <small>" + crr[i].type + "</small></h4>";
                    connectionUp+= $("span[idAnnotation='" + crr[i].start + "']").attr("type");
                    connectionUp+= " -> ";
                    connectionUp+= $("span[idAnnotation='" + crr[i].end + "']").attr("type");

                    connectionDown= "<br>";
                    connectionDown+= "\"<span class='annotationRight' idAnnotationRight='" + crr[i].start + "'>" + $("span[idAnnotation='" + crr[i].start + "']").text() + "</span>\"";
                    connectionDown+= " -> ";
                    connectionDown+= "\"<span class='annotationRight' idAnnotationRight='" + crr[i].end + "'>" + $("span[idAnnotation='" + crr[i].end + "']").text() + "</span>\"";

                    connection+= connectionUp;
                    connection+= connectionDown;
                    connection+= "<br>Id: " + crr[i].id;
                    connection+= "<br><div class='button' id='editButton' onclick='editButton(" + crr[i].start + "," + crr[i].end + ")'><span class='glyphicon glyphicon-pencil'></span></div>";

                    content+= connection;

                    connection = "";
                    
                    divId = "#"+crr[i].end;
                    imageHtml = "<span class='glyphicon glyphicon-arrow-down'></span>";
            
                    $(divId).html(imageHtml);
                }

                // end
                if (crr[i].end == $(this).attr("idAnnotation")) {
                    connectionFound = true;

                    $("span[idAnnotation='" + crr[i].start + "']").addClass("selectedSentence");
                    $("span[idAnnotation='" + crr[i].end + "']").addClass("selectedSentence");

                    connectionUp = "<br><br><h4>Conex�o <small>" + crr[i].type + "</small></h4>";
                    connectionUp+= $("span[idAnnotation='" + crr[i].start + "']").attr("type");
                    connectionUp+= " -> ";
                    connectionUp+= $("span[idAnnotation='" + crr[i].end + "']").attr("type");

                    connectionDown= "<br>";
                    connectionDown+= "\"<span class='annotationRight' idAnnotationRight='" + crr[i].start + "'>" + $("span[idAnnotation='" + crr[i].start + "']").text() + "</span>\"";
                    connectionDown+= " -> ";
                    connectionDown+= "\"<span class='annotationRight' idAnnotationRight='" + crr[i].end + "'>" + $("span[idAnnotation='" + crr[i].end + "']").text() + "</span>\"";

                    connection+= connectionUp;
                    connection+= connectionDown;
                    connection+= "<br>Id: " + crr[i].id;
                    connection+= "<br><div class='button' id='editButton' onclick='editButton(" + crr[i].start + "," + crr[i].end + ")'><span class='glyphicon glyphicon-pencil'></span></div>";

                    content+= connection;

                    connection = "";
                    
                    divId = "#"+crr[i].start;
                    imageHtml = "<span class='glyphicon glyphicon-arrow-up'></span>";
            
                    $(divId).html(imageHtml);
                }
            }

            //if (!connectionFound) {
            //    content=annotation;
            //}

            $("#sidebar").html(content);
        }
    }
    });
    
    $(".annotation").on({ mouseleave: function(){
        if (!editMode) {
            var divId = "";
            var imageHtml = "";
        }
    }
    });
    
    $(".annotation").click(function(){
        if (listClicked[$(this).attr("idAnnotation")] === $(this).attr("idAnnotation")) {
            
        } else {
            editMode = true;
            
            var id = document.getElementById("id");
            
            id.value = $(this).attr("idAnnotation");
            
            var crr = ${annotation};
        
            for (i=0; i<crr.length; i++) {
                $("span[idAnnotation='" + crr[i].id + "']").removeClass("selectedSentence");
            }
            
            $(this).addClass("selectedSentence");
            
            var content = "<h3>Elemento selecionado:</h3>";
            content+= "\"";
            
            content+= $(this).text();
            
            content+= " \"<br><br>";
            
            $("#sidebar").html(content);
            
            var category = ${typeAnnotation};
            var $newDiv = $("<div class='controls'>Categoria:<br></div>");
            var $dropDown = $("<select name='category' id='category' class='form-control'></select>");
            $dropDown.attr("name", "fieldName");

            for(i=0; i<category.length; i++) {
                var $newInput = $("<option></option>");
                $newInput
                 .attr("value", category[i])
                 .addClass("text")
                 .html(category[i]);
                $newInput.appendTo($dropDown);
                $dropDown.appendTo($newDiv);
                $newDiv.appendTo($("#sidebar"));
            }
            
            $submitContent = $("<br><div class='button' id='button' onclick='submitButton()'>Confirmar</div><div class='button' id='cancelButton' onclick='cancelButton()'>Cancelar</div><div class='button' id='undoButton' onclick='deleteButton()'>Excluir</div>");
            $submitContent.appendTo($("#sidebar"));
        }
    });
    
    $(document).on("mouseenter", ".annotationRight", function(){
        $("span[idAnnotation='" + $(this).attr("idAnnotationRight") + "']").removeClass("selectedSentence");
        $("span[idAnnotation='" + $(this).attr("idAnnotationRight") + "']").addClass("annotationLeft");
    }).on("mouseleave", ".annotationRight", function(){
        $("span[idAnnotation='" + $(this).attr("idAnnotationRight") + "']").removeClass("annotationLeft");
        $("span[idAnnotation='" + $(this).attr("idAnnotationRight") + "']").addClass("selectedSentence");
    });
    
    function cancelButton() {
        editMode = false;
        listClicked = {};
        
        var crr = ${annotation};
        
        for (i=0; i<crr.length; i++) {
            $("span[idAnnotation='" + crr[i].id + "']").removeClass("selectedSentence");
        }
        
        content= "";
            
        $("#sidebar").html(content);
    };
    
    function submitButton() {
        var type = document.getElementById("type");
        var category = document.getElementById("category");
        type.value = category.value;
        
        $("#form").submit();
    };
    
    function deleteButton() {
        var start = document.getElementById("start");
        var end = document.getElementById("end");
        
        start.value = "0";
        end.value = "0";
        
        $("#form").submit();
    };
    
    function editButton(start, end) {
        editMode = true;

        var id = document.getElementById("id");

        id.value = $(this).attr("idAnnotation");

        var crr = ${annotation};

        for (i=0; i<crr.length; i++) {
            $("span[idAnnotation='" + crr[i].id + "']").removeClass("selectedSentence");
        }

        $("span[idAnnotation='" + start + "']").addClass("selectedSentence");
        $("span[idAnnotation='" + end + "']").addClass("selectedSentence");

        var content = "<h3>Conex�o selecionada:</h3>";
        content+= "\"";

        content+= $("span[idAnnotation='" + start + "']").attr("type");
        content+= " -> ";
        content+= $("span[idAnnotation='" + end + "']").attr("type");
        
        content+= "<br>";
        content+= "\"<span class='annotationRight' idAnnotationRight='" + start + "'>" + $("span[idAnnotation='" + start + "']").text() + "</span>\"";
        content+= " -> ";
        content+= "\"<span class='annotationRight' idAnnotationRight='" + end + "'>" + $("span[idAnnotation='" + end + "']").text() + "</span>\"";

        content+= " \"<br><br>";

        $("#sidebar").html(content);

        var category = ${typeConnection};
        var $newDiv = $("<div class='controls'>Categoria:<br></div>");
        var $dropDown = $("<select name='category' id='category' class='form-control'></select>");
        $dropDown.attr("name", "fieldName");

        for(i=0; i<category.length; i++) {
            var $newInput = $("<option></option>");
            $newInput
             .attr("value", category[i])
             .addClass("text")
             .html(category[i]);
            $newInput.appendTo($dropDown);
            $dropDown.appendTo($newDiv);
            $newDiv.appendTo($("#sidebar"));
        }

        $submitContent = $("<br><div class='button' id='button' onclick='submitButton()'>Confirmar</div><div class='button' id='cancelButton' onclick='cancelButton()'>Cancelar</div><div class='button' id='undoButton' onclick='deleteButton()'>Excluir</div>");
        $submitContent.appendTo($("#sidebar"));
    }
</script>