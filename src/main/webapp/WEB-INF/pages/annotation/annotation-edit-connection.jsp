<%-- 
    Document   : annotation-edit-conection
    Created on : 17/12/2014, 17:10:09
    Author     : regis
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    .annotation {
        text-decoration:none;
        padding: 3px 0px;
        line-height: 25px;
        background-color: rgb(135, 230, 0);
    }
    .annotation:hover, .selectedSentence {
        background-color: rgb(135, 206, 235);
    }
    .word {
        padding: 3px;
        margin: 3px;
    }
</style>

<td width="20">
    <a href="<%=request.getContextPath()%>/annotation/annotation-edit-element?id_corpus=${id_corpus}" title="Alterar">
        <span class="glyphicon glyphicon-pencil"></span>
    </a>
</td>
<br><br>

<div class="row">
    <div id="text" idText="${text.id}" idCorpus="${id_corpus}" class="col-xs-9">
        
        <c:set var="end" value="0" scope="page" />
        
        <c:forEach var="w" items="${text_words}" varStatus="status">
            <c:choose>
                <c:when test="${end==0}">
                    <c:forEach var="a" items="${annotation}" varStatus="status">
                        <c:if test="${a.start == w.id}">
                            <span class="annotation" idAnnotation="${a.id}" type="${a.type}" connection="${a.connection}">
                            <c:set var="end" value="${a.end}" scope="page" />
                        </c:if>
                    </c:forEach>
                    <span class="word" idWord="${w.id}">${w.word} </span>
                    <c:if test="${end == w.id}">
                        </span>
                        <c:set var="end" value="0" scope="page" />
                    </c:if>
                </c:when>
                <c:when test="${w.id!=end}">
                    <span class="word" idWord="${w.id}">${w.word} </span> 
                </c:when>
                <c:otherwise>
                    <span class="word" idWord="${w.id}">${w.word} </span></span>
                    <c:set var="end" value="0" scope="page" />
                </c:otherwise>
            </c:choose>
        </c:forEach>        
    </div>
        
    <div id="sidebar" class="col-xs-3">
        <h3>Selecione os elementos de cada rela��o.</h3>
    </div>
</div>
        
<form id="form" method="post" action="<%=request.getContextPath()%>/annotation/annotation-view?id_corpus=${id_corpus}" enctype="multipart/form-data">
    <input type="hidden" name="id_corpus" value="${id_corpus}" />
    <input type="hidden" name="edit_type" value="connection">
    
    <input type="hidden" name="start" id="start" value="">
    <input type="hidden" name="end" id="end" value="">
    <input type="hidden" name="type" id="type" value="">
</form>
    
<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    //To view
    var listClicked = {};
    $(".annotation").click(function(){
        
        var content = "";
        
        listClickedKeys = Object.keys(listClicked);
        
        if (listClickedKeys.length == 0) {
            listClicked[$(this).attr("idAnnotation")] = $(this).attr("idAnnotation");
            $(this).addClass("selectedSentence");
        }
        if (listClickedKeys.length == 1) {
            listClicked[$(this).attr("idAnnotation")] = $(this).attr("idWord");
            
            var min = listClickedKeys.pop();
            var max = $(this).attr("idAnnotation");
            
            var start = document.getElementById("start");
            start.value = min.toString();
            var end = document.getElementById("end");
            end.value = max.toString();
            
            content = "<h3>Rela��o criada:</h3>";
            content+= "\"";
            
            $("span[idAnnotation='" + max + "']").addClass("selectedSentence");
            content+= $("span[idAnnotation='" + min + "']").text() + "\" -> \"" + $("span[idAnnotation='" + max + "']").text();
            
            content+= " \"<br><br>";
            
            $("#sidebar").html(content);
            
            var category = ${category};
            var $newDiv = $("<div class='controls'>Categoria:<br></div>");
            var $dropDown = $("<select name='category' id='category' class='form-control'></select>");
            $dropDown.attr("name", "fieldName");

            for(i=0; i<category.length; i++) {
                var $newInput = $("<option></option>");
                $newInput
                 .attr("value", category[i])
                 .addClass("text")
                 .html(category[i]);
                $newInput.appendTo($dropDown);
                $dropDown.appendTo($newDiv);
                $newDiv.appendTo($("#sidebar"));
            }
            
            $submitContent = $("<br><div class='button' id='button' onclick='submitButton()'>Confirmar</div><div class='button' id='undoButton' onclick='undoButton()'>Desfazer</div>");
            $submitContent.appendTo($("#sidebar"));
        }
    });
        
    function submitButton() {
        var type = document.getElementById("type");
        var category = document.getElementById("category");
        type.value = category.value;
        
        $("#form").submit();
    };
    
    function undoButton() {
        var start = document.getElementById("start");
        va = document.getElementById("end");
        
        $("span[idAnnotation='" + start.value + "']").removeClass("selectedSentence");
        $("span[idAnnotation='" + end.value + "']").removeClass("selectedSentence");
        
        listClicked = {};
        
        start.value = "";
        end.value = "";
        
        content= "<h3>Selecione os elementos de cada rela��o.</h3>";
            
        $("#sidebar").html(content);
    };
    
</script>