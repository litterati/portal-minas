<%-- 
    Document   : ngramas
    Created on : 24/09/2014
    Author     : Marcel
    Controller : br.project.controller.NgramaController
    Variables  : page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form action="<%=request.getContextPath()%>/ngrama?id_corpus=${page_menu_id_corpus}" method="post">    
    <input value="${page_menu_id_corpus}" name="idCorpus" hidden="true">
    <div class="form-group">
        <label for="gramSize">Tamanho dos n-gramas</label>
        <select id="gramSizeSelect" name="gramSize" class="form-control">
            <option value="2" selected>bigrama</option>
            <option value="3">trigrama</option>
        </select>
    </div>
    <div class="form-group" id="bigramSelect">
        <label for="statistic">Estatítica</label>
        <select size="1" name="statistic" class="form-control">
            <option value="dice">Dice Coefficient </option>
            <option value="left">Fishers exact test - left sided</option>
            <option value="right">Fishers exact test - right sided</option>
            <option value="twotailed">Fishers twotailed test - right sided</option>
            <option value="jaccard">Jaccard Coefficient</option>
            <option value="ll">Log-likelihood ratio</option>
            <option value="tmi">Mutual Information</option>
            <option value="odds">Odds Ratio</option>
            <option value="pmi">Pointwise Mutual Information</option>
            <option value="phi">Phi Coefficient</option>
            <option value="x2">Pearson's Chi Squared test</option>
            <option value="ps">Poisson Stirling Measure</option>
            <option value="tscore">T-score</option>
        </select>
        <p class="help-block">Selecionar a estatística a ser utilizada.</p>
    </div>
    <div class="form-group hidden" id="trigramSelect">
        <label for="statistic">Estatítica</label>
        <select size="1" name="statistic" class="form-control">
            <option value="ll">Log-likelihood ratio</option>
            <option value="tmi">Mutual Information</option>
            <option value="pmi">Pointwise Mutual Information</option>
            <option value="ps">Poisson Stirling Measure</option>
        </select>
        <p class="help-block">Selecionar a estatística a ser utilizada.</p>
    </div>
    <div class="form-group">
        <label for="frequency">Filtro de frequências</label>
        <input type="text" value="5" name="frequency" class="form-control">
        <p class="help-block">Ignorar os itens com frequência menor que o número informado.</p>
    </div>
    <div class="form-group">
        <label for="score">Filtro estatístico</label>
        <input type="text" value="0" name="score" class="form-control">
        <p class="help-block">Filtrar resultados por valor de estatística (ex. <%=0.5%>).</p>
    </div>
    <div class="form-group">
        <label for="window">Resultados por página</label>
        <input type="text" value="50" name="window" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Gerar</button>
</form>

<script src="<%=request.getContextPath()%>/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript">
    $("#gramSizeSelect").change(function() {
        var value = $(this).val();
        if (value === "2"){
            $("#bigramSelect").removeClass("hidden");
            $("#trigramSelect").addClass("hidden");
        } else {
            $("#bigramSelect").addClass("hidden");
            $("#trigramSelect").removeClass("hidden");
        }
    });
</script>