<%-- 
    Document   : result
    Created on : 24/09/2014
    Author     : Marcel
    Controller : br.project.controller.NgramaController
    Variables  : allResults (List<NgramResults>)
                 page_menu_id_corpus (int)
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            
<div class="row">
    <form action="<%=request.getContextPath()%>/ngramfilter?id_corpus=${page_menu_id_corpus}" method="post">    
        <div class="form-group">
            <label for="score">Filtro estat�stico</label>
            <input type="text" value="0" name="score" class="form-control">
            <p class="help-block">Filtrar resultados por valor de estat�stica (ex. <%=0.5%>).</p>
            <button type="submit" class="btn btn-primary">Aplicar Filtro</button>
            <a href="<%=request.getContextPath()%>/ngrama?id_corpus=${page_menu_id_corpus}&ngrama=true" class="btn btn-default">Gerar novo Ngram</a>
        </div>
    </form>    
</div>

<div class="row">
    <form action="<%=request.getContextPath()%>/ngramsearch?id_corpus=${page_menu_id_corpus}" method="post">    
        <div class="form-group">
            <label for="score">Buscar por:</label>
            <input type="text" name="searchterm" class="form-control"/>
            <input type="submit" name="submit" value="Buscar"/>
        </div>
    </form>    
</div>
        
<div class="row">
    <c:forEach var="oneResult" items="${allResults}">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                        N-gramas em ${oneResult.language}
                        <small><a href="<%=request.getContextPath()%>/ngramdownload?language=${oneResult.language}" title="Download">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                        </a></small>
                    </h3>
                </div>
                <table class="table table-hover table-striped table-responsive">
                    <tr>
                        <th>Estat�stica</th>
                        <th>N-grama</th>
                    </tr>
                    <c:forEach var="ngram" items="${oneResult.results}">
                        <tr>
                            <td>${ngram.stat}</td>
                            <td>${ngram.ngram}</td>
                        </tr>
                    </c:forEach>
                </table>
                <div class="panel-footer">P�g. ${oneResult.page}, total ${oneResult.total}</div>
                <div>
                    <ul class="pagination">
                    <c:forEach var="page_i" begin="1" end="${oneResult.total}">
                        <li <c:if test="${page_i == oneResult.page}"> class="active" </c:if> > <a href="<%=request.getContextPath()%>/ngramview?page=${page_i}">${page_i}</a></li>
                    </c:forEach>
                    </ul>
                </div>    
            </div>
        </div>
    </c:forEach>
</div>
<a href="<%=request.getContextPath()%>/ngrama?id_corpus=${page_menu_id_corpus}" class="btn btn-default">Voltar</a>