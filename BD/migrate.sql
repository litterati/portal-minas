-- DATA: 17 de setembro 2014
-- DESCRIÇÃO: Removendo a tabela de comentários pois eles serão adicionados a uma coluna na tabela text_images

DROP TABLE image_comments;

ALTER TABLE text_images
   ADD COLUMN comment character varying;

-- DATA: 20 de agosto 2014
-- DESCRIÇÃO: index na tabela words para a coluna pos
CREATE INDEX idx_words_pos
  ON words
  USING btree
  (pos);


-- DATA: 18 de agosto 2014
-- DESCRIÇÃO: Adicionado tabela relacionada ao lematizador, contendo o dicionário/lemma

CREATE TABLE dictionaries
(
  id serial NOT NULL,
  word character varying,
  lemma character varying,
  pos character varying,
  flex character varying,
  lang int,
  CONSTRAINT pk_dictionaries PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dictionaries
  OWNER TO portalminas;
COMMENT ON TABLE dictionaries
  IS 'Tabela referente ao lemmatizador';

-- DATA: 28 de setembro de 2014
-- DESCRIÇÃO: Adicionado tabela de tarefas

CREATE SEQUENCE task_id_seq;

CREATE TABLE task (
                id INTEGER NOT NULL DEFAULT nextval('task_id_seq'),
                id_user INTEGER DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
                type SMALLINT,
                progress SMALLINT,
                status SMALLINT,
                response VARCHAR,
                date_inserted timestamp without time zone,
                date_started timestamp without time zone,
                date_finished timestamp without time zone,
                CONSTRAINT task_id PRIMARY KEY (id)
);


ALTER SEQUENCE task_id_seq OWNED BY task.id;

ALTER TABLE task ADD CONSTRAINT users_task_fk
FOREIGN KEY (id_user)
REFERENCES users (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

-- DATA: 05 de novembro de 2014
-- DESCRIÇÃO: Adicionado tabela de tags de alinhamentos

CREATE SEQUENCE public.align_tag_id_seq;

CREATE TABLE public.align_tag (
                id INTEGER NOT NULL DEFAULT nextval('public.align_tag_id_seq'),
                value VARCHAR NOT NULL,
                id_corpus INTEGER NOT NULL,
                CONSTRAINT align_tag_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.align_tag_id_seq OWNED BY public.align_tag.id;

ALTER TABLE public.sentence_align ADD COLUMN id_align_tag INTEGER;

ALTER TABLE public.words_align ADD COLUMN id_align_tag INTEGER;

ALTER TABLE public.words_align ADD CONSTRAINT align_tag_words_align_fk1
FOREIGN KEY (id_align_tag)
REFERENCES public.align_tag (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sentence_align ADD CONSTRAINT align_tag_sentence_align_fk
FOREIGN KEY (id_align_tag)
REFERENCES public.align_tag (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

-- DATA: 19 de dezembro 2014
-- DESCRIÇÃO: criação da coluna para armazenamento do objeto da tarefa
ALTER TABLE task
   ADD COLUMN java_object BYTEA;

-- DATA: 13 de janeiro 2015
-- DESCRIÇÃO: tabela que registra o número de acessos aos corpora e ferramentas
CREATE TABLE public.access (
                id_user INTEGER NOT NULL,
                id_corpus INTEGER NOT NULL,
                date_access TIMESTAMP with time zone NOT NULL,
                tool VARCHAR NOT NULL,
                CONSTRAINT access_pk PRIMARY KEY (id_user, id_corpus, date_access)
);

ALTER TABLE public.access ADD CONSTRAINT users_access_fk
FOREIGN KEY (id_user)
REFERENCES public.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.access ADD CONSTRAINT corpus_access_fk
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

-- DATA: 16 de janeiro 2015
-- DESCRIÇÃO: subcorpus
CREATE SEQUENCE public.subcorpus_id_seq;

CREATE TABLE public.subcorpus (
                id INTEGER NOT NULL DEFAULT nextval('public.subcorpus_id_seq'),
                id_corpus INTEGER NOT NULL,
                name VARCHAR,
                description VARCHAR,
                CONSTRAINT subcorpus_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.subcorpus_id_seq OWNED BY public.subcorpus.id;

CREATE TABLE public.subcorpus_texts (
                id_subcorpus INTEGER NOT NULL,
                id_text INTEGER NOT NULL,
                CONSTRAINT subcorpus_texts_pk PRIMARY KEY (id_subcorpus, id_text)
);

ALTER TABLE public.subcorpus ADD CONSTRAINT corpus_subcorpus_fk1
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.subcorpus_texts ADD CONSTRAINT subcorpus_subcorpus_texts_fk
FOREIGN KEY (id_subcorpus)
REFERENCES public.subcorpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.subcorpus_texts ADD CONSTRAINT texts_subcorpus_fk
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

-- DATA: 28 de janeiro 2015
-- DESCRIÇÃO: adicionando tabela user_history; adicionando coluna na tabela corpus para o texto de termos de uso user_terms
--TABELA SEARCH_HISTORY
CREATE TABLE search_history
(
  id_user integer,
  term character varying,
  id_history serial NOT NULL,
  CONSTRAINT id_history_pk PRIMARY KEY (id_history),
  CONSTRAINT id_user_fk FOREIGN KEY (id_user)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE search_history
  OWNER TO portalminas;
--ALTER TABLE DA TABELA DE CORPUS
ALTER TABLE corpus
   ADD COLUMN user_terms character varying;

