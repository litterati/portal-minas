
CREATE SEQUENCE public.support_id_seq;

CREATE TABLE public.support (
                id INTEGER NOT NULL DEFAULT nextval('public.support_id_seq'),
                name VARCHAR,
                email VARCHAR,
                subject VARCHAR,
                message VARCHAR,
                status INTEGER,
                CONSTRAINT support_id PRIMARY KEY (id)
);


ALTER SEQUENCE public.support_id_seq OWNED BY public.support.id;

CREATE SEQUENCE public.users_id_seq;

CREATE TABLE public.users (
                id INTEGER NOT NULL DEFAULT nextval('public.users_id_seq'),
                name VARCHAR(500) NOT NULL,
                email VARCHAR,
                login VARCHAR(250) NOT NULL,
                password VARCHAR(256) NOT NULL,
                type SMALLINT DEFAULT 1 NOT NULL,
                CONSTRAINT pk_users PRIMARY KEY (id)
);


ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;

CREATE UNIQUE INDEX uq_users_login
 ON public.users USING BTREE
 ( login );

CREATE SEQUENCE public.task_id_seq;

CREATE TABLE public.task (
                id INTEGER NOT NULL DEFAULT nextval('public.task_id_seq'),
                task_id INTEGER NOT NULL,
                type INTEGER NOT NULL,
                progress INTEGER DEFAULT 0,
                status INTEGER DEFAULT 0,
                response VARCHAR,
                date_inserted TIMESTAMP,
                date_started TIMESTAMP,
                date_finished TIMESTAMP,
                java_object BYTEA,
                CONSTRAINT task_id PRIMARY KEY (id)
);


ALTER SEQUENCE public.task_id_seq OWNED BY public.task.id;

CREATE SEQUENCE public.fields_type_id_seq;

CREATE TABLE public.fields_type (
                id INTEGER NOT NULL DEFAULT nextval('public.fields_type_id_seq'),
                value VARCHAR(255),
                CONSTRAINT pk_fields_type PRIMARY KEY (id)
);


ALTER SEQUENCE public.fields_type_id_seq OWNED BY public.fields_type.id;

CREATE SEQUENCE public.fields_type_values_id_seq;

CREATE TABLE public.fields_type_values (
                id INTEGER NOT NULL DEFAULT nextval('public.fields_type_values_id_seq'),
                id_type INTEGER NOT NULL,
                value VARCHAR(255),
                CONSTRAINT pk_fields_type_values PRIMARY KEY (id)
);


ALTER SEQUENCE public.fields_type_values_id_seq OWNED BY public.fields_type_values.id;

CREATE INDEX fk_fields_type_values_id_type
 ON public.fields_type_values USING BTREE
 ( id_type );

CREATE SEQUENCE public.corpus_id_seq;

CREATE TABLE public.corpus (
                id INTEGER NOT NULL DEFAULT nextval('public.corpus_id_seq'),
                name VARCHAR(50) NOT NULL,
                description VARCHAR,
                type SMALLINT DEFAULT 1 NOT NULL,
                CONSTRAINT pk_corpus PRIMARY KEY (id)
);


ALTER SEQUENCE public.corpus_id_seq OWNED BY public.corpus.id;

CREATE UNIQUE INDEX uq_corpus_name
 ON public.corpus USING BTREE
 ( name );

CREATE SEQUENCE public.subcorpus_id_seq;

CREATE TABLE public.subcorpus (
                id INTEGER NOT NULL DEFAULT nextval('public.subcorpus_id_seq'),
                id_corpus INTEGER NOT NULL,
                name VARCHAR,
                description VARCHAR,
                CONSTRAINT subcorpus_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.subcorpus_id_seq OWNED BY public.subcorpus.id;

CREATE TABLE public.access (
                id_user INTEGER NOT NULL,
                id_corpus INTEGER NOT NULL,
                date_access TIMESTAMP NOT NULL,
                tool VARCHAR NOT NULL,
                CONSTRAINT access_pk PRIMARY KEY (id_user, id_corpus, date_access)
);


CREATE SEQUENCE public.align_tag_id_seq;

CREATE TABLE public.align_tag (
                id INTEGER NOT NULL DEFAULT nextval('public.align_tag_id_seq'),
                value VARCHAR NOT NULL,
                id_corpus INTEGER NOT NULL,
                CONSTRAINT align_tag_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.align_tag_id_seq OWNED BY public.align_tag.id;

CREATE SEQUENCE public.corpus_tools_id_seq;

CREATE TABLE public.corpus_tools (
                id INTEGER NOT NULL DEFAULT nextval('public.corpus_tools_id_seq'),
                id_corpus INTEGER NOT NULL,
                tool VARCHAR NOT NULL,
                CONSTRAINT corpus_tools_id PRIMARY KEY (id)
);


ALTER SEQUENCE public.corpus_tools_id_seq OWNED BY public.corpus_tools.id;

CREATE TABLE public.permission (
                id_user INTEGER NOT NULL,
                id_corpus INTEGER NOT NULL,
                level SMALLINT DEFAULT 0 NOT NULL,
                CONSTRAINT permission_id PRIMARY KEY (id_user, id_corpus)
);


CREATE SEQUENCE public.texts_id_seq;

CREATE TABLE public.texts (
                id INTEGER NOT NULL DEFAULT nextval('public.texts_id_seq'),
                id_corpus INTEGER NOT NULL,
                language VARCHAR(255),
                title VARCHAR,
                CONSTRAINT pk_texts PRIMARY KEY (id)
);


ALTER SEQUENCE public.texts_id_seq OWNED BY public.texts.id;

CREATE INDEX idx_texts_id_corpus
 ON public.texts USING BTREE
 ( id_corpus );

CREATE TABLE public.subcorpus_texts (
                id_subcorpus INTEGER NOT NULL,
                id_text INTEGER NOT NULL,
                CONSTRAINT subcorpus_texts_pk PRIMARY KEY (id_subcorpus, id_text)
);


CREATE SEQUENCE public.text_images_id_seq;

CREATE TABLE public.text_images (
                id INTEGER NOT NULL DEFAULT nextval('public.text_images_id_seq'),
                id_text INTEGER NOT NULL,
                file_name VARCHAR NOT NULL,
                CONSTRAINT id PRIMARY KEY (id)
);


ALTER SEQUENCE public.text_images_id_seq OWNED BY public.text_images.id;

CREATE TABLE public.image_comments (
                id_image INTEGER NOT NULL,
                label VARCHAR,
                comment VARCHAR NOT NULL,
                CONSTRAINT image_comments_id PRIMARY KEY (id_image)
);


CREATE TABLE public.text_align (
                id_source INTEGER NOT NULL,
                id_target INTEGER NOT NULL,
                type VARCHAR(10) NOT NULL,
                CONSTRAINT texa_id PRIMARY KEY (id_source, id_target)
);


CREATE INDEX fki_text_align_id_text1
 ON public.text_align USING BTREE
 ( id_source );

CREATE INDEX fki_text_align_id_text2
 ON public.text_align USING BTREE
 ( id_target );

CREATE SEQUENCE public.sentences_id_seq;

CREATE TABLE public.sentences (
                id INTEGER NOT NULL DEFAULT nextval('public.sentences_id_seq'),
                id_text INTEGER NOT NULL,
                position INTEGER,
                type SMALLINT NOT NULL,
                CONSTRAINT pk_sentences PRIMARY KEY (id)
);
COMMENT ON COLUMN public.sentences.type IS 'words or meta_words';


ALTER SEQUENCE public.sentences_id_seq OWNED BY public.sentences.id;

CREATE INDEX fki_sentences_id_text
 ON public.sentences USING BTREE
 ( id_text );

CREATE TABLE public.sentence_align (
                id_source_sentence INTEGER NOT NULL,
                id_target_sentence INTEGER NOT NULL,
                id_source_text INTEGER NOT NULL,
                id_target_text INTEGER NOT NULL,
                type VARCHAR(10) NOT NULL,
                id_align_tag INTEGER,
                CONSTRAINT sena_id PRIMARY KEY (id_source_sentence, id_target_sentence)
);


CREATE SEQUENCE public.words_id_seq;

CREATE TABLE public.words (
                id INTEGER NOT NULL DEFAULT nextval('public.words_id_seq'),
                id_corpus INTEGER NOT NULL,
                id_text INTEGER NOT NULL,
                id_sentence INTEGER NOT NULL,
                position INTEGER,
                word VARCHAR(500) NOT NULL,
                lemma VARCHAR(255),
                pos VARCHAR(255),
                norm VARCHAR(255),
                transkription VARCHAR(255),
                dep_head VARCHAR(255),
                dep_function VARCHAR(255),
                contraction BOOLEAN,
                contraction_size INTEGER,
                CONSTRAINT pk_words PRIMARY KEY (id)
);


ALTER SEQUENCE public.words_id_seq OWNED BY public.words.id;

CREATE INDEX idx_words_id_corpus_word
 ON public.words USING BTREE
 ( word );

CREATE INDEX idx_words_id_text
 ON public.words USING BTREE
 ( id_text );

CREATE INDEX idx_words_id_text_word
 ON public.words USING BTREE
 ( id_text, word );

CREATE INDEX idx_words_word
 ON public.words USING BTREE
 ( lower((word)::text) );

CREATE TABLE public.words_align (
                id_source_word INTEGER NOT NULL,
                id_target_word INTEGER NOT NULL,
                id_source_sentence INTEGER NOT NULL,
                id_target_sentence INTEGER NOT NULL,
                type VARCHAR(10) NOT NULL,
                id_align_tag INTEGER,
                CONSTRAINT wora_id PRIMARY KEY (id_source_word, id_target_word)
);


CREATE SEQUENCE public.word_tags_id_seq;

CREATE TABLE public.word_tags (
                id INTEGER NOT NULL DEFAULT nextval('public.word_tags_id_seq'),
                id_word_open INTEGER NOT NULL,
                id_word_close INTEGER NOT NULL,
                value VARCHAR(1000),
                CONSTRAINT pk_word_tags PRIMARY KEY (id)
);


ALTER SEQUENCE public.word_tags_id_seq OWNED BY public.word_tags.id;

CREATE SEQUENCE public.word_tags_attributes_id_seq;

CREATE TABLE public.word_tags_attributes (
                id INTEGER NOT NULL DEFAULT nextval('public.word_tags_attributes_id_seq'),
                id_word_tags INTEGER NOT NULL,
                value VARCHAR NOT NULL,
                CONSTRAINT id_word_tags_attributes PRIMARY KEY (id)
);


ALTER SEQUENCE public.word_tags_attributes_id_seq OWNED BY public.word_tags_attributes.id;

CREATE SEQUENCE public.text_tags_id_seq;

CREATE TABLE public.text_tags (
                id INTEGER NOT NULL DEFAULT nextval('public.text_tags_id_seq'),
                id_corpus INTEGER NOT NULL,
                tag VARCHAR(1000),
                is_required BOOLEAN DEFAULT false,
                id_fields_type INTEGER NOT NULL,
                CONSTRAINT pk_text_tags PRIMARY KEY (id)
);


ALTER SEQUENCE public.text_tags_id_seq OWNED BY public.text_tags.id;

CREATE UNIQUE INDEX uk_text_tags_id_corpus_tag
 ON public.text_tags USING BTREE
 ( id_corpus, tag );

CREATE INDEX fki_text_tags_id_fields_type
 ON public.text_tags USING BTREE
 ( id_fields_type );

CREATE INDEX idx_text_tags_id_corpus
 ON public.text_tags USING BTREE
 ( id_corpus );

CREATE INDEX idx_text_tags_id_corpus_tag
 ON public.text_tags USING BTREE
 ( id_corpus, lower((tag)::text) );

CREATE SEQUENCE public.text_tag_values_id_seq;

CREATE TABLE public.text_tag_values (
                id INTEGER NOT NULL DEFAULT nextval('public.text_tag_values_id_seq'),
                id_text INTEGER NOT NULL,
                id_text_tag INTEGER NOT NULL,
                value VARCHAR(30000),
                CONSTRAINT pk_text_tag_values PRIMARY KEY (id)
);


ALTER SEQUENCE public.text_tag_values_id_seq OWNED BY public.text_tag_values.id;

CREATE INDEX idx_text_tags_values_id_text_id_text_tag
 ON public.text_tag_values USING BTREE
 ( id_text, id_text_tag );

CREATE INDEX idx_text_tags_values_id_text_tag
 ON public.text_tag_values USING BTREE
 ( id_text_tag );

CREATE SEQUENCE public.meta_words_id_seq;

CREATE TABLE public.meta_words (
                id INTEGER NOT NULL DEFAULT nextval('public.meta_words_id_seq'),
                id_corpus INTEGER NOT NULL,
                id_text INTEGER NOT NULL,
                id_sentence INTEGER NOT NULL,
                position INTEGER,
                word VARCHAR(500) NOT NULL,
                lemma VARCHAR(255),
                pos VARCHAR(255),
                contraction BOOLEAN,
                contraction_size INTEGER,
                CONSTRAINT pk_meta_words PRIMARY KEY (id)
);


ALTER SEQUENCE public.meta_words_id_seq OWNED BY public.meta_words.id;

CREATE SEQUENCE public.meta_word_tags_id_seq;

CREATE TABLE public.meta_word_tags (
                id INTEGER NOT NULL DEFAULT nextval('public.meta_word_tags_id_seq'),
                value CHAR(1000),
                id_meta_word_open INTEGER NOT NULL,
                id_meta_word_close INTEGER NOT NULL,
                CONSTRAINT pk_meta_word_tags PRIMARY KEY (id)
);


ALTER SEQUENCE public.meta_word_tags_id_seq OWNED BY public.meta_word_tags.id;

CREATE SEQUENCE public.meta_word_tags_attributes_id_seq;

CREATE TABLE public.meta_word_tags_attributes (
                id INTEGER NOT NULL DEFAULT nextval('public.meta_word_tags_attributes_id_seq'),
                id_meta_word_tags INTEGER NOT NULL,
                value VARCHAR NOT NULL,
                CONSTRAINT meta_word_tags_attributes_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.meta_word_tags_attributes_id_seq OWNED BY public.meta_word_tags_attributes.id;

CREATE SEQUENCE public.corpus_tags_id_seq;

CREATE TABLE public.corpus_tags (
                id INTEGER NOT NULL DEFAULT nextval('public.corpus_tags_id_seq'),
                id_corpus INTEGER NOT NULL,
                tag VARCHAR(1000),
                is_required BOOLEAN DEFAULT false,
                is_default BOOLEAN,
                id_fields_type INTEGER NOT NULL,
                CONSTRAINT pk_corpus_tags PRIMARY KEY (id)
);


ALTER SEQUENCE public.corpus_tags_id_seq OWNED BY public.corpus_tags.id;

CREATE UNIQUE INDEX uk_corpus_tags_id_corpus_tags
 ON public.corpus_tags USING BTREE
 ( id_corpus, tag );

CREATE INDEX fk_corpus_tags_id_corpus
 ON public.corpus_tags USING BTREE
 ( id_fields_type );

CREATE INDEX idx_corpus_tags_id_corpus_value
 ON public.corpus_tags USING BTREE
 ( id_corpus, lower((tag)::text) );

CREATE SEQUENCE public.corpus_tag_values_id_seq;

CREATE TABLE public.corpus_tag_values (
                id INTEGER NOT NULL DEFAULT nextval('public.corpus_tag_values_id_seq'),
                id_corpus INTEGER NOT NULL,
                id_corpus_tag INTEGER NOT NULL,
                value VARCHAR(100000),
                CONSTRAINT pk_corpus_tag_values PRIMARY KEY (id)
);


ALTER SEQUENCE public.corpus_tag_values_id_seq OWNED BY public.corpus_tag_values.id;

CREATE INDEX fk_corpus_tag_values_id_corpus_value
 ON public.corpus_tag_values USING BTREE
 ( id_corpus, value );

CREATE INDEX idx_corpus_tag_values_id_corpus_id_corpus_tag
 ON public.corpus_tag_values USING BTREE
 ( id_corpus, id_corpus_tag );

ALTER TABLE public.permission ADD CONSTRAINT users_permission_fk
FOREIGN KEY (id_user)
REFERENCES public.users (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.task ADD CONSTRAINT users_task_fk
FOREIGN KEY (task_id)
REFERENCES public.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.access ADD CONSTRAINT users_access_fk
FOREIGN KEY (id_user)
REFERENCES public.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.corpus_tags ADD CONSTRAINT fk_corpus_tags_id_fields_type
FOREIGN KEY (id_fields_type)
REFERENCES public.fields_type (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.fields_type_values ADD CONSTRAINT fk_fields_type_values_id_type
FOREIGN KEY (id_type)
REFERENCES public.fields_type (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_tags ADD CONSTRAINT fk_text_tags_id_fields_type
FOREIGN KEY (id_fields_type)
REFERENCES public.fields_type (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.corpus_tag_values ADD CONSTRAINT fk_corpus_tag_values_id_corpus
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.corpus_tags ADD CONSTRAINT fk_corpus_tags_id_corpus
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.meta_words ADD CONSTRAINT fk_corpus_id_corpus
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_tags ADD CONSTRAINT fk_text_tags_id_corpus
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.texts ADD CONSTRAINT fk_texts_id_corpus
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words ADD CONSTRAINT fk_words_id_corpus
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.permission ADD CONSTRAINT corpus_permission_fk
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.corpus_tools ADD CONSTRAINT corpus_corpus_tools_fk
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.align_tag ADD CONSTRAINT corpus_align_tag_fk
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.access ADD CONSTRAINT corpus_access_fk
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.subcorpus ADD CONSTRAINT corpus_subcorpus_fk1
FOREIGN KEY (id_corpus)
REFERENCES public.corpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.subcorpus_texts ADD CONSTRAINT subcorpus_subcorpus_texts_fk
FOREIGN KEY (id_subcorpus)
REFERENCES public.subcorpus (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words_align ADD CONSTRAINT align_tag_words_align_fk1
FOREIGN KEY (id_align_tag)
REFERENCES public.align_tag (id)
ON DELETE SET NULL
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.sentence_align ADD CONSTRAINT align_tag_sentence_align_fk
FOREIGN KEY (id_align_tag)
REFERENCES public.align_tag (id)
ON DELETE SET NULL
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.meta_words ADD CONSTRAINT fk_texts_id_text
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.sentences ADD CONSTRAINT fk_sentences_id_text
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_align ADD CONSTRAINT fk_text_align_id_text1
FOREIGN KEY (id_source)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_align ADD CONSTRAINT fk_text_align_id_text2
FOREIGN KEY (id_target)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_tag_values ADD CONSTRAINT fk_text_tag_values_id_text
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words ADD CONSTRAINT fk_words_id_text
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_images ADD CONSTRAINT texts_text_images_fk
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.sentence_align ADD CONSTRAINT texts_sentence_align_fk
FOREIGN KEY (id_source_text)
REFERENCES public.texts (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sentence_align ADD CONSTRAINT texts_sentence_align_fk1
FOREIGN KEY (id_target_text)
REFERENCES public.texts (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.subcorpus_texts ADD CONSTRAINT texts_subcorpus_fk
FOREIGN KEY (id_text)
REFERENCES public.texts (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.image_comments ADD CONSTRAINT text_images_image_comments_fk
FOREIGN KEY (id_image)
REFERENCES public.text_images (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words ADD CONSTRAINT fk_words_id_sentence
FOREIGN KEY (id_sentence)
REFERENCES public.sentences (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.sentence_align ADD CONSTRAINT sentences_sentence_align_fk
FOREIGN KEY (id_source_sentence)
REFERENCES public.sentences (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.sentence_align ADD CONSTRAINT sentences_sentence_align_fk1
FOREIGN KEY (id_target_sentence)
REFERENCES public.sentences (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.meta_words ADD CONSTRAINT sentences_meta_words_fk
FOREIGN KEY (id_sentence)
REFERENCES public.sentences (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words_align ADD CONSTRAINT sentence_align_words_align_fk
FOREIGN KEY (id_source_sentence, id_target_sentence)
REFERENCES public.sentence_align (id_source_sentence, id_target_sentence)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.word_tags ADD CONSTRAINT fk_word_tags_id_word_close
FOREIGN KEY (id_word_close)
REFERENCES public.words (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.word_tags ADD CONSTRAINT fk_word_tags_id_word_open
FOREIGN KEY (id_word_open)
REFERENCES public.words (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words_align ADD CONSTRAINT words_words_align_fk
FOREIGN KEY (id_source_word)
REFERENCES public.words (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.words_align ADD CONSTRAINT words_words_align_fk1
FOREIGN KEY (id_target_word)
REFERENCES public.words (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.word_tags_attributes ADD CONSTRAINT word_tags_word_tags_attributes_fk
FOREIGN KEY (id_word_tags)
REFERENCES public.word_tags (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.text_tag_values ADD CONSTRAINT fk_text_tag_values_id_text_tags
FOREIGN KEY (id_text_tag)
REFERENCES public.text_tags (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.meta_word_tags ADD CONSTRAINT meta_words_meta_word_tags_fk
FOREIGN KEY (id_meta_word_open)
REFERENCES public.meta_words (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.meta_word_tags ADD CONSTRAINT meta_words_meta_word_tags_fk1
FOREIGN KEY (id_meta_word_close)
REFERENCES public.meta_words (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.meta_word_tags_attributes ADD CONSTRAINT meta_word_tags_meta_word_tags_attributes_fk
FOREIGN KEY (id_meta_word_tags)
REFERENCES public.meta_word_tags (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.corpus_tag_values ADD CONSTRAINT fk_corpus_tag_values_id_corpus_tags
FOREIGN KEY (id_corpus_tag)
REFERENCES public.corpus_tags (id)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;
